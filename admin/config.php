<?php
// HTTP
define('HTTP_SERVER', 'http://tft.n2.iworklab.com/admin/');
define('HTTP_CATALOG', 'http://tft.n2.iworklab.com/');

// HTTPS
define('HTTPS_SERVER', 'http://tft.n2.iworklab.com/admin/');
define('HTTPS_CATALOG', 'http://tft.n2.iworklab.com/');

// DIR
define('DIR_APPLICATION', '/home/bitu/web/tft.n2.iworklab.com/public_html/admin/');
define('DIR_SYSTEM', '/home/bitu/web/tft.n2.iworklab.com/public_html/system/');
define('DIR_LANGUAGE', '/home/bitu/web/tft.n2.iworklab.com/public_html/admin/language/');
define('DIR_TEMPLATE', '/home/bitu/web/tft.n2.iworklab.com/public_html/admin/view/template/');
define('DIR_CONFIG', '/home/bitu/web/tft.n2.iworklab.com/public_html/system/config/');
define('DIR_IMAGE', '/home/bitu/web/tft.n2.iworklab.com/public_html/image/');
define('DIR_CACHE', '/home/bitu/web/tft.n2.iworklab.com/public_html/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/bitu/web/tft.n2.iworklab.com/public_html/system/storage/download/');
define('DIR_LOGS', '/home/bitu/web/tft.n2.iworklab.com/public_html/system/storage/logs/');
define('DIR_MODIFICATION', '/home/bitu/web/tft.n2.iworklab.com/public_html/system/storage/modification/');
define('DIR_UPLOAD', '/home/bitu/web/tft.n2.iworklab.com/public_html/system/storage/upload/');
define('DIR_CATALOG', '/home/bitu/web/tft.n2.iworklab.com/public_html/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'bitu_tft');
define('DB_PASSWORD', 'tft@123');
define('DB_DATABASE', 'bitu_tft');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
