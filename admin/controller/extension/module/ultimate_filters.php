<?php
//==============================================================================
// Ultimate Filters Module v303.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

class ControllerExtensionModuleUltimateFilters extends Controller {
	private $type = 'module';
	private $name = 'ultimate_filters';
	
	public function index() {
		$data = array(
			'type'			=> $this->type,
			'name'			=> $this->name,
			'autobackup'	=> false,
			'save_type'		=> 'keepediting',
			'permission'	=> $this->hasPermission('modify'),
		);
		
		$this->loadSettings($data);
		
		// extension-specific
		$this->cache->delete('filter');
		
		$module_table = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "module WHERE Field = 'setting'");
		if (strtoupper($module_table->row['Type']) == 'TEXT') {
			$this->db->query("ALTER TABLE " . DB_PREFIX . "module MODIFY `setting` MEDIUMTEXT NOT NULL");
		}
		
		//------------------------------------------------------------------------------
		// Modules
		//------------------------------------------------------------------------------
		$modules = array();
		$module_info = array();
		$module_id = 0;
		
		$this->load->model((version_compare(VERSION, '3.0', '<') ? 'extension' : 'setting') . '/module');
		
		if (isset($this->request->get['module_id'])) {
			$module_info = $this->{'model_' . (version_compare(VERSION, '3.0', '<') ? 'extension' : 'setting') . '_module'}->getModule($this->request->get['module_id']);
			$module_info['module_id'] = $this->request->get['module_id'];
			$module_layouts = array();
			
			if (!empty($module_info['module_id'])) {
				$layout_modules = $this->db->query("SELECT * FROM " . DB_PREFIX . "layout_module lm LEFT JOIN " . DB_PREFIX . "layout l ON (l.layout_id = lm.layout_id) WHERE lm.code = '" . $this->db->escape($this->name . "." . $module_info['module_id']) . "'")->rows;
				foreach ($layout_modules as $layout_module) {
					$module_layouts[] = '<a href="index.php?route=design/layout/edit&layout_id=' . $layout_module['layout_id'] . '&token=' . $data['token'] . '">' . $layout_module['name'] . '</a>';
				}
			}
		} else {
			foreach ($this->{'model_' . (version_compare(VERSION, '3.0', '<') ? 'extension' : 'setting') . '_module'}->getModulesByCode($this->name) as $module) {
				$modules[$module['module_id']] = $module['name'];
			}
		}
		
		//------------------------------------------------------------------------------
		// Data Arrays
		//------------------------------------------------------------------------------
		$data['store_array'] = array(0 => $this->config->get('config_name'));
		$store_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store ORDER BY name");
		foreach ($store_query->rows as $store) {
			$data['store_array'][$store['store_id']] = $store['name'];
		}
		
		$data['language_array'] = array($this->config->get('config_language') => '');
		$data['language_flags'] = array();
		$this->load->model('localisation/language');
		foreach ($this->model_localisation_language->getLanguages() as $language) {
			$data['language_array'][$language['code']] = $language['name'];
			$data['language_flags'][$language['code']] = (version_compare(VERSION, '2.2', '<')) ? 'view/image/flags/' . $language['image'] : 'language/' . $language['code'] . '/' . $language['code'] . '.png';
		}
		
		$data['customer_group_array'] = array(0 => $data['text_guests']);
		$this->load->model((version_compare(VERSION, '2.1', '<') ? 'sale' : 'customer') . '/customer_group');
		foreach ($this->{'model_' . (version_compare(VERSION, '2.1', '<') ? 'sale' : 'customer') . '_customer_group'}->getCustomerGroups() as $customer_group) {
			$data['customer_group_array'][$customer_group['customer_group_id']] = $customer_group['name'];
		}
		
		$data['currency_array'] = array($this->config->get('config_currency') => '');
		$this->load->model('localisation/currency');
		foreach ($this->model_localisation_currency->getCurrencies() as $currency) {
			$data['currency_array'][$currency['code']] = $currency['code'];
		}
		
		// Filters
		$this->load->model('catalog/attribute');
		$data['attributes'] = $this->model_catalog_attribute->getAttributes();
		
		$data['categories'] = array();
		$category_count = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category")->row['total'];
		if ($category_count < ((int)ini_get('max_input_vars') - 100)) {
			$this->load->model('catalog/category');
			foreach ($this->model_catalog_category->getCategories(0) as $category) {
				$data['categories'][$category['category_id']] = $category['name'];
			}
			natcasesort($data['categories']);
		}
		
		$this->load->model('catalog/filter');
		$data['filters'] = $this->model_catalog_filter->getFilterGroups();
		
		$this->load->model('catalog/option');
		$data['options'] = $this->model_catalog_option->getOptions();
		
		//------------------------------------------------------------------------------
		// Extension Settings
		//------------------------------------------------------------------------------
		$data['settings'] = array();
		
		$data['settings'][] = array(
			'key'		=> 'status',
			'type'		=> 'hidden',
			'default'	=> 1,
		);
		$data['settings'][] = array(
			'key'		=> 'tooltips',
			'type'		=> 'hidden',
			'default'	=> 0,
		);
		
		if (!isset($this->request->get['module_id'])) {
			
			$data['save_type'] = 'none';
			$data['warning'] = $data['help_caching'];
			
			$data['settings'][] = array(
				'type'		=> 'html',
				'content'	=> '<div class="text-info text-center pad-bottom">' . $data['help_module_locations'] . '  <a href="index.php?route=design/layout&token=' . $data['token'] . '">' . (version_compare(VERSION, '2.1', '<') ? ' System >' : '') . ' Design > Layouts</a></div>',
			);
			$data['settings'][] = array(
				'key'		=> 'module_list',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> 'module_list',
				'type'		=> 'table_start',
				'columns'	=> array('module_name', 'edit_module', 'copy_module', 'delete_module'),
			);
			foreach ($modules as $module_id => $module_name) {
				$data['settings'][] = array(
					'type'		=> 'row_start',
				);
				$data['settings'][] = array(
					'key'		=> 'module_link',
					'type'		=> 'button',
					'module_id'	=> $module_id,
					'text'		=> $module_name,
				);
				$data['settings'][] = array(
					'type'		=> 'column',
				);
				$data['settings'][] = array(
					'key'		=> 'edit_module',
					'type'		=> 'button',
					'module_id'	=> $module_id,
				);
				$data['settings'][] = array(
					'type'		=> 'column',
				);
				$data['settings'][] = array(
					'key'		=> 'copy_module',
					'type'		=> 'button',
					'module_id'	=> $module_id,
				);
				$data['settings'][] = array(
					'type'		=> 'column',
				);
				$data['settings'][] = array(
					'key'		=> 'delete_module',
					'type'		=> 'button',
					'module_id'	=> $module_id,
				);
				$data['settings'][] = array(
					'type'		=> 'row_end',
				);
			}
			$data['settings'][] = array(
				'type'		=> 'table_end',
			);
			$data['settings'][] = array(
				'type'		=> 'html',
				'content'	=> '<a class="btn btn-primary" href="index.php?route=extension/' . $this->type . '/' . $this->name . '&module_id=0&token=' . $data['token'] . '"><i class="fa fa-plus pad-right"></i> ' . $data['button_add_module'] . '</a>',
			);
			
		} else {
			
			//------------------------------------------------------------------------------
			// Module Editing Page
			//------------------------------------------------------------------------------
			$data['exit'] = $this->url->link('extension/' . $this->type . '/' . $this->name . '&token=' . $data['token'], '', 'SSL');
			$data['module_id'] = $this->request->get['module_id'];
			
			$module_prefix = 'module_' . $data['module_id'] . '_';
			
			$data['settings'][] = array(
				'type'		=> 'tabs',
				'tabs'		=> array('module_settings', 'attribute_filter', 'category_filter', 'manufacturer_filter', 'opencart_filters', 'option_filter', 'price_filter', 'width_filter', 'height_filter', 'depth_filter', 'rating_filter', 'search_filter', 'stock_filter'),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'module_id',
				'type'		=> 'hidden',
				'default'	=> $data['module_id'],
			);
			
			if ($data['module_id'] == 0) {
				$data['settings'][] = array(
					'key'		=> 'add_new_module',
					'type'		=> 'heading',
				);
			} else {
				$data['settings'][] = array(
					'key'		=> 'edit',
					'type'		=> 'heading',
					'text'		=> $data['heading_edit'] . ' "' . (!empty($module_info['name']) ? $module_info['name'] : '(no name)') . '"',
				);
				foreach ($module_info as $key => $value) {
					$data['saved'][$module_prefix . $key] = $value;
				}
			}
			
			//------------------------------------------------------------------------------
			// Module Settings
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'status',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_enabled'], 0 => $data['text_disabled']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'name',
				'type'		=> 'text',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'caching',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_enabled'], 0 => $data['text_disabled']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Filter Products',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_name',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'block_width',
				'type'		=> 'text',
				'attributes'=> array('style' => 'width: 50px !important'),
				'default'	=> '19%',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'block_height',
				'type'		=> 'text',
				'attributes'=> array('style' => 'width: 50px !important'),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'choice_display',
				'type'		=> 'text',
				'class'		=> 'short',
				'default'	=> 5,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'default_sorting',
				'type'		=> 'select',
				'options'	=> array(
					''				=> $data['text_default'],
					'name_asc'		=> $data['text_name_asc'],
					'name_desc'		=> $data['text_name_desc'],
					'price_asc'		=> $data['text_price_asc'],
					'price_desc'	=> $data['text_price_desc'],
                                        'width_asc'		=> $data['text_width_asc'],
					'width_desc'	=> $data['text_width_desc'],
                                        'height_asc'		=> $data['text_height_asc'],
					'height_desc'	=> $data['text_height_desc'],
                                        'depth_asc'		=> $data['text_depth_asc'],
					'depth_desc'	=> $data['text_depth_desc'],
					'rating_asc'	=> $data['text_rating_asc'],
					'rating_desc'	=> $data['text_rating_desc'],
					'model_asc'		=> $data['text_model_asc'],
					'model_desc'	=> $data['text_model_desc'],
					'bestseller'	=> $data['text_bestselling'],
					'latest'		=> $data['text_latest'],
					'popular'		=> $data['text_most_popular'],
				),
				'default'	=> '',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'automatic_filter',
				'type'		=> 'text',
				'class'		=> 'short',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'page_loading',
				'type'		=> 'select',
				'options'	=> array('normal' => $data['text_normally'], 'ajax_quick' => $data['text_via_ajax_quick_method'], 'ajax_slow' => $data['text_via_ajax_slower_method']),
				'default'	=> 'normal',
			);
			
			// Module Locations
			$data['settings'][] = array(
				'key'		=> 'module_locations',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'type'		=> 'html',
				'title'		=> $data['entry_module_locations'],
				'content'	=> '
					<div style="margin-top: -7px">' . $data['help_module_locations'] . ' <a href="index.php?route=design/layout&token=' . $data['token'] . '">' . (version_compare(VERSION, '2.1', '<') ? ' System >' : '') . ' Design > Layouts</a>
					<br /><br />
					' . $data['help_assigned_layouts'] . ' ' . implode(', ', $module_layouts) . '</div>
				',
			);
			
			// Module Text
			$data['settings'][] = array(
				'key'		=> 'module_text',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'show_more_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Show More',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'show_all_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Show All',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'all_text',
				'type'		=> 'multilingual_text',
				'default'	=> '--- All ---',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'clear_filter_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Clear',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'clear_all_filters_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Clear All Filters',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'filter_button',
				'type'		=> 'multilingual_text',
				'default'	=> 'Filter',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'mobile_button',
				'type'		=> 'multilingual_text',
				'default'	=> 'Filter',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'mobile_close',
				'type'		=> 'multilingual_text',
				'default'	=> '&amp;times;',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'additional_css',
				'type'		=> 'textarea',
			);
			
			// Restrictions
			$data['settings'][] = array(
				'key'		=> 'restrictions',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'stores',
				'type'		=> 'checkboxes',
				'options'	=> $data['store_array'],
				'default'	=> array_keys($data['store_array']),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'languages',
				'type'		=> 'checkboxes',
				'options'	=> $data['language_array'],
				'default'	=> array_keys($data['language_array']),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'customer_groups',
				'type'		=> 'checkboxes',
				'options'	=> $data['customer_group_array'],
				'default'	=> array_keys($data['customer_group_array']),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'currencies',
				'type'		=> 'checkboxes',
				'options'	=> $data['currency_array'],
				'default'	=> array_keys($data['currency_array']),
			);
			
			//------------------------------------------------------------------------------
			// Attribute Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'attribute_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'attribute_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'attribute_filter',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_enabled'], 0 => $data['text_disabled']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'attribute_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'attribute_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'attribute_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'attribute_choices',
				'type'		=> 'select',
				'options'	=> array('all' => $data['text_all_values'], 'relevant' => $data['text_only_relevant_values']),
				'default'	=> 'all',
			);
			
			// Attributes
			$data['settings'][] = array(
				'key'		=> 'attributes',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> 'set_all_dropdowns_to',
				'type'		=> 'html',
				'content'	=> '
					<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">' . $data['text_set_all_to'] . ' <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=hide]").attr("selected", "selected")\'>' . $data['text_hide'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=checkbox]").attr("selected", "selected")\'>' . $data['text_checkboxes'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=radio]").attr("selected", "selected")\'>' . $data['text_radio_buttons'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=select]").attr("selected", "selected")\'>' . $data['text_select_dropdown'] . '</a></li>
						</ul>
					</div>
				',
			);
			foreach ($data['attributes'] as $attribute) {
				$data['settings'][] = array(
					'key'		=> $module_prefix . 'attribute_filter_' . $attribute['attribute_id'],
					'type'		=> 'select',
					'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
					'default'	=> 'hide',
					'title'		=> $attribute['name'] . ':',
				);
			}
			
			//------------------------------------------------------------------------------
			// Category Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'category_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'category_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_filter',
				'type'		=> 'select',
				'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'links' => $data['text_links'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
				'default'	=> 'select',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_links',
				'type'		=> 'select',
				'options'	=> array('current' => $data['text_show_children_of_current'], 'top' => $data['text_show_children_of_top']),
				'default'	=> 'current',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Category:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_choices',
				'type'		=> 'select',
				'options'	=> array('all' => $data['text_all_values'], 'relevant' => $data['text_only_relevant_values']),
				'default'	=> 'all',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_images',
				'type'		=> 'text',
				'class'		=> 'short',
				'default'	=> 25,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'category_subcategories',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 1,
			);
			
			// Categories
			if ($data['categories']) {
				$data['settings'][] = array(
					'key'		=> 'categories',
					'type'		=> 'heading',
				);
				$data['settings'][] = array(
					'key'		=> $module_prefix . 'categories',
					'type'		=> 'checkboxes',
					'options'	=> $data['categories'],
					'default'	=> array_keys($data['categories']),
					'before'	=> '
						<a onclick="$(this).parent().find(\'input[type=checkbox]\').prop(\'checked\', \'checked\')">' . $data['text_select_all'] . '</a>
						/
						<a onclick="$(this).parent().find(\'input[type=checkbox]\').removeAttr(\'checked\')">' . $data['text_unselect_all'] . '</a>
						<br /><br />
					',
				);
			}
			
			//------------------------------------------------------------------------------
			// Manufacturer Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'manufacturer_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'manufacturer_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'manufacturer_filter',
				'type'		=> 'select',
				'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'links' => $data['text_links'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
				'default'	=> 'select',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'manufacturer_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'manufacturer_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Manufacturer:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'manufacturer_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'manufacturer_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'manufacturer_choices',
				'type'		=> 'select',
				'options'	=> array('all' => $data['text_all_values'], 'relevant' => $data['text_only_relevant_values']),
				'default'	=> 'all',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'manufacturer_images',
				'type'		=> 'text',
				'class'		=> 'short',
				'default'	=> 25,
			);
			
			//------------------------------------------------------------------------------
			// OpenCart Filters
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'opencart_filters',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'opencart_filters',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'filter_filter',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_enabled'], 0 => $data['text_disabled']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'filter_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'filter_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'filter_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'filter_choices',
				'type'		=> 'select',
				'options'	=> array('all' => $data['text_all_values'], 'relevant' => $data['text_only_relevant_values']),
				'default'	=> 'all',
			);
			
			// Filter Groups
			$data['settings'][] = array(
				'key'		=> 'filter_groups',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> 'set_all_dropdowns_to',
				'type'		=> 'html',
				'content'	=> '
					<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">' . $data['text_set_all_to'] . ' <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=hide]").attr("selected", "selected")\'>' . $data['text_hide'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=checkbox]").attr("selected", "selected")\'>' . $data['text_checkboxes'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=radio]").attr("selected", "selected")\'>' . $data['text_radio_buttons'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=select]").attr("selected", "selected")\'>' . $data['text_select_dropdown'] . '</a></li>
						</ul>
					</div>
				',
			);
			foreach ($data['filters'] as $filter) {
				$data['settings'][] = array(
					'key'		=> $module_prefix . 'filter_filter_' . $filter['filter_group_id'],
					'type'		=> 'select',
					'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
					'default'	=> 'hide',
					'title'		=> $filter['name'] . ':',
				);
			}
			
			//------------------------------------------------------------------------------
			// Option Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'option_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'option_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'option_filter',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_enabled'], 0 => $data['text_disabled']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'option_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'option_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'option_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'option_choices',
				'type'		=> 'select',
				'options'	=> array('all' => $data['text_all_values'], 'relevant' => $data['text_only_relevant_values']),
				'default'	=> 'all',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'option_images',
				'type'		=> 'text',
				'class'		=> 'short',
				'default'	=> 25,
			);
			
			// Options
			$data['settings'][] = array(
				'key'		=> 'options',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'type'		=> 'html',
				'content'	=> '<div class="text-info text-center pad-bottom">' . $data['help_options'] . '</div>',
			);
			$data['settings'][] = array(
				'key'		=> 'set_all_dropdowns_to',
				'type'		=> 'html',
				'content'	=> '
					<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">' . $data['text_set_all_to'] . ' <span class="caret"></span></button>
						<ul class="dropdown-menu">
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=hide]").attr("selected", "selected")\'>' . $data['text_hide'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=checkbox]").attr("selected", "selected")\'>' . $data['text_checkboxes'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=radio]").attr("selected", "selected")\'>' . $data['text_radio_buttons'] . '</a></li>
							<li><a onclick=\'$(this).parents(".form-group").nextAll().find("option[value=select]").attr("selected", "selected")\'>' . $data['text_select_dropdown'] . '</a></li>
						</ul>
					</div>
				',
			);
			foreach ($data['options'] as $option) {
				if ($option['type'] != 'checkbox' && $option['type'] != 'radio' && $option['type'] != 'select') {
					continue;
				}
				$data['settings'][] = array(
					'key'		=> $module_prefix . 'option_filter_' . $option['option_id'],
					'type'		=> 'select',
					'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
					'default'	=> 'hide',
					'title'		=> $option['name'] . ':',
				);
			}
			
			//------------------------------------------------------------------------------
			// Price Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'price_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'price_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_filter',
				'type'		=> 'select',
				'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'links' => $data['text_links'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
				'default'	=> 'checkbox',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Price Range:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_flexible',
				'type'		=> 'select',
				'options'	=> array('slider' => $data['text_yes_with_price_slider']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_bottom_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Under [price]',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_middle_text',
				'type'		=> 'multilingual_text',
				'default'	=> '[from] to [to]',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'price_top_text',
				'type'		=> 'multilingual_text',
				'default'	=> '[price] and up',
			);
			
			// Price Ranges
			$data['settings'][] = array(
				'key'		=> 'price_ranges',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'type'		=> 'html',
				'content'	=> '<div class="text-info text-center pad-bottom">' . $data['help_price_ranges'] . '</div>',
			);
			foreach ($data['currency_array'] as $code => $name) {
				$data['settings'][] = array(
					'key'		=> $module_prefix . 'price_range_' . $code,
					'type'		=> 'textarea',
					'title'		=> $code . ' ' . $data['entry_price_range'],
				);
			}
                        
                        
                        
                        
                        
                        
                        //------------------------------------------------------------------------------
			// Custom Code For Width Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'width_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'width_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_filter',
				'type'		=> 'select',
				'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'links' => $data['text_links'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
				'default'	=> 'checkbox',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Width Range:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_flexible',
				'type'		=> 'select',
				'options'	=> array('slider' => $data['text_yes_with_width_slider']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_bottom_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Under [width]',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_middle_text',
				'type'		=> 'multilingual_text',
				'default'	=> '[from] to [to]',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'width_top_text',
				'type'		=> 'multilingual_text',
				'default'	=> '[width] and up',
			);
			
			// Width Ranges
			$data['settings'][] = array(
				'key'		=> 'width_ranges',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'type'		=> 'html',
				'content'	=> '<div class="text-info text-center pad-bottom">' . $data['help_width_ranges'] . '</div>',
			);
			foreach ($data['currency_array'] as $code => $name) {
				$data['settings'][] = array(
					'key'		=> $module_prefix . 'width_range_' . $code,
					'type'		=> 'textarea',
					'title'		=> $code . ' ' . $data['entry_width_range'],
				);
			}
                        
                        
                        
                        
                        
                        
                        //------------------------------------------------------------------------------
			// Custom Code For height Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'height_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'height_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_filter',
				'type'		=> 'select',
				'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'links' => $data['text_links'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
				'default'	=> 'checkbox',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Height Range:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_flexible',
				'type'		=> 'select',
				'options'	=> array('slider' => $data['text_yes_with_height_slider']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_bottom_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Under [height]',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_middle_text',
				'type'		=> 'multilingual_text',
				'default'	=> '[from] to [to]',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'height_top_text',
				'type'		=> 'multilingual_text',
				'default'	=> '[height] and up',
			);
			
			// Height Ranges
			$data['settings'][] = array(
				'key'		=> 'height_ranges',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'type'		=> 'html',
				'content'	=> '<div class="text-info text-center pad-bottom">' . $data['help_height_ranges'] . '</div>',
			);
			foreach ($data['currency_array'] as $code => $name) {
				$data['settings'][] = array(
					'key'		=> $module_prefix . 'height_range_' . $code,
					'type'		=> 'textarea',
					'title'		=> $code . ' ' . $data['entry_height_range'],
				);
			}
                        
                        
                        
                        
                        
                        
                        
                        //------------------------------------------------------------------------------
			// Custom Code For Depth Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'depth_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'depth_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_filter',
				'type'		=> 'select',
				'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'links' => $data['text_links'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
				'default'	=> 'checkbox',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Depth Range:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_flexible',
				'type'		=> 'select',
				'options'	=> array('slider' => $data['text_yes_with_depth_slider']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_bottom_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Under [depth]',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_middle_text',
				'type'		=> 'multilingual_text',
				'default'	=> '[from] to [to]',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'depth_top_text',
				'type'		=> 'multilingual_text',
				'default'	=> '[depth] and up',
			);
			
			// Depth Ranges
			$data['settings'][] = array(
				'key'		=> 'depth_ranges',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'type'		=> 'html',
				'content'	=> '<div class="text-info text-center pad-bottom">' . $data['help_depth_ranges'] . '</div>',
			);
			foreach ($data['currency_array'] as $code => $name) {
				$data['settings'][] = array(
					'key'		=> $module_prefix . 'depth_range_' . $code,
					'type'		=> 'textarea',
					'title'		=> $code . ' ' . $data['entry_depth_range'],
				);
			}
                        
                        
                        
                        
                        
                        
                        
			
			//------------------------------------------------------------------------------
			// Rating Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'rating_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'rating_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'rating_filter',
				'type'		=> 'select',
				'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
				'default'	=> 'checkbox',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'rating_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'rating_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Rating:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'rating_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'rating_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'rating_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'and up',
			);
			
			//------------------------------------------------------------------------------
			// Search Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'search_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'search_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'search_filter',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_enabled'], 0 => $data['text_disabled']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'search_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'search_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Search:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'search_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'search_placeholder',
				'type'		=> 'multilingual_text',
				'default'	=> 'Enter keyword(s)',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'search_description',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_enabled'], 0 => $data['text_disabled']),
				'default'	=> 1,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'search_subcategory',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_enabled'], 0 => $data['text_disabled']),
				'default'	=> 1,
			);
			
			//------------------------------------------------------------------------------
			// Stock Status Filter
			//------------------------------------------------------------------------------
			$data['settings'][] = array(
				'key'		=> 'stock_filter',
				'type'		=> 'tab',
			);
			$data['settings'][] = array(
				'key'		=> 'stock_filter',
				'type'		=> 'heading',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'stock_filter',
				'type'		=> 'select',
				'options'	=> array('hide' => $data['text_hide'], 'checkbox' => $data['text_checkboxes'], 'radio' => $data['text_radio_buttons'], 'select' => $data['text_select_dropdown']),
				'default'	=> 'select',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'stock_sort_order',
				'type'		=> 'text',
				'class'		=> 'short',
				'attributes'=> array('maxlength' => 2),
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'stock_heading',
				'type'		=> 'multilingual_text',
				'default'	=> 'Stock Status:',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'stock_display',
				'type'		=> 'select',
				'options'	=> array('expanded' => $data['text_expanded'], 'collapsed' => $data['text_collapsed']),
				'default'	=> 'expanded',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'stock_count',
				'type'		=> 'select',
				'options'	=> array(1 => $data['text_yes'], 0 => $data['text_no']),
				'default'	=> 0,
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'instock_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'In Stock',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'outofstock_text',
				'type'		=> 'multilingual_text',
				'default'	=> 'Out of Stock',
			);
			$data['settings'][] = array(
				'key'		=> $module_prefix . 'stock_default',
				'type'		=> 'select',
				'options'	=> array('' => $data['text_neither'], 'in' => $data['text_in_stock'], 'out' => $data['text_out_of_stock']),
				'default'	=> 'select',
			);
			
			//------------------------------------------------------------------------------
			// end module settings
			//------------------------------------------------------------------------------
			
		}
		
		//------------------------------------------------------------------------------
		// end settings
		//------------------------------------------------------------------------------
		
		$this->document->setTitle($data['heading_title']);
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$template_file = DIR_TEMPLATE . 'extension/' . $this->type . '/' . $this->name . '.twig';

		if (is_file($template_file)) {
			extract($data);
			
			ob_start();
			require(class_exists('VQMod') ? VQMod::modCheck(modification($template_file)) : modification($template_file));
			$output = ob_get_clean();
			
			if (version_compare(VERSION, '3.0', '>=')) {
				$output = str_replace('&token=', '&user_token=', $output);
			}
			
			echo $output;
		} else {
			echo 'Error loading template file';
		}
	}
	
	//==============================================================================
	// Helper functions
	//==============================================================================
	private function hasPermission($permission) {
		return ($this->user->hasPermission($permission, $this->type . '/' . $this->name) || $this->user->hasPermission($permission, 'extension/' . $this->type . '/' . $this->name));
	}
	
	private function loadLanguage($path) {
		$_ = array();
		$language = array();
		$admin_language = (version_compare(VERSION, '2.2', '<')) ? $this->db->query("SELECT * FROM " . DB_PREFIX . "language WHERE `code` = '" . $this->db->escape($this->config->get('config_admin_language')) . "'")->row['directory'] : $this->config->get('config_admin_language');
		foreach (array('english', 'en-gb', $admin_language) as $directory) {
			$file = DIR_LANGUAGE . $directory . '/' . $directory . '.php';
			if (file_exists($file)) require($file);
			$file = DIR_LANGUAGE . $directory . '/default.php';
			if (file_exists($file)) require($file);
			$file = DIR_LANGUAGE . $directory . '/' . $path . '.php';
			if (file_exists($file)) require($file);
			$file = DIR_LANGUAGE . $directory . '/extension/' . $path . '.php';
			if (file_exists($file)) require($file);
			$language = array_merge($language, $_);
		}
		return $language;
	}
	
	private function getTableRowNumbers(&$data, $table, $sorting) {
		$groups = array();
		$rules = array();
		
		foreach ($data['saved'] as $key => $setting) {
			if (preg_match('/' . $table . '_(\d+)_' . $sorting . '/', $key, $matches)) {
				$groups[$setting][] = $matches[1];
			}
			if (preg_match('/' . $table . '_(\d+)_rule_(\d+)_type/', $key, $matches)) {
				$rules[$matches[1]][] = $matches[2];
			}
		}
		
		if (empty($groups)) $groups = array('' => array('1'));
		ksort($groups, defined('SORT_NATURAL') ? SORT_NATURAL : SORT_REGULAR);
		
		foreach ($rules as $key => $rule) {
			ksort($rules[$key], defined('SORT_NATURAL') ? SORT_NATURAL : SORT_REGULAR);
		}
		
		$data['used_rows'][$table] = array();
		$rows = array();
		foreach ($groups as $group) {
			foreach ($group as $num) {
				$data['used_rows'][preg_replace('/module_(\d+)_/', '', $table)][] = $num;
				$rows[$num] = (empty($rules[$num])) ? array() : $rules[$num];
			}
		}
		sort($data['used_rows'][$table]);
		
		return $rows;
	}
	
	//==============================================================================
	// Setting functions
	//==============================================================================
	private $encryption_key = '';
	
	public function loadSettings(&$data) {
		$backup_type = (empty($data)) ? 'manual' : 'auto';
		if ($backup_type == 'manual' && !$this->hasPermission('modify')) {
			return;
		}
		
		$this->cache->delete($this->name);
		unset($this->session->data[$this->name]);
		$code = (version_compare(VERSION, '3.0', '<') ? '' : $this->type . '_') . $this->name;
		
		// Set exit URL
		$data['token'] = $this->session->data[version_compare(VERSION, '3.0', '<') ? 'token' : 'user_token'];
		$data['exit'] = $this->url->link((version_compare(VERSION, '3.0', '<') ? 'extension' : 'marketplace') . '/' . (version_compare(VERSION, '2.3', '<') ? '' : 'extension&type=') . $this->type . '&token=' . $data['token'], '', 'SSL');
		
		// Load saved settings
		$data['saved'] = array();
		$settings_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($code) . "' ORDER BY `key` ASC");
		
		foreach ($settings_query->rows as $setting) {
			$key = str_replace($code . '_', '', $setting['key']);
			$value = $setting['value'];
			if ($setting['serialized']) {
				$value = (version_compare(VERSION, '2.1', '<')) ? unserialize($setting['value']) : json_decode($setting['value'], true);
			}
			
			$data['saved'][$key] = $value;
			
			if (is_array($value)) {
				foreach ($value as $num => $value_array) {
					foreach ($value_array as $k => $v) {
						$data['saved'][$key . '_' . $num . '_' . $k] = $v;
					}
				}
			}
		}
		
		// Load language and run standard checks
		$data = array_merge($data, $this->loadLanguage($this->type . '/' . $this->name));
		
		if (ini_get('max_input_vars') && ((ini_get('max_input_vars') - count($data['saved'])) < 50)) {
			$data['warning'] = $data['standard_max_input_vars'];
		}
		
		// Modify files according to OpenCart version
		if ($this->type == 'total' && version_compare(VERSION, '2.2', '<')) {
			file_put_contents(DIR_CATALOG . 'model/' . $this->type . '/' . $this->name . '.php', str_replace('public function getTotal($total) {', 'public function getTotal(&$total_data, &$order_total, &$taxes) {' . "\n\t\t" . '$total = array("totals" => &$total_data, "total" => &$order_total, "taxes" => &$taxes);', file_get_contents(DIR_CATALOG . 'model/' . $this->type . '/' . $this->name . '.php')));
		}
		
		if (version_compare(VERSION, '2.3', '>=')) {
			$filepaths = array(
				DIR_APPLICATION . 'controller/' . $this->type . '/' . $this->name . '.php',
				DIR_CATALOG . 'controller/' . $this->type . '/' . $this->name . '.php',
				DIR_CATALOG . 'model/' . $this->type . '/' . $this->name . '.php',
			);
			foreach ($filepaths as $filepath) {
				if (file_exists($filepath)) {
					rename($filepath, str_replace('.php', '.php-OLD', $filepath));
				}
			}
		}
		
		// Set save type and skip auto-backup if not needed
		if (!empty($data['saved']['autosave'])) {
			$data['save_type'] = 'auto';
		}
		
		if ($backup_type == 'auto' && empty($data['autobackup'])) {
			return;
		}
		
		// Create settings auto-backup file
		$manual_filepath = DIR_LOGS . $this->name . $this->encryption_key . '.backup';
		$auto_filepath = DIR_LOGS . $this->name . $this->encryption_key . '.autobackup';
		$filepath = ($backup_type == 'auto') ? $auto_filepath : $manual_filepath;
		if (file_exists($filepath)) unlink($filepath);
		
		file_put_contents($filepath, 'SETTING	NUMBER	SUB-SETTING	SUB-NUMBER	SUB-SUB-SETTING	VALUE' . "\n", FILE_APPEND|LOCK_EX);
		
		foreach ($data['saved'] as $key => $value) {
			if (is_array($value)) continue;
			
			$parts = explode('|', preg_replace(array('/_(\d+)_/', '/_(\d+)/'), array('|$1|', '|$1'), $key));
			
			$line = '';
			for ($i = 0; $i < 5; $i++) {
				$line .= (isset($parts[$i]) ? $parts[$i] : '') . "\t";
			}
			$line .= str_replace(array("\t", "\n"), array('    ', '\n'), $value) . "\n";
			
			file_put_contents($filepath, $line, FILE_APPEND|LOCK_EX);
		}
		
		$data['autobackup_time'] = date('Y-M-d @ g:i a');
		$data['backup_time'] = (file_exists($manual_filepath)) ? date('Y-M-d @ g:i a', filemtime($manual_filepath)) : '';
		
		if ($backup_type == 'manual') {
			echo $data['autobackup_time'];
		}
	}
	
	public function saveSettings() {
		if (!$this->hasPermission('modify')) {
			echo 'PermissionError';
			return;
		}
		
		$this->cache->delete($this->name);
		unset($this->session->data[$this->name]);
		$code = (version_compare(VERSION, '3.0', '<') ? '' : $this->type . '_') . $this->name;
		
		if ($this->request->get['saving'] == 'manual') {
			$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($code) . "' AND `key` != '" . $this->db->escape($this->name . '_module') . "'");
		}
		
		$module_id = 0;
		$modules = array();
		$module_instance = false;
		
		foreach ($this->request->post as $key => $value) {
			if (strpos($key, 'module_') === 0) {
				$parts = explode('_', $key, 3);
				$module_id = $parts[1];
				$modules[$parts[1]][$parts[2]] = $value;
				if ($parts[2] == 'module_id') $module_instance = true;
			} else {
				$key = (version_compare(VERSION, '3.0', '<') ? '' : $this->type . '_') . $this->name . '_' . $key;
				
				if ($this->request->get['saving'] == 'auto') {
					$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "'");
				}
				
				$this->db->query("
					INSERT INTO " . DB_PREFIX . "setting SET
					`store_id` = 0,
					`code` = '" . $this->db->escape($code) . "',
					`key` = '" . $this->db->escape($key) . "',
					`value` = '" . $this->db->escape(stripslashes(is_array($value) ? implode(';', $value) : $value)) . "',
					`serialized` = 0
				");
			}
		}
		
		foreach ($modules as $module_id => $module) {
			if (!$module_id) {
				$this->db->query("
					INSERT INTO " . DB_PREFIX . "module SET
					`name` = '" . $this->db->escape($module['name']) . "',
					`code` = '" . $this->db->escape($this->name) . "',
					`setting` = ''
				");
				$module_id = $this->db->getLastId();
				$module['module_id'] = $module_id;
			}
			$module_settings = (version_compare(VERSION, '2.1', '<')) ? serialize($module) : json_encode($module);
			$this->db->query("
				UPDATE " . DB_PREFIX . "module SET
				`name` = '" . $this->db->escape($module['name']) . "',
				`code` = '" . $this->db->escape($this->name) . "',
				`setting` = '" . $this->db->escape($module_settings) . "'
				WHERE module_id = " . (int)$module_id . "
			");
		}
	}
	
	public function deleteSetting() {
		if (!$this->hasPermission('modify')) {
			echo 'PermissionError';
			return;
		}
		$prefix = (version_compare(VERSION, '3.0', '<')) ? '' : $this->type . '_';
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE `code` = '" . $this->db->escape($prefix . $this->name) . "' AND `key` = '" . $this->db->escape($prefix . $this->name . '_' . str_replace('[]', '', $this->request->get['setting'])) . "'");
	}
	
	//==============================================================================
	// Module functions
	//==============================================================================
	public function copyModule() {
		$module_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "module WHERE module_id = " . (int)$this->request->get['module_id']);
		$module_settings = (version_compare(VERSION, '2.1', '<')) ? unserialize($module_query->row['setting']) : json_decode($module_query->row['setting'], true);
		$module_settings['name'] .= ' (Copy)';
		$this->db->query("INSERT INTO " . DB_PREFIX . "module SET `name` = '" . $this->db->escape($module_settings['name']) . "', `code` = '" . $this->db->escape($this->name) . "', setting = '" . $this->db->escape(version_compare(VERSION, '2.1', '<') ? serialize($module_settings) : json_encode($module_settings)) . "'");
	}
	
	public function deleteModule() {
		$this->db->query("DELETE FROM " . DB_PREFIX . "module WHERE module_id = " . (int)$this->request->get['module_id']);
	}
}
?>