<?php

/*
 * ***********************************************************************************
 * Abandoned Cart Module -                                                          **
 * Provide Coupons to the customers whose carts left abandoned.                     **
 *                                         **
 * ***********************************************************************************
 */

class ControllerModuleAbandonedCartIncentive extends Controller {

    private $error = array();

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public function index() {




        if (isset($this->request->get['tableactive']) && !empty($this->request->get['tableactive'])) {
            $data['tableactive'] = $this->request->get['tableactive'];
        } else {
            $data['tableactive'] = ' ';
        }

        $browser = ($this->request->server['HTTP_USER_AGENT']);
        if (preg_match('/(?i)msie [1-7]/', $browser)) {
            $data['IE7'] = true;
        } else {
            $data['IE7'] = false;
        }
        $this->RightMenuCookie();

        //Get store id from URL
        if (isset($this->request->get['store_id'])) {
            $store_id = $this->request->get['store_id'];
            $data['store_id'] = '1';
        } else {
            $store_id = 0;
            $data['store_id'] = '0';
        }

        $data['start_date'] = date('Y-m-d', strtotime('today - 30 days'));
        $data['end_date'] = date('Y-m-d', strtotime('today + 1 day'));
        $data['group_by'] = "days";
        // Load Language
        $data = array_merge($data, $this->language->load('module/abandonedcartincentive'));

        $this->document->setTitle($this->language->get('aci_heading_title'));
        $data['aci']['general']['adv_id'] = 0;
        // Load Models
        $this->load->model('setting/setting');
        $this->load->model('module/abandonedcartincentive');
        $result = $this->model_setting_setting->getSetting('aci', $store_id);
        //echo "<pre>";print_r($result); echo "</pre>";die;
        if (!empty($result)) {
            $result['aci']['general']['delay'] = $result['aci']['general']['delay1'] * 24 + $result['aci']['general']['delay'];
        }
        $this->getAbandonedCarts($result, $store_id);
        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
        } else {
            $page = 1;
        }
        if (isset($this->request->get['tableactive']) && !empty($this->request->get['tableactive'])) {
            $data['tableactive'] = $this->request->get['tableactive'];
        } else {
            $data['tableactive'] = '';
        }
        if (!empty($result)) {
            $registeredAbCartPagination = new Pagination();
            $registeredAbCartPagination->num_links = 9;
            $registeredAbCartPagination->total = $this->model_module_abandonedcartincentive->getTotalAbandonedCarts($result['aci']['general']['delay'], $store_id);
            $registeredAbCartPagination->page = $page;
            $registeredAbCartPagination->limit = 5;
            $registeredAbCartPagination->text = $this->language->get('text_pagination');
            $registeredAbCartPagination->url = $this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'] . '&tableactive=abregistered&page={page}&store=' . $store_id, 'SSL');
            $data['aci_abandoned_carts'] = $this->model_module_abandonedcartincentive->getAbandonedCarts($result['aci']['general']['delay'], $page, $registeredAbCartPagination->limit, $store_id);

            $data['limit'] = $registeredAbCartPagination->limit;

            $data['registeredAbCartPagination'] = $registeredAbCartPagination->render();

            $data['registeredAbCartPagination_results'] = sprintf($this->language->get('text_pagination'), ($registeredAbCartPagination->total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($registeredAbCartPagination->total - 5)) ? $registeredAbCartPagination->total : ((($page - 1) * 5) + $this->config->get('config_limit_admin')), $registeredAbCartPagination->total, ceil($registeredAbCartPagination->total / 5));




            $guestAbCartPagination = new Pagination();
            $guestAbCartPagination->num_links = 9;
            $guestAbCartPagination->total = $this->model_module_abandonedcartincentive->getTotalAbandonedCarts($result['aci']['general']['delay'], $store_id, 0);
            $guestAbCartPagination->page = $page;
            $guestAbCartPagination->limit = 5;
            $guestAbCartPagination->text = $this->language->get('text_pagination');
            $guestAbCartPagination->url = $this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'] . '&tableactive=abregistered&page={page}&store=' . $store_id, 'SSL');
            $data['aci_abandoned_guest_carts'] = $this->model_module_abandonedcartincentive->getAbandonedCarts($result['aci']['general']['delay'], $page, $guestAbCartPagination->limit, $store_id, 0);
            $data['limit'] = $guestAbCartPagination->limit;
            $data['guestAbCartPagination'] = $guestAbCartPagination->render();

            $data['guestAbCartPagination_results'] = sprintf($this->language->get('text_pagination'), ($guestAbCartPagination->total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($guestAbCartPagination->total - 5)) ? $guestAbCartPagination->total : ((($page - 1) * 5) + 5), $guestAbCartPagination->total, ceil($guestAbCartPagination->total / 5));

            $data['aci_abandoned_carts'] = $this->model_module_abandonedcartincentive->getAbandonedCarts($result['aci']['general']['delay'], $page, 5, $store_id);
            $data['aci_abandoned_guest_carts'] = $this->model_module_abandonedcartincentive->getAbandonedCarts($result['aci']['general']['delay'], $page, 5, $store_id, 0);

            //print_r($data['aci_abandoned_guest_carts']);
        }
        $this->getConvertedOrders($store_id);
        $this->load->model('module/abandonedcartincentive');
        //$data['aci_resgistered_customers_converted_carts'] = $this->model_module_abandonedcartincentive->getConvertedCarts($store_id, 1);
        //$data['aci_guest_customers_converted_carts'] = $this->model_module_abandonedcartincentive->getConvertedCarts($store_id, 0);
        $data['aci_converted_carts'] = array();
        $data['aci_converted_carts'] = $this->model_module_abandonedcartincentive->getConvertedCarts($store_id);


        //print_R($data['aci_converted_carts']);
        $j = 0;
        foreach ($data['aci_converted_carts'] as $converted_cart[$j]) {
            $this->load->model('sale/order');
            $this->load->model('localisation/order_status');

            $data['order'][$j] = $this->model_sale_order->getOrder($converted_cart[$j]['order_id']);
            //$data['customer'][$j] = unserialize($converted_cart[$j]['customer_info']);
            //print_R($data['order']);
            $data['order_status_converted'][$j] = $this->model_localisation_order_status->getOrderStatus($data['order'][$j]['order_status_id']);

            $data['total_view_converted'][$j] = $this->currency->format($converted_cart[$j]['cartTotal'], $this->session->data['currency']);

            $data['final_converted_data'][$j] = $converted_cart[$j];

            $j++;
        }


        //print_r($data['final_converted_data']);
        $this->DefaultValues($result);
        // Load CSS & JS
        $this->AddCSSandJS();

        // Get Stores
        if (!empty($result)) {
            $data['aci'] = $result['aci'];

            $data['aci_abandoned_carts'] = $this->model_module_abandonedcartincentive->getAbandonedCarts($result['aci']['general']['delay'], $page, 5, $store_id);            
            $i = 1;
            foreach ($data['aci_abandoned_carts'] as $data['abandonedcart']) {

                $data['modal_couponName'] = 'ACI[' . $data['abandonedcart']['restore_id'] . ']';
                $data['modal_coupons'][$i] = $this->model_module_abandonedcartincentive->getCoupons($data['modal_couponName']);


                $data['couponName'] = 'ACI[' . $data['abandonedcart']['restore_id'] . ']';
                
                $data['coupons'][$i] = $this->model_module_abandonedcartincentive->getCoupons($data['couponName']);                
                if (!empty($data['coupons'][$i])) {
                    $s = 0;
                    foreach($data['coupons'][$i] as $coupons){
                        if ($coupons['type'] == 'F') {
                            $data['fixed_amount'][$i][$s] = $this->currency->format($coupons['discount'], $this->session->data['currency']);
                            $data['discount_amount'][$i][$s] = $this->currency->format($coupons['discount'], $this->session->data['currency']);
                            $data['discount_total'][$i][$s] = $this->currency->format($coupons['total'], $this->session->data['currency']);                            
                        }
                        $s++;
                    }
                }
                $this->load->model('tool/image');
                
                $data['total'][$i] = 0;
                $data['quantity'][$i] = 0;
                $data['products'][$i] = unserialize($data['abandonedcart']['cart']);
                
                $s = 0;
                foreach ($data['products'][$i] as $products) {
                    if ($products['image']) {
                        $data['image_thumb'][$i][$s] = $this->model_tool_image->resize($products['image'], 30, 30);
                        $data['image'][$i][$s] = $this->model_tool_image->resize($products['image'], 125, 125);
                    } else {
                        $data['image_thumb'][$i][$s] = false;
                        $data['image'][$i][$s] = false;
                    }                    
                    $data['product'][$i][$s]['price'] = $this->currency->format($products['price'], $this->session->data['currency']);
                    $data['product'][$i][$s]['total'] = $this->currency->format($products['quantity'] * $products['price'], $this->session->data['currency']);

                    $data['total'][$i] = $products['total'] + $products['price'];
                    //$data['quantity'] = $data['product']['quantity'] + $data['product']['quantity'];
                    $s++;
                }                      
                $data['abandoned_cart_total'][$i] = $this->currency->format($data['abandonedcart']['cartTotal'], $this->session->data['currency']);
                $i++;
            }                        
            
            $data['aci_abandoned_guest_carts'] = $this->model_module_abandonedcartincentive->getAbandonedCarts($result['aci']['general']['delay'], $page, 5, $store_id, 0);
            $i = 1;
            foreach ($data['aci_abandoned_guest_carts'] as $data['guest_abandonedcart']) {                                
                
                $customer = unserialize($data['guest_abandonedcart']['customer_info']);
                
                $data['modal_guest_couponName'] = 'ACI[' . $customer['email'] . ']';
                $data['model_guest_coupons'][$i] = $this->model_module_abandonedcartincentive->getCoupons($data['modal_guest_couponName']);


                $data['guestcouponName'] = 'ACI[' . $customer['email'] . ']';
                
                $data['guest_coupons'][$i] = $this->model_module_abandonedcartincentive->getCoupons($data['guestcouponName']);                
                if (!empty($data['guest_coupons'][$i])) {
                    $t = 0;
                    foreach($data['guest_coupons'][$i] as $guest_coupons){
                        if ($guest_coupons['type'] == 'F') {
                            $data['guest_fixed_amount'][$i][$t] = $this->currency->format($guest_coupons['discount'], $this->session->data['currency']);
                            $data['guest_discount_amount'][$i][$t] = $this->currency->format($guest_coupons['discount'], $this->session->data['currency']);
                            $data['guest_discount_total'][$i][$t] = $this->currency->format($guest_coupons['total'], $this->session->data['currency']);
                        }
                        $t++;
                    }
                }
                
                $this->load->model('tool/image');                
                
                $data['guest_total'][$i] = 0;
                $data['guest_quantity'][$i] = 0;
                $data['guest_products'][$i] = unserialize($data['guest_abandonedcart']['cart']);

                $t = 0;
                foreach($data['guest_products'][$i] as $guest_products) {
                    if ($guest_products['image']) {
                        $data['guest_image_thumb'][$i][$t] = $this->model_tool_image->resize($guest_products['image'], 30, 30);
                        $data['guest_image'][$i][$t] = $this->model_tool_image->resize($guest_products['image'], 125, 125);
                    } else {
                        $data['guest_image_thumb'][$i][$t] = false;
                        $data['guest_image'][$i][$t] = false;
                    }
                                        
                    $data['guest_product'][$i][$t]['price'] = $this->currency->format($guest_products['price'], $this->session->data['currency']);
                    $data['guest_product'][$i][$t]['total'] = $this->currency->format($guest_products['quantity'] * $guest_products['price'], $this->session->data['currency']);
                    
                    $data['guest_total'][$i] = $guest_products['total'] + $guest_products['price'];
                    $t++;
                }
                //print_r($data['abandonedcart']['cartTotal'])   ;          
                $data['abandoned_guest_cart_total'][$i] = $this->currency->format($data['guest_abandonedcart']['cartTotal'], $this->session->data['currency']);
                $i++;
            }
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        // BreadCrumb
        //Breadcrumbs
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('aci_text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('aci_text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('aci_heading_title'),
            'href' => $this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );



        $this->ConvertedCartsInfo($result, $store_id);

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->session->data['success'] = $this->language->get('text_success');
            $this->model_setting_setting->editSetting('aci', $this->request->post, $store_id);
            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        if (!empty($result)) {

            $data['start_date'] = date('Y-m-d', strtotime('today - 30 days'));
            $data['end_date'] = date('Y-m-d', strtotime('today + 1 day'));
            $data['group_by'] = "days";
            // Data for graphs
            $total_registered_ab_carts = $this->model_module_abandonedcartincentive->getTotalAbandonedCarts($result['aci']['general']['delay'], $store_id);
            $total_guest_ab_carts = $this->model_module_abandonedcartincentive->getTotalAbandonedCarts($result['aci']['general']['delay'], $store_id, 0);
            $data['total_ab_carts'] = $total_registered_ab_carts + $total_guest_ab_carts;


            $total_converted_carts = $this->model_module_abandonedcartincentive->getTotalConvertedCartsCount($store_id);
            $data['total_converted_carts'] = $total_converted_carts;

            // $total_carts = $total_registered_ab_carts + $total_guest_ab_carts + $total_guest_converted_carts + $total_registered_converted_carts;
            //if( $total_carts > 0){    
            //$data['total_ab_carts'] = ($data['total_ab_carts']/ $total_carts) * 100;
            //$data['total_converted_carts'] = ($data['total_converted_carts']/ $total_carts) * 100;
            //}
            $total_registered_ab_amount = $this->model_module_abandonedcartincentive->getTotalAbandonedCartsAmount($result['aci']['general']['delay'], $store_id);
            $total_guest_ab_amount = $this->model_module_abandonedcartincentive->getTotalAbandonedCartsAmount($result['aci']['general']['delay'], $store_id, 0);
            $data['total_ab_amount'] = round($total_registered_ab_amount + $total_guest_ab_amount, 2);

            $total_registered_converted_amount = $this->model_module_abandonedcartincentive->getTotalConvertedCartsAmount($store_id, 1);
            $total_guest_converted_amount = $this->model_module_abandonedcartincentive->getTotalConvertedCartsAmount($store_id, 0);
            ;
            $data['total_converted_amount'] = round($total_guest_converted_amount + $total_registered_converted_amount, 2);

            $total_amount = $total_registered_ab_amount + $total_guest_ab_amount + $total_guest_converted_amount + $total_registered_converted_amount;
            if ($total_amount > 0) {
                //$data['total_ab_amount'] = round((($data['total_ab_amount']/ $total_amount) * 100),2);
                //$data['total_converted_amount'] = round((($data['total_converted_amount']/ $total_amount) * 100),2);
            }
            $monthly_ab_cart_details = $this->model_module_abandonedcartincentive->getMonthlyAbandonedCart($result['aci']['general']['delay'], $store_id);

            $data['monthly_ab_cart'] = array();
            foreach ($monthly_ab_cart_details as $ab_cart) {
                $data['monthly_ab_cart'][] = array(
                    'date_modified' => date('d-m-Y', strtotime($ab_cart['date_modified'])),
                    'abandoned_amount' => $ab_cart['cartTotal']
                );
            }
            $monthly_converted_cart_details = $this->model_module_abandonedcartincentive->getMonthlyConvertedCart($store_id);
            ;

            $data['monthly_converted_cart'] = array();
            foreach ($monthly_converted_cart_details as $converted_cart) {
                $data['monthly_converted_cart'][] = array(
                    'date_modified' => date('Y-m-d', strtotime($converted_cart['date_modified'])),
                    'converted_amount' => $converted_cart['cartTotal']
                );
            }


            $monthly_ab_orders_details = $this->model_module_abandonedcartincentive->getMonthlyAbandonedCart($result['aci']['general']['delay'], $store_id);
            $data31['monthly_ab_orders'] = array();
            foreach ($monthly_ab_orders_details as $ab_orders) {
                $data['monthly_ab_orders'][] = array(
                    'date_modified' => date('Y-m-d', strtotime($ab_orders['date_modified'])),
                    'abandoned_count' => $ab_orders['datefield_count'],
                    'abandoned_amount' => $ab_orders['cartTotal']
                );
            }

            $monthly_converted_orders_details = $this->model_module_abandonedcartincentive->getMonthlyConvertedCart($store_id);
            ;

            $data21['monthly_converted_orders'] = array();
            foreach ($monthly_converted_orders_details as $converted_order) {
                $data['monthly_converted_orders'][] = array(
                    'date_modified' => date('Y-m-d', strtotime($converted_order['date_modified'])),
                    'converted_count' => $converted_order['datefield_count'],
                    'converted_amount' => $converted_order['cartTotal']
                );
            }

            //print_r($data['monthly_ab_orders']);
            //print_r($data['monthly_converted_orders']);
            $data['monthly_orders'] = array();
            $mergedarray = array();
            foreach ($data31['monthly_ab_orders'] as $key => $data) {
                $mergedarray[$data['date_modified']]['date_modified'] = $data['date_modified'];
                $mergedarray[$data['date_modified']]['abandoned_count'] = $data['abandoned_count'];
                $mergedarray[$data['date_modified']]['abandoned_amount'] = $data['abandoned_amount'];
            }

            foreach ($data21['monthly_converted_orders'] as $key => $data) {
                $mergedarray[$data['date_modified']]['date_modified'] = $data['date_modified'];
                $mergedarray[$data['date_modified']]['converted_count'] = $data['converted_count'];
                $mergedarray[$data['date_modified']]['converted_amount'] = $data['converted_amount'];
            }
            ksort($mergedarray);
            $finalarray = array();

            foreach ($mergedarray as $key => $data) {
                if (!isset($data['abandoned_count'])) {
                    $data['abandoned_count'] = 0;
                }
                if (!isset($data['abandoned_amount'])) {
                    $data['abandoned_amount'] = 0;
                }
                if (!isset($data['converted_count'])) {
                    $data['converted_count'] = 0;
                }
                if (!isset($data['converted_amount'])) {
                    $data['converted_amount'] = 0;
                }
                $finalarray[] = $data;
            }

            $data['finalarray'] = $finalarray;
        }
        //links
        $data['action'] = $this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'] . '&store_id=' . $store_id, 'SSL');
        //$data['action_copy_registered'] = $this->url->link('module/abandonedcartincentive/getGuestData', 'token=' . $this->session->data['token'] . '&store_id=' . $store_id, 'SSL');        
        $data['route'] = $this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $data['token'] = $this->session->data['token'];
        $data['action_copy_registered'] = 'index.php?route=module/abandonedcartincentive/getGuestData&token=' . $data['token'] . '&store_id=' . $store_id;
        $data['action_generate_graph'] = 'index.php?route=module/abandonedcartincentive/generateGraph&token=' . $data['token'] . '&store_id=' . $store_id;
        // $data['action_generate_graph'] = 'index.php?route=module/abandonedcartincentive/generateGraph&token=' . $data['token'] . '&store_id=' . $store_id;
        //$data['aci']    = array();
        // Set Template
        $this->template = 'module/abandonedcartincentive.tpl';
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Render Output
        //$this->response->setOutput($this->render());

        $this->response->setOutput($this->load->view('module/abandonedcartincentive.tpl', $data));
    }

    public function RightMenuCookie() {
        // Right menu cookies check
        if (isset($this->request->cookie['rightMenu'])) {
            $data['rightMenu'] = true;
        } else {
            $data['rightMenu'] = false;
        }
    }

    private function GetStores() {
        $this->load->model('setting/store');
        $results = $this->model_setting_store->getStores();
        if ($results) {
            $data['stores'][] = array('store_id' => 0, 'name' => $this->config->get('config_name'));
            foreach ($results as $result) {
                $data['stores'][] = array(
                    'store_id' => $result['store_id'],
                    'name' => $result['name'],
                    'href' => $result['url']
                );
            }
        }
    }

    public function AddCSSandJS() {

        $this->document->addStyle('view/stylesheet/aci/lte/css/AdminLTE.css');
        $this->document->addStyle('view/stylesheet/aci/lte/css/velsof_lte.css');
        //$this->document->addStyle('view/stylesheet/aci/lte/css/stylesheet.css');
        $this->document->addStyle('view/stylesheet/aci/lte/css/stylesheet2.css');
        // Adding Styles for ACI Page
        $this->document->addStyle('view/stylesheet/aci/bootstrap/bootstrap.css');
        $this->document->addStyle('view/stylesheet/aci/theme/fonts/glyphicons/css/glyphicons_regular.css');
        $this->document->addStyle('view/stylesheet/aci/theme/fonts/font-awesome/css/font-awesome.min.css');
        $this->document->addStyle('view/stylesheet/aci/theme/scripts/plugins/forms/pixelmatrix-uniform/css/uniform.default.css');
        $this->document->addStyle('view/stylesheet/aci/bootstrap/extend/bootstrap-select/bootstrap-select.css');
        $this->document->addStyle('view/stylesheet/aci/bootstrap/extend/bootstrap-switch/static/stylesheets/bootstrap-switch.css');
        $this->document->addStyle('view/stylesheet/aci/theme/scripts/plugins/forms/select2/select2.css');
        $this->document->addStyle('view/stylesheet/aci/theme/scripts/plugins/notifications/notyfy/jquery.notyfy.css');
        $this->document->addStyle('view/stylesheet/aci/theme/scripts/plugins/notifications/notyfy/themes/default.css');
        $this->document->addStyle('view/stylesheet/aci/theme/scripts/plugins/notifications/Gritter/css/jquery.gritter.css');
        $this->document->addStyle('view/stylesheet/aci/theme/style-light.css?1386063042');
        $this->document->addStyle('view/stylesheet/aci/theme/scripts/plugins/sliders/jQRangeSlider/css/iThing.css');
        $this->document->addStyle('view/stylesheet/aci/theme/scripts/plugins/color/jquery-miniColors/jquery.miniColors.css');
        $this->document->addStyle('view/stylesheet/aci/theme/scripts/plugins/charts/easy-pie/jquery.easy-pie-chart.css');
        $this->document->addStyle('view/javascript/aci/theme/plugins/tables/DataTables/media/css/DT_bootstrap.css');
        $this->document->addStyle('view/javascript/aci/theme/plugins/tables/DataTables/media/css/jquery.dataTables.css');
        $this->document->addStyle('view/stylesheet/aci/theme/aci.css');

        //Adding required scripts/jquery for ACI page
        // $this->document->addScript('view/javascript/aci/theme/plugins/system/less.min.js');
        $this->document->addScript('view/javascript/aci/tinysort/jquery.tinysort.min.js');
        $this->document->addScript('view/javascript/aci/jquery/jquery.autosize.min.js');
        // $this->document->addScript('view/javascript/aci/uniform/jquery.uniform.min.js');
        $this->document->addScript('view/javascript/aci/codemirror/codemirror.js');
        $this->document->addScript('view/javascript/aci/codemirror/css.js');
        $this->document->addScript('view/javascript/aci/tooltip/tooltip.js');
        $this->document->addScript('view/javascript/aci/bootbox.js');
        $this->document->addScript('view/javascript/aci/exportExcel/jquery.battatech.excelexport.min.js');
        $this->document->addScript('view/javascript/aci/theme/common.js?1386063042');
        //$this->document->addScript('view/javascript/aci/bootstrap/bootstrap.min.js');       
        $this->document->addScript('view/javascript/aci/bootstrap/extend/bootstrap-select/bootstrap-select.js');
        $this->document->addScript('view/javascript/aci/bootstrap/extend/bootstrap-switch/static/js/bootstrap-switch.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/notifications/Gritter/js/jquery.gritter.min.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/notifications/notyfy/jquery.notyfy.js');
        $this->document->addScript('view/javascript/aci/theme/notifications.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/system/jquery.cookie.js');
        $this->document->addScript('view/javascript/aci/theme/themer.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/color/jquery-miniColors/jquery.miniColors.js');
        $this->document->addScript('view/javascript/aci/ckeditor/ckeditor.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.ba-resize.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.easy-pie-chart.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.orderBars.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.pie.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.resize.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.selection.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.tooltip.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.stack.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.symbol.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.time.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.flot.axislabels.js');
        //$this->document->addScript('view/javascript/aci/theme/plugins/charts/jquery.sparkline.min.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/tables/DataTables/media/js/jquery.dataTables.js');
        $this->document->addScript('view/javascript/aci/theme/plugins/tables/DataTables/media/js/DT_bootstrap.js');
        //$this->document->addScript('view/javascript/aci/aci.js');
        $this->document->addScript('http://www.google.com/jsapi');
    }

    public function CreateBreadCrumb() {
        //bread crumbs
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('aci_text_home'),
            'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('aci_text_module'),
            'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('aci_heading_title'),
            'href' => $this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
    }

    public function DefaultValues($result) {
        if (isset($this->request->post['aci'])) {

            $data['aci'] = $this->request->post['aci'];
        } elseif (!empty($result)) {
            // print_r($result);
            $data['aci'] = $result['aci'];
            //print_r($data['aci']);
        }
    }

    private function validate() {

        if (!$this->user->hasPermission('modify', 'module/abandonedcartincentive')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function install() {
        $this->load->model('module/abandonedcartincentive');
        $this->model_module_abandonedcartincentive->createTable();
        $this->load->model('setting/setting');
        $old_settings = $this->model_setting_setting->getSetting('velocity_menubar');
        $new_settings['velsof_aci'] = array('1', 'Abandoned Cart', 'module/abandonedcartincentive');
        $new_settings = array_merge($old_settings, $new_settings);
        $this->model_setting_setting->editSetting('velocity_menubar', $new_settings);
    }

    public function uninstall() {
        $this->load->model('module/abandonedcartincentive');
        $this->model_module_abandonedcartincentive->dropTable();
        $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int) $store_id . "' AND `group` = 'velocity_menubar' AND `key`='velsof_aci'");
    }

    private function getAbandonedCarts($result, $store_id = 0) {
        $this->load->model('module/abandonedcartincentive');

        if (isset($this->request->get['page']) && !empty($this->request->get['page'])) {
            $page = (int) $this->request->get['page'];
        } else {
            $page = 1;
        }
        if (isset($this->request->get['tableactive']) && !empty($this->request->get['tableactive'])) {
            $data['tableactive'] = $this->request->get['tableactive'];
        } else {
            $data['tableactive'] = '';
        }
        if (!empty($result)) {
            $registeredAbCartPagination = new Pagination();
            $registeredAbCartPagination->num_links = 9;
            $registeredAbCartPagination->total = $this->model_module_abandonedcartincentive->getTotalAbandonedCarts($result['aci']['general']['delay'], $store_id);
            $registeredAbCartPagination->page = $page;
            $registeredAbCartPagination->limit = 5;
            $registeredAbCartPagination->text = $this->language->get('text_pagination');
            $registeredAbCartPagination->url = $this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'] . '&tableactive=abregistered&page={page}&store=' . $store_id, 'SSL');
            $data['aci_abandoned_carts'] = $this->model_module_abandonedcartincentive->getAbandonedCarts($result['aci']['general']['delay'], $page, $registeredAbCartPagination->limit, $store_id);
            //print_r($data['aci_abandoned_carts']);
            $data['limit'] = $registeredAbCartPagination->limit;

            $data['registeredAbCartPagination'] = $registeredAbCartPagination->render();


            $guestAbCartPagination = new Pagination();
            $guestAbCartPagination->num_links = 9;
            $guestAbCartPagination->total = $this->model_module_abandonedcartincentive->getTotalAbandonedCarts($result['aci']['general']['delay'], $store_id, 0);
            $guestAbCartPagination->page = $page;
            $guestAbCartPagination->limit = 5;
            $guestAbCartPagination->text = $this->language->get('text_pagination');
            $guestAbCartPagination->url = $this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'] . '&tableactive=abregistered&page={page}&store=' . $store_id, 'SSL');
            $data['aci_abandoned_guest_carts'] = $this->model_module_abandonedcartincentive->getAbandonedCarts($result['aci']['general']['delay'], $page, $guestAbCartPagination->limit, $store_id, 0);
            $data['limit'] = $guestAbCartPagination->limit;
            $data['guestAbCartPagination'] = $guestAbCartPagination->render();
        }
    }

    private function getConvertedOrders($store_id = 0) {
        $this->load->model('module/abandonedcartincentive');
        //$data['aci_resgistered_customers_converted_carts'] = $this->model_module_abandonedcartincentive->getConvertedCarts($store_id, 1);
        //$data['aci_guest_customers_converted_carts'] = $this->model_module_abandonedcartincentive->getConvertedCarts($store_id, 0);
        $data['aci_converted_carts'] = $this->model_module_abandonedcartincentive->getConvertedCarts($store_id);
        //print_r( $data['aci_converted_carts'] );
    }

    public function deleteAbandonedCart() {
        $this->load->model('module/abandonedcartincentive');
        if (isset($_POST['cart_id'])) {
            $cartid = $_POST['cart_id'];
            $this->model_module_abandonedcartincentive->deleteAbandonedCart($cartid);
        } else {
            return false;
        }
    }

    public function ConvertedCartsInfo($result, $store_id) {
        $this->load->model('module/abandonedcartincentive');
        $data['totalOrdersRegistered'] = $this->model_module_abandonedcartincentive->getTotalConvertedCartsCount($store_id, 1);
        $data['totalAmountRegistered'] = $this->model_module_abandonedcartincentive->getTotalConvertedCartsAmount($store_id, 1);

        $data['totalOrdersGuest'] = $this->model_module_abandonedcartincentive->getTotalConvertedCartsCount($store_id, 0);
        $data['totalAmountGuest'] = $this->model_module_abandonedcartincentive->getTotalConvertedCartsAmount($store_id, 0);
    }

    public function sendCustomEmail() {
        $catalog_link = HTTP_CATALOG;
        $this->load->model('marketing/coupon');
        $this->load->model('tool/image');
        $this->load->model('module/abandonedcartincentive');

        // Disable previous coupons
        $couponName = 'ACI[' . $_POST['manual_email_id'] . ']';
        $coupons = $this->model_module_abandonedcartincentive->getCoupons($couponName);

        if (!empty($coupons)) {
            foreach ($coupons as $coupon) {
                $result = $this->model_module_abandonedcartincentive->getCouponsWithCouponHistory($coupon['coupon_id']);
                if (!$result) {
                    $this->model_module_abandonedcartincentive->disableCoupon($coupon['coupon_id']);
                }
            }
        }

        // Generate and save coupon
        $couponCode = $this->generateCoupon();
        $couponValidity = date('Y-m-d', time() + $_POST['manual_coupon_active'] * 24 * 60 * 60);
        $couponInfo = array('name' => 'ACI[' . $_POST['manual_email_id'] . ']',
            'code' => $couponCode,
            'discount' => $_POST['manual_discount'],
            'type' => $_POST['manual_discounttype'],
            'total' => $_POST['manual_minimum_amount'],
            'logged' => '0',
            'shipping' => '0',
            'date_start' => date('Y-m-d', time()),
            'date_end' => $couponValidity,
            'uses_total' => '1',
            'uses_customer' => '1',
            'status' => '1');
        $this->model_module_abandonedcartincentive->saveCoupon($couponInfo);

        // Create Cart Contents Table
        $this->language->load('module/abandonedcartincentive');
        $data['aci_text_price'] = $this->language->get('aci_text_price');
        $data['aci_text_qty'] = $this->language->get('aci_text_qty');
        $cartContent = '<table width="100%">';
        $cartContent .= '<thead>
                                                        <tr class="table-header">
                                                          <td class="left" width="70%"><strong>Product</strong></td>
                                                          <td class="left" width="15%"><strong>' . $data['aci_text_qty'] . '</strong></td>
                                                          <td class="left" width="15%"><strong>' . $data['aci_text_price'] . '</strong></td>
                                                        </tr>
                                                    </thead>';

        $cartItems = $this->model_module_abandonedcartincentive->getCartInfo($_POST['manual_cartid']);

        //print_r(unserialize($cartItems['cart']));
        foreach (unserialize($cartItems['cart']) as $product) {
            if ($product['image']) {
                $image_thumb = $this->model_tool_image->resize($product['image'], 60, 60);
                $image_thumb=str_replace(' ','%20',$image_thumb);// to fix space issue in url
            } else {
                $image = false;
            }
            $cartContent .='<tr>';
            $cartContent .='<td class="name"><div id="picture" style="float:left;padding-right:3px;"><a href="' . $catalog_link . 'index.php?route=product/product&product_id=' . $product['product_id'] . '" target="_blank"><img src="' . $image_thumb . '" /></a></div> <a href="' . $catalog_link . 'index.php?route=product/product&product_id=' . $product['product_id'] . '" target="_blank">' . $product['name'] . '</a><br />';
            foreach ($product['option'] as $option) {
                $cartContent .= '- <small>' . $option['name'] . ' ' . $option['option_value'] . '</small><br />';
            }
            $cartContent .= '</td>
								  <td class="quantity">x&nbsp;' . $product['quantity'] . '</td>
								  <td class="price">' . ($this->currency->format($product['price'], $this->session->data['currency'])) . '</td>
								</tr>';
        }
        $cartContent .='</table>';



        // Replace Place holders value in email body
        $placeHolderName = array();
        $placeHolderName[0] = '{firstname}';
        $placeHolderName[1] = '{lastname}';
        $placeHolderName[2] = '{cart_content}';
        $placeHolderName[3] = '{discount_code}';
        $placeHolderName[4] = '{discount_value}';
        $placeHolderName[5] = '{total_amount}';
        $placeHolderName[6] = '{date_end}';

        $placeHolderValues = array();
        $placeHolderValues[0] = (empty($_POST['manual_fname'])) ? '' : $_POST['manual_fname'];
        $placeHolderValues[1] = (empty($_POST['manual_lname'])) ? '' : $_POST['manual_lname'];
        $placeHolderValues[2] = $cartContent;
        $placeHolderValues[3] = $couponCode;
        if ($_POST['manual_discounttype'] == 'F') {
            $placeHolderValues[4] = $this->currency->format($_POST['manual_discount'], $this->session->data['currency']);
        } else {
            $placeHolderValues[4] = $_POST['manual_discount'];
        }
        $placeHolderValues[5] = $_POST['manual_minimum_amount'];
        $placeHolderValues[6] = $couponValidity;

        $mailContentHTML = str_replace($placeHolderName, $placeHolderValues, $_POST['manual_email_body']);



        $MailData = array(
            'email' => $_POST['manual_email_id'],
            'message' => html_entity_decode($mailContentHTML),
            'subject' => $_POST['manual_email_subject']);
        $emailResult = $this->model_module_abandonedcartincentive->sendMail($MailData);
        if ($emailResult) {
            echo "The email is sent successfully.";
            if (isset($_POST['manual_auto_email']) && $_POST['manual_auto_email'] == '1')
                $this->db->query("UPDATE `" . DB_PREFIX . "abandonedcartincentive` SET `autoEmail`= '1' WHERE restore_id = '" . $_POST['manual_email_id'] . "' AND isConvertedOrder = '0'");
        }
        else {
            echo "There is an error with the provided data in the form below.";
        }
    }

    private function generateCoupon() {
        //$this->load->model('module/abandonedcartincentive ');
        $length = 8;
        $code = "";
        $chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ0123456789";
        $maxlength = strlen($chars);
        if ($length > $maxlength) {
            $length = $maxlength;
        }
        $i = 0;
        while ($i < $length) {
            $char = substr($chars, mt_rand(0, $maxlength - 1), 1);
            if (!strstr($code, $char)) {
                $code .= $char;
                $i++;
            }
        }
        if ($this->model_module_abandonedcartincentive->distinctCode($code)) {
            return $code;
        } else {
            return $this->generateCoupon();
        }

        return $code;
    }

    public function getGuestData() {
        //Get store id from URL
        if (isset($this->request->get['store_id'])) {
            $store_id = $this->request->get['store_id'];
        } else {
            $store_id = 0;
        }
        // Load Models
        $this->load->model('setting/setting');
        $result = $this->model_setting_setting->getSetting('aci', $store_id);
        $result['aci']['general']['delay'] = $result['aci']['general']['delay1'] * 24 + $result['aci']['general']['delay'];
        $result['aci']['incentive']['guest'] = $result['aci']['incentive']['registered'];
        $this->model_setting_setting->editSetting('aci', $result, $store_id);
        $this->response->redirect($this->url->link('module/abandonedcartincentive', 'token=' . $this->session->data['token'], 'SSL'));
    }

    public function generateGraph() {


        //Get store id from URL
        if (isset($this->request->get['store_id'])) {
            $store_id = $this->request->get['store_id'];
        } else {
            $store_id = 0;
        }
        if (isset($this->request->post['submit'])) {

            $from = ($this->request->post['from']);
            $from1 = strtotime($this->request->post['from']);
            $to = ($this->request->post['to']);
            $to1 = strtotime($this->request->post['to']);

            $from_date = explode("-", $from);

            $to_date = explode("-", $to);


            $range = "";
            if ($from_date[0] == $to_date[0] && $from_date[1] == $to_date[1] && $from_date[2] == $to_date[2]) {
                $range = 'hour';
            } else if ($from_date[0] == $to_date[0] && $from_date[1] == $to_date[1]) {
                $range = 'day';
            } elseif ($from_date[0] == $to_date[0]) {
                $range = "month";
            } else {
                $range = "year";
            }
        }
//        $this->load->model('setting/setting');
//        $this->load->model('module/abandonedcartincentive');
//        $result = $this->model_setting_setting->getSetting('aci', $store_id);

        if (isset($this->request->post['range'])) {
            $range = $this->request->post['range'];
        }

        switch ($range) {
            case 'today':


                for ($i = 0; $i < 24; $i++) {
                    $abandon_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND (DATE(date_modified) = DATE(NOW()) AND HOUR(date_modified) = '" . (int) $i . "') GROUP BY HOUR(date_modified) ORDER BY date_modified ASC");
                    $converted_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted = 1 AND isConvertedOrder = 1
                                    AND store_id = '" . $store_id . "' AND (DATE(date_modified) = DATE(NOW()) AND HOUR(date_modified) = '" . (int) $i . "') GROUP BY HOUR(date_modified) ORDER BY date_modified ASC");
                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('h A', mktime($i, 0, 0, date('n'), date('j'), date('Y'))),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;

            case 'yesterday':


                for ($i = 0; $i < 24; $i++) {

                    $abandon_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =0
                                    AND store_id = '" . $store_id . "'AND (DATE(date_modified) = DATE_SUB(CURDATE(),INTERVAL 1 DAY) AND HOUR(date_modified) = '" . (int) $i . "') GROUP BY HOUR(date_modified) ORDER BY date_modified ASC");
                    $converted_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted = 1 AND isConvertedOrder = 1
                                    AND store_id = '" . $store_id . "' AND (DATE(date_modified) = DATE_SUB(CURDATE(),INTERVAL 1 DAY) AND HOUR(date_modified) = '" . (int) $i . "') GROUP BY HOUR(date_modified) ORDER BY date_modified ASC");
                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('h A', mktime($i, 0, 0, date('n'), date('j'), date('Y'))),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;


            case 'this_week':
                $data['view']['type'] = 'Days';
                $date_start = strtotime('this week');

                for ($i = 0; $i < 7; $i++) {

                    $date = date('Y-m-d', $date_start + ($i * 86400));

                    $abandon_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND DATE(date_modified) = '" . $this->db->escape($date) . "' GROUP BY DATE(date_modified)");
                    $converted_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =1
                               AND isConvertedOrder = 1 AND store_id = '" . $store_id . "' AND DATE(date_modified) = '" . $this->db->escape($date) . "' GROUP BY DATE(date_modified)");

                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('D', strtotime($date)),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }

                break;

            case 'last_week':
                $data['view']['type'] = 'Days';
                $date_start = strtotime('last week');

                for ($i = 0; $i < 7; $i++) {

                    $date = date('Y-m-d', $date_start + ($i * 86400));

                    $abandon_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND DATE(date_modified) = '" . $this->db->escape($date) . "' GROUP BY DATE(date_modified)");
                    $converted_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =1
                               AND isConvertedOrder = 1 AND store_id = '" . $store_id . "' AND DATE(date_modified) = '" . $this->db->escape($date) . "' GROUP BY DATE(date_modified)");

                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('D', strtotime($date)),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }

                break;

            default:
            case 'this_month':


                for ($i = 1; $i <= date('t'); $i++) {


                    $date = date('Y') . '-' . date('m') . '-' . $i;

                    $abandon_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND DATE(date_modified) = '" . $this->db->escape($date) . "' GROUP BY DATE(date_modified)");
                    $converted_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =1
                               AND isConvertedOrder = 1 AND store_id = '" . $store_id . "' AND DATE(date_modified) = '" . $this->db->escape($date) . "' GROUP BY DATE(date_modified)");

                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('M j', strtotime($date)),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;

            case 'last_month':

                for ($i = 1; $i <= date('t'); $i++) {
                    $date = date('Y') . '-' . date('m', strtotime('last month')) . '-' . $i;
                    $abandon_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND DATE(date_modified) = '" . $this->db->escape($date) . "' GROUP BY DATE(date_modified)");
                    $converted_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =1
                               AND isConvertedOrder = 1 AND store_id = '" . $store_id . "' AND DATE(date_modified) = '" . $this->db->escape($date) . "' GROUP BY DATE(date_modified)");

                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('M j', strtotime($date)),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;

            case 'this_year':

                for ($i = 1; $i <= 12; $i++) {
                    $abandon_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND  YEAR(date_modified) = '" . date('Y') . "' AND MONTH(date_modified) = '" . $i . "' GROUP BY MONTH(date_modified)");
                    $converted_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =1
                               AND isConvertedOrder = 1 AND store_id = '" . $store_id . "'AND  YEAR(date_modified) = '" . date('Y') . "' AND MONTH(date_modified) = '" . $i . "' GROUP BY MONTH(date_modified)");
                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('M', mktime(0, 0, 0, $i, 1, date('Y'))),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;

            case 'last_year':

                for ($i = 1; $i <= 12; $i++) {
                    $abandon_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND  YEAR(date_modified) = '" . date('Y', strtotime('last year')) . "' AND MONTH(date_modified) = '" . $i . "' GROUP BY MONTH(date_modified)");
                    $converted_query = $this->db->query("SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive`  WHERE isOrderCompleted =1
                               AND isConvertedOrder = 1 AND store_id = '" . $store_id . "' AND  YEAR(date_modified) = '" . date('Y', strtotime('last year')) . "' AND MONTH(date_modified) = '" . $i . "' GROUP BY MONTH(date_modified)");
                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('M', mktime(0, 0, 0, $i, 1, date('Y'))),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;

            case 'hour':

                $date = date('Y-m-d', strtotime($this->request->post['from']));

                for ($i = 0; $i < 24; $i++) {
                    $abandon_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND (DATE(date_modified) = '" . $date . "') AND HOUR(date_modified) = '" . (int) $i . "' GROUP BY HOUR(date_modified) ORDER BY date_modified ASC");
                    $converted_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted = 1 AND isConvertedOrder = 1
                                    AND store_id = '" . $store_id . "' AND (DATE(date_modified) = '" . $date . "') AND HOUR(date_modified) = '" . (int) $i . "' GROUP BY HOUR(date_modified) ORDER BY date_modified ASC");
                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('h A', mktime($i, 0, 0, date('n'), date('j'), date('Y'))),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;

            case 'day':

                for ($i = date('d', $from1); $i <= date('d', $to1); $i++) {
                    $date = date('Y', $from1) . '-' . date('m', $from1) . '-' . $i;

                    $abandon_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND (DATE(date_modified) = '" . $this->db->escape($date) . "') GROUP BY DAY(date_modified)");
                    $converted_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted = 1 AND isConvertedOrder = 1
                                    AND store_id = '" . $store_id . "' AND (DATE(date_modified) = '" . $this->db->escape($date) . "') GROUP BY DAY(date_modified)");


                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('M j', strtotime($date)),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;

            case 'month':

                for ($i = date('m', $from1); $i <= date('m', $to1); $i++) {
                    $abandon_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND   YEAR(date_modified) = '" . date('Y', $from1) . "' AND MONTH(date_modified) = '" . $i . "' GROUP BY MONTH(date_modified)");
                    $converted_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted = 1 AND isConvertedOrder = 1
                                    AND store_id = '" . $store_id . "' AND YEAR(date_modified) = '" . date('Y', $from1) . "' AND MONTH(date_modified) = '" . $i . "' GROUP BY MONTH(date_modified)");
                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => date('M', mktime(0, 0, 0, $i, 1, date('Y'))),
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;

            case 'year':

                for ($i = date('Y', $from1); $i <= date('Y', $to1); $i++) {
                    $abandon_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Abandon FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted =0
                                    AND store_id = '" . $store_id . "' AND   YEAR(date_modified) = '" . $i . "' ");
                    $converted_query = $this->db->query(" SELECT SUM( cartTotal ) as Amount , COUNT( * ) AS Converted FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE  isOrderCompleted = 1 AND isConvertedOrder = 1
                                    AND store_id = '" . $store_id . "'  AND YEAR(date_modified) = '" . $i . "' ");

                    if ($abandon_query->num_rows) {
                        $data['abandon'] = array((int) $abandon_query->row['Amount'], (int) $abandon_query->row['Abandon']);
                    } else {
                        $data['abandon'] = array(0, 0);
                    }
                    if ($converted_query->num_rows) {
                        $data['converted'] = array((int) $converted_query->row['Amount'], (int) $converted_query->row['Converted']);
                    } else {
                        $data['converted'] = array(0, 0);
                    }

                    $data['monthly_orders'][] = array(
                                'date_modified' => $i,
                                'abandoned_count' => $data['abandon'][1],
                                'converted_count' => $data['converted'][1],
                                'abandoned_amount' => $data['abandon'][0],
                                'converted_amount' => $data['converted'][0]);
                }
                break;
        }
        echo json_encode($data['monthly_orders']);
    }

}
