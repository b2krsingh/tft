<?php
class ControllerModuleFacebookStore extends Controller {
	private $error = array();
	private $version = '3.9.3';	
	
	public function install() {
		$this->load->model('module/facebook_store');
		$this->model_module_facebook_store->createFacebookStoreLayout();	
	}
	
	public function index() {   
		$this->load->language('module/facebook_store');

		$this->document->setTitle($this->language->get('heading_title'));
		
		// check update | if new version is available	
		$this->document->addScript('https://www.oc-extensions.com/catalog/view/javascript/api/js/update.min.js?extension_version=' . $this->version . '&oc_version=' . VERSION . '&email=' . $this->config->get('config_email'));		
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('facebook_store', $this->request->post);		
			
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$data['heading_title'] = $this->language->get('heading_title') . ' ' . $this->version;
		
		$data['tab_setting'] = $this->language->get('tab_setting');
		$data['tab_help'] = $this->language->get('tab_help');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_requirements'] = $this->language->get('text_requirements');
		
		$data['entry_app_id'] = $this->language->get('entry_app_id');
		$data['entry_logo'] = $this->language->get('entry_logo');
		$data['entry_template'] = $this->language->get('entry_template');
		$data['entry_redirect_checkout'] = $this->language->get('entry_redirect_checkout');
		$data['entry_code'] = $this->language->get('entry_code');
		
		$data['help_app_id'] = $this->language->get('help_app_id');
		$data['help_logo'] = $this->language->get('help_logo');
		$data['help_redirect_checkout'] = $this->language->get('help_redirect_checkout');
		$data['help_code'] = $this->language->get('help_code');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['app_id'])) {
			$data['error_app_id'] = $this->error['app_id'];
		} else {
			$data['error_app_id'] = array();
		}
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/facebook_store', 'token=' . $this->session->data['token'], 'SSL')
   		);
		
		$data['action'] = $this->url->link('module/facebook_store', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->update_check();
		
		$data['update'] = '';		
		
		if (isset($this->request->post['facebook_store_app_id'])) {
			$data['facebook_store_app_id'] = $this->request->post['facebook_store_app_id'];
		} elseif ($this->config->get('facebook_store_app_id')) { 
			$data['facebook_store_app_id'] = $this->config->get('facebook_store_app_id');
		} else {
			$data['facebook_store_app_id'] = '';
		}
		
		if (isset($this->request->post['facebook_store_logo'])) {
			$data['facebook_store_logo'] = $this->request->post['facebook_store_logo'];
		} elseif ($this->config->get('facebook_store_logo')) { 
			$data['facebook_store_logo'] = (string)$this->config->get('facebook_store_logo');
		} else {
			$data['facebook_store_logo'] = '';
		}
		
		$this->load->model('tool/image');
		
		if ($data['facebook_store_logo'] ){
			$data['logo'] = $this->model_tool_image->resize($data['facebook_store_logo'], 810, 200);
		} else {
		    $data['logo'] = $this->model_tool_image->resize('facebook_store_no_logo.png', 810, 200);
		}

		if (isset($this->request->post['facebook_store_template'])) {
			$data['facebook_store_template'] = $this->request->post['facebook_store_template'];
		} elseif ($this->config->get('facebook_store_template')) {
			$data['facebook_store_template'] = $this->config->get('facebook_store_template');
		} else {
			$data['facebook_store_template'] = '';
		}		
		
		if (isset($this->request->post['facebook_store_redirect_checkout'])) {
			$data['facebook_store_redirect_checkout'] = $this->request->post['facebook_store_redirect_checkout'];
		} elseif ($this->config->get('facebook_store_redirect_checkout')) { 
			$data['facebook_store_redirect_checkout'] = $this->config->get('facebook_store_redirect_checkout');
		} else {
			$data['facebook_store_redirect_checkout'] = '';
		}	
		
		$data['token'] = $this->session->data['token'];
		
		$server = HTTPS_CATALOG;
		
		$app_url = $server .'index.php?route=facebook_store/home';
		$data['generated_code'] = htmlspecialchars('<iframe src="' . $app_url . '" frameborder="0" scrolling="no" width="810" height="1400"></iframe>');
		
		// -- Get all themes that start with "facebook_store"
		$data['templates'] = array();

		$directories = glob(DIR_CATALOG . 'view/theme/*', GLOB_ONLYDIR);

		foreach ($directories as $directory) {
			if (strpos(basename($directory), "facebook_store") !== false) {
				$data['templates'][] = basename($directory);
			}	
		}
		// -- Stop get all themes that start with "facebook_store"	
			
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/facebook_store.tpl', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/facebook_store')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ( utf8_strlen($this->request->post['facebook_store_app_id']) < 1 ){
			$this->error['app_id'] = $this->language->get('error_app_id');
		}
				
		return !$this->error;
	}
	
	private function update_check() {
		$data = 'v=' . $this->version . '&ex=12&e=' . $this->config->get('config_email') . '&ocv=' . VERSION;
        $curl = false;
        
        if (extension_loaded('curl')) {
			$ch = curl_init();
			
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_URL, 'https://www.oc-extensions.com/api/v1/update_check');
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'OCX-Adaptor: curl'));
			curl_setopt($ch, CURLOPT_REFERER, HTTP_CATALOG);
			
			if (function_exists('gzinflate')) {
				curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
			}
            
			$result = curl_exec($ch);
			$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			
			if ($http_code == 200) {
				$result = json_decode($result);
				
                if ($result) {
                    $curl = true;
                }
                
                if ( isset($result->version) && ($result->version > $this->version) ) {
				    $this->error['update'] = 'A new version of this extension is available. <a target="_blank" href="' . $result->url . '">Click here</a> to see the Changelog.';
				}
			}
		}
        
        if (!$curl) {
			if (!$fp = @fsockopen('ssl://www.oc-extensions.com', 443, $errno, $errstr, 20)) {
				return false;
			}

			socket_set_timeout($fp, 20);
			
			$headers = array();
			$headers[] = "POST /api/v1/update_check HTTP/1.0";
			$headers[] = "Host: www.oc-extensions.com";
			$headers[] = "Referer: " . HTTP_CATALOG;
			$headers[] = "OCX-Adaptor: socket";
			if (function_exists('gzinflate')) {
				$headers[] = "Accept-encoding: gzip";
			}	
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			$headers[] = "Accept: application/json";
			$headers[] = 'Content-Length: '.strlen($data);
			$request = implode("\r\n", $headers)."\r\n\r\n".$data;
			fwrite($fp, $request);
			$response = $http_code = null;
			$in_headers = $at_start = true;
			$gzip = false;
			
			while (!feof($fp)) {
				$line = fgets($fp, 4096);
				
				if ($at_start) {
					$at_start = false;
					
					if (!preg_match('/HTTP\/(\\d\\.\\d)\\s*(\\d+)\\s*(.*)/', $line, $m)) {
						return false;
					}
					
					$http_code = $m[2];
					continue;
				}
				
				if ($in_headers) {

					if (trim($line) == '') {
						$in_headers = false;
						continue;
					}

					if (!preg_match('/([^:]+):\\s*(.*)/', $line, $m)) {
						continue;
					}
					
					if ( strtolower(trim($m[1])) == 'content-encoding' && trim($m[2]) == 'gzip') {
						$gzip = true;
					}
					
					continue;
				}
				
                $response .= $line;
			}
					
			fclose($fp);
			
			if ($http_code == 200) {
				if ($gzip && function_exists('gzinflate')) {
					$response = substr($response, 10);
					$response = gzinflate($response);
				}
				
				$result = json_decode($response);
				
                if ( isset($result->version) && ($result->version > $this->version) ) {
				    $this->error['update'] = 'A new version of this extension is available. <a target="_blank" href="' . $result->url . '">Click here</a> to see the Changelog.';
				}
			}
		}
	}	
}
?>