<?php
class ControllerModuleFSSpecial extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/fs_special');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('fs_special', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}

			$this->cache->delete('product');

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_grid'] = $this->language->get('text_grid');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_seconds'] = $this->language->get('text_seconds');
		
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_days'] = $this->language->get('entry_days');
		$data['entry_hours'] = $this->language->get('entry_hours');
		$data['entry_minutes'] = $this->language->get('entry_minutes');
		$data['entry_seconds'] = $this->language->get('entry_seconds');
		$data['entry_limit'] = $this->language->get('entry_limit');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_random_start'] = $this->language->get('entry_random_start');
		$data['entry_auto_slider'] = $this->language->get('entry_auto_slider');
		$data['entry_slide_time'] = $this->language->get('entry_slide_time');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['help_title'] = $this->language->get('help_title');
		$data['help_days'] = $this->language->get('help_days');
		$data['help_hours'] = $this->language->get('help_hours');
		$data['help_minutes'] = $this->language->get('help_minutes');
		$data['help_seconds'] = $this->language->get('help_seconds');
		$data['help_width'] = $this->language->get('help_width');
		$data['help_height'] = $this->language->get('help_height');
		$data['help_random_start'] = $this->language->get('help_random_start');
		$data['help_auto_slider'] = $this->language->get('help_auto_slider');
		$data['help_slide_time'] = $this->language->get('help_slide_time');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}
		
		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = array();
		}	

		if (isset($this->error['days'])) {
			$data['error_days'] = $this->error['days'];
		} else {
			$data['error_days'] = array();
		}

		if (isset($this->error['hours'])) {
			$data['error_hours'] = $this->error['hours'];
		} else {
			$data['error_hours'] = array();
		}

		if (isset($this->error['minutes'])) {
			$data['error_minutes'] = $this->error['minutes'];
		} else {
			$data['error_minutes'] = array();
		}

		if (isset($this->error['seconds'])) {
			$data['error_seconds'] = $this->error['seconds'];
		} else {
			$data['error_seconds'] = array();
		}		
		
		if (isset($this->error['width'])) {
			$data['error_width'] = $this->error['width'];
		} else {
			$data['error_width'] = '';
		}
		
		if (isset($this->error['height'])) {
			$data['error_height'] = $this->error['height'];
		} else {
			$data['error_height'] = '';
		}
		
		if (isset($this->error['slide_time'])) {
			$data['error_slide_time'] = $this->error['slide_time'];
		} else {
			$data['error_slide_time'] = '';
		}		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/banner', 'token=' . $this->session->data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('module/fs_special', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);			
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('module/fs_special', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('module/fs_special', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}	
			
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}
		
		if (isset($this->request->post['language_texts'])) {
			$data['language_texts'] = $this->request->post['language_texts'];
		} elseif (!empty($module_info)) {
			$data['language_texts'] = $module_info['language_texts'];
		} else {
			$data['language_texts'] = array();
		}		
				
		if (isset($this->request->post['limit'])) {
			$data['limit'] = $this->request->post['limit'];
		} elseif (!empty($module_info)) {
			$data['limit'] = $module_info['limit'];
		} else {
			$data['limit'] = 5;
		}	
				
		if (isset($this->request->post['width'])) {
			$data['width'] = $this->request->post['width'];
		} elseif (!empty($module_info)) {
			$data['width'] = $module_info['width'];
		} else {
			$data['width'] = 180;
		}	
			
		if (isset($this->request->post['height'])) {
			$data['height'] = $this->request->post['height'];
		} elseif (!empty($module_info)) {
			$data['height'] = $module_info['height'];
		} else {
			$data['height'] = 180;
		}

		if (isset($this->request->post['random_start'])) {
			$data['random_start'] = $this->request->post['random_start'];
		} elseif (!empty($module_info)) {
			$data['random_start'] = $module_info['random_start'];
		} else {
			$data['random_start'] = '';
		}

		if (isset($this->request->post['auto_slider'])) {
			$data['auto_slider'] = $this->request->post['auto_slider'];
		} elseif (!empty($module_info)) {
			$data['auto_slider'] = $module_info['auto_slider'];
		} else {
			$data['auto_slider'] = '';
		}

		if (isset($this->request->post['slide_time'])) {
			$data['slide_time'] = $this->request->post['slide_time'];
		} elseif (!empty($module_info)) {
			$data['slide_time'] = $module_info['slide_time'];
		} else {
			$data['slide_time'] = '5';
		}		
				
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}
		
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
			
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/fs_special.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/fs_special')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		
		foreach($this->request->post['language_texts'] as $language_id => $value) {
			if (utf8_strlen($value['title']) < 1) {
				$this->error['title'][$language_id] = $this->language->get('error_title');
			}
			
			if (utf8_strlen($value['days']) < 1) {
				$this->error['days'][$language_id] = $this->language->get('error_days');
			}	

			if (utf8_strlen($value['hours']) < 1) {
				$this->error['hours'][$language_id] = $this->language->get('error_hours');
			}			
			
			if (utf8_strlen($value['minutes']) < 1) {
				$this->error['minutes'][$language_id] = $this->language->get('error_minutes');
			}

			if (utf8_strlen($value['seconds']) < 1) {
				$this->error['seconds'][$language_id] = $this->language->get('error_seconds');
			}			
		}
		
		if (!$this->request->post['width']) {
			$this->error['width'] = $this->language->get('error_width');
		}
		
		if (!$this->request->post['height']) {
			$this->error['height'] = $this->language->get('error_height');
		}
		
		if ($this->request->post['auto_slider']) {
			if (utf8_strlen($this->request->post['slide_time']) < 1 || !is_numeric($this->request->post['slide_time'])) {
				$this->error['slide_time'] = $this->language->get('error_slide_time');
			}		
		}		

		return !$this->error;
	}
}