<?php 
class ControllerPaymentCommweb3dsecure extends Controller {
	private $error = array(); 

	public function index() {
		$this->language->load('payment/commweb_3dsecure');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('commweb_3dsecure', $this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_edit'] = $this->language->get('text_edit');
		
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_transaction_method'] = $this->language->get('text_transaction_method');
		$data['value_transaction_method'] = $this->language->get('value_transaction_method');
		
		$data['entry_merchant_id'] = $this->language->get('entry_merchant_id');
		$data['entry_access_code'] = $this->language->get('entry_access_code');
		$data['entry_secure_hash'] = $this->language->get('entry_secure_hash');
		$data['entry_test'] = $this->language->get('entry_test');
		$data['entry_transaction_method'] = $this->language->get('entry_transaction_method');
		$data['entry_total'] = $this->language->get('entry_total');	
		$data['entry_order_status'] = $this->language->get('entry_order_status');		
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

 		if (isset($this->error['merchant_id'])) {
			$data['error_merchant_id'] = $this->error['merchant_id'];
		} else {
			$data['error_merchant_id'] = '';
		}
		
 		if (isset($this->error['access_code'])) {
			$data['error_access_code'] = $this->error['access_code'];
		} else {
			$data['error_access_code'] = '';
		}
		
 		if (isset($this->error['secure_hash'])) {
			$data['error_secure_hash'] = $this->error['secure_hash'];
		} else {
			$data['error_secure_hash'] = '';
		}

		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_extension'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/commweb_3dsecure', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
				
		$data['action'] = $this->url->link('payment/commweb_3dsecure', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'] . '&type=payment', true);
/*echo '<pre>';
print_r($this->config);
exit;*/

		if (isset($this->request->post['commweb_3dsecure_merchant_id'])) {
			$data['commweb_3dsecure_merchant_id'] = $this->request->post['commweb_3dsecure_merchant_id'];
		} else {
			$data['commweb_3dsecure_merchant_id'] = $this->config->get('commweb_3dsecure_merchant_id');
		}
		
		if (isset($this->request->post['commweb_3dsecure_access_code'])) {
			$data['commweb_3dsecure_access_code'] = $this->request->post['commweb_3dsecure_access_code'];
		} else {
			$data['commweb_3dsecure_access_code'] = $this->config->get('commweb_3dsecure_access_code');
		}
				
		if (isset($this->request->post['commweb_3dsecure_secure_hash'])) {
			$data['commweb_3dsecure_secure_hash'] = $this->request->post['commweb_3dsecure_secure_hash'];
		} else {
			$data['commweb_3dsecure_secure_hash'] = $this->config->get('commweb_3dsecure_secure_hash');
		}
		
		if (isset($this->request->post['commweb_3dsecure_test'])) {
			$data['commweb_3dsecure_test'] = $this->request->post['commweb_3dsecure_test'];
		} else {
			$data['commweb_3dsecure_test'] = $this->config->get('commweb_3dsecure_test');
		}
		
		if (isset($this->request->post['commweb_3dsecure_method'])) {
			$data['commweb_3dsecure_method'] = $this->request->post['commweb_3dsecure_method'];
		} else {
			$data['commweb_3dsecure_method'] = $this->config->get('commweb_3dsecure_method');
		}
		
		if (isset($this->request->post['commweb_3dsecure_total'])) {
			$data['commweb_3dsecure_total'] = $this->request->post['commweb_3dsecure_total'];
		} else {
			$data['commweb_3dsecure_total'] = $this->config->get('commweb_3dsecure_total');
		} 
				
		if (isset($this->request->post['commweb_3dsecure_order_status_id'])) {
			$data['commweb_3dsecure_order_status_id'] = $this->request->post['commweb_3dsecure_order_status_id'];
		} else {
			$data['commweb_3dsecure_order_status_id'] = $this->config->get('commweb_3dsecure_order_status_id');
		} 

		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['commweb_3dsecure_geo_zone_id'])) {
			$data['commweb_3dsecure_geo_zone_id'] = $this->request->post['commweb_3dsecure_geo_zone_id'];
		} else {
			$data['commweb_3dsecure_geo_zone_id'] = $this->config->get('commweb_3dsecure_geo_zone_id');
		} 
		
		$this->load->model('localisation/geo_zone');
										
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['commweb_3dsecure_status'])) {
			$data['commweb_3dsecure_status'] = $this->request->post['commweb_3dsecure_status'];
		} else {
			$data['commweb_3dsecure_status'] = $this->config->get('commweb_3dsecure_status');
		}
		
		if (isset($this->request->post['commweb_3dsecure_sort_order'])) {
			$data['commweb_3dsecure_sort_order'] = $this->request->post['commweb_3dsecure_sort_order'];
		} else {
			$data['commweb_3dsecure_sort_order'] = $this->config->get('commweb_3dsecure_sort_order');
		}
	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
        
		$this->response->setOutput($this->load->view('payment/commweb_3dsecure.tpl', $data));
		
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'payment/commweb_3dsecure')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->request->post['commweb_3dsecure_merchant_id']) {
			$this->error['merchant_id'] = $this->language->get('error_merchant_id');
		}

		if (!$this->request->post['commweb_3dsecure_access_code']) {
			$this->error['access_code'] = $this->language->get('error_access_code');
		}

		/*if (!$this->request->post['commweb_3dsecure_secure_hash']) {
			$this->error['secure_hash'] = $this->language->get('error_secure_hash');
		}*/
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>