<?php
// Heading
$_['heading_title']        = 'ZipMoney Widget';

//Text
$_['text_module']          = 'Modules';
$_['text_success']         = 'Success: You have modified ZipMoney Widget module!';
$_['text_edit']            = 'Edit ZipMoney Widget Module';
$_['text_button_grey']     = 'Grey';
$_['text_button_blue']     = 'Blue (Recommended)';

//Entry
$_['entry_client_id']      = 'Client ID';
$_['entry_secret']         = 'Secret';
$_['entry_sandbox']        = 'Sandbox Mode';
$_['entry_debug']          = 'Debug Logging';
$_['entry_customer_group'] = 'Customer Group';
$_['entry_button']         = 'Button Colour';
$_['entry_seamless']       = 'Allow "Seamless Checkout"';
$_['entry_locale']         = 'Locale';
$_['entry_return_url']     = 'Return URL';
$_['entry_status']         = 'Status';

//Help
$_['help_sandbox']         = 'Use sandbox (testing) environment?';
$_['help_customer_group']  = 'For new customers, which Customer Group should they get created as?';
$_['help_debug_logging']   = 'Enabling this will allow data to be added to your error log to help debug any problems.';
$_['help_locale']          = 'This is the ZipMoney Widget locale setting for your store languages';
$_['help_return_url']      = 'This needs to be added in the PayPal app configuration under app redirect URLs.';

//Error
$_['error_permission']     = 'Warning: You do not have permission to modify ZipMoney Widget module!';
$_['error_client_id']      = 'Client ID required!';
$_['error_secret']         = 'Secret required!';