<?php
//Headings
$_['heading_title']                     = 'zipMoney';

$_['text_success']                  = 'zipMoney module has been updated';

$_['text_edit']						= 'Edit zipMoney setting';
$_['text_zipmoney']                 = '<img src="http://d3k1w8lx8mqizo.cloudfront.net/logo/25px/zipmoney.png" alt="zipMoney" title="zipMoney" style="border: 1px solid #EEEEEE;" />';


// Buttons
$_['button_capture']				= 'Capture';
$_['button_refund']					= 'Refund';
$_['button_cancel']					= 'Cancel';
