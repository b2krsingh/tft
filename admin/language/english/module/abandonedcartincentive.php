<?php

/*
 * ***********************************************************************************
 * Abandoned Cart Module -                                                          **
 * Provide Coupons to the customers whose carts left abandoned.                     **
 * By Velocity Software Solutions Pvt. Ltd.                                         **
 * ***********************************************************************************
 */

$_['aci_heading_title']         = 'Abandoned Cart';
$_['heading_title']             = '<span style="color:#946846; font-weight:bold">Abandoned Cart v0.7</span>';
$_['aci_plugin_version']        = ' v0.5';
$_['aci_plugin_version_number'] = '0.5';
$_['aci_plugin_name']           = 'Abandoned Cart';
$_['aci_plugin_id']           = 'OC0006';

// Tab Names
$_['aci_tab_general_settings']   = 'General Settings';
$_['aci_tab_incentive_settings'] = 'Incentive Settings';
$_['aci_tab_email_template']     = 'Email Template';
$_['aci_tab_abandoned_cart']     = 'View Abandoned Cart';
$_['aci_tab_conversion_rate']    = 'View Conversion Rate';
$_['aci_tab_archived_orders']    = 'View Archive Orders';
$_['aci_tab_reports_visualization'] = 'Analytics';
$_['aci_tab_themer'] = 'Themer';


// Entry Names
// General Settings
$_['aci_entry_general_enable']            = 'Enable/Disable : ';
$_['aci_entry_general_allow_guest_users'] = 'Allow guest users cart : ';
$_['aci_entry_general_delay']             = 'Delay : ';
$_['aci_entry_general_popup_callback']    = 'Popup callback time : ';
$_['aci_entry_general_scheduled_task']    = 'Scheduled Task : ';
$_['aci_entry_general_hours']             = 'hour(s)';
$_['aci_entry_general_minutes']           = 'minute(s)';

// Incentive Settings
$_['aci_entry_incentive_registered']                     = 'Registered ';
$_['aci_entry_incentive_guest']                          = 'Guest ';
$_['aci_entry_incentive_same_as_registered']             = 'Copy same for Guest ';
// Incentive Settings Table
$_['aci_entry_incentive_table_discount_type']            = 'Discount Type';
$_['aci_entry_incentive_table_discount']                 = 'Discount';
$_['aci_entry_incentive_table_miminum_amount']           = 'Minimum Amount';
$_['aci_entry_incentive_table_coupon_active_time']       = 'Coupon Active Time';
$_['aci_entry_incentive_table_delay']                    = 'Delay';
$_['aci_entry_incentive_table_status']                   = 'Status';
$_['aci_entry_incentive_table_status_enabled']           = 'Enabled';
$_['aci_entry_incentive_table_status_disabled']          = 'Disabled';
$_['aci_entry_incentive_table_discount_type_fixed']      = 'Fixed Amount';
$_['aci_entry_incentive_table_discount_type_percentage'] = 'Percentage';
$_['aci_entry_incentive_table_remove']                   = 'Remove';
$_['aci_entry_incentive_table_add_module']               = 'Add New';

// Email Template
$_['aci_entry_email_subject'] = 'Email subject : ';
$_['aci_entry_email_body']    = 'Email body : ';
$_['aci_entry_email_stop']    = 'Stop automatic emails after this email : ';
$_['aci_entry_email_cancel']    = 'Cancel';
$_['aci_entry_email_send']    = 'Send Email';

// Tooltips
// General Settings
$_['aci_tooltip_general_enable']            = 'Enable or Disable the plugin on the store front. If disabled, no abandoned cart is captured.';
$_['aci_tooltip_general_allow_guest_users'] = 'Capture guest users abandoned cart or not.';
$_['aci_tooltip_general_delay']             = 'Time period after which the cart is assumed as abandoned.';
$_['aci_tooltip_general_popup_callback']    = 'Tooltip';
$_['aci_tooltip_general_scheduled_task']    = 'Send or not automatic incentive mails.';
// Email Template
$_['aci_tooltip_email_subject']             = 'Subject of the incentive email';
$_['aci_tooltip_email_body']                = 'Body of the incentive mail send to the customers.';

$_['aci_tooltip_incentive_table_discount_type']      = 'Type of discount given to the customer.';
$_['aci_tooltip_incentive_table_coupon_active_time'] = 'Time period in which the coupon code is valid.';
$_['aci_tooltip_incentive_table_discount']           = 'Amount of discount to be provided.';
$_['aci_tooltip_email_stop']                         = 'No further emails will be send to the customer after this email.';
$_['aci_tooltip_incentive_table_miminum_amount']     = 'The minimum cart total against which the coupon code is valid.';


//Buttons
$_['aci_button_save_and_stay'] = 'Save and stay';
$_['aci_button_save']          = 'Save';
$_['aci_button_cancel']        = 'Cancel';

// Texts
$_['aci_text_home']   = 'Home';
$_['aci_text_module'] = 'Modules';
$_['aci_text_yes']    = 'Yes';
$_['aci_text_no']     = 'No';

$_['aci_text_qty']   = 'Qty:';
$_['aci_text_price'] = 'Price:';

$_['aci_text_cron_instructions'] = 'Cron Instructions';
$_['aci_text_cron_instructions_1'] = 'Add the cron to your store via control panel/putty to run every hour. Find below the Instruction to add the cron.';
$_['aci_text_cron_instructions_2'] = 'URL to Add Cron via Control Panel';
$_['aci_text_cron_instructions_3'] = 'Cron setup via SSH';
$_['aci_text_cron_instructions_4'] = '0 * * * * wget -O /dev/null ';

$_['aci_text_registered_customer_incentive'] = 'Registered Customers Incentive';
$_['aci_text_guest_customer_incentive'] = 'Guest Customers Incentive';
$_['aci_text_customer'] = 'Customer';
$_['aci_text_current_incentive'] = 'Current Incentive';
$_['aci_text_date_time'] = 'Date & Time';
$_['aci_text_action'] = 'Action';
$_['aci_text_no_coupon_code_sent'] = 'No coupon code sent.';
$_['aci_text_previous_coupons'] = 'Previous Coupons';
$_['aci_text_coupon'] = 'Coupon';
$_['aci_text_discount'] = 'Discount';
$_['aci_text_min_amt'] = 'Minimum Amount';
$_['aci_text_created_on'] = 'Created On';
$_['aci_text_expired_on'] = 'Expired On';
$_['aci_text_first_visit'] = 'First Visit : ';
$_['aci_text_last_visit'] = 'Last Visit : ';
$_['aci_text_time_spent'] = 'Time Spent : ';
$_['aci_text_coupon_email'] = '-- Coupon Email';
$_['aci_text_cart_items'] = ' -- Cart Items';
$_['aci_text_image'] = 'Image';
$_['aci_text_items'] = 'Items';
$_['aci_text_model'] = 'Model';
$_['aci_text_qty'] = 'Qty';
$_['aci_text_unit_price'] = 'Unit Price';
$_['aci_text_total'] = 'Total';
$_['aci_text_guest_ab_cart_requirement'] = 'Guest Abandoned Cart Requirement';
$_['aci_text_guest_ab_cart_message'] = 'To track abandoned cart for guest users <a>*Velocity Auto-subcribe plugin</a> is required.';
$_['aci_text_filter'] = 'Filter';
$_['aci_text_days'] = 'Days';
$_['aci_text_months'] = 'Months';
$_['aci_text_years'] = 'Years';
$_['aci_text_start_date'] = 'Start Date';
$_['aci_text_end_date'] = 'End Date';
$_['aci_text_group_by'] = 'Group By';
$_['aci_text_carts_count_vs_days'] = 'Carts Count Vs Days';
$_['aci_text_converted_orders'] = 'Converted Orders';
$_['aci_text_status'] = 'Status';
$_['aci_text_order_id'] = 'Order ID';
//$_[''] = '';

$_['error_permission'] = 'Warning: You do not have permission to modify module Abandoned Cart!';
$_['text_success']     = 'Success: You have modified the module Abandoned Cart!';

$_['aci_email_default_subject'] = 'You have items left in your shopping cart!';

$_['aci_email_default_content'] = '<table style="width:100%">
                                        <tbody>
                                                <tr>
                                                        <td align="center">
                                                        <table style="width:650px;margin:0 auto;border:1px solid #f0f0f0;padding:10px;line-height:1.8">
                                                                <tbody>
                                                                        <tr>
                                                                                <td>
                                                                                <p>Hello <strong>{firstname} {lastname}</strong>,</p>

                                                                                <p>We are getting in touch since we saw that you had the following product(s) left in your shopping cart but for some reason you did not complete the order:</p>
                                                                                {cart_content}

                                                                                <p>We do not know why you decided not to purchase this time. We are hoping you have not faced any hiccups during the process.</p>
										<p>We are offering you a special discount code - <strong>{discount_code}</strong> - which gives you <strong>{discount_value} OFF</strong>. The code applies after you spent <strong>${total_amount}</strong>. This promotion is just for you and expires on <strong>{date_end}</strong>.</p>

                                                                                <p>Kind Regards,</p>

                                                                                <p>YourStore<br />
                                                                                <a href="http://www.example.com" target="_blank">http://www.example.com</a></p>
                                                                                </td>
                                                                        </tr>
                                                                </tbody>
                                                        </table>
                                                        </td>
                                                </tr>
                                        </tbody>
                                    </table>';

