<?php
// Heading
$_['heading_title']                = 'Facebook Store Pro';

// Tab 
$_['tab_setting']                  = 'General Setting';
$_['tab_option']                   = 'Options';
$_['tab_image']                    = 'Image';
$_['tab_help']                     = 'Help';

// Text
$_['text_module']                  = 'Modules';
$_['text_success']                 = 'Success: You have modified module Facebook Store Pro!';
$_['text_edit']                    = 'Edit Facebook Store Pro';
$_['text_requirements']            = 'Before defining extension settings <b>enable SSL option</b> (System > Settings > Your Store > tab Server set option "USE SSL" to YES).<br />Also check <b>config.php</b> and <b>admin/config.php</b> to be sure that HTTPS_SERVER, HTTPS_CATALOG link looks like <b>https://</b> (NOT http://)';

// Entry
$_['entry_app_id']                 = 'Facebook APP ID';
$_['entry_logo']                   = 'Logo (810 X 200)';
$_['entry_template']               = 'Template';
$_['entry_redirect_checkout']  	   = 'Redirect checkout to main site';
$_['entry_code']                   = 'Generated code';

// Help
$_['help_app_id']                  = 'Please check help guide to find instructions about how to create an FB APP and where to find APP ID';
$_['help_logo']                    = 'Recommended logo size: 810 x 150 pixels';
$_['help_redirect_checkout']       = 'Disabled = checkout inside facebook store<br />Enabled = redirect checkout process to main site';
$_['help_code']                    = 'Copy this code and paste it in Facebook Static HTML (see help guide for details)';

// Error
$_['error_permission']    	       = 'Warning: You do not have permission to modify module facebook store!';
$_['error_app_id']                 = 'Facebook App ID - required!';
?>