<?php
// Heading
$_['heading_title']    = 'Facebook Store - Banner';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Facebook Store - Banner module!';
$_['text_edit']        = 'Edit Banner Module';
$_['text_seconds']     = 'seconds';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_status']     = 'Status';
$_['entry_banner']     = 'Banner';
$_['entry_dimension']  = 'Dimension (W x H) and Resize Type';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_slide_time'] = 'Slide Time';

// Help
$_['help_width']       = 'Recomended size:<br />- column left: 160<br/>- content top/bottom: 585';
$_['help_slide_time']  = 'Seconds between slides (recommended: 5)';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Facebook Store - Banner module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';
$_['error_slide_time'] = 'Slide time - required!';