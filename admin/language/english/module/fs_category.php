<?php
// Heading
$_['heading_title']    = 'Facebook Store - Category';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Facebook Store Category module!';
$_['text_edit']        = 'Edit Facebook Store Category Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Facebook Store - Category module!';