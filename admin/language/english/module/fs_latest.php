<?php
// Heading
$_['heading_title']      = 'Facebook Store - Latest';

// Text
$_['text_module']        = 'Modules';
$_['text_success']       = 'Success: You have modified Facebook Store - Latest module!';
$_['text_edit']          = 'Edit Facebook Store - Latest Module';
$_['text_seconds']       = 'seconds';
$_['text_grid']          = 'Grid';
$_['text_list']          = 'List';

// Entry
$_['entry_name']         = 'Module Name';
$_['entry_title']        = 'Title';
$_['entry_limit']        = 'Limit';
$_['entry_image']        = 'Image (W x H) and Resize Type';
$_['entry_width']        = 'Image Width';
$_['entry_height']       = 'Image Height';
$_['entry_display_type'] = 'Display Type';
$_['entry_random_start'] = 'Random Start';
$_['entry_auto_slider']  = 'Auto Slider';
$_['entry_slide_time']   = 'Slide Time';
$_['entry_status']       = 'Status';

// Help
$_['help_title']         = 'Heading Title displayed on front store';
$_['help_width']         = 'Depending on position where you want to display module, we recommend:<br />- for content top/bottom: 180<br />- for column left/right: 160';
$_['help_height']        = 'Depending on position where you want to display module, we recommend:<br />- for content top/bottom: 180<br />- for column left/right: 160';
$_['help_random_start']  = 'Shuffle products (position) everytime when page is loaded';
$_['help_auto_slider']   = 'Auto go to next slide';
$_['help_slide_time']    = 'Seconds between slides (recommended: 5)';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Facebook Store - Latest module!';
$_['error_name']         = 'Module Name must be between 3 and 64 characters!';
$_['error_title']        = 'Title - required!';
$_['error_width']        = 'Width required!';
$_['error_height']       = 'Height required!';
$_['error_slide_time']   = 'Slide time - required!';