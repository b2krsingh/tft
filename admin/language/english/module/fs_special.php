<?php
// Heading
$_['heading_title']      = 'Facebook Store - Special Offers';

// Text
$_['text_module']        = 'Modules';
$_['text_success']       = 'Success: You have modified Facebook Store - Special Offers module!';
$_['text_edit']          = 'Edit Facebook Store - Special Offers Module';
$_['text_seconds']       = 'seconds';
$_['text_grid']          = 'Grid';
$_['text_list']          = 'List';

// Entry
$_['entry_name']         = 'Module Name';
$_['entry_title']        = 'Title';
$_['entry_days']         = '\'Days\' text';
$_['entry_hours']        = '\'Hours\' text';
$_['entry_minutes']      = '\'Minutes\' text';
$_['entry_seconds']      = '\'Seconds\' text';
$_['entry_limit']        = 'Limit';
$_['entry_image']        = 'Image (W x H) and Resize Type';
$_['entry_width']        = 'Image Width';
$_['entry_height']       = 'Image Height';
$_['entry_random_start'] = 'Random Start';
$_['entry_auto_slider']  = 'Auto Slider';
$_['entry_slide_time']   = 'Slide Time';
$_['entry_status']       = 'Status';

// Help
$_['help_title']         = 'Heading Title displayed on front store';
$_['help_days']          = 'Translate word \'days\' to current language';
$_['help_hours']         = 'Translate word \'hours\' to current language';
$_['help_minutes']       = 'Translate word \'minutes\' to current language';
$_['help_seconds']       = 'Translate word \'seconds\' to current language';
$_['help_width']         = 'Depending on position where you want to display module, we recommend:<br />- for content top/bottom: 180<br />- for column left/right: 160';
$_['help_height']        = 'Depending on position where you want to display module, we recommend:<br />- for content top/bottom: 180<br />- for column left/right: 160';
$_['help_random_start']  = 'Shuffle products (position) everytime when page is loaded';
$_['help_auto_slider']   = 'Auto go to next slide';
$_['help_slide_time']    = 'Seconds between slides (recommended: 5)';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Facebook Store - Special Offers module!';
$_['error_name']         = 'Module Name must be between 3 and 64 characters!';
$_['error_title']        = 'Title - required!';
$_['error_days']         = 'Days - required!';
$_['error_hours']        = 'Hours - required!';
$_['error_minutes']      = 'Minutes - required!';
$_['error_seconds']      = 'Seconds - required!';
$_['error_width']        = 'Width required!';
$_['error_height']       = 'Height required!';
$_['error_slide_time']   = 'Slide time - required!';