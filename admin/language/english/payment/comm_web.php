<?php
// Heading
$_['heading_title']					= 'Comm Web';

// Text
$_['text_success']					= 'Success: You have modified account details!';
$_['text_edit']                     = 'Edit Commweb';
$_['text_comm_web']				= ' ';
$_['text_authorization']			= 'Authorization';
$_['text_sale']						= 'Sale';

// Entry
$_['entry_vendor']					= 'Vendor';
$_['entry_user']					= 'User';
$_['entry_password']				= 'Password';
$_['entry_partner']					= 'Partner';
$_['entry_test']					= 'Test Mode';
$_['entry_transaction']				= 'Transaction Method';
$_['entry_total']					= 'Total';
$_['entry_order_status']			= 'Order Status';
$_['entry_geo_zone']				= 'Geo Zone';
$_['entry_status']					= 'Status';
$_['entry_sort_order']				= 'Sort Order';

// Help
$_['help_vendor']					= 'Your merchant login ID that you created when you registered for the Website Payments Pro account';
$_['help_user']						= 'If you set up one or more additional users on the account, this value is the ID of the user authorised to process transactions. If, however, you have not set up additional users on the account, USER has the same value as VENDOR';
$_['help_password']					= 'The 6 to 32 character password that you defined while registering for the account';
$_['help_partner']					= ' ';
$_['help_test']						= 'Use the live or testing (sandbox) gateway server to process transactions?';
$_['help_total']					= 'The checkout total the order must reach before this payment method becomes active';

// Error
$_['error_permission']				= 'Warning: You do not have permission to modify    !';
$_['error_vendor']					= 'Vendor Required!';
$_['error_user']					= 'User Required!';
$_['error_password']				= 'Password Required!';
$_['error_partner']					= 'Partner Required!';