<?php
// Heading
$_['heading_title']      = 'CommWeb Virtual Payment';
$_['text_edit']                     = 'Edit CommWeb Virtual Payment';
// Text 
$_['text_payment']       = 'Payment';
$_['text_success']       = 'Success: You have modified CommWeb Virtual Payment Checkout account details!';
$_['text_commweb']        = '<a href="http://www.commbank.com.au/" target="_blank"><img src="view/image/payment/commweb.png" alt="CommWeb Virtual Payment" title="CommWeb Virtual Payment" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_transaction_method'] = 'Pay';
$_['value_transaction_method']          = '0';

// Entry
$_['entry_merchant_id']     = 'Merchant ID:';
$_['entry_access_code']     = 'Access Code:';
$_['entry_secure_hash']     = 'Secure Hash Secret:';
$_['entry_test']         = 'Test Mode:<br /><span class="help">Use the live or testing gateway server to process transactions?</span>';
$_['entry_transaction_method']  = 'Transaction Method:';
$_['entry_total']        = 'Total:<br /><span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_order_status'] = 'Order Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment  CommWeb Virtual Payment!';
$_['error_merchant_id']     = 'Merchant ID Required!';
$_['error_access_code']     = 'Access Code Required!';
$_['error_secure_hash']    = 'Secure Hash Secret Required!';
?>