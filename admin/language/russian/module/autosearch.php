<?php
####################################################
#    AutoSearch 1.11 for Opencart 2.x by AlexDW    #
####################################################

$_['heading_title']    = '<i class="fa fa-cubes fa-lg" style="color: #5BC0DE"></i> AutoSearch 2X - Быстрый поиск с автозаполнением [2000-2200]';

// Text
$_['text_module']      = 'Модули';
$_['text_edit']        = 'Настройки модуля AutoSearch';
$_['text_search']      = 'Поиск по';
$_['text_show']        = 'Показывать';
$_['text_sort_date']   = 'по дате поступления';
$_['text_sort_name']   = 'по имени';
$_['text_viewall']     = 'показать все результаты';
$_['text_code_variant1']   = 'по умолчанию';
$_['text_code_variant2']   = 'вариант 1';
$_['text_code_variant3']   = 'вариант 2';
$_['text_success']     = 'Изменения модуля AutoSearch сохранены';

// Entry
$_['entry_show']          = 'Изображение';
$_['entry_show_model']    = 'Модель';
$_['entry_show_price']    = 'Цену';
$_['entry_show_quantity'] = 'Количество / статус';
$_['entry_limit']         = 'Лимит результатов:';
$_['entry_symbol']        = 'Символов для начала поиска:';
$_['entry_status']        = 'Статус модуля:';
$_['entry_tag']   	  = 'Tag';
$_['entry_model']  	  = 'Model';
$_['entry_sku']   	  = 'SKU';
$_['entry_upc']   	  = 'UPC';
$_['entry_ean']   	  = 'EAN';
$_['entry_jan']   	  = 'JAN';
$_['entry_isbn']   	  = 'ISBN';
$_['entry_mpn']   	  = 'MPN';
$_['entry_location']  = 'Location';
$_['entry_attr']  	  = 'Атрибуты';
$_['entry_sort']   	  = 'Сортировать результаты:';
$_['entry_codepage']   	  = 'Вариант кодировки:';
$_['entry_viewall']   	  = 'Показать все результаты:';
$_['entry_asr_image'] 	  = 'Размер изображения';

// Error
$_['error_permission'] = 'Внимание: У вас нет прав на изменение модуля AutoSearch!';
?>