<?php

/*
 * ***********************************************************************************
 * Abandoned Cart Module -                                                          **
 * Provide Coupons to the customers whose carts left abandoned.                     **
 * By Velocity Software Solutions Pvt. Ltd.                                         **
 * ***********************************************************************************
 */

class ModelModuleAbandonedCartIncentive extends Model
{

    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "abandonedcartincentive` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`date_created` DATETIME NULL DEFAULT NULL,
			`date_modified` DATETIME NULL DEFAULT NULL,
			`cart` TEXT NULL DEFAULT NULL,
			`ip` VARCHAR(100) NULL DEFAULT NULL, 
			`store_id` TINYINT NOT NULL DEFAULT 0,
			`restore_id` TEXT NULL DEFAULT NULL,
			`isOrderCompleted` INT(11) NULL DEFAULT 0,
			`isConvertedOrder` INT(11) NULL DEFAULT 0,
			`order_id` INT(11) NULL DEFAULT 0,
			`isRegistered` INT(11) NULL DEFAULT 0,
			`session_id` TEXT NULL DEFAULT NULL,
			`customer_info` TEXT NULL DEFAULT NULL,
			`cartTotal` VARCHAR(100) NULL DEFAULT NULL,
                        `autoEmail` INT(11) NULL DEFAULT 0,
			 PRIMARY KEY (`id`))");
    }

    public function dropTable()
    {
//$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "abandonedcartincentive`");
    }

    public function getAbandonedCarts($delay, $page = 1, $limit = 8, $store_id = 0, $registered = 1, $sort = "id", $order = "DESC")
    {
        $start = 0;
        if ($page)
        {
            $start = ($page - 1) * $limit;
        }
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcartincentive`
		   WHERE `date_modified` < '" . date("Y-m-d H:i:s", strtotime('-' . $delay . ' hours')) . "' AND `store_id`='" . $store_id . "' AND `isRegistered`='" . $registered . "' AND `isOrderCompleted` = '0'
			ORDER BY `id` DESC
			LIMIT " . $start . ", " . $limit);
			

        return $query->rows;
    }

    public function getTotalAbandonedCarts($delay, $store_id = 0, $registered = 1)
    {
        $query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `date_modified` < '" . date("Y-m-d H:i:s", strtotime('-' . $delay . ' hours')) . "' AND `store_id`='" . $store_id . "'AND `isRegistered`='" . $registered . "' AND `isOrderCompleted` = '0'");

        return $query->row['count'];
    }
    public function getTotalAbandonedCartsAmount($delay, $store_id = 0, $registered = 1)
    {
        $cartTotal = 0;
        $query     = $this->db->query("SELECT cartTotal FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `date_modified` < '" . date("Y-m-d H:i:s", strtotime('-' . $delay . ' hours')) . "' AND `store_id`='" . $store_id . "'AND `isRegistered`='" . $registered . "' AND `isOrderCompleted` = '0'");
        foreach ($query->rows as $row)
        {
            $cartTotal = $cartTotal + $row['cartTotal'];
        }
        return $cartTotal;
    }

//    public function getConvertedCarts($store = 0, $registered = 1)
//    {
//        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `store_id` = '" . $store . "' AND `order_id` > '0' AND `isOrderCompleted` = '1' AND `isRegistered` = '" . $registered . "' ORDER BY `id` DESC");
//        return $query->rows;
//    }
    
    public function getConvertedCarts($store = 0)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `store_id` = '" . $store . "' AND `order_id` > '0' AND `isOrderCompleted` = '1' AND `isConvertedOrder` = '1' ORDER BY `id` DESC");
        return $query->rows;
    }
    
    public function getCouponsWithCouponHistory($coupon_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` c1 LEFT JOIN `" . DB_PREFIX . "coupon_history` c2 ON c1.coupon_id = c2.coupon_id WHERE c2.`coupon_id` = '" . $coupon_id . "'");
        if($query->num_rows > 0){
            return true;
        }else{
            return false;
        }
        
    }

    public function getCoupons($couponName)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE `name` = '" . $couponName . "' ORDER BY `date_added` DESC");
        return $query->rows;
    }

    public function getOrderDetails($orderid)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE `order_id` = '" . $orderid . "'");
        return $query->rows;
    }

    public function sendMail($MailData = array())
    {   
        if(version_compare(VERSION, '2.0.2.0', '<')) {
            $mail = new Mail($this->config->get('config_mail'));
            
            $mail->setTo($MailData['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender($this->config->get('config_name'));	
            $mail->setSubject(html_entity_decode($MailData['subject'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($MailData['message']);        
            $mail->send();
        }
        else{
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($MailData['email']);
            $mail->setFrom($this->config->get('config_email'));                
            $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));                
            $mail->setSubject(html_entity_decode($MailData['subject'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($MailData['message']);
            $mail->send();
        }
				
        if ($mail)
            return true;
        else
            return false;
    }

    public function deleteAbandonedCart($cartid)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `id` = '" . $cartid . "'");
    }

    public function getTotalConvertedCartsCount($store = 0)
    {
        $query = $this->db->query("SELECT COUNT(*) as `count`  FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `store_id`='" . $store . "'AND `order_id` > '0' AND `isConvertedOrder`='1' AND `isOrderCompleted` = '1'");

        return $query->row['count'];
    }

   

    public function getTotalConvertedCartsAmount($store = 0, $registered = 1)
    {
        $cartTotal = 0;
        $query     = $this->db->query("SELECT cartTotal FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `store_id` = '" . $store . "' AND `order_id` > '0' AND `isOrderCompleted` = '1' AND `isRegistered` = '" . $registered . "' ORDER BY `id` DESC");
        foreach ($query->rows as $row)
        {
            $cartTotal = $cartTotal + $row['cartTotal'];
        }
        return $cartTotal;
    }

   

    public function distinctCode($coupon)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($coupon) . "'");
        if ($query->num_rows == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function saveCoupon($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "coupon SET name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', discount = '" . (float) $data['discount'] . "', type = '" . $this->db->escape($data['type']) . "', total = '" . (float) $data['total'] . "', logged = '" . (int) $data['logged'] . "', shipping = '" . (int) $data['shipping'] . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "', uses_total = '" . (int) $data['uses_total'] . "', uses_customer = '" . (int) $data['uses_customer'] . "', status = '" . (int) $data['status'] . "', date_added = NOW()");

        $coupon_id = $this->db->getLastId();

        if (isset($data['coupon_product']))
        {
            foreach ($data['coupon_product'] as $product_id)
            {
                $this->db->query("INSERT INTO " . DB_PREFIX . "coupon_product SET coupon_id = '" . (int) $coupon_id . "', product_id = '" . (int) $product_id . "'");
            }
        }

        if (isset($data['coupon_category']))
        {
            foreach ($data['coupon_category'] as $category_id)
            {
                $this->db->query("INSERT INTO " . DB_PREFIX . "coupon_category SET coupon_id = '" . (int) $coupon_id . "', category_id = '" . (int) $category_id . "'");
            }
        }
    }
    
    public function disableCoupon($coupon_id)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "coupon SET status = '0' WHERE coupon_id = '".$coupon_id."'");
        return;
    }

    public function getCartInfo($cartid)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `id`=" . $cartid);

        return $query->row;
    }

    
    // Get Abandoned Orders for Last One Month
    public function getMonthlyAbandonedOrders($delay, $store_id, $start_date= null, $end_date= null)
    {
        //$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `date_modified` < '" . date("Y-m-d H:i:s", strtotime('-' . $delay . ' hours')) . "' AND `date_modified` > '" . date("Y-m-d H:i:s", strtotime('-30 day')) . "' AND `store_id`='" . $store_id . "' AND `order_id` = '0' AND `isOrderCompleted` = '0' GROUP BY `date_modified` ORDER BY `date_modified` DESC");
        if(($start_date == null)&&( $end_date == null)) {
        $query = $this->db->query("SELECT COUNT( date_modified ) datefield_count, LEFT( all_fields, 10 ) date_modified
                                    FROM (
                                    SELECT DATE( date_modified ) date_modified, CONCAT( DATE( date_modified ) ) all_fields
                                    FROM `" . DB_PREFIX . "abandonedcartincentive`
                                    WHERE isOrderCompleted =0
                                    AND store_id = '".$store_id."'
                                    )A
                                    GROUP BY all_fields");
        return $query->rows;
        }
        else {
            $query = $this->db->query("SELECT COUNT( date_modified ) datefield_count, LEFT( all_fields, 10 ) date_modified
                                    FROM (
                                    SELECT DATE( date_modified ) date_modified, CONCAT( DATE( date_modified ) ) all_fields
                                    FROM `" . DB_PREFIX . "abandonedcartincentive`
                                        WHERE `date_modified` >= '". date("Y-m-d H:i:s", strtotime($start_date)) ."' AND `date_modified` <= '". date("Y-m-d H:i:s", strtotime($end_date)) ."' 
                                    AND isOrderCompleted =0
                                    AND store_id = '".$store_id."'
                                    )A
                                    GROUP BY all_fields");
        
            return $query->rows; 
        }
    }
    
    
    
     // Get Converted Orders for Last One Month
    public function getMonthlyConvertedOrders($store_id, $start_date= null, $end_date= null)
    {
        //$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `date_modified` < '" . date("Y-m-d H:i:s", strtotime('-' . $delay . ' hours')) . "' AND `date_modified` > '" . date("Y-m-d H:i:s", strtotime('-30 day')) . "' AND `store_id`='" . $store_id . "' AND `order_id` = '0' AND `isOrderCompleted` = '0' GROUP BY `date_modified` ORDER BY `date_modified` DESC");
        if(($start_date == null)&&( $end_date == null)) {
        $query = $this->db->query("SELECT COUNT( date_modified ) datefield_count, LEFT( all_fields, 10 ) date_modified
                                    FROM (
                                    SELECT DATE(date_modified) date_modified, CONCAT( DATE( date_modified ) ) all_fields
                                    FROM `" . DB_PREFIX . "abandonedcartincentive`
                                    WHERE order_id > 0
                                    AND isOrderCompleted = 1 AND isConvertedOrder = 1
                                    )A
                                    GROUP BY all_fields");
        return $query->rows;
        
        }
        else {
            $query = $this->db->query("SELECT COUNT( date_modified ) datefield_count, LEFT( all_fields, 10 ) date_modified
                                    FROM (
                                    SELECT DATE( date_modified ) date_modified, CONCAT( DATE( date_modified ) ) all_fields
                                    FROM `" . DB_PREFIX . "abandonedcartincentive`
                                    WHERE `date_modified` >= '". date("Y-m-d H:i:s", strtotime($start_date)) ."' AND `date_modified` <= '". date("Y-m-d H:i:s", strtotime($end_date)) ."' AND order_id > 0
                                    AND isOrderCompleted = 1 AND isConvertedOrder = 1
                                    )A
                                    GROUP BY all_fields");
        return $query->rows;
        }
    }





// Get Abandoned Carts for Last One Month
		public function getMonthlyAbandonedCart($delay, $store_id, $start_date = null, $end_date= null)
		{
			if(($start_date == null)&&( $end_date == null)) {
			    $query = $this->db->query("SELECT SUM(amount ) cartTotal, COUNT(date_modified) datefield_count, LEFT(all_fields, 10) date_modified
			                            FROM (SELECT (cartTotal) amount, DATE(date_modified) date_modified, CONCAT(DATE(date_modified)) all_fields
			                            FROM `" . DB_PREFIX . "abandonedcartincentive`
			                            WHERE isOrderCompleted = 0 AND store_id = '".$store_id."') A
			                            GROUP BY all_fields");			
			    return $query->rows;            
			} else {
			    $query = $this->db->query("SELECT SUM(amount) cartTotal, COUNT(date_modified) datefield_count, LEFT(all_fields, 10) date_modified
			                            FROM (SELECT (cartTotal) amount, DATE(date_modified) date_modified, CONCAT(DATE(date_modified)) all_fields
		                                FROM `" . DB_PREFIX . "abandonedcartincentive` 
										WHERE `date_modified` >= '". date("Y-m-d H:i:s", strtotime($start_date)) ."' AND `date_modified` <= '". date("Y-m-d H:i:s", strtotime($end_date)) ."' AND `isOrderCompleted` = 0 AND store_id = '".$store_id."') A
			                            GROUP BY all_fields");			
			    return $query->rows; 
			}
		}									



 	// Get Converted Carts for Last One Month
    public function getMonthlyConvertedCart($store_id, $start_date= null, $end_date= null)
    {
        if(($start_date == null)&&( $end_date == null)) {
		    $query = $this->db->query("SELECT SUM(amount) cartTotal, COUNT(date_modified) datefield_count, LEFT(all_fields, 10) date_modified
		                                FROM (
		                                SELECT (cartTotal)amount, DATE(date_modified) date_modified, CONCAT(DATE(date_modified)) all_fields
		                                FROM `" . DB_PREFIX . "abandonedcartincentive`
		                                WHERE order_id > 0
		                                AND isOrderCompleted = 1 AND isConvertedOrder = 1 AND store_id = '".$store_id."') A
		                                GROUP BY all_fields");
		    return $query->rows;
        } else {				
            $query = $this->db->query("SELECT SUM(amount) cartTotal, COUNT(date_modified) datefield_count, LEFT( all_fields, 10 ) date_modified
                                    FROM (
                                    SELECT (cartTotal)amount, DATE(date_modified) date_modified, CONCAT( DATE( date_modified ) ) all_fields
                                    FROM `" . DB_PREFIX . "abandonedcartincentive`
                                    WHERE `date_modified` >= '". date("Y-m-d H:i:s", strtotime($start_date)) ."' AND `date_modified` <= '". date("Y-m-d H:i:s", strtotime($end_date)) ."' AND order_id > 0 AND isOrderCompleted = 1 AND isConvertedOrder = 1 AND store_id = '".$store_id."') A
                                    GROUP BY all_fields");
             return $query->rows;
        }
    }
    }
