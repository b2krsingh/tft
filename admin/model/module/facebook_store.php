<?php
class ModelModuleFacebookStore extends Model {
	public function createFacebookStoreLayout(){
	
		$store_id = (int)$this->config->get('config_store_id');
		
		$fs_layouts = array(
					array(
						'name'   =>  'Facebook Store - Home',
						array(
							'store_id'  => $store_id,
							'route'     => 'facebook_store/home'
						)
					),
					array(
						'name'   =>  'Facebook Store - Product',
						array(
							'store_id'  => $store_id,
							'route'     => 'facebook_store/product'
						)
					),
					array(
						'name'   =>  'Facebook Store - Search',
						array(
							'store_id'  => $store_id,
							'route'     => 'facebook_store/search'
						)
					),
					array(
						'name'   =>  'Facebook Store - Category',
						array(
							'store_id'  => $store_id,
							'route'     => 'facebook_store/category'
						)
					)
		);
	
	
		foreach($fs_layouts as $fs_layout) {
			if ( $this->getLayoutByRoute($fs_layout[0]['route']) == 0 ) {  // check if route already exist
				$this->load->model('design/layout');
				
				$layout_route = array();
				$layout_route[0] =  array (
						'store_id' => $store_id,
						'route'    => $fs_layout[0]['route']
					);
				
				$data = array(
					'name'         => $fs_layout['name'],
					'layout_route' => $layout_route
				);
				
				$this->model_design_layout->addLayout($data);
			}
		}	
	}
	
	public function getLayoutByRoute($route){
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "layout_route WHERE route='" . $route . "'";
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
}
?>