$(document).ready(function() {
            
            setTimeout(function () { changeActiveTab(); }, 2000);
            
            $('#date-start').datepicker({ dateFormat: 'dd-mm-yy' });
	
            $('#date-end').datepicker({ dateFormat: 'dd-mm-yy' });
            
            var tableactive = '<?php echo $tableactive; ?>';
            if (tableactive === 'abregistered')
            {
                $('#aci_tab_general_settings').removeClass('active');
                $('#aci_tab_abandoned_cart').addClass('active');

                $('#aci_tab_general_settings-li').removeClass('active');
                $('#aci_tab_abandoned_cart-li').addClass('active');
            }
            if (tableactive === 'abguest')
            {
                $('#aci_tab_general_settings').removeClass('active');
                $('#aci_tab_abandoned_cart').addClass('active');

                $('#aci_tab_general_settings-li').removeClass('active');
                $('#aci_tab_abandoned_cart-li').addClass('active');

                $('#tab-abandoned-cart-registered').removeClass('active');
                $('#tab-abandoned-cart-guest').addClass('active');

                $('#tab-abandoned-cart-registered-li').removeClass('active');
                $('#tab-abandoned-cart-guest-li').addClass('active');
            }
            if (tableactive === 'convertedguest')
            {
                $('#aci_tab_general_settings').removeClass('active');
                $('#aci_tab_conversion_rate').addClass('active');

                $('#aci_tab_general_settings-li').removeClass('active');
                $('#aci_tab_conversion_rate-li').addClass('active');

                $('#tab-conversion-rate-registered').removeClass('active');
                $('#tab-conversion-rate-guest').addClass('active');

                $('#tab-conversion-rate-registered-li').removeClass('active');
                $('#tab-conversion-rate-guest-li').addClass('active');
            }
            if (tableactive === 'convertedregistered')
            {
                $('#aci_tab_general_settings').removeClass('active');
                $('#aci_tab_conversion_rate').addClass('active');

                $('#aci_tab_general_settings-li').removeClass('active');
                $('#aci_tab_conversion_rate-li').addClass('active');
            }
            $('#slide1_controls').on('click', function() {
                $('.supercheckout-sidebarMenu').removeAttr('style');
                $('#slide1_controls').hide();
                $('#slide2_controls').show();
            });
            $('#slide2_controls').on('click', function() {
                $('.supercheckout-sidebarMenu').attr('style', 'right:0px;');
                $('#slide1_controls').show();

                $('#slide2_controls').hide();
            });
            $("#btnExportExcelGuest").click(function() {
                $("#tblGuestConvertedCustomers").btechco_excelexport({
                    containerid: "tblGuestConvertedCustomers"
                    , datatype: $datatype.Table
                });
            });
            $("#btnExportExcelRegistered").click(function() {
                $("#tblRegisteredConvertedCustomers").btechco_excelexport({
                    containerid: "tblRegisteredConvertedCustomers"
                    , datatype: $datatype.Table
                });
            });
            $("#aci_incentive_copy_registered").change(function() {
                if(this.checked)
                {
                   save();
                   get_data();
                }
            });
            $(function()
            {
                if ($('.dynamicTable').size() > 0)
                {
                    $('.dynamicTable').dataTable({
                        "aoColumnDefs": [
                                            { 'bSortable': false, 'aTargets': [ 0, 3, 4, 6 ] }
                                        ],
                        "sPaginationType": "bootstrap",
                        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                        "oLanguage": {
                                "sLengthMenu": "_MENU_ records per page"
                        }
                    });
                }
            });

        });
        var primaryColor = '#496CAD',
                dangerColor = '#bd362f',
                successColor = '#609450',
                warningColor = '#ab7a4b',
                inverseColor = '#45484d';
        var themerPrimaryColor = primaryColor;
        function get_data() {
             $.ajax({
                type: "POST",
                url: '<?php echo $action_copy_registered; ?>',
                beforeSend: function() {
                    $('#form').fadeTo('slow', 0.4);
                },
                complete: function() {
                    $('#form').fadeTo('slow', 1);
                },
                success: function(response) {
                    location.reload();
                }
            });
        }
        function save_modal() {             
            //var errors = '<?php echo $incentive_table_error;?>';
            //if(!errors){                
            $.ajax({
                type: "POST",
                url: $('#form').attr('action') + '&save',
                data: $('#form').serialize(),
                beforeSend: function() {
                    $('#form').fadeTo('slow', 0.4);
                },
                complete: function() {
                    $('#form').fadeTo('slow', 1);
                },
                success: function(response) {
                    $('.gritter-add-primary').trigger('click');
                    $('#button_close_modal').trigger('click');
                }
            });
            
        }
        function form_save(){
            //var errors = '<?php echo $incentive_table_error;?>';
            //var errors = $("#incentive_tab_error").val();
            //if(errors == '0'){
                $('#form').submit();
           
        }
        function save() {            
            //var errors = '<?php if(isset($incentive_table_error)){ echo $incentive_table_error; } else{ echo "false"; }?>';
            //if(!errors){
                $.ajax({
                type: "POST",
                url: $('#form').attr('action') + '&save',
                data: $('#form').serialize(),
                beforeSend: function() {
                    $('#form').fadeTo('slow', 0.4);
                },
                complete: function() {
                    $('#form').fadeTo('slow', 1);
                },
                success: function(response) {
                    $('.gritter-add-primary').trigger('click');
                }
            });
        //}
        //else{
          //      html = '<div class="alert alert-error" style="display:block; margin:10px 0px 10px 0px; padding:10px !important;">';
          //      html +=   '<button class="close" data-dismiss="alert" type="button">&times;</button>';
          //      html +=   '<strong>Error!&nbsp;&nbsp;</strong>';
          //      html +=   '<span style="font-size:14px;">There are some invalid values on Incentive Settings Tab.</span>';
          //      html +='</div>';
          //      $('#form').before(html);
           // }
        }
        function autosave(locate) {
            
            $.ajax({
                type: "POST",
                url: $('#form').attr('action') + '&save',
                data: $('#form').serialize(),
                beforeSend: function() {
                    $('#form').fadeTo('slow', 0.9);
                },
                complete: function() {
                    $('#form').fadeTo('slow', 1);
                },
                success: function(response) {
                    if (locate !== "") {
                        location = locate;
                    }
                }
            });
        }
        var data1= [];
        var data2= [];
        var data3= [];
        var data4= [];
        function generateGraph() {
             $.ajax({
                type: "POST",
                url:'<?php echo $action_generate_graph; ?>',
                data: $('#form').serialize(),
                dataType: 'json',
                beforeSend: function() {
                    $('#form').fadeTo('slow', 0.9);
                },
                complete: function() {
                    $('#form').fadeTo('slow', 1);   
                                    
                },
                success: function(response) {                     
                    //updateGraph();
                    $(response.monthly_ab_orders).each(function(k,v) {
                      var date = (v.date_modified).split("-");
                      if (!v.abandoned_count) {
                       data1.push([gd(date[2], date[1],date[0]), v.abandoned_count]);}
                    });
                    
                    $(response.monthly_ab_cart).each(function(k,v) {
                      var date = (v.date_modified).split("-");
                      if (!v.abandoned_amount) {
                       data2.push([gd(date[2], date[1],date[0]), v.abandoned_amount]);}
                    });
                    
                    $(response.monthly_converted_orders).each(function(k,v) {
                      var date = (v.date_modified).split("-");
                      if (!v.abandoned_count) {
                       //data3.push([gd(date[2], date[1],date[0]), v.abandoned_count]);
                        }
                    });
                    
                    $(response.monthly_converted_cart).each(function(k,v) {
                      var date = (v.date_modified).split("-");
                      if (!v.abandoned_amount) {
                         //data4.push([gd(date[2], date[1],date[0]), v.abandoned_amount]);
                    }
                      
                    });
                    
                     //alert(d1);
                    //alert(d2);
                    // alert(d3);
                    // alert(d4);
                    
                    updateGraph();
                      
                 }
            });              
        }
        
        function updateGraph()
        {                                    
                var charts  = 
                    {
                        initCharts: function()
                        {
                            // init chart_carts_count_vs_days
                            this.chart_carts_count_vs_days.init();
                        },
                        // utility class
                        utility:
                        {
                                chartColors: [ themerPrimaryColor, "#444", "#777", "#999", "#DDD", "#EEE" ],
                                chartBackgroundColors: ["#fff", "#fff"],

                                applyStyle: function(that)
                                {
                                        that.options.colors = charts.utility.chartColors;
                                        that.options.grid.backgroundColor = { colors: charts.utility.chartBackgroundColors };
                                        that.options.grid.borderColor = charts.utility.chartColors[0];
                                        that.options.grid.color = charts.utility.chartColors[0];
                                },

                                // generate random number for charts
                                randNum: function()
                                {
                                        return (Math.floor( Math.random()* (1+40-20) ) ) + 20;
                                }
                        },
                        chart_carts_count_vs_days:
                        {
                                // chart data
                                data: null,

                                // will hold the chart object
                                plot: null,

                                // chart options
                                options:
                                {

                                        grid: {
                                            show: true,
                                            aboveData: false,
                                            color: "#3f3f3f" ,
                                            labelMargin: 5,
                                            axisMargin: 0, 
                                            borderWidth: 0,
                                            borderColor:null,
                                            minBorderMargin: 5 ,
                                            clickable: true, 
                                            hoverable: true,
                                            autoHighlight: true,
                                            mouseActiveRadius: 20,
                                            backgroundColor : { }
                                        },
                                series: {
                                        grow: { active:false },
                                },
                                legend: { 
                                    noColumns: 0,
                                    labelBoxBorderColor: "#000000",
                                    position: "ne" 
                                },
                                colors: [],
                                shadowSize:1,
                                tooltip: true,
                                        tooltipOpts: {
                                                content: "%s : %x : %y1",
                                                dateFormat: "%m-%d-%y",
                                                shifts: {
                                                        x: -30,
                                                        y: -50
                                                },
                                                defaultTheme: false
                                        },
                                        xaxes: [
                                                {   
                                                    mode: "time",
                                                    timeformat: "%m/%d/%Y",
                                                    tickSize:[1,"day"],
                                                    position: 'bottom', 
                                                    axisLabel: "Date", 
                                                    axisLabelUseCanvas:true,
                                                    axisLabelFontSizePixels: 12,
                                                    axisLabelFontFamily: 'Verdana, Arial',
                                                    axisLabelPadding: 20
                                                }
                                        ],
                                        yaxes: [
                                                { 
                                                    position: 'left',
                                                    axisLabel: "Carts Count",
                                                    axisLabelUseCanvas:true ,
                                                    axisLabelFontSizePixels: 12,
                                                    axisLabelFontFamily: 'Verdana, Arial',
                                                    axisLabelPadding: 25
                                                },
                                                { 
                                                    position: 'right', 
                                                    axisLabel: "Amount", 
                                                    axisLabelUseCanvas:true ,
                                                    axisLabelFontSizePixels: 12,
                                                    axisLabelFontFamily: 'Verdana, Arial',
                                                    axisLabelPadding: 25

                                                }
                                        ]

                                },

                                // initialize
                                init: function()
                                {
                                        // apply styling
                                        charts.utility.applyStyle(this);

                                        //some data
                                                                     

                                    var ds = new Array();

                                    ds.push({
                                        label: "Abandoned Carts",
                                        data:data1,
                                        bars: 
                                        {
                                            show:true,
                                            barWidth: 0.5,
                                            fill:1,
                                            order: 1,
                                            lineWidth: 5
                                        },
                                        yaxis: 1
                                    });
                                    ds.push({
                                        label: "Converted Carts",
                                        data:data2,
                                        bars: 
                                        {
                                            show:true,
                                            barWidth: 0.5,
                                            fill: 1,
                                            order: 2,
                                            lineWidth: 5
                                        },
                                        yaxis: 1
                                    });
                                    
                                    //ds.push({
                                    //    label: "Abandoned Amount", 
                                    //    data: data3,                      
                                    //    lines: { show:true,  fill: false, lineWidth: 4, steps: false, fillColor:"#fff8f2" },
                                    //    points: { show:true, radius: 5, symbol: "circle", fill: true, borderColor: "#fff",fillColor: "#fff" },
                                    //    yaxis: 2
                                    //});
                                    //ds.push({
                                    //   label: "Converted Amount", 
                                    //    data: data4,                     
                                    //    lines: { show:true, fill: false, lineWidth: 4, steps: false,fillColor:"rgba(0,0,0,0.1)" },
                                    //    points: { show:true, radius: 5, symbol: "circle", fill: true, borderColor: "#fff",fillColor: "#fff" },                        
                                    //    yaxis: 2
                                    //});

                                        this.data = ds;

                                        this.plot = $.plot($("#chart_carts_count_vs_days"), this.data, this.options);
                                }
                        }        
                    };
                if (typeof charts !== 'undefined') {  
                    charts.initCharts();     
                } 
        }
       
       function changeActiveTab(){
            $("#aci_tab_reports").removeClass("active");
            $("#aci_tab_general_settings").addClass("active"); 
            $("#loader_div").removeClass("loader_div");
            $("#loader_div").addClass("blank_div");
    }
// Flot Charts JS Starts
	var d1 = []; 
	var d2 = [];
	var d3 = [];
	var d4 = [];
	var ds = new Array();
    var charts  = 
    {
        initCharts: function()
        {
            // init chart_carts_count_vs_days
            this.chart_carts_count_vs_days.init();
                
            // init chart_total_amount_vs_days
            this.chart_total_amount_vs_days.init();
            
            // init total amount pie chart
            this.chart_total_amount_pie.init();		
		
            // init carts pie chart
            this.chart_carts_pie.init();
        },
        // utility class
	utility:
	{
		chartColors: [ themerPrimaryColor, "#444", "#777", "#999", "#DDD", "#EEE" ],
		chartBackgroundColors: ["#fff", "#fff"],

		applyStyle: function(that)
		{
			that.options.colors = charts.utility.chartColors;
			that.options.grid.backgroundColor = { colors: charts.utility.chartBackgroundColors };
			that.options.grid.borderColor = charts.utility.chartColors[0];
			that.options.grid.color = charts.utility.chartColors[0];
		},
		
		// generate random number for charts
		randNum: function()
		{
			return (Math.floor( Math.random()* (1+40-20) ) ) + 20;
		}
	},
        chart_carts_count_vs_days:
	{
		// chart data
		data: null,

		// will hold the chart object
		plot: null,

                // chart options
		options:
		{
			
			grid: {
                            show: true,
			    aboveData: false,
			    color: "#3f3f3f" ,
			    labelMargin: 5,
			    axisMargin: 0, 
			    borderWidth: 0,
			    borderColor:null,
			    minBorderMargin: 5 ,
			    clickable: true, 
			    hoverable: true,
			    autoHighlight: true,
			    mouseActiveRadius: 20,
			    backgroundColor : { }
			},
	        series: {
	        	grow: { active:false },
	        },
	        legend: { 
                    noColumns: 0,
                    labelBoxBorderColor: "#000000",
                    position: "ne" 
                },
	        colors: [],
	        shadowSize:1,
	        tooltip: true,
			tooltipOpts: {
				content: "%s : %x : %y1",
				dateFormat: "%m-%d-%y",
				shifts: {
					x: -30,
					y: -50
				},
				defaultTheme: false
			},
                        xaxes: [
								{   
                                    mode: "time",
                                    timeformat: "%m/%d/%Y",
                                    tickSize:[1,"day"],
                                    position: 'bottom', 
                                    axisLabel: "Date", 
                                    axisLabelUseCanvas:true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelFontFamily: 'Verdana, Arial',
                                    axisLabelPadding: 20,
  									labelWidth: 30,
									//min: <?php echo $start_date; ?>,
									//max: <?php echo $end_date; ?>
									min: (new Date(2014, 4, 24)).getTime(),
									max: (new Date(2014, 5, 24)).getTime(), 
									reserveSpace: true,
								    alignTicksWithAxis: 1

                                }
			],
			yaxes: [
				{ 
                                    position: 'left',
                                    axisLabel: "Carts Count",
                                    tickSize:1,
                                    axisLabelUseCanvas:true ,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelFontFamily: 'Verdana, Arial',
                                    axisLabelPadding: 25
                                },
				{ 
                                    position: 'right', 
                                    axisLabel: "Amount", 
                                    axisLabelUseCanvas:true ,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelFontFamily: 'Verdana, Arial',
                                    axisLabelPadding: 25
                                    
                                }
			]
                        
		},

		// initialize
		init: function()
		{
			// apply styling
			charts.utility.applyStyle(this);
			
			//some data
                    
                    
                    //for (var i = 0; i <= 10; i += 1) 
                    //{
                    //    d1.push([i, parseInt(Math.random() * 40)]);
                    //    d2.push([i, parseInt(Math.random() * 40)]);
                    //    d3.push([i, parseInt(Math.random() * 4000)]);
                    //    d4.push([i, parseInt(Math.random() * 4000)]);
                    //}
                    
                    <?php if(!empty($monthly_ab_orders)) { foreach($monthly_ab_orders as $ab_cart) { 
                        $date = $ab_cart['date_modified'];
                        $day = date('d' , strtotime($date));
                        $month = date('m' , strtotime($date));
                        $year = date('Y' , strtotime($date)); ?>
                        d1.push([gd(<?php echo $year; ?>, <?php echo $month; ?>,<?php echo $day; ?>),<?php echo $ab_cart['abandoned_count']; ?>]);
                    <?php  } } ?>
                <?php if(!empty($monthly_converted_orders)) {  foreach($monthly_converted_orders as $converted_orders) { 
                        $date = $converted_orders['date_modified'];
                        $day = date('d' , strtotime($date));
                        $month = date('m' , strtotime($date));
                        $year = date('Y' , strtotime($date));
                        ?>
                        d2.push([gd(<?php echo $year; ?>, <?php echo $month; ?>,<?php echo $day; ?>),1]);
                    <?php } } ?>
                
                
                    <?php if(!empty($monthly_ab_cart)) {   foreach($monthly_ab_cart as $ab_cart) { 
                        $date = $ab_cart['date_modified'];
                        $day = date('d' , strtotime($date));
                        $month = date('m' , strtotime($date));
                        $year = date('Y' , strtotime($date)); ?>
                        d3.push([gd(<?php echo $year; ?>, <?php echo $month; ?>,<?php echo $day; ?>),<?php echo $ab_cart['abandoned_amount']; ?>]);
                    <?php  } } ?>
                
                    <?php if(!empty($monthly_converted_cart)) {   foreach($monthly_converted_cart as $converted_cart) { 
                        $date = $converted_cart['date_modified'];
                        $day = date('d' , strtotime($date));
                        $month = date('m' , strtotime($date));
                        $year = date('Y' , strtotime($date));
                        ?>
                        d4.push([gd(<?php echo $year; ?>, <?php echo $month; ?>,<?php echo $day; ?>),<?php echo $converted_cart['converted_amount']; ?>]);
                    <?php } } ?>
                
		    
		 
		    ds.push({
		     	label: "Abandoned Carts",
		        data:d1,
                        bars: 
                        {
                            show:true,
                            barWidth: 0.5,
                            fill:1,
                            order: 1,
                            lineWidth: 5
                        },
                        yaxis: 1
		    });
		    ds.push({
		    	label: "Converted Carts",
		        data:d2,
                        bars: 
                        {
                            show:true,
                            barWidth: 0.5,
                            fill: 1,
                            order: 2,
                            lineWidth: 5
		    	},
                        yaxis: 1
		    });
                    ds.push({
                        label: "Abandoned Amount", 
                        data: d3,                      
                        lines: { show:true,  fill: false, lineWidth: 4, steps: false, fillColor:"#fff8f2" },
                        points: { show:true, radius: 5, symbol: "circle", fill: true, borderColor: "#fff",fillColor: "#fff" },
                        yaxis: 2
                    });
                    
                    ds.push({
                       label: "Converted Amount", 
                        data: d4,                     
                        lines: { show:true, fill: false, lineWidth: 4, steps: false,fillColor:"rgba(0,0,0,0.1)" },
                        points: { show:true, radius: 5, symbol: "circle", fill: true, borderColor: "#fff",fillColor: "#fff" },                        
                        yaxis: 2
                    });
                    
                        this.data = ds;

			//this.plot = $.plot($("#chart_carts_count_vs_days"), this.data, this.options);
		}
	},
        
        
        
        
        // chart_total_amount_vs_days
	chart_total_amount_vs_days:
	{
		// chart data
		data: null,

		// will hold the chart object
		plot: null,

		// chart options
		options:
		{
			bars: {
				show:true,
				barWidth: 0.2,
				fill:1,
                                linewidth:1
			},
			grid: {
				show: true,
			    aboveData: true,
			    color: "#3f3f3f" ,
			    labelMargin: 5,
			    axisMargin: 0, 
			    borderWidth: 0,
			    borderColor:null,
			    minBorderMargin: 5 ,
			    clickable: true, 
			    hoverable: true,
			    autoHighlight: false,
			    mouseActiveRadius: 20,
			    backgroundColor : { }
			},
	        series: {
	        	grow: { active:false },
                        stack: 1
	        },
	        legend: { position: "ne" },
	        colors: [],
	        shadowSize:1,
	        tooltip: true,
			tooltipOpts: {
				content: "%s : %y.0",
				shifts: {
					x: -30,
					y: -50
				},
				defaultTheme: false
			}
		},




		// initialize
		init: function()
		{
			// apply styling
			 charts.utility.applyStyle(this);
			
			//some data
                    var d1 = [];
		    for (var i = 0; i <= 10; i += 1)
		        d1.push([i, parseInt(Math.random() * 30)]);
		 
		    var d2 = [];
		    for (var i = 0; i <= 10; i += 1)
		        d2.push([i, parseInt(Math.random() * 30)]);
		 
		    var ds = new Array();
		 
		    ds.push({
		     	label: "Abandoned Order",
		        data:d1,
		        bars: { order: 1 }
		    });
		    ds.push({
		    	label: "Converted Order",
		        data:d2,
		        bars: { order: 2 }
		    });
			this.data = ds;

			//this.plot = $.plot($("#chart_total_amount_vs_days"), this.data, this.options);
		}
	},
        
        // pie chart
	chart_total_amount_pie:
	{
		// chart data
		data: [
		    { label: "Abandoned Amount",  data: parseInt('<?php if(isset($total_ab_amount)) { echo $total_ab_amount; } ?>') },
		    { label: "Converted Amount",  data: parseInt('<?php if(isset($total_converted_amount)) {  echo $total_converted_amount; } ?>')  }
		],

		// will hold the chart object
		plot: null,

		// chart options
		options: 
		{
			series: {
				pie: { 
					show: true,
					highlight: {
						opacity: 0.1
					},
					radius: 1,
					stroke: {
						color: '#fff',
						width: 2
					},
					startAngle: 2,
				    combine: {
	                    color: '#353535',
	                    threshold: 0.05
	                },
	                label: {
	                    show: true,
	                    radius: 2/4,
	                    formatter: function(label, series){
	                        return '<div class="label label-inverse">'+label+'&nbsp;'+Math.round(series.percent)+'%</div>';
	                    }
	                }
				},
				grow: {	active: false}
			},
			colors: [],
			legend:{ show:true },
			grid: {
	            hoverable: true,
	            clickable: true,
	            backgroundColor : { }
	        },
	        tooltip: true,
			tooltipOpts: {
				content: "%s : %y.1"+"%",
				shifts: {
					x: -30,
					y: -50
				},
				defaultTheme: false
			}
		},
		
		// initialize
		init: function()
		{
			// apply styling
			charts.utility.applyStyle(this);
			
			//this.plot = $.plot($("#chart_total_amount_pie"), this.data, this.options);
		}
	},
	
        // pie chart     
        chart_carts_pie:
	{
            
		// chart data
		data: [
		    { label:"Abandoned Carts",  data: parseInt('<?php if(isset($total_ab_carts)) { echo $total_ab_carts; } ?>') },
		    { label: "Converted Carts",  data: parseInt('<?php if(isset($total_converted_carts)) { echo $total_converted_carts; }?>') }
		],

		// will hold the chart object
		plot: null,

		// chart options
		options: 
		{
			series: {
				pie: { 
					show: true,
					highlight: {
						opacity: 0.1
					},
					radius: 1,
					stroke: {
						color: '#fff',
						width: 2
					},
					startAngle: 2,
				    combine: {
	                    color: '#353535',
	                    threshold: 0.05
	                },
	                label: {
	                    show: true,
	                    radius: 2/4,
	                    formatter: function(label, series){
	                        return '<div class="label label-inverse">'+label+'&nbsp;'+Math.round(series.percent)+'%</div>';
	                    }
	                }
				},
				grow: {	active: false}
			},
			colors: [],
			legend:{ show:true },
			grid: {
	            hoverable: true,
	            clickable: true,
	            backgroundColor : { }
	        },
	        tooltip: true,
			tooltipOpts: {
				content: "%s : %y.1"+"%",
				shifts: {
					x: -30,
					y: -50
				},
				defaultTheme: false
			}
		},
		
		// initialize
		init: function()
		{
			// apply styling
			charts.utility.applyStyle(this);
			//this.plot = $.plot($("#chart_carts_pie"), this.data, this.options);
		}
	}
   
    };
$(function()
{
	// initialize charts
	if (typeof charts !== 'undefined')   
                charts.initCharts();
        
});

function gd(year, month, day) {
    return new Date(year, month - 1, day).getTime();
}
// Flot Charts JS Ends 

