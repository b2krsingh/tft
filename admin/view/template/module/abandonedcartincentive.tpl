<?php 

/*
 * ***********************************************************************************
 * Abandoned Cart Module -                                                          **
 * Provide Coupons to the customers whose carts left abandoned.                     **
                                                                           
 * ***********************************************************************************
 */
?>
<?php echo $header; ?><?php echo $column_left; ?>
<!-- Main Container Fluid Starts -->
<div id="content">
    <div id="velsof_plugin">
        <div  class="container-960 fluid  ">

    <?php $incentive_table_error = false;?>
            <!-- Style Starts -->
            <style>
                #velsof-aci-popup-cart-table 
                {
                    background-color: #FFFFFF !important;
                    border: medium none !important;
                    border-collapse: collapse !important;
                    table-layout: fixed !important;
                    width: 100% !important;
                }
                .velsof-aci-popup-td-image 
                {
                    border-bottom: 1px dotted #C0C0C0 !important;
                    text-align: center !important;
                    background-color: #FFFFFF;
                    overflow: hidden;
                    padding-left: 10px !important;
                    padding-right: 10px !important;
                    text-align: center !important;
                }
                .velsof-aci-popup-td-name 
                {
                    background-color: #FCFCFC !important;
                    border-bottom: 1px dotted #C0C0C0 !important;
                    font-family: arial,tahoma,verdana,sans-serif !important;
                    font-size: 13px !important;
                    padding-left: 10px !important;
                    padding-right: 10px !important;
                    text-decoration: none !important;
                }
                .velsof-aci-popup-td-model 
                {
                    background-color: #F9F9F9 !important;
                    border-bottom: 1px dotted #C0C0C0 !important;
                    font-family: arial,tahoma,verdana,sans-serif !important;
                    font-size: 13px !important;
                    padding-left: 10px !important;
                    padding-right: 10px !important;
                    text-align: center !important;
                }
                .velsof-aci-popup-td-quantity 
                {
                    background-color: #FCFCFC !important;
                    border-bottom: 1px dotted #C0C0C0 !important;
                    font-family: arial,tahoma,verdana,sans-serif !important;
                    font-size: 13px !important;
                    padding-left: 10px !important;
                    padding-right: 10px !important;
                    text-align: center !important;
                }
                .velsof-aci-popup-td-price 
                {
                    background-color: #F9F9F9 !important;
                    border-bottom: 1px dotted #C0C0C0 !important;
                    font-family: arial,tahoma,verdana,sans-serif !important;
                    font-size: 13px !important;
                    font-weight: bold !important;
                    padding-left: 20px !important;
                    padding-right: 20px !important;
                    text-align: center !important;
                }
                .velsof-aci-popup-td-total 
                {
                    background-color: #FCFCFC !important;
                    border-bottom: 1px dotted #C0C0C0 !important;
                    font-family: arial,tahoma,verdana,sans-serif !important;
                    font-size: 17px !important;
                    font-weight: bold !important;
                    padding-left: 10px !important;
                    padding-right: 10px !important;
                    text-align: center !important;
                }
                .velsof-aci-popup-th-image, .velsof-aci-popup-th-name, .velsof-aci-popup-th-model,
                .velsof-aci-popup-th-quantity, .velsof-aci-popup-th-price, .velsof-aci-popup-th-total
                {
                    border-bottom: 1px solid #A9A9A9 !important;
                    font-family: arial,tahoma,verdana,sans-serif !important;
                    font-size: 14px !important;
                    padding: 10px 0 7px !important;
                    text-align: center !important;
                    text-transform: uppercase !important;
                    font-weight: normal !important;
                }
                .velsof-aci-popup-th-image
                {
                    width: 20% !important;
                }
                .velsof-aci-popup-th-name
                {
                    width: 25% !important;
                }
                .velsof-aci-popup-th-model
                {
                    width: 25% !important;
                }
                .velsof-aci-popup-th-quantity
                {
                    width: 8% !important;
                }
                .velsof-aci-popup-th-price
                {
                    width: 12% !important;
                }
                .velsof-aci-popup-th-total
                {
                    width: 15% !important;
                }
                .popup
                {
                    position: absolute;
                    border: 5px solid gray;
                    padding: 10px;
                    background: white;
                    width: 60%;
                    float: right;
                    margin-left: 10px;
                    padding-top: 10px;
                    border: 1px solid #eee;
                    border-radius: 5px;
                    padding-right: 7px;
                    padding-left: 7px;
                    min-height: 200px;
                }
                .list tbody tr:hover td
                {
                    background-color:#FFFFFF !important;
                }
                .glyphicons.customer_info i:before {
                    font-size: 15px !important;
                }

                .customer_info{
                    padding-left:30px !important;
                    top:-7px !important;
                }
                .loader_div{
                    background: url("view/image/aci/loader.gif") no-repeat scroll 50% 10% #ffffff;               
                    height:800px;
                    width:100%;
                    text-align:center;
                }
                .blank_div
                {
                    display: none !important;
                    z-index: -2;
                }

            </style>
            <!-- Style Ends -->
            <!-- Script Starts -->
            <script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
            <link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
            <script type="text/javascript">


                $(document).ready(function() {
                // set timer job for changeActiveTab() 
                //setTimeout(function() { changeActiveTab() }, 2000);

                // show charts
                drawBarChart();
                        drawTotalCartsPie();
                        drawTotalAmountDonut();
                        // show/hide cron instructor on page load
                        var value = $("input:radio[name='aci[general][scheduled_task]']").attr('checked');
                        if (value == 'checked'){
                $("#cron_instructions").show();
                }
                else{
                $("#cron_instructions").hide();
                }





                $("#btnExportExcelGuest").click(function() {
                $("#tblGuestConvertedCustomers").btechco_excelexport({
                containerid: "tblGuestConvertedCustomers"
                        , datatype: $datatype.Table
                });
                });
                        $("#btnExportExcelRegistered").click(function() {
                $("#tblRegisteredConvertedCustomers").btechco_excelexport({
                containerid: "tblRegisteredConvertedCustomers"
                        , datatype: $datatype.Table
                });
                });
                        $("#aci_incentive_copy_registered").change(function() {
                if (this.checked)
                {
                save();
                        get_data();
                }
                });
                        $(function()
                        {
                        if ($('.dynamicTable').size() > 0)
                        {
                        $('.dynamicTable').dataTable({
                        "aoColumnDefs": [
                        { 'bSortable': false, 'aTargets': [ 0, 3, 4, 6 ] }
                        ],
                                "sPaginationType": "bootstrap",
                                "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                                "oLanguage": {
                                "sLengthMenu": "_MENU_ records per page"
                                }
                        });
                        }
                        });
                });
                        var primaryColor = '#496CAD',
                        dangerColor = '#bd362f',
                        successColor = '#609450',
                        warningColor = '#ab7a4b',
                        inverseColor = '#45484d';
                        var themerPrimaryColor = primaryColor;
                        function get_data() {
                        $.ajax({
                        type: "POST",
                                url: '<?php echo $action_copy_registered; ?>',
                                                    beforeSend: function() {
                                                    $('#form').fadeTo('slow', 0.4);
                                                    },
                                                    complete: function() {
                                                    $('#form').fadeTo('slow', 1);
                                                    },
                                                    success: function(response) {
                                                    location.reload();
                                                    }
                                            });
                                            }
                                    function save_modal() {
                                    //var errors = '<?php echo $incentive_table_error;?>';
                                    //if(!errors){                
                                    $.ajax({
                                    type: "POST",
                                            url: $('#form').attr('action') + '&save',
                                            data: $('#form').serialize(),
                                            beforeSend: function() {
                                            $('#form').fadeTo('slow', 0.4);
                                            },
                                            complete: function() {
                                            $('#form').fadeTo('slow', 1);
                                            },
                                            success: function(response) {
                                            $('.gritter-add-primary').trigger('click');
                                                    $('#button_close_modal').trigger('click');
                                            }
                                    });
                                            //}
                                            //else{
                                            //      html = '<div class="alert alert-error" style="display:block; margin:10px 0px 10px 0px; padding:10px !important;">';
                                            //      html +=   '<button class="close" data-dismiss="alert" type="button">&times;</button>';
                                            //      html +=   '<strong>Error!&nbsp;&nbsp;</strong>';
                                            //      html +=   '<span style="font-size:14px;">There are some invalid values on Incentive Settings Tab.</span>';
                                            //      html +='</div>';
                                            //      $('#form').before(html);
                                            // }
                                    }
                                    function form_save(){
                                    //var errors = '<?php echo $incentive_table_error;?>';
                                    //var errors = $("#incentive_tab_error").val();
                                    //if(errors == '0'){
                                    var editor_content = CKEDITOR.instances.emailbody.getData();
                                            $('#emailbody').val(editor_content);
                                            $('#form').submit();
                                            //}
                                            //}
                                            //else{
                                            //      html = '<div class="alert alert-error" style="display:block; margin:10px 0px 10px 0px; padding:10px !important;">';
                                            //      html +=   '<button class="close" data-dismiss="alert" type="button">&times;</button>';
                                            //      html +=   '<strong>Error!&nbsp;&nbsp;</strong>';
                                            //      html +=   '<span style="font-size:14px;">There are some invalid values on Incentive Settings Tab.</span>';
                                            //      html +='</div>';
                                            //      $('#form').before(html);
                                            // }
                                    }
                                    function save() {
                                    //var errors = '<?php if(isset($incentive_table_error)){ echo $incentive_table_error; } else{ echo "false"; }?>';
                                    //if(!errors){
                                    var editor_content = CKEDITOR.instances.emailbody.getData();
                                            $('#emailbody').val(editor_content);
                                            $.ajax({
                                            type: "POST",
                                                    url: $('#form').attr('action') + '&save',
                                                    data: $('#form').serialize(),
                                                    beforeSend: function() {
                                                    $('#form').fadeTo('slow', 0.4);
                                                    },
                                                    complete: function() {
                                                    $('#form').fadeTo('slow', 1);
                                                    },
                                                    success: function(response) {
                                                    $('.gritter-add-primary').trigger('click');
                                                    }
                                            });
                                            //}
                                            //else{
                                            //      html = '<div class="alert alert-error" style="display:block; margin:10px 0px 10px 0px; padding:10px !important;">';
                                            //      html +=   '<button class="close" data-dismiss="alert" type="button">&times;</button>';
                                            //      html +=   '<strong>Error!&nbsp;&nbsp;</strong>';
                                            //      html +=   '<span style="font-size:14px;">There are some invalid values on Incentive Settings Tab.</span>';
                                            //      html +='</div>';
                                            //      $('#form').before(html);
                                            // }
                                    }
                                    function autosave(locate) {

                                    $.ajax({
                                    type: "POST",
                                            url: $('#form').attr('action') + '&save',
                                            data: $('#form').serialize(),
                                            beforeSend: function() {
                                            $('#form').fadeTo('slow', 0.9);
                                            },
                                            complete: function() {
                                            $('#form').fadeTo('slow', 1);
                                            },
                                            success: function(response) {
                                            if (locate !== "") {
                                            location = locate;
                                            }
                                            }
                                    });
                                    }
                                    var data1 = [];
                                            var data2 = [];
                                            var data3 = [];
                                            var data4 = [];
                                            function validateStartEndDate(start_date, end_date)
                                            {
                                            var startDate = Date.parse(start_date);
                                                    var endDate = Date.parse(end_date);
                                                    if (startDate > endDate) {
                                            bootbox.alert("Start date cannot be greater than end date!", function(result)
                                            {

                                            });
                                                    return false;
                                            }
                                            else{
                                            return true;
                                            }
                                            }


                                    function changeActiveTab(){
                                    $("#aci_tab_reports").removeClass("active");
                                            $("#aci_tab_general_settings").addClass("active");
                                            $("#loader_div").removeClass("loader_div");
                                            $("#loader_div").addClass("blank_div");
                                            //changeActiveTabOnPaging();
                                    }

                                    function changeActiveTabOnPaging(){
                                    var tableactive = '<?php echo $tableactive; ?>';
                                                if (tableactive === 'abregistered')
                                        {
                                        $('#aci_tab_general_settings').removeClass('active');
                                                $('#aci_tab_abandoned_cart').addClass('active');
                                                $('#aci_tab_general_settings-li').removeClass('active');
                                                $('#aci_tab_abandoned_cart-li').addClass('active');
                                        }
                                        if (tableactive === 'abguest')
                                        {
                                        $('#aci_tab_general_settings').removeClass('active');
                                                $('#aci_tab_abandoned_cart').addClass('active');
                                                $('#aci_tab_general_settings-li').removeClass('active');
                                                $('#aci_tab_abandoned_cart-li').addClass('active');
                                                $('#tab-abandoned-cart-registered').removeClass('active');
                                                $('#tab-abandoned-cart-guest').addClass('active');
                                                $('#tab-abandoned-cart-registered-li').removeClass('active');
                                                $('#tab-abandoned-cart-guest-li').addClass('active');
                                        }
                                        if (tableactive === 'convertedguest')
                                        {
                                        $('#aci_tab_general_settings').removeClass('active');
                                                $('#aci_tab_conversion_rate').addClass('active');
                                                $('#aci_tab_general_settings-li').removeClass('active');
                                                $('#aci_tab_conversion_rate-li').addClass('active');
                                                $('#tab-conversion-rate-registered').removeClass('active');
                                                $('#tab-conversion-rate-guest').addClass('active');
                                                $('#tab-conversion-rate-registered-li').removeClass('active');
                                                $('#tab-conversion-rate-guest-li').addClass('active');
                                        }
                                        if (tableactive === 'convertedregistered')
                                        {
                                        $('#aci_tab_general_settings').removeClass('active');
                                                $('#aci_tab_conversion_rate').addClass('active');
                                                $('#aci_tab_general_settings-li').removeClass('active');
                                                $('#aci_tab_conversion_rate-li').addClass('active');
                                        }
                                        }



            </script>
            <!-- Script Ends-->
            <!-- Top Navbar Starts -->
            <div style="z-index: 0! important; background-color: #3c8dbc;" class="navbar navbar-static-top skin-blue">
                <!-- Brand Starts -->
                <ul style="margin-top: 15px;" class="pull-left">
                    <li ><a href="" class="appbrand"><span style="margin-left: 20px; color:white;font-weight: 700;"><?php echo $aci_plugin_name; ?><span><?php echo $aci_plugin_version; ?></span></span></a></li>
                </ul>
                <div style="color: white; margin-right:243px; max-width:85%; text-align: center;  floxxat: left;    margssin-left: 350px;    margin-top: 15px;"  class=" layhout">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <?php echo $breadcrumb['separator']; ?><a class="text-center layhout" style="color:white;" href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                    <?php } ?>
                </div>

                <!-- Brand Ends -->
                <!-- Save Button Starts -->
        <?php
        if(!isset($_GET['store_id'])) {
        $_GET['store_id'] = 0;
        }
        ?>
                <div style="float: right;margin-top: -27px;margin-right: -180px;" class="topbuttons"> <?php if(isset($stores)) { ?>
                    <select class="form-control" onChange="location = '<?php echo $route; ?>&store_id=' + $(this).val()">
                <?php  foreach($stores as $store) { ?>
                <?php if($store['store_id'] == $_GET['store_id'] && $_GET['store_id'] != 0) { ?>
                        <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['name']; ?></option>
                <?php }else{ ?>
                        <option value="<?php echo $store['store_id']; ?>" ><?php echo $store['name']; ?></option>
                <?php } ?>
                <?php } ?>
                    </select>
            <?php }?> 
                    <a onClick="save();" class="btn btn-default"><span><?php echo $aci_button_save_and_stay; ?></span></a>&nbsp;&nbsp;&nbsp;
                    <a onClick="form_save();" class="btn btn-default"><span><?php echo $aci_button_save; ?></span></a>&nbsp;&nbsp;&nbsp;
                    <a onClick="location = '<?php echo $cancel; ?>';" class="btn btn-default"><span><?php echo $aci_button_cancel; ?></span></a>
                    <span class="gritter-add-primary btn btn-default btn-block hidden">For notifications on saving</span>

                </div>



                <!-- Save Button Ends -->
            </div>
            <!-- Top Navbar Ends -->

            <!-- Sidebar Menu & Content Wrapper Starts -->
            <div id="wrapper" style="overflow: visible! important; ">

                <!-- Sidebar Menu -->
                <div style="margin-top: -20px !important;"class="row">
                    <div style="width:100% !important;" class="col-md-6">
                        <!-- Scrollable menu wrapper with Maximum height -->
                        <div class="nav-tabs-custom" >
                            <!-- Regular Size Menu -->
                            <ul class="nav nav-tabs" id="aci-tabs" style="margin-left:15px">
                                <!-- Menu Regular Item -->
                                <li class="active"  id="aci_tab_general_settings-li"><a class="menu_bar" href="#aci_tab_general_settings" data-toggle="tab"><i></i><span><?php echo $aci_tab_general_settings; ?></span></a></li>
                                <li class="" id="aci_tab_incentive_settings-li"><a class="menu_bar" href="#aci_tab_incentive_settings" data-toggle="tab"><i></i><span><?php echo $aci_tab_incentive_settings; ?></span></a></li>
                                <li class="" id="aci_tab_email_template-li"><a class="menu_bar" href="#aci_tab_email_template" data-toggle="tab"><i></i><span><?php echo $aci_tab_email_template; ?></span></a></li>
                                <li class="" id="aci_tab_abandoned_cart-li"><a class="menu_bar" href="#aci_tab_abandoned_cart" data-toggle="tab"><i></i><span><?php echo $aci_tab_abandoned_cart; ?></span></a></li>
                                <li class="" id="aci_tab_conversion_rate-li"><a class="menu_bar" href="#aci_tab_conversion_rate" data-toggle="tab"><i></i><span><?php echo $aci_tab_conversion_rate; ?></span></a></li>
                                <li class="" id="aci_tab_reports-li"><a class="menu_bar" href="#aci_tab_reports" data-toggle="tab"><i></i><span><?php echo $aci_tab_reports_visualization; ?></span></a></li>


                                <!-- Menu Regular Items Ends -->
                            </ul>
                            <div class="clearfix"></div>
                            <div class="separator bottom"></div>
                            <!-- Regular Size Menu END -->
           <?php if ($error_warning) { ?>
                            <div class="alert alert-error" style="display:block;">
                                <button class="close" data-dismiss="alert" type="button">&times;</button>
                                <strong>Warning!</strong>
                <?php echo $error_warning; ?>
                            </div>
            <?php } ?>
                            <!-- Menu Position Change -->

                            <!-- Menu Position Change END -->

                            <!-- Scrollable Menu wrapper with Maximum Height END -->

                            <!-- Sidebar Menu END -->

                            <!-- Content Starts-->

                            <!-- Breadcrumb Starts -->

                            <!-- Breadcrumb Ends -->
                            <!-- Error Message Starts -->

                            <!-- Breadcrumb Starts -->
                            <div class="sucsscessSave"></div>
                            <div class="bssox">
                                <div class="content tabs" style="padding:0 50px;">
                                    <!-- Content Form Starts -->
                                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                                        <div class="layout">

                                            <div class="tab-content">
                                                <!-- Tab Content Starts -->


                                                <!-- General Settings Tab Starts -->
                                                <div id="aci_tab_general_settings" class="tab-pane active">
                                                    <div style="margin-top:12px;" class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"><i class="fa fa-wrench"></i> <?php echo $aci_tab_general_settings; ?></h3>
                                                        </div>
                                                        <div class="panel-body">

                                                            <div class="blfock">
                                                                <table style="border-top:none !important;text-align:right;" class="table table-hover">
                                                                    <tr>
                                                                        <td class="left-td"><span class="control-label"><?php echo $aci_entry_general_enable; ?></span>
                                                                            <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_general_enable; ?>"></i>
                                                                            <input type ="hidden" name="aci[general][version]" value="<?php echo $aci_plugin_version;?>" />
                                                                        </td>
                                                                        <td class="right-td">
                                                                            <input type="hidden" value="0" name="aci[general][enable]" />
                                                       <?php if(isset($aci['general']['enable']) && $aci['general']['enable'] == 1){ ?>
                                                                            <div <?php if($IE7==true){ echo""; }else{ echo'class="onoffswitch" '; } ?> >
                                                                                <input <?php if($IE7==true){ echo'class="onoffswitch-checkbox"'; }else{ echo'class="onoffswitch-checkbox"'; } ?>  type="checkbox" value="1" name="aci[general][enable]" id="checkout_enable" checked="checked"/>
                                                                                <label class="onoffswitch-label" for="checkout_enable">  
                                                                                    <div class="onoffswitch-inner">  
                                                                                        <div class="onoffswitch-active"><div class="onoffswitch-switch">ON</div></div>  
                                                                                        <div class="onoffswitch-inactive"><div class="onoffswitch-switch">OFF</div></div>  
                                                                                    </div>  
                                                                                </label> </div>
                                                    <?php } else { ?>

                                                                            <div <?php if($IE7==true){ echo""; }else{ echo'class="onoffswitch" '; } ?> >
                                                                                <input <?php if($IE7==true){ echo'class="onoffswitch-checkbox"'; }else{ echo'class="onoffswitch-checkbox"'; } ?>  type="checkbox" value="1" name="aci[general][enable]" id="checkout_enable"  />
                                                                                <label class="onoffswitch-label" for="checkout_enable">  
                                                                                    <div class="onoffswitch-inner">  
                                                                                        <div class="onoffswitch-active"><div class="onoffswitch-switch">ON</div></div>  
                                                                                        <div class="onoffswitch-inactive"><div class="onoffswitch-switch">OFF</div></div>  
                                                                                    </div>  
                                                                                </label>  </div>
                                                    <?php } ?>
                                                                        </td>



                                                                    </tr>
                                                                    <!-- Allow Guest Users-->
                                                                    <tr>
                                                                        <td class="left-td"><span class="control-label"><?php echo $aci_entry_general_allow_guest_users; ?></span>
                                                                            <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_general_allow_guest_users; ?>"></i>
                                                                        </td>
                                                                        <td class="right-td ">
                                                                            <input type="hidden" value="0" name="aci[general][allow_guest]" />
                                                        <?php if(isset($aci['general']['allow_guest']) && $aci['general']['allow_guest'] == 1){ ?>

                                                                            <input class="onoffswitch-checkbox"  type="checkbox" value="1" name="aci[general][allow_guest]" id="checkout_guest_enable" checked="checked" />

                                                                            <label class="onoffswitch-label" for="checkout_guest_enable">  
                                                                                <div class="onoffswitch-inner">  
                                                                                    <div class="onoffswitch-active"><div class="onoffswitch-switch">ON</div></div>  
                                                                                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch">OFF</div></div>  
                                                                                </div>  
                                                                            </label>
                                                        <?php } else { ?>

                                                                            <input  class="onoffswitch-checkbox" type="checkbox" value="1" name="aci[general][allow_guest]" id="checkout_guest_enable"  />

                                                                            <label class="onoffswitch-label" for="checkout_guest_enable">  
                                                                                <div class="onoffswitch-inner">  
                                                                                    <div class="onoffswitch-active"><div class="onoffswitch-switch">ON</div></div>  
                                                                                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch">OFF</div></div>  
                                                                                </div>  
                                                                            </label>


                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- Delay-->
                                                                    <tr>
                                                                        <td class="left-td"><span class="control-label"><?php echo $aci_entry_general_delay; ?></span>
                                                                            <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_general_delay; ?>"></i>
                                                                        </td>
                                                                        <td class="right-td">
                                                                            <div style='float:left;'>
                                                                                <select name="aci[general][delay1]" style="width: 57%; float: left;" class="form-control">
                                                                <?php                                                                
                                                                    if(!empty($aci)){
                                                                        for($i=0;$i<=31;$i++) { 
                                                                            if(!isset($aci['general']['delay1'])){
                                                                                $aci['general']['delay'] = 0;
                                                                            }
                                                                            if($i == $aci['general']['delay1']) { ?>
                                                                                    <option selected="selected" value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                            <?php } else {  ?>
                                                                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                            <?php }                                                                
                                                                        }
                                                                    }
                                                                ?>
                                                                                </select>
                                                                                <label style="color: #A7A7A7;border:0px;
                                                                                       width: 49px;
                                                                                       border: 0px solid #D1D3D4;
                                                                                       font-size: 15px;
                                                                                       float:right;
                                                                                       margin-right: 0px;
                                                                                       margin-top: 10px;;
                                                                                       font-weight: normal;">&nbsp;day(s)</label>
                                                                            </div>
                                                                            <div style='margin-left: 15px;'>
                                                                                <select name="aci[general][delay]" style="width: 10%;
                                                                                        float: left;
                                                                                        margin-left: 15px;" class="form-control">
                                                                <?php for($i=0;$i<=23;$i++) { ?>
                                                                <?php if(isset($aci['general']['delay']) && ($aci['general']['delay'] == $i || $aci['general']['delay'] == $i.':30')){ ?>
                                                                                    <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                                <?php } else{ ?>
                                                                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                <?php } ?>
                                                                <?php } ?>
                                                                                </select>
                                                                                <label style="color: #A7A7A7;
                                                                                       font-size: 15px;
                                                                                       border: 0px solid #D1D3D4;
                                                                                       padding-top: 10px;
                                                                                       font-weight: normal;">&nbsp;hour(s)</label>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <!-- Scheduled Task-->
                                                                    <tr>
                                                                        <td class="left-td"><span class="control-label"><?php echo $aci_entry_general_scheduled_task; ?></span>
                                                                            <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_general_scheduled_task; ?>"></i>
                                                                        </td>
                                                                        <td class="right-td">
                                                        <?php if(isset($aci['general']['scheduled_task'])){ ?>
                                                                            <div class="widget-body uniformjs">

                                                                                <span>   <input class="radio" type="radio" value="yes" id="scheduled_task_yes" name="aci[general][scheduled_task]" <?php if($aci['general']['scheduled_task']=='yes') echo 'checked="checked"'; ?>  />
                                                                                    <label for="scheduled_task_yes">   <?php echo $aci_text_yes; ?>
                                                                                    </label> </span>
                                                                                <span>   <input class="radio" type="radio" value="no" id="scheduled_task_no"  name="aci[general][scheduled_task]" <?php if($aci['general']['scheduled_task']=='no') echo 'checked="checked"'; ?> />
                                                                                    <label for="scheduled_task_no">   <?php echo $aci_text_no; ?>
                                                                                    </label> </span>

                                                                            </div>
                                                        <?php }else{ ?>

                                                                            <div class="widget-body uniformjs">

                                                                                <span>   <input class="radio" type="radio" value="yes" id="scheduled_task_yes" name="aci[general][scheduled_task]" checked="checked"' />
                                                                                    <label for="scheduled_task_yes">   <?php echo $aci_text_yes; ?>
                                                                                    </label> </span>
                                                                                <span>   <input class="radio" type="radio" value="no" id="scheduled_task_no"  name="aci[general][scheduled_task]" />
                                                                                    <label for="scheduled_task_no">   <?php echo $aci_text_no; ?>
                                                                                    </label> </span>

                                                                            </div>

                                                        <?php } ?>
                                                                        </td>
                                                                    </tr> 
                                                                    </tbody>
                                                                </table>
                                                                <div class="widget" id="cron_instructions" data-toggle="collapse-widget">
                                                                    <div class="widget-head">
                                                                        <h3 class="heading"><?php echo $aci_text_cron_instructions; ?></h3>
                                                                    </div>
                                                                    <div class="widget-body">
                                                                        <p>
                                                                    <?php if(isset($aci['general']['scheduled_task'])){ ?>
                                                                            <label style="font-family: inherit;width:100%; border: 0px; color:#A7A7A7; font-size: 14px; font-weight:normal;">
                                                                    <?php echo $aci_text_cron_instructions_1; ?><br /><br />
                                                                                <b><?php echo $aci_text_cron_instructions_2; ?></b><br />
                                                                        <?php echo HTTP_CATALOG ."index.php?route=module/abandonedcartincentive/sendCouponMail"; ?><br /><br />
                                                                                <b><?php echo $aci_text_cron_instructions_3; ?></b><br />
                                                                        <?php echo $aci_text_cron_instructions_4; ?><?php echo HTTP_CATALOG ."index.php?route=module/abandonedcartincentive/sendCouponMail"; ?><br /><br />
                                                                            </label>
                                                                    <?php } ?>
                                                                        </p>
                                                                    </div>
                                                                </div>   
                                                                <script>
                                                                            $("input:radio[name='aci[general][scheduled_task]']").click(function(){
                                                                    var value = $(this).val();
                                                                            if (value == "yes")
                                                                    {
                                                                    $("#cron_instructions").show();
                                                                    }
                                                                    else
                                                                    {
                                                                    $("#cron_instructions").hide();
                                                                    }
                                                                    });                                                    </script>
                                                            </div>
                                                        </div> </div> </div>
                                                <!-- General Settings Tab Ends -->

                                                <!-- Incentive Settings Tab Starts -->
                                                <div id="aci_tab_incentive_settings" class="tab-pane">
                                                    <div style="margin-top:12px;" class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"><i class="fa fa-wrench"></i> <?php echo $aci_tab_incentive_settings; ?></h3>
                                                        </div>
                                                        <div class="panel-body">

                                                            <div class="blfock">



                                                                <div class="widget widget-tabs">
                                                                    <div class="widget-head">
                                                                        <ul>
                                                                            <li class="active"><a class="glyphicons glyphicons user" href="#tab-incentive-registered" data-toggle="tab"><i></i> <?php echo $aci_entry_incentive_registered; ?> </a></li>
                                                                            <li><a class="glyphicons old_man" href="#tab-incentive-guest" data-toggle="tab"><i></i><?php echo $aci_entry_incentive_guest; ?></a></li>
                                                                            <li style='text-align: right;'>
                                                                                <div class="widget-body uniformjs" style="margin-top:-10px; float: left; font-weight: normal !important; color: #8D8980;">
                                                                                    <input type="hidden" value="0" name="aci[incentive][copy_registered]" />
                                                            <?php if(isset($aci['incentive']['copy_registered']) && $aci['incentive']['copy_registered'] == 1){ ?>
                                                                                    <input style="width:14px; min-height:14px;"class="checkbox" type="checkbox" value="1" name="aci[incentive][copy_registered]" checked="checked" id="aci_incentive_copy_registered"/>
                                                            <?php }else{ ?>
                                                                                    <input style="width:14px; min-height:14px;" class="checkbox" type="checkbox" value="1" name="aci[incentive][copy_registered]" id="aci_incentive_copy_registered" />
                                                            <?php } ?>
                                                                                    <label for="aci_incentive_copy_registered" style='float: right; width: 170px;font-weight:normal;border: 0px;width: 156px;margin-top: -19px;'><?php echo $aci_entry_incentive_same_as_registered; ?></label>
                                                                                </div>                                                                                                         
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="widget-body">
                                                                        <div class="tab-content">
                                                                            <div id="tab-incentive-registered" class="tab-pane active">
                                                                                <div class="innerAll">
                                                                                    <div class="widget">
                                                                                        <div class="widget-head">
                                                                                            <h3 class="heading">
                                                                        <?php echo $aci_text_registered_customer_incentive; ?>
                                                                                            </h3>
                                                                                        </div>
                                                                                        <div class="widget-body">
                                                                                            <input type="hidden" value="0" id="incentive_tab_error"/>


                                                                                        </table> <table id="incentive-registered-table" class="table table-striped table-bordered table-condensed table-vertical-center" style='width:100%;'>
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <td class="center" width='205px'><?php echo $aci_entry_incentive_table_discount_type;?></td>
                                                                                                <td class="center" width='80px'><?php echo $aci_entry_incentive_table_discount;?></td>
                                                                                                <td class="center" width='130px'><?php echo $aci_entry_incentive_table_miminum_amount;?></td>
                                                                                                <td class="center" width='150px'><?php echo $aci_entry_incentive_table_coupon_active_time;?></td>
                                                                                                <td class="center" width='150px'><?php echo $aci_entry_incentive_table_status;?></td>
                                                                                                <td class="center" width='200px'><?php echo $aci_entry_incentive_table_delay;?></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                        </thead>
                                                                        <?php $module_row = 0;
                                                                        if(!empty($aci['incentive']['registered']['row'])){
                                                                        $modules = $aci['incentive']['registered']['row'];?>
                                                                        <?php foreach ($modules as $module) { ?>
                                                                                        <tbody id="module-row<?php echo $module_row; ?>">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <select name="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_discount_type]" style="width:82%! important;" class="form-control" id="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_discount_type]">
                                                                                        <?php if ($module['aci_incentive_table_discount_type'] == 'F') { ?>
                                                                                                        <option value="F" selected="selected"><?php echo $aci_entry_incentive_table_discount_type_fixed; ?></option>
                                                                                                        <option value="P"><?php echo $aci_entry_incentive_table_discount_type_percentage; ?></option>
                                                                                        <?php } else { ?>
                                                                                                        <option value="F"><?php echo $aci_entry_incentive_table_discount_type_fixed; ?></option>
                                                                                                        <option value="P" selected="selected"><?php echo $aci_entry_incentive_table_discount_type_percentage; ?></option>
                                                                                        <?php } ?>
                                                                                                    </select>
                                                                                                </td>
                                                                                                <td>
                                                                                    <?php if ($module['aci_incentive_table_discount_type'] == 'F') { ?>
                                                                                                    <input type="text" name="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_discount]" value="<?php echo $module['aci_incentive_table_discount']; ?>" size="3" class="form-control" style="width:80%;" id="discount_<?php echo $module_row; ?>"/>
                                                                                    <?php } else { ?>
                                                                                                    <input type="text" name="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_discount]" value="<?php echo $module['aci_incentive_table_discount']; ?>" size="3" class="form-control" style="width:80%;" id="discount_<?php echo $module_row; ?>"/>
                                                                                    <?php } ?>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" name="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_miminum_amount]" value="<?php echo $module['aci_incentive_table_miminum_amount']; ?>" size="3" class="form-control" style="width:80%;" id="min_amt_<?php echo $module_row; ?>"/>
                                                                                                </td>
                                                                                                <td width='150px'>
                                                                                                    <input type="text" name="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_coupon_active_time]" value="<?php echo $module['aci_incentive_table_coupon_active_time']; ?>" size="3" class="form-control" style="width:80%; width: 41%;
                                                                                                           margin-top: -2px;
                                                                                                           float: left;" id="coupon_active_<?php echo $module_row; ?>"/>
                                                                                                    <p style="color:#A7A7A7; font-size: 15px; padding-top:10px;">&nbsp;day(s)</p>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <select name="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_status]" style="width:88%! important;" class="form-control">
                                                                                        <?php if ($module['aci_incentive_table_status']) { ?>
                                                                                                        <option value="1" selected="selected"><?php echo $aci_entry_incentive_table_status_enabled; ?></option>
                                                                                                        <option value="0"><?php echo $aci_entry_incentive_table_status_disabled; ?></option>
                                                                                        <?php } else { ?>
                                                                                                        <option value="1"><?php echo $aci_entry_incentive_table_status_enabled; ?></option>
                                                                                                        <option value="0" selected="selected"><?php echo $aci_entry_incentive_table_status_disabled; ?></option>
                                                                                        <?php } ?>
                                                                                                    </select>
                                                                                                </td>
                                                                                                <td>

                                                                                                    <select name="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_delay_days]" style="margin-bottom:5px; width:38%! important;" class="form-control">
                                                                                        <?php for($i=0;$i<=31;$i++) { ?>
                                                                                        <?php if(isset($module['aci_incentive_table_delay_days']) && ($module['aci_incentive_table_delay_days'] == $i) ){ ?>
                                                                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                                                        <?php } else{ ?>
                                                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                                        <?php } ?>
                                                                                        <?php } ?>
                                                                                                    </select>
                                                                                                    <label style="color:#A7A7A7; float: right; margin-top: -30px;font-size:  15px; border:0px! important; padding-top:5px; font-weight:normal;">&nbsp;day(s)</label>
                                                                                                    <select name="aci[incentive][registered][row][<?php echo $module_row; ?>][aci_incentive_table_delay]" style="width:38%! important;" class="form-control">
                                                                                        <?php for($i=0;$i<=23;$i++) { ?>
                                                                                        <?php if(isset($module['aci_incentive_table_delay']) && ($module['aci_incentive_table_delay'] == $i) ){ ?>
                                                                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                                                        <?php } else{ ?>
                                                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                                        <?php } ?>
                                                                                        <?php } ?>
                                                                                                    </select>


                                                                                                    <label style="color:#A7A7A7; float: right; margin-top: -38px;font-size:  15px; border:0px! important; padding-top:15px; font-weight:normal;">&nbsp;hour(s)</label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="btn btn-default"><span><?php echo $aci_entry_incentive_table_remove; ?></span></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        <script>
                                                                                                    $("#discount_<?php echo $module_row; ?>").focusout(function() {
                                                                                            //alert($.isNumeric(  $( "#discount_<?php echo $module_row; ?>" ).val() ));
                                                                                            if (!$.isNumeric($("#discount_<?php echo $module_row; ?>").val()))
                                                                                            {
                                                                                            //<?php $incentive_table_error = true;  ?>
                                                                                            $("#incentive_tab_error").val("1");
                                                                                            }

                                                                                            });
                                                                                                    $("#min_amt_<?php echo $module_row; ?>").focusout(function() {
                                                                                            //alert($.isNumeric(  $( "#min_amt_<?php echo $module_row; ?>" ).val() ));
                                                                                            if (!$.isNumeric($("#min_amt_<?php echo $module_row; ?>").val()))
                                                                                            {
                                                                                            //<?php $incentive_table_error = true;  ?>
                                                                                            $("#incentive_tab_error").val("1");
                                                                                            }

                                                                                            });
                                                                                                    $("#coupon_active_<?php echo $module_row; ?>").focusout(function() {
                                                                                            //alert($.isNumeric(  $( "#coupon_active_<?php echo $module_row; ?>" ).val() ));
                                                                                            if (!$.isNumeric($("#coupon_active_<?php echo $module_row; ?>").val()))
                                                                                            {
                                                                                            //<?php $incentive_table_error = true;  ?>
                                                                                            $("#incentive_tab_error").val("1");
                                                                                            }

                                                                                            });                                                                            </script>
                                                                                        </tbody>
                                                                        <?php $module_row++; ?>
                                                                        <?php } }?>
                                                                                        <tfoot>
                                                                                            <tr>
                                                                                                <td colspan="6"></td>
                                                                                                <td class="left"><a onclick="addModule();" class="btn btn-default"><span><?php echo $aci_entry_incentive_table_add_module; ?></span></a></td>
                                                                                            </tr>
                                                                                        </tfoot>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div id="tab-incentive-guest" class="tab-pane">
                                                                        <div class="innerAll">
                                                                            <div class="widget">
                                                                                <div class="widget-head">
                                                                                    <h3 class="heading"><?php echo $aci_text_guest_customer_incentive; ?></h3>
                                                                                </div>
                                                                                <div class="widget-body">
                                                                                    <table id="incentive-guest-table" class="table table-striped table-bordered table-condensed table-vertical-center" style='width:100%;'>
                                                                                        <thead>
                                                                                            <tr>
                                                                                                <td class="center" width='205px'><?php echo $aci_entry_incentive_table_discount_type;?></td>
                                                                                                <td class="center" width='80px'><?php echo $aci_entry_incentive_table_discount;?></td>
                                                                                                <td class="center" width='130px'><?php echo $aci_entry_incentive_table_miminum_amount;?></td>
                                                                                                <td class="center" width='150px'><?php echo $aci_entry_incentive_table_coupon_active_time;?></td>
                                                                                                <td class="center" width='150px'><?php echo $aci_entry_incentive_table_status;?></td>
                                                                                                <td class="center" width='200px'><?php echo $aci_entry_incentive_table_delay;?></td>
                                                                                                <td></td>
                                                                                            </tr>
                                                                                        </thead>
                                                                        <?php $guest_module_row = 0;
                                                                        if(!empty($aci['incentive']['guest']['row'])){
                                                                        $modules = $aci['incentive']['guest']['row'];?>
                                                                        <?php foreach ($modules as $module) { ?>
                                                                                        <tbody id="guest-module-row<?php echo $guest_module_row; ?>">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <select name="aci[incentive][guest][row][<?php echo $guest_module_row; ?>][aci_incentive_table_discount_type]" style="width:82%! important;" class="form-control">
                                                                                        <?php if ($module['aci_incentive_table_discount_type'] == 'F') { ?>
                                                                                                        <option value="F" selected="selected"><?php echo $aci_entry_incentive_table_discount_type_fixed; ?></option>
                                                                                                        <option value="P"><?php echo $aci_entry_incentive_table_discount_type_percentage; ?></option>
                                                                                        <?php } else { ?>
                                                                                                        <option value="F"><?php echo $aci_entry_incentive_table_discount_type_fixed; ?></option>
                                                                                                        <option value="P" selected="selected"><?php echo $aci_entry_incentive_table_discount_type_percentage; ?></option>
                                                                                        <?php } ?>
                                                                                                    </select>
                                                                                                </td>
                                                                                                <td>
                                                                                    <?php if ($module['aci_incentive_table_discount_type'] == 'F') { ?>
                                                                                                    <input type="text" name="aci[incentive][guest][row][<?php echo $guest_module_row; ?>][aci_incentive_table_discount]" value="<?php echo $module['aci_incentive_table_discount']; ?>" size="3" class="form-control" style="width:80%;" id="guest_discount_<?php echo $guest_module_row; ?>"/>
                                                                                    <?php } else { ?>
                                                                                                    <input type="text" name="aci[incentive][guest][row][<?php echo $guest_module_row; ?>][aci_incentive_table_discount]" value="<?php echo $module['aci_incentive_table_discount']; ?>" size="3" class="form-control" style="width:80%;" id="guest_discount_<?php echo $guest_module_row; ?>"/>
                                                                                    <?php } ?>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input type="text" name="aci[incentive][guest][row][<?php echo $guest_module_row; ?>][aci_incentive_table_miminum_amount]" value="<?php echo $module['aci_incentive_table_miminum_amount']; ?>" size="3" class="form-control" style="width:80%;" id="guest_min_amt_<?php echo $guest_module_row; ?>"/>
                                                                                                </td>
                                                                                                <td width='150px'>
                                                                                                    <input type="text" name="aci[incentive][guest][row][<?php echo $guest_module_row; ?>][aci_incentive_table_coupon_active_time]" value="<?php echo $module['aci_incentive_table_coupon_active_time']; ?>" size="3" class="form-control" style="width:80%; width: 41%;
                                                                                                           margin-top: -2px;
                                                                                                           float: left;" id="guest_coupon_active_<?php echo $guest_module_row; ?>"/>
                                                                                                    <p style="color:#A7A7A7; font-size: 15px; padding-top:10px;">&nbsp;day(s)</p>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <select name="aci[incentive][guest][row][<?php echo $guest_module_row; ?>][aci_incentive_table_status]" style="width:88%! important;" class="form-control">
                                                                                        <?php if ($module['aci_incentive_table_status']) { ?>
                                                                                                        <option value="1" selected="selected"><?php echo $aci_entry_incentive_table_status_enabled; ?></option>
                                                                                                        <option value="0"><?php echo $aci_entry_incentive_table_status_disabled; ?></option>
                                                                                        <?php } else { ?>
                                                                                                        <option value="1"><?php echo $aci_entry_incentive_table_status_enabled; ?></option>
                                                                                                        <option value="0" selected="selected"><?php echo $aci_entry_incentive_table_status_disabled; ?></option>
                                                                                        <?php } ?>
                                                                                                    </select>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <!--<input type="text" name="aci[incentive][guest][row][<?php echo $module_row; ?>][aci_incentive_table_delay]" value="<?php echo $module['aci_incentive_table_delay']; ?>" size="3" class="form-control" style="width:80%;"/>-->
                                                                                                    <select name="aci[incentive][guest][row][<?php echo $guest_module_row; ?>][aci_incentive_table_delay_days]" style="width:38%! important; margin-bottom:5px;" class="form-control">
                                                                                        <?php for($i=0;$i<=31;$i++) { ?>
                                                                                        <?php if(isset($module['aci_incentive_table_delay_days']) && ($module['aci_incentive_table_delay_days'] == $i)){ ?>
                                                                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                                                        <?php } else{ ?>
                                                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                                        <?php } ?>
                                                                                        <?php } ?>
                                                                                                    </select>

                                                                                                    <label style="color:#A7A7A7; font-size: 15px;border:0px! important;float: right; margin-top: -30px;padding-top:5px; font-weight:normal;">&nbsp;day(s)</label>

                                                                                                    <select name="aci[incentive][guest][row][<?php echo $guest_module_row; ?>][aci_incentive_table_delay]" style="width:38%! important;" class="form-control">
                                                                                        <?php for($i=0;$i<=23;$i++) { ?>
                                                                                        <?php if(isset($module['aci_incentive_table_delay']) && ($module['aci_incentive_table_delay'] == $i)){ ?>
                                                                                                        <option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
                                                                                        <?php } else{ ?>
                                                                                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                                                                        <?php } ?>
                                                                                        <?php } ?>
                                                                                                    </select>

                                                                                                    <label style="color:#A7A7A7; float: right; margin-top: -38px;font-size:  15px; border:0px! important; padding-top:15px; font-weight:normal;">&nbsp;hour(s)</label>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <a onclick="$('#guest-module-row<?php echo $guest_module_row; ?>').remove();" class="btn btn-default"><span><?php echo $aci_entry_incentive_table_remove; ?></span></a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        <script>
                                                                                                    $("#guest_discount_<?php echo $guest_module_row; ?>").focusout(function() {
                                                                                            //alert($.isNumeric(  $( "#discount_<?php echo $module_row; ?>" ).val() ));
                                                                                            if (!$.isNumeric($("#guest_discount_<?php echo $guest_module_row; ?>").val()))
                                                                                            {
                                                                                                    <?php $incentive_table_error = true;  ?>
                                                                                                        }

                                                                                                        });
                                                                                                                $("#guest_min_amt_<?php echo $guest_module_row; ?>").focusout(function() {
                                                                                                        //alert($.isNumeric(  $( "#min_amt_<?php echo $module_row; ?>" ).val() ));
                                                                                                        if (!$.isNumeric($("#guest_min_amt_<?php echo $guest_module_row; ?>").val()))
                                                                                                        {
                                                                                                    <?php $incentive_table_error = true;  ?>
                                                                                                        }

                                                                                                        });
                                                                                                                $("#guest_coupon_active_<?php echo $guest_module_row; ?>").focusout(function() {
                                                                                                        //alert($.isNumeric(  $( "#coupon_active_<?php echo $module_row; ?>" ).val() ));
                                                                                                        if (!$.isNumeric($("#guest_coupon_active_<?php echo $guest_module_row; ?>").val()))
                                                                                                        {
                                                                                                    <?php $incentive_table_error = true;  ?>
                                                                                                        }

                                                                                                        });                                                                            </script>
                                                                                        </tbody>
                                                                        <?php $guest_module_row++; ?>
                                                                        <?php } }?>
                                                                                        <tfoot>
                                                                                            <tr>
                                                                                                <td colspan="6"></td>
                                                                                                <td class="left"><a onclick="addGuestModule();" class="btn btn-default"><span><?php echo $aci_entry_incentive_table_add_module; ?></span></a></td>
                                                                                            </tr>
                                                                                        </tfoot>
                                                                                    </table>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>             
                                        <!-- Incentive Settings Tab Ends -->

                                        <!-- Email Template Tab Starts -->
                                        <div id="aci_tab_email_template" class="tab-pane">
                                            <div style="margin-top:12px;" class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><i class="fa fa-wrench"></i> <?php echo $aci_tab_email_template; ?></h3>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="blfock">
                                                        <table style="border-top:none !important;text-align:right;" class="table table-hover">
                                                            <tr>
                                                                <td class="left-td">

                                                                    <span class="control-label"><?php echo $aci_entry_email_subject; ?></span>
                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_email_subject; ?>"></i>
                                                                </td>
                                                                <td class="right-td">
                            <?php if(isset($aci['email']['subject'])) { ?>
                                                                    <input type="text" name="aci[email][subject]" value="<?php echo $aci['email']['subject']; ?>" class="form-control" style="width:80%;"/>
                        <?php } else { ?>
                                                                    <input type="text" name="aci[email][subject]" value="<?php echo $aci_email_default_subject; ?>" class="form-control" style="width:80%;"/>
                        <?php } ?>
                                                                </td>
                                                            </tr>
                                                            <tr style="height:300px;">
                                                                <td class="left-td"><span class="control-label"><?php echo $aci_entry_email_body; ?></span>
                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_email_body; ?>"></i>
                                                                </td>
                                                                <td class="right-td">
                                                                    <textarea style="width:70%; margin-left:-1px; border-left:1px solid #a9a9a9;" name="aci[email][body]" id="emailbody">
                            <?php if(isset($aci['email']['body'])) { ?>
                            <?php echo $aci['email']['body']; ?>
                            <?php } else { ?>
                                                        <?php echo $aci_email_default_content; ?>
                            <?php } ?>
                                                                    </textarea>

                                                                    <script type="text/javascript">
                                                                                function CKupdate(){
                                                                                for (instance in CKEDITOR.instances)
                                                                                        CKEDITOR.instances[instance].updateElement();
                                                                                }
                                                                        CKEDITOR.replace('emailbody', {
                                                                        filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                height: '450px'
                                                                        });                                                        </script>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Email Template Tab Ends -->

                                        <!-- View Abandoned Cart Tab Starts -->
                                        <div id="aci_tab_abandoned_cart" class="tab-pane">
                                            <div style="margin-top:12px;" class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><i class="fa fa-wrench"></i> <?php echo $aci_tab_abandoned_cart; ?></h3>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="blfock">

                                                        <div class="widget widget-tabs">
                                                            <div class="widget-head">
                                                                <ul>
                                                                    <li class="active" id="tab-abandoned-cart-registered-li"><a class="glyphicons glyphicons user" href="#tab-abandoned-cart-registered" data-toggle="tab"><i></i> <?php echo $aci_entry_incentive_registered; ?> </a></li>
                                                                    <li id="tab-abandoned-cart-guest-li"><a class="glyphicons old_man" href="#tab-abandoned-cart-guest" data-toggle="tab"><i></i>Guest</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="widget-body">
                                                                <div class="tab-content">
                                                                    <!-- Registered Customers Tab -->
                                                                    <div id="tab-abandoned-cart-registered" class="tab-pane active">

                                                                        <table class="list form">
                                                                            <thead>
                                                                                <tr>
                                                                                    <td class="left" style="width:15px;"></td>
                                                                                    <td class="left" style="width:100px;"><?php echo $aci_text_customer;?></td>
                                                                                    <td class="left" style="width:140px;"><?php echo $aci_text_current_incentive;?></td>
                                                                                    <td class="left" style="width:120px;"><?php echo $aci_text_date_time;?></td>
                                                                                    <td class="left" style="width:80px;"><?php echo $aci_text_action;?></td>
                                                                                </tr>
                                                                            </thead>
                                                            <?php if(isset($aci_abandoned_carts) && (!empty($aci_abandoned_carts))) {
                                                            $i = 1;
                                                            foreach($aci_abandoned_carts as $abandonedcart){
                                                            
                                                            $customer = unserialize($abandonedcart['customer_info']); ?>
                                                                            <tbody id="registered<?php echo $i;?>">
                                                                                <tr>
                                                                                    <td style="width:15px;"><?php echo $i;?></td>
                                                                                    <td style="width:100px;">
                                                                                        <p style="margin-bottom: 15px;">
                                                                                            <span class="glyphicons customer_info user" style="cursor: pointer;"><i></i></span>
                                                                            <?php echo $customer['firstname'];?>&nbsp;<?php echo $customer['lastname'];?>
                                                                                        </p>
                                                                                        <p style="margin-bottom: 22px;">
                                                                                            <span class="glyphicons customer_info message_empty" style="cursor: pointer;"><i></i></span>
                                                                            <?php echo $customer['email'];?>
                                                                                        </p>
                                                                                        <p>
                                                                                            <span class="glyphicons customer_info iphone" style="cursor: pointer;"><i></i></span>
                                                                            <?php echo $customer['telephone'];?>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td style="width:150px;">
                                                                       <?php 
                                                                        if(!empty($coupons[$i])) { ?>
                                                                                        <p>Coupon code : <?php echo $coupons[$i][0]['code'];?></p>
                                                                                        <p>Discount :
                                                                            <?php if($coupons[$i][0]['type'] == 'F')
                                                                            echo $fixed_amount[$i][0];
                                                                            else
                                                                            echo $coupons[$i][0]['discount'].'%';?>
                                                                                        </p>
                                                                                        <p>Start Date : <?php echo $coupons[$i][0]['date_start'];?></p>
                                                                                        <p>End Date : <?php echo $coupons[$i][0]['date_end'];?></p>
                                                                                        <p style="text-align: right;cursor: pointer;text-decoration: underline;" id="tab-abandoned-cart-registered-previouscoupons-<?php echo $i; ?>">
                                                                                            <a href="#modal-coupons<?php echo $i;?>" data-toggle="modal">Previous Coupons</a>
                                                                                        </p>
                                                                        <?php } else { ?>
                                                                                        <p><?php echo $aci_text_no_coupon_code_sent; ?></p>
                                                                        <?php }  ?>

                                                                                        <!-- Modal Coupons Starts -->
                                                                                        <div class="modal fade" id="modal-coupons<?php echo $i;?>" style="width: 600px; height: 500px;">

                                                                                            <!-- Modal heading -->
                                                                                            <div class="modal-header">
                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                                                                                <h3><?php echo $customer['firstname'];?>&nbsp;<?php echo $customer['lastname'];?> -- <?php echo $aci_text_previous_coupons; ?></h3>
                                                                                            </div>
                                                                                            <!-- Modal heading END -->

                                                                                            <!-- Modal body -->
                                                                                            <div class="modal-body">
                                                                                                <table class="list form">

                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <td width='10%'></td>
                                                                                                            <td width='20%'><?php echo $aci_text_coupon?></td>
                                                                                                            <td width='15%'><?php echo $aci_text_discount; ?></td>
                                                                                                            <td width='15%'><?php echo $aci_text_min_amt; ?></td>
                                                                                                            <td width='15%'><?php echo $aci_text_created_on; ?></td>
                                                                                                            <td width='15%'><?php echo $aci_text_expired_on; ?></td>
                                                                                                        </tr>
                                                                                                    </thead>

                                                                                  <?php  $couponrow = 0;foreach($modal_coupons[$i] as $modal_coupon) {//print_R($modal_coupon); ?>

                                                                                    <?php if($couponrow > 0) { ?>
                                                                                                    <tbody id='coupon-row<?php echo $couponrow;?>'>
                                                                                                        <tr>
                                                                                                            <td><?php echo $couponrow;?></td>
                                                                                                            <td><?php echo $modal_coupon['code'];?></td>
                                                                                            <?php if($modal_coupon['type'] == 'F') { ?>
                                                                                                            <td><?php echo $discount_amount[$i][$couponrow];?></td>
                                                                                                            <td><?php echo $discount_total[$i][$couponrow];?></td>
                                                                                            <?php } else{ ?>
                                                                                                            <td><?php echo $modal_coupon['discount'];?></td>
                                                                                                            <td><?php echo $modal_coupon['total'];?></td>
                                                                                            <?php } ?>
                                                                                                            <td><?php echo $modal_coupon['date_start'];?></td>
                                                                                                            <td><?php echo $modal_coupon['date_end'];?></td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                    <?php } ?>
                                                                                    <?php $couponrow++; } ?>
                                                                                                </table>
                                                                                            </div>
                                                                                            <!-- Modal body END -->

                                                                                            <!-- Modal footer -->
                                                                                            <div class="modal-footer">
                                                                                                <a href="#" data-dismiss="modal" class="btn btn-default">Close</a>
                                                                                            </div>
                                                                                            <!-- Modal footer END -->

                                                                                        </div>
                                                                                        <!-- Modal Coupons END -->
                                                                                    </td>
                                                                                    <td style="width:120px;"><p><?php echo $aci_text_first_visit; ?><?php echo $abandonedcart['date_created']; ?></p>
                                                                                        <p><?php echo $aci_text_last_visit; ?><?php echo $abandonedcart['date_modified']; ?></p>
                                                                                        <p><?php echo $aci_text_time_spent; ?><?php $time = strtotime($abandonedcart['date_modified']) - strtotime($abandonedcart['date_created']); echo gmdate("H:i:s", $time) ?></p>
                                                                                    </td>
                                                                                    <td style="width:70px; text-align: center;">
                                                                                        <a href="#modal-email<?php echo $i;?>" data-toggle="modal"><span style="padding:5px;"><img src='./view/image/aci/email.png' alt='Email' style="width:24px; height:24px; cursor:pointer;" id="tab-abandoned-cart-registered-sendemail-<?php echo $i; ?>"/></span></a>
                                                                                        <a href="#modal-cart<?php echo $i;?>" data-toggle="modal"><span style="padding:10px;"><img src='./view/image/aci/cart.png' alt='Cart' style="width:24px; height:24px; cursor:pointer;" id="tab-abandoned-cart-registered-cartproducts-<?php echo $i; ?>"/></span></a>
                                                                                        <input type='hidden' id='cart_id_<?php echo $i;?>' value="<?php echo $abandonedcart['id'];?>"/>
                                                                                        <span style="padding:0px;"><a id="modals-bootbox-confirm<?php echo $i;?>" ><img src='./view/image/aci/remove.png' alt='Remove' style="width:24px; height:24px; cursor:pointer;" /></a></span>
                                                                                        <script type="text/javascript">
                                                                                                    $('#modals-bootbox-confirm<?php echo $i;?>').on('click', function() {
                                                                                            bootbox.confirm("Are you sure to delete this abandoned cart?", function(result)
                                                                                            {

                                                                                            if (result)
                                                                                            {
                                                                                            $.ajax({
                                                                                            url: 'index.php?route=module/abandonedcartincentive/deleteAbandonedCart&token=<?php echo $token; ?>',
                                                                                                    type: 'post',
                                                                                                    data: 'cart_id=' + $('#cart_id_<?php echo $i;?>').val(),
                                                                                                    success: function(response) {
                                                                                                    $('.gritter-add-primary').trigger('click');
                                                                                                            $('#button_close_modal').trigger('click');
                                                                                                            $('#registered<?php echo $i;?>').remove();
                                                                                                    }
                                                                                            });
                                                                                            }
                                                                                            });
                                                                                            });                                                                        </script>

                                                                                        <!-- Modal Email Starts -->
                                                                                        <div class="modal  fade" id="modal-email<?php echo $i;?>" style="width: 75%; height: 630px; left:12% !important; top:2% !important; margin-left: 0px !important;">

                                                                                            <!-- Modal heading -->
                                                                                            <div class="modal-header">
                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                <h3><?php echo $customer['firstname'];?>&nbsp;<?php echo $customer['lastname'];?> <?php echo $aci_text_coupon_email?></h3>
                                                                                            </div>
                                                                                            <!-- Modal heading END -->
                                                                                            <!-- Modal body -->
                                                                                            <div class="modal-body">
                                                                                                <form id="manual-email-form">
                                                                                                    <div style="overflow-y:auto !important; height:500px;">
                                                                                                        <table class="list form" style="width:1000px; text-align: center;">
                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><?php echo $aci_entry_incentive_table_discount_type; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_incentive_table_discount_type; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >
                                                                                                                    <select class="form-control" name="aci[manual][email][discount_type]" id="manual_discounttype_<?php echo $i;?>">
                                                                                                                        <option value="F" selected="selected"><?php echo $aci_entry_incentive_table_discount_type_fixed; ?></option>
                                                                                                                        <option value="P"><?php echo $aci_entry_incentive_table_discount_type_percentage; ?></option>
                                                                                                                    </select>
                                                                                                                    <input type="hidden" name="aci[manual][email][emailid]" value="<?php echo $customer['email'];?>" class="form-control" style="width:80%;" id="manual_email_id_<?php echo $i;?>"/>
                                                                                                                    <input type="hidden" name="aci[manual][email][firstname]" value="<?php echo $customer['firstname'];?>" class="form-control" style="width:80%;" id="manual_fname_<?php echo $i;?>"/>
                                                                                                                    <input type="hidden" name="aci[manual][email][lastname]" value="<?php echo $customer['lastname'];?>" class="form-control" style="width:80%;" id="manual_lname_<?php echo $i;?>"/>
                                                                                                                    <input type="hidden" name="aci[manual][email][cart_id]" value="<?php echo $abandonedcart['id'];?>" class="form-control" style="width:80%;" id="manual_cartid_<?php echo $i;?>"/>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><span class="required"> *</span><?php echo $aci_entry_incentive_table_discount; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_incentive_table_discount; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >
                                                                                                                    <input type="text" name="aci[manual][email][discount]" value="" class="form-control" style="width:80%;" id="manual_discount_<?php echo $i;?>"/>
                                                                                                                    <br><span id="required_manual_discount_<?php echo $i;?>" style="display: none;float: left;padding-left: 15px;" class="required"></span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><span class="required"> *</span><?php echo $aci_entry_incentive_table_coupon_active_time; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_incentive_table_coupon_active_time; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >
                                                                                                                    <input type="text" name="aci[manual][email][coupon_active_time]" value="" class="form-control" style="width:80%;" id="manual_coupon_active_<?php echo $i;?>"/>
                                                                                                                    <br><span id="required_manual_coupon_active_<?php echo $i;?>" style="display: none;float: left;padding-left: 15px;" class="required"></span>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><span class="required"> *</span><?php echo $aci_entry_incentive_table_miminum_amount; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_incentive_table_miminum_amount; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >
                                                                                                                    <input type="text" name="aci[manual][email][minimum_amount]" value="" class="form-control" style="width:80%;" id="manual_minimum_amount_<?php echo $i;?>"/>
                                                                                                                    <br><span id="required_manual_minimum_amount_<?php echo $i;?>" style="display: none;float: left;padding-left: 15px;" class="required"></span>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><span class="required"> *</span><?php echo $aci_entry_email_subject; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_email_subject; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;">
                                                                                                                    <input type="text" name="aci[manual][email][subject]" value="<?php echo $aci['email']['subject']; ?>" class="form-control" style="width:80%;" id="manual_email_subject_<?php echo $i;?>"/>
                                                                                                                    <br><span id="required_manual_email_subject_<?php echo $i;?>" style="display: none;float: left;padding-left: 15px;" class="required"></span>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><?php echo $aci_entry_email_body; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_email_body; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;">
                                                                                                                    <textarea name="aci[manual][email][body]" id="registeredemailbody<?php echo $i;?>">
                                                                                                        <?php echo $aci['email']['body']; ?>
                                                                                                                    </textarea>
                                                                                                                    <script type="text/javascript">
                                                                                                                                <!--
                                                                                                                  function CKupdate(){
                                                                                                                                for (instance in CKEDITOR.instances)
                                                                                                                                        CKEDITOR.instances[instance].updateElement();
                                                                                                                                }
                                                                                                                        CKEDITOR.replace('registeredemailbody<?php echo $i;?>', {
                                                                                                                        filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                height: '250px', width: '720px'
                                                                                                                        });
                                                                                                                                //-->
                                                                                                                    </script>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><?php echo $aci_entry_email_stop; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_email_stop; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >

                                                                                                                    <input <?php if($IE7==true){ echo'class="checkbox"'; }else{ echo'class="make-switch"'; } ?>  type="checkbox" value="0" name="aci[manual][email][remove_ab_cart]" id="manual_auto_email_<?php echo $i;?>" />

                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                            <!-- Modal body END -->

                                                                                            <!-- Modal footer -->
                                                                                            <div class="modal-footer">
                                                                                                <a href="#" class="btn btn-default" data-dismiss="modal"><?php echo $aci_entry_email_cancel;?></a>
                                                                                                <a href="#" data-dismiss="modal" class="btn btn-primary" id="sendemail<?php echo $i;?>"><?php echo $aci_entry_email_send;?></a>
                                                                                            </div>
                                                                                            <!-- Modal footer END -->

                                                                                            <script>

                                                                                                        $('#sendemail<?php echo $i;?>').on('click', function() {
                                                                                                var validate = 0;
                                                                                                        if ($.trim($('#manual_discount_<?php echo $i;?>').val()) == ''){
                                                                                                $('#required_manual_discount_<?php echo $i;?>').show();
                                                                                                        $('#required_manual_discount_<?php echo $i;?>').html('Please enter discount!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                if (!$.isNumeric($('#manual_discount_<?php echo $i;?>').val()))
                                                                                                {
                                                                                                $('#required_manual_discount_<?php echo $i;?>').show();
                                                                                                        $('#required_manual_discount_<?php echo $i;?>').html('Please enter numeric value for discount!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                $('#required_manual_discount_<?php echo $i;?>').hide();
                                                                                                        $('#required_manual_discount_<?php echo $i;?>').html('');
                                                                                                }
                                                                                                }
                                                                                                if ($.trim($('#manual_coupon_active_<?php echo $i;?>').val()) == ''){
                                                                                                $('#required_manual_coupon_active_<?php echo $i;?>').show();
                                                                                                        $('#required_manual_coupon_active_<?php echo $i;?>').html('Please enter coupon active time!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                if (!$.isNumeric($('#manual_coupon_active_<?php echo $i;?>').val()))
                                                                                                {
                                                                                                $('#required_manual_coupon_active_<?php echo $i;?>').show();
                                                                                                        $('#required_manual_coupon_active_<?php echo $i;?>').html('Please enter numeric value for active time!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                $('#required_manual_coupon_active_<?php echo $i;?>').hide();
                                                                                                        $('#required_manual_coupon_active_<?php echo $i;?>').html('');
                                                                                                }
                                                                                                }
                                                                                                if ($.trim($('#manual_minimum_amount_<?php echo $i;?>').val()) == ''){
                                                                                                $('#required_manual_minimum_amount_<?php echo $i;?>').show();
                                                                                                        $('#required_manual_minimum_amount_<?php echo $i;?>').html('Please enter minimum amount!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                if (!$.isNumeric($('#manual_minimum_amount_<?php echo $i;?>').val()))
                                                                                                {
                                                                                                $('#required_manual_minimum_amount_<?php echo $i;?>').show();
                                                                                                        $('#required_manual_minimum_amount_<?php echo $i;?>').html('Please enter numeric value for minimum amount!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                $('#required_manual_minimum_amount_<?php echo $i;?>').hide();
                                                                                                        $('#required_manual_minimum_amount_<?php echo $i;?>').html('');
                                                                                                }
                                                                                                }
                                                                                                if ($.trim($('#manual_email_subject_<?php echo $i;?>').val()) == ''){
                                                                                                $('#required_manual_email_subject_<?php echo $i;?>').show();
                                                                                                        $('#required_manual_email_subject_<?php echo $i;?>').html('Please enter email subject!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                $('#required_manual_email_subject_<?php echo $i;?>').hide();
                                                                                                        $('#required_manual_email_subject_<?php echo $i;?>').html('');
                                                                                                }

                                                                                                if ($("#manual_auto_email_<?php echo $i;?>").attr('checked')){
                                                                                                var auto_email = 1;
                                                                                                } else{
                                                                                                var auto_email = 0;
                                                                                                }

                                                                                                if (validate == 0){

                                                                                                var editor_content = CKEDITOR.instances.registeredemailbody<?php echo $i;?> .getData();
                                                                                                            // code added by Nitin Jain to fix &nbsp issue while sending manual mail
                                                                                                            editor_content = editor_content.replace('&nbsp;', ' ');
                                                                                                            editor_content = editor_content.replace('&nbsp; ', ' ');
                                                                                                            editor_content = editor_content.replace(' &nbsp;', ' ');
                                                                                                            editor_content = editor_content.replace('. &nbsp;', '. ');
                                                                                                            $.ajax({
                                                                                                            url: 'index.php?route=module/abandonedcartincentive/sendCustomEmail&token=<?php echo $token; ?>',
                                                                                                                    type: 'post',
                                                                                                                    data: 'manual_email_id=' + $("#manual_email_id_<?php echo $i;?>").val() + '&manual_fname=' + $("#manual_fname_<?php echo $i;?>").val()
                                                                                                                    + '&manual_lname=' + $("#manual_lname_<?php echo $i;?>").val() + '&manual_cartid=' + $("#manual_cartid_<?php echo $i;?>").val()
                                                                                                                    + '&manual_discount=' + $("#manual_discount_<?php echo $i;?>").val() + '&manual_discounttype=' + $("#manual_discounttype_<?php echo $i;?>").val()
                                                                                                                    + '&manual_coupon_active=' + $("#manual_coupon_active_<?php echo $i;?>").val() + '&manual_minimum_amount=' + $("#manual_minimum_amount_<?php echo $i;?>").val()
                                                                                                                    + '&manual_email_subject=' + $("#manual_email_subject_<?php echo $i;?>").val() + '&manual_email_body=' + editor_content
                                                                                                                    + '&manual_auto_email=' + auto_email,
                                                                                                                    beforeSend: function() {
                                                                                                                    $('#manual-email-form').fadeTo('slow', 0.9);
                                                                                                                    },
                                                                                                                    complete: function() {
                                                                                                                    $('#manual-email-form').fadeTo('slow', 1);
                                                                                                                    },
                                                                                                                    success: function(response) {

                                                                                                                    $('.gritter-add-primary').trigger('click');
                                                                                                                            $('#button_close_modal').trigger('click');
                                                                                                                    }
                                                                                                            });
                                                                                                    } else{
                                                                                                    return false;
                                                                                                    }
                                                                                                    });                                                                            </script>
                                                                                        </div>
                                                                                        <!-- Modal Email END -->

                                                                                        <!-- Modal Cart Starts -->
                                                                                        <div class="modal fade" id="modal-cart<?php echo $i;?>" style="width: 800px; height: 570px; left: 40% !important; overflow-y:hidden !important;">

                                                                                            <!-- Modal heading -->
                                                                                            <div class="modal-header">
                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                                                                                                <h3><?php echo $customer['firstname'];?>&nbsp;<?php echo $customer['lastname'];?> <?php echo $aci_text_cart_items; ?></h3>
                                                                                            </div>
                                                                                            <!-- // Modal heading END -->

                                                                                            <!-- Modal body -->
                                                                                            <div class="modal-body">
                                                                                <?php //$this->load->model('tool/image'); ?>
                                                                                                <div style="overflow-y:auto !important; height:430px;">
                                                                                                    <table id="velsof-aci-popup-cart-table">
                                                                                                        <thead>
                                                                                                            <tr>                                                                                           
                                                                                                                <td class="velsof-aci-popup-th-image"><?php echo $aci_text_image; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-name"><?php echo $aci_text_items; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-model"><?php echo $aci_text_model; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-quantity"><?php echo $aci_text_qty; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-price"><?php echo $aci_text_unit_price; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-total"><?php echo $aci_text_total; ?></td>
                                                                                                            </tr>
                                                                                                        </thead>


                                                                                   <?php $s=0; 
                                                                                     $quantity=0;
                                                                                     //echo "<pre>"; print_r($products); echo "</pre>";
                                                                                    foreach($products[$i] as $produc) { 
                                                                                    $produc[$i][$s] = $produc;
                                                                                    ?>
                                                                                                        <tbody>                                                                                    
                                                                                                            <tr height="100px;">
                                                                                                                <td class="velsof-aci-popup-td-image">
                                                                                                                    <a href="<?php echo '../index.php?route=product/product&product_id='. $produc[$i][$s]['product_id']; ?>" target="_blank">
                                                                                                                        <img src="<?php echo $image[$i][$s]; ?>" />
                                                                                                                    </a>
                                                                                                                </td>
                                                                                                                <td class="velsof-aci-popup-td-name">                                                                                            
                                                                                                                    <a href="<?php echo HTTP_CATALOG.'index.php?route=product/product&product_id='. $produc[$i][$s]['product_id']; ?>" target="_blank" style="padding:10px;"><?php echo $produc[$i][$s]['name']; ?></a>
                                                                                                                </td>
                                                                                                                <td class="velsof-aci-popup-td-quantity"><?php echo $produc[$i][$s]['model']; ?></td>
                                                                                                                <td class="velsof-aci-popup-td-model"><?php 
                                                                                       
                                                                                            $quantity=$produc[$i][$s]['quantity']+$quantity;
                                                                                            echo $produc[$i][$s]['quantity']; ?></td>                                                                                        
                                                                                                                <td class="velsof-aci-popup-td-price"><?php echo $product[$i][$s]['price']; ?></td>
                                                                                                                <td class="velsof-aci-popup-td-total"><?php echo $product[$i][$s]['total']; ?></td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                  <?php $s++;} ?>
                                                                                                        <tfoot>
                                                                                                            <tr>
                                                                                                                <td colspan="3" style="text-align:right;padding-right:5px;"><?php echo $aci_text_total; ?>&nbsp;:&nbsp; </td>
                                                                                                                <td style="text-align:center;padding-left:5px;"><?php 
                                                                                            echo $quantity; ?></td>
                                                                                                                <td></td>
                                                                                                                <td style="text-align:center;padding-left:5px;"><?php echo $abandoned_cart_total[$i]; ?></td>

                                                                                                            </tr>
                                                                                                        </tfoot>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                            <!-- // Modal body END -->

                                                                                            <!-- Modal footer -->
                                                                                            <div class="modal-footer">
                                                                                                <a href="#" data-dismiss="modal" class="btn btn-default">Close</a>
                                                                                            </div>
                                                                                            <!-- // Modal footer END -->

                                                                                        </div>
                                                                                        <!-- Modal Cart END -->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                            <?php $i++; } ?>
<?php } ?>
                                                                            <tfoot >
                                                                                <tr>
                                                                                    <td colspan="5" style="padding:7px;">
                                                                                        <div class="pagination">
                                                                            <?php  if(isset($registeredAbCartPagination)){
                                                                             echo $registeredAbCartPagination; echo $registeredAbCartPagination_results; }  ?>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>

                                                                    <!-- Guest Customers Tab -->
                                                                    <div id="tab-abandoned-cart-guest" class="tab-pane">
                                                                        <table class="list form">
                                                                            <thead>
                                                                                <tr>
                                                                                    <td class="left" style="width:15px;"></td>
                                                                                    <td class="left" style="width:100px;"><?php echo $aci_text_customer; ?></td>
                                                                                    <td class="left" style="width:150px;"><?php echo $aci_text_current_incentive; ?></td>
                                                                                    <td class="left" style="width:120px;"><?php echo $aci_text_date_time; ?></td>
                                                                                    <td class="left" style="width:70px;"><?php echo $aci_text_action; ?></td>
                                                                                </tr>
                                                                            </thead>

                                                            <?php if(!empty($aci_abandoned_guest_carts)) { 
                                                        
                                                            $i = 1;foreach($aci_abandoned_guest_carts as $abandonedcart){
                                                            $customer = unserialize($abandonedcart['customer_info']); ?>
                                                                            <tbody id="guest<?php echo $i;?>">
                                                                                <tr>
                                                                                    <td style="width:15px;"><?php echo $i;?></td>
                                                                                    <td style="width:100px;">
                                                                                        <p style="margin-bottom: 15px;">
                                                                                            <span class="glyphicons customer_info user" style="cursor: pointer;"><i></i></span>
                                                                            <?php echo $customer['firstname'];?>&nbsp;<?php echo $customer['lastname'];?>
                                                                                        </p>
                                                                                        <p style="margin-bottom: 22px;">
                                                                                            <span class="glyphicons customer_info message_empty" style="cursor: pointer;"><i></i></span>
                                                                            <?php echo $customer['email'];?>
                                                                                        </p>
                                                                                        <p>
                                                                                            <span class="glyphicons customer_info iphone" style="cursor: pointer;"><i></i></span>
                                                                            <?php echo $customer['telephone'];?>
                                                                                        </p>
                                                                                    </td>
                                                                                    <td style="width:150px;">
                                                                    <?php 
                                                                        if(!empty($guest_coupons[$i])) { ?>
                                                                                        <p>Coupon code : <?php echo $guest_coupons[$i][0]['code'];?></p>
                                                                                        <p>Discount :
                                                                            <?php if($guest_coupons[$i][0]['type'] == 'F')
                                                                            echo $guest_fixed_amount[$i][0];
                                                                            else
                                                                            echo $guest_coupons[$i][0]['discount'].'%';?>
                                                                                        </p>
                                                                                        <p>Start Date : <?php echo $guest_coupons[$i][0]['date_start'];?></p>
                                                                                        <p>End Date : <?php echo $guest_coupons[$i][0]['date_end'];?></p>
                                                                                        <p style="text-align: right;cursor: pointer;text-decoration: underline;" id="tab-abandoned-cart-guest-previouscoupons-<?php echo $i; ?>">
                                                                                            <a href="#modal-guest-coupons<?php echo $i;?>" data-toggle="modal"><?php echo $aci_text_previous_coupons;?></a>
                                                                                        </p>
                                                                        <?php } else { ?>
                                                                                        <p><?php echo $aci_text_no_coupon_code_sent;?></p>
                                                                        <?php }  ?>

                                                                                        <!-- Modal Coupons Starts -->
                                                                                        <div class="modal  fade" id="modal-guest-coupons<?php echo $i;?>" style="width: 600px; height: 500px;">

                                                                                            <!-- Modal heading -->
                                                                                            <div class="modal-header">
                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                <!-- <h3>Send Coupon Email</h3> -->
                                                                                                <h3><?php echo $customer['firstname'];?>&nbsp;<?php echo $customer['lastname'];?> -- <?php echo $aci_text_previous_coupons;?></h3>
                                                                                            </div>
                                                                                            <!-- Modal heading END -->

                                                                                            <!-- Modal body -->
                                                                                            <div class="modal-body">
                                                                                                <table class="list form">
                                                                                                    <thead>
                                                                                                        <tr>
                                                                                                            <td width='10%'></td>
                                                                                                            <td width='20%'><?php echo $aci_text_coupon;?></td>
                                                                                                            <td width='15%'><?php echo $aci_text_discount;?></td>
                                                                                                            <td width='15%'><?php echo $aci_text_min_amt;?></td>
                                                                                                            <td width='15%'><?php echo $aci_text_created_on;?></td>
                                                                                                            <td width='15%'><?php echo $aci_text_expired_on;?></td>
                                                                                                        </tr>
                                                                                                    </thead>

                                                                                    <?php $couponrow = 0;foreach($model_guest_coupons[$i] as $model_guest_coupon) { ?>

                                                                                    <?php if($couponrow > 0) { ?>
                                                                                                    <tbody id='coupon-row<?php echo $couponrow;?>'>
                                                                                                        <tr>
                                                                                                            <td><?php echo $couponrow;?></td>
                                                                                                            <td><?php echo $model_guest_coupon['code'];?></td>
                                                                                                            <?php if($model_guest_coupon['type'] == 'F') { ?>
                                                                                                                <td><?php echo $guest_discount_amount[$i][$couponrow];?></td>
                                                                                                                <td><?php echo $guest_discount_total[$i][$couponrow];?></td>
                                                                                                            <?php } else{ ?>
                                                                                                                <td><?php echo $model_guest_coupon['discount'];?></td>
                                                                                                                <td><?php echo $model_guest_coupon['total'];?></td>
                                                                                                            <?php } ?>
                                                                                                            <td><?php echo $model_guest_coupon['date_start'];?></td>
                                                                                                            <td><?php echo $model_guest_coupon['date_end'];?></td>
                                                                                                        </tr>
                                                                                                    </tbody>
                                                                                    <?php } ?>
                                                                                    <?php $couponrow++; } ?>
                                                                                                </table>
                                                                                            </div>
                                                                                            <!-- Modal body END -->

                                                                                            <!-- Modal footer -->
                                                                                            <div class="modal-footer">
                                                                                                <a href="#" data-dismiss="modal" class="btn btn-default">Close</a>
                                                                                            </div>
                                                                                            <!-- Modal footer END -->

                                                                                        </div>
                                                                                        <!-- Modal Coupons END -->
                                                                                    </td>
                                                                                    <td style="width:120px;">
                                                                                        <p><?php echo $aci_text_first_visit; ?><?php echo $abandonedcart['date_created']; ?></p>
                                                                                        <p><?php echo $aci_text_last_visit; ?><?php echo $abandonedcart['date_modified']; ?></p>
                                                                                        <p><?php echo $aci_text_time_spent; ?><?php $time = strtotime($abandonedcart['date_modified']) - strtotime($abandonedcart['date_created']); echo gmdate("H:i:s", $time) ?></p>
                                                                                    </td>
                                                                                    <td style="width:70px; text-align: center;">
                                                                        <?php if(!empty($customer['email'])) { ?>
                                                                                        <a href="#modal-guest-email<?php echo $i;?>" data-toggle="modal"><span style="padding:5px;"><img src='./view/image/aci/email.png' alt='Email' style="width:24px; height:24px; cursor:pointer;" id="tab-abandoned-cart-guest-sendemail-<?php echo $i; ?>"/></span></a>
                                                                        <?php } ?>
                                                                                        <a href="#modal-guest-cart<?php echo $i;?>" data-toggle="modal"><span style="padding:10px;"><img src='./view/image/aci/cart.png' alt='Cart' style="width:24px; height:24px; cursor:pointer;" id="tab-abandoned-cart-guest-cartproducts-<?php echo $i; ?>"/></span></a>
                                                                                        <input type='hidden' id='guest_cart_id_<?php echo $i;?>' value="<?php echo $abandonedcart['id'];?>"/>
                                                                                        <span style="padding:0px;"><a id="modals-bootbox-guest-confirm<?php echo $i;?>" ><img src='./view/image/aci/remove.png' alt='Remove' style="width:24px; height:24px; cursor:pointer;" /></a></span>
                                                                                        <script type="text/javascript">
                                                                                                    $('#modals-bootbox-guest-confirm<?php echo $i;?>').on('click', function() {
                                                                                            bootbox.confirm("Are you sure to delete this abandoned cart?", function(result)
                                                                                            {
                                                                                            if (result)
                                                                                            {
                                                                                            $.ajax({
                                                                                            url: 'index.php?route=module/abandonedcartincentive/deleteAbandonedCart&token=<?php echo $token; ?>',
                                                                                                    type: 'post',
                                                                                                    data: 'cart_id=' + $('#guest_cart_id_<?php echo $i;?>').val(),
                                                                                                    success: function(response) {

                                                                                                    $('.gritter-add-primary').trigger('click');
                                                                                                            $('#button_close_modal').trigger('click');
                                                                                                            $('#guest<?php echo $i;?>').remove();
                                                                                                    }
                                                                                            });
                                                                                            }
                                                                                            });
                                                                                            });                                                                        </script>

                                                                                        <!-- Modal Email Starts -->
                                                                                        <div class="modal  fade" id="modal-guest-email<?php echo $i;?>" style="width: 75%; height: 630px; left:12% !important; top:2% !important; margin-left: 0px !important;overflow-y:hidden !important;">

                                                                                            <!-- Modal heading -->
                                                                                            <div class="modal-header">
                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                <h3><?php echo $customer['firstname'];?>&nbsp;<?php echo $customer['lastname'];?> <?php echo $aci_text_coupon_email; ?></h3>
                                                                                            </div>
                                                                                            <!-- Modal heading END -->
                                                                                            <!-- Modal body -->
                                                                                            <div class="modal-body">
                                                                                                <form id="manual-email-form">
                                                                                                    <div style="overflow-y:auto !important; height:500px;">
                                                                                                        <table class="list form" style="width:1000px; text-align: center;">
                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><?php echo $aci_entry_incentive_table_discount_type; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_incentive_table_discount_type; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >
                                                                                                                    <select class="form-control" name="aci[manual][email][discount_type]" id="guest_manual_discounttype_<?php echo $i;?>">
                                                                                                                        <option value="F" selected="selected"><?php echo $aci_entry_incentive_table_discount_type_fixed; ?></option>
                                                                                                                        <option value="P"><?php echo $aci_entry_incentive_table_discount_type_percentage; ?></option>
                                                                                                                    </select>
                                                                                                                    <input type="hidden" name="aci[manual][email][emailid]" value="<?php echo $customer['email'];?>" class="form-control" style="width:80%;" id="guest_manual_email_id_<?php echo $i;?>"/>
                                                                                                                    <input type="hidden" name="aci[manual][email][firstname]" value="<?php echo $customer['firstname'];?>" class="form-control" style="width:80%;" id="guest_manual_fname_<?php echo $i;?>"/>
                                                                                                                    <input type="hidden" name="aci[manual][email][lastname]" value="<?php echo $customer['lastname'];?>" class="form-control" style="width:80%;" id="guest_manual_lname_<?php echo $i;?>"/>
                                                                                                                    <input type="hidden" name="aci[manual][email][cart_id]" value="<?php echo $abandonedcart['id'];?>" class="form-control" style="width:80%;" id="guest_manual_cartid_<?php echo $i;?>"/>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><span class="required"> *</span><?php echo $aci_entry_incentive_table_discount; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_incentive_table_discount; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >
                                                                                                                    <input type="text" name="aci[manual][email][discount]" value="" class="form-control" style="width:80%;" id="guest_manual_discount_<?php echo $i;?>"/>
                                                                                                                    <br><span id="required_guest_manual_discount_<?php echo $i;?>" style="display: none;float: left;padding-left: 15px;" class="required"></span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><span class="required"> *</span><?php echo $aci_entry_incentive_table_coupon_active_time; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_incentive_table_coupon_active_time; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >
                                                                                                                    <input type="text" name="aci[manual][email][coupon_active_time]" value="" class="form-control" style="width:80%;" id="guest_manual_coupon_active_<?php echo $i;?>"/>
                                                                                                                    <br><span id="required_guest_manual_coupon_active_<?php echo $i;?>" style="display: none;float: left;padding-left: 15px;" class="required"></span>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><span class="required"> *</span><?php echo $aci_entry_incentive_table_miminum_amount; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_incentive_table_miminum_amount; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >
                                                                                                                    <input type="text" name="aci[manual][email][minimum_amount]" value="" class="form-control" style="width:80%;" id="guest_manual_minimum_amount_<?php echo $i;?>"/>
                                                                                                                    <br><span id="required_guest_manual_minimum_amount_<?php echo $i;?>" style="display: none;float: left;padding-left: 15px;" class="required"></span>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><span class="required"> *</span><?php echo $aci_entry_email_subject; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_email_subject; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;">
                                                                                                                    <input type="text" name="aci[manual][email][subject]" value="<?php echo $aci['email']['subject']; ?>" class="form-control" style="width:80%;" id="guest_manual_email_subject_<?php echo $i;?>"/>
                                                                                                                    <br><span id="required_guest_manual_email_subject_<?php echo $i;?>" style="display: none;float: left;padding-left: 15px;" class="required"></span>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><?php echo $aci_entry_email_body; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_email_body; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;">
                                                                                                                    <textarea name="aci[manual][email][body]" id="guestemailbody<?php echo $i;?>">
                                                                                                        <?php echo $aci['email']['body']; ?>
                                                                                                                    </textarea>
                                                                                                                    <script type="text/javascript">
                                                                                                                                <!--
                                                                                                                function CKupdate(){
                                                                                                                                for (instance in CKEDITOR.instances)
                                                                                                                                        CKEDITOR.instances[instance].updateElement();
                                                                                                                                }
                                                                                                                        CKEDITOR.replace('guestemailbody<?php echo $i;?>', {
                                                                                                                        filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
                                                                                                                                height: '250px', width: '720px'
                                                                                                                        });
                                                                                                                                //-->
                                                                                                                    </script>
                                                                                                                </td>
                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                <td class="name" width="100px;"><span class="control-label"><?php echo $aci_entry_email_stop; ?></span>
                                                                                                                    <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="<?php echo $aci_tooltip_email_stop; ?>"></i>
                                                                                                                </td>
                                                                                                                <td class="settings" width="800px;" >

                                                                                                                    <input <?php if($IE7==true){ echo'class="checkbox"'; }else{ echo'class="make-switch"'; } ?>  type="checkbox" value="0" name="aci[manual][email][remove_ab_cart]" id="guest_manual_auto_email_<?php echo $i;?>" />

                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </form>
                                                                                            </div>
                                                                                            <!-- Modal body END -->

                                                                                            <!-- Modal footer -->
                                                                                            <div class="modal-footer">
                                                                                                <a href="#" class="btn btn-default" data-dismiss="modal"><?php echo $aci_entry_email_cancel; ?></a>
                                                                                                <a href="#" data-dismiss="modal" class="btn btn-primary" id="guest_sendemail<?php echo $i;?>"><?php echo $aci_entry_email_send; ?></a>
                                                                                            </div>
                                                                                            <!-- Modal footer END -->

                                                                                            <script>

                                                                                                        $('#guest_sendemail<?php echo $i;?>').on('click', function() {
                                                                                                var validate = 0;
                                                                                                        if ($.trim($('#guest_manual_discount_<?php echo $i;?>').val()) == ''){
                                                                                                $('#required_guest_manual_discount_<?php echo $i;?>').show();
                                                                                                        $('#required_guest_manual_discount_<?php echo $i;?>').html('Please enter discount!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                if (!$.isNumeric($('#guest_manual_discount_<?php echo $i;?>').val()))
                                                                                                {
                                                                                                $('#required_guest_manual_discount_<?php echo $i;?>').show();
                                                                                                        $('#required_guest_manual_discount_<?php echo $i;?>').html('Please enter numeric value for discount!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                $('#required_guest_manual_discount_<?php echo $i;?>').hide();
                                                                                                        $('#required_guest_manual_discount_<?php echo $i;?>').html('');
                                                                                                }
                                                                                                }
                                                                                                if ($.trim($('#guest_manual_coupon_active_<?php echo $i;?>').val()) == ''){
                                                                                                $('#required_guest_manual_coupon_active_<?php echo $i;?>').show();
                                                                                                        $('#required_guest_manual_coupon_active_<?php echo $i;?>').html('Please enter coupon active time!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                if (!$.isNumeric($('#guest_manual_coupon_active_<?php echo $i;?>').val()))
                                                                                                {
                                                                                                $('#required_guest_manual_coupon_active_<?php echo $i;?>').show();
                                                                                                        $('#required_guest_manual_coupon_active_<?php echo $i;?>').html('Please enter numeric value for active time!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                $('#required_guest_manual_coupon_active_<?php echo $i;?>').hide();
                                                                                                        $('#required_guest_manual_coupon_active_<?php echo $i;?>').html('');
                                                                                                }
                                                                                                }
                                                                                                if ($.trim($('#guest_manual_minimum_amount_<?php echo $i;?>').val()) == ''){
                                                                                                $('#required_guest_manual_minimum_amount_<?php echo $i;?>').show();
                                                                                                        $('#required_guest_manual_minimum_amount_<?php echo $i;?>').html('Please enter minimum amount!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                if (!$.isNumeric($('#guest_manual_minimum_amount_<?php echo $i;?>').val()))
                                                                                                {
                                                                                                $('#required_guest_manual_minimum_amount_<?php echo $i;?>').show();
                                                                                                        $('#required_guest_manual_minimum_amount_<?php echo $i;?>').html('Please enter numeric value for minimum amount!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                $('#required_guest_manual_minimum_amount_<?php echo $i;?>').hide();
                                                                                                        $('#required_guest_manual_minimum_amount_<?php echo $i;?>').html('');
                                                                                                }
                                                                                                }
                                                                                                if ($.trim($('#guest_manual_email_subject_<?php echo $i;?>').val()) == ''){
                                                                                                $('#required_guest_manual_email_subject_<?php echo $i;?>').show();
                                                                                                        $('#required_guest_manual_email_subject_<?php echo $i;?>').html('Please enter email subject!');
                                                                                                        validate = 1;
                                                                                                } else{
                                                                                                $('#required_guest_manual_email_subject_<?php echo $i;?>').hide();
                                                                                                        $('#required_guest_manual_email_subject_<?php echo $i;?>').html('');
                                                                                                }

                                                                                                if ($("#guest_manual_auto_email_<?php echo $i;?>").attr('checked')){
                                                                                                var auto_email = 1;
                                                                                                } else{
                                                                                                var auto_email = 0;
                                                                                                }

                                                                                                if (validate == 0){


                                                                                                var guest_editor_content = CKEDITOR.instances.guestemailbody<?php echo $i;?> .getData();
                                                                                                            guest_editor_content = guest_editor_content.replace('&nbsp;', ' ');
                                                                                                            guest_editor_content = guest_editor_content.replace('&nbsp; ', ' ');
                                                                                                            guest_editor_content = guest_editor_content.replace(' &nbsp;', ' ');
                                                                                                            guest_editor_content = guest_editor_content.replace('. &nbsp;', '. ');
                                                                                                            $.ajax({
                                                                                                            url: 'index.php?route=module/abandonedcartincentive/sendCustomEmail&token=<?php echo $token; ?>',
                                                                                                                    type: 'post',
                                                                                                                    data: 'manual_email_id=' + $("#guest_manual_email_id_<?php echo $i;?>").val() + '&manual_fname=' + $("#guest_manual_fname_<?php echo $i;?>").val()
                                                                                                                    + '&manual_lname=' + $("#guest_manual_lname_<?php echo $i;?>").val() + '&manual_cartid=' + $("#guest_manual_cartid_<?php echo $i;?>").val()
                                                                                                                    + '&manual_discount=' + $("#guest_manual_discount_<?php echo $i;?>").val() + '&manual_discounttype=' + $("#guest_manual_discounttype_<?php echo $i;?>").val()
                                                                                                                    + '&manual_coupon_active=' + $("#guest_manual_coupon_active_<?php echo $i;?>").val() + '&manual_minimum_amount=' + $("#guest_manual_minimum_amount_<?php echo $i;?>").val()
                                                                                                                    + '&manual_email_subject=' + $("#guest_manual_email_subject_<?php echo $i;?>").val() + '&manual_email_body=' + guest_editor_content
                                                                                                                    + '&manual_auto_email=' + auto_email,
                                                                                                                    beforeSend: function() {
                                                                                                                    $('#form').fadeTo('slow', 0.9);
                                                                                                                    },
                                                                                                                    complete: function() {
                                                                                                                    $('#form').fadeTo('slow', 1);
                                                                                                                    },
                                                                                                                    success: function(response) {
                                                                                                                    $('.gritter-add-primary').trigger('click');
                                                                                                                            $('#button_close_modal').trigger('click');
                                                                                                                    }
                                                                                                            });
                                                                                                    } else{
                                                                                                    return false;
                                                                                                    }
                                                                                                    });                                                                            </script>
                                                                                        </div>
                                                                                        <!-- Modal Email END -->

                                                                                        <!-- Modal Cart Starts -->
                                                                                        <div class="modal  fade" id="modal-guest-cart<?php echo $i;?>" style="width: 800px; height: 570px; left: 40% !important; overflow-y:hidden !important;">

                                                                                            <!-- Modal heading -->
                                                                                            <div class="modal-header">
                                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                                                                <!-- <h3>Send Coupon Email</h3> -->
                                                                                                <h3><?php echo $customer['firstname'];?>&nbsp;<?php echo $customer['lastname'];?> <?php echo $aci_text_cart_items; ?></h3>
                                                                                            </div>
                                                                                            <!-- // Modal heading END -->

                                                                                            <!-- Modal body -->
                                                                                            <div class="modal-body">
                                                                                <?php 
                                                                                //$this->load->model('tool/image'); ?>
                                                                                                <div style="overflow-y:auto !important; height:430px;">
                                                                                                    <table id="velsof-aci-popup-cart-table">
                                                                                                        <thead>
                                                                                                            <tr>                                                                                           
                                                                                                                <td class="velsof-aci-popup-th-image"><?php echo $aci_text_image; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-name"><?php echo $aci_text_items; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-model"><?php echo $aci_text_model; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-quantity"><?php echo $aci_text_qty; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-price"><?php echo $aci_text_unit_price; ?></td>
                                                                                                                <td class="velsof-aci-popup-th-total"><?php echo $aci_text_total; ?></td>
                                                                                                            </tr>
                                                                                                        </thead>

                                                                            <?php   $t=0; 
                                                                                    $guest_quantity=0;
                                                                                    foreach($guest_products[$i] as $guest_produc) { 
                                                                                    $guest_produc[$i][$t] = $guest_produc;
                                                                                    ?>

                                                                                                        <tbody>                                                                                    
                                                                                                            <tr height="100px;">
                                                                                                                <td class="velsof-aci-popup-td-image">
                                                                                                                    <a href="<?php echo '../index.php?route=product/product&product_id='. $guest_produc[$i][$t]['product_id']; ?>" target="_blank">
                                                                                                                        <img src="<?php echo $guest_image[$i][$t]; ?>" />
                                                                                                                    </a>
                                                                                                                </td>
                                                                                                                <td class="velsof-aci-popup-td-name">                                                                                            
                                                                                                                    <a href="<?php echo HTTP_CATALOG.'index.php?route=product/product&product_id='. $guest_produc[$i][$t]['product_id']; ?>" target="_blank" style="padding:10px;"><?php echo $guest_produc[$i][$t]['name']; ?></a>
                                                                                                                </td>
                                                                                                                <td class="velsof-aci-popup-td-quantity"><?php echo $guest_produc[$i][$t]['model']; ?></td>
                                                                                                                <td class="velsof-aci-popup-td-model"><?php echo $guest_produc[$i][$t]['quantity'];
                                                                                        $guest_quantity = $guest_quantity + $guest_produc[$i][$t]['quantity']; ?></td>                                                                                        
                                                                                                                <td class="velsof-aci-popup-td-price"><?php  echo $guest_product[$i][$t]['price']; ?></td>
                                                                                                                <td class="velsof-aci-popup-td-total"><?php echo $guest_product[$i][$t]['total']; ?></td>
                                                                                                            </tr>
                                                                                                        </tbody>
                                                                                   <?php $t++;} ?>

                                                                                                        <tfoot>
                                                                                                            <tr>
                                                                                                                <td colspan="3" style="text-align:right;padding-right:5px;"><?php echo $aci_text_total; ?>&nbsp;:&nbsp; </td>
                                                                                                                <td style="text-align:center;padding-left:5px;"><?php echo $guest_quantity; ?></td>
                                                                                                                <td></td>
                                                                                                                <td style="text-align:center;padding-left:5px;"><?php echo $abandoned_guest_cart_total[$i]; ?></td>

                                                                                                            </tr>
                                                                                                        </tfoot>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </div>
                                                                                            <!-- // Modal body END -->

                                                                                            <!-- Modal footer -->
                                                                                            <div class="modal-footer">
                                                                                                <a href="#" data-dismiss="modal" class="btn btn-default">Close</a>
                                                                                            </div>
                                                                                            <!-- // Modal footer END -->

                                                                                        </div>
                                                                                        <!-- Modal Cart END -->
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                            <?php $i++; } ?>
<?php } ?>
                                                                            <tfoot >
                                                                                <tr>
                                                                                    <td colspan="5" style="padding:7px;">
                                                                                        <div class="pagination">
                                                                            <?php if(isset($guestAbCartPagination)) { echo $guestAbCartPagination; echo $guestAbCartPagination_results; } ?>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                        <div class="widget" data-toggle="collapse-widget">
                                                                            <div class="widget-head">
                                                                                <h3 class="heading"><?php echo $aci_text_guest_ab_cart_requirement; ?></h3>
                                                                            </div>
                                                                            <div class="widget-body">
                                                                                <p>
                                                                                    <label style="width: 650px; border: 0px solid #D1D3D4; color:#A7A7A7; font-size: 15px; font-weight:normal;">
                                                                       <?php echo $aci_text_guest_ab_cart_message; ?> <br />
                                                                                    </label>
                                                                                </p>
                                                                            </div>
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div></div>
                                        </div>
                                        <!-- View Abandoned Cart Tab Ends -->

                                        <!-- View Conversion Rates Tab Starts -->

                                        <div id="aci_tab_conversion_rate" class="tab-pane">
                                            <div style="margin-top:12px;" class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><i class="fa fa-wrench"></i> <?php echo $aci_tab_conversion_rate; ?></h3>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="blfock">



                                                        <!-- Converted Orders Table Starts -->
                                                        <div class="innerLR">                                            
                                                            <div class="widget">
                                                                <div class="widget-head">
                                                                    <h4 class="heading"><?php echo $aci_text_converted_orders;  ?></h4>
                                                                </div>
                                                                <div class="widget-body">


                                                                    <!-- Converted Orders List Starts -->
                                                                    <table id="tblConvertedOrderList" class="dynamicTable table table-striped table-bordered table-condensed dataTable table-vertical-center" aria-describedby="tblConvertedOrderList_info">
                                                                        <thead>
                                                                            <tr role="row">
                                                                                <th class="center" role="columnheader" tabindex="0" aria-controls="tblUsageReport" rowspan="1" colspan="1" style="width: 8%;"></th>
                                                                                <th class="center" role="columnheader" tabindex="0" aria-controls="tblUsageReport" rowspan="1" colspan="1" style="width: 8%;"><?php echo $aci_text_order_id; ?></th>
                                                                                <th class="center" role="columnheader" tabindex="0" aria-controls="tblUsageReport" rowspan="1" colspan="1" style="width: 30%;" ><?php echo $aci_text_customer; ?></th>
                                                                                <th class="center" role="columnheader" tabindex="0" aria-controls="tblUsageReport" rowspan="1" colspan="1" style="width: 14%;"><?php echo $aci_text_status; ?></th>
                                                                                <th class="center" role="columnheader" tabindex="0" aria-controls="tblUsageReport" rowspan="1" colspan="1" style="width: 10%;" ><?php echo $aci_text_total; ?></th>
                                                                                <th class="center" role="columnheader" tabindex="0" aria-controls="tblUsageReport" rowspan="1" colspan="1" style="width: 20%;" ><?php echo $aci_text_date_time; ?></th>
                                                                                <th class="center" role="columnheader" tabindex="0" aria-controls="tblUsageReport" rowspan="1" colspan="1" style="width: 10%;" ><?php echo $aci_text_action; ?></th>
                                                                            </tr>
                                                                        </thead>
                                                        <?php if(!empty($aci_converted_carts)) {?>

                                                                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                            <?php $i=0;  foreach($final_converted_data as $final_converted_datas){ ?>


                                                            <?php if(isset($order_status_converted[$i]['name']) && ($order[$i]['order_status_id'] != 0)){
                                                
                                                                   $order_status_name = $order_status_converted[$i]['name'];
                                                                }else{
                                                                    $order_status_name = "No Status";
                                                                }
                                                            ?>
                                                                            <tr>
                                                                                <td><?php echo $i; ?></td>
                                                                                <td><?php echo $final_converted_data[$i]['order_id'];?></td>
                                                                                <td>
                                                                                    <p style="margin-bottom: 15px;">
                                                                                        <span class="glyphicons customer_info user" style="cursor: pointer;"><i></i></span>
                                                                        <?php 
                                                                        $customer=unserialize($final_converted_data[$i]['customer_info']);
                                                                        echo $customer['firstname']; ?>&nbsp;
                                                                        <?php echo $customer['lastname']; ?>
                                                                                    </p>
                                                                                    <p style="margin-bottom: 22px;">
                                                                                        <span class="glyphicons customer_info iphone" style="cursor: pointer;"><i></i></span>
                                                                        <?php echo $customer['telephone']; ?>
                                                                                    </p>
                                                                                    <p>
                                                                                        <span class="glyphicons customer_info message_empty" style="cursor: pointer;"><i></i></span>
                                                                        <?php echo $customer['email']; ?>
                                                                                    </p>
                                                                                </td>
                                                                                <td><?php echo $order_status_name;?></td>
                                                                                <td><?php echo  $total_view_converted[$i]; ?></td>
                                                                                <td><?php echo $final_converted_data[$i]['date_modified']; ?></td>
                                                                                <td>
                                                                                    <a href="<?php echo HTTP_SERVER.'index.php?route=sale/order/info&order_id='. $final_converted_data[$i]['order_id'].'&token='.$token; ?>" target="_blank">View</a>
                                                                                </td>
                                                                            </tr>
                                                            <?php $i++; } ?>
                                                                        </tbody>
                                                        <?php } ?>
                                                                    </table>
                                                                    <!-- Converted Orders List Ends -->
                                                                </div>
                                                            </div>   
                                                        </div>
                                                    </div>                       
                                                    <!-- Converted Orders Table Ends -->
                                                </div>
                                            </div></div>


                                        <!-- View Conversion Rates Tab Ends -->

                                        <!-- Analytics Tab Start -->
                                        <div id="aci_tab_reports" class="tab-pane">
                                            <div style="margin-top:12px;" class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><i class="fa fa-wrench"></i> <?php echo $aci_tab_reports_visualization; ?></h3>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="blfock">


                                                        <div id="chart_total_amount_pie" style="width: 900px; height: 500px;"></div>
                                                        <div id="chart_total_carts_pie" style="width: 95%; height: 500px;"></div>
                                                        <!-- Carts count vs days Graph Starts -->
                                                        <div class="widget" data-toggle="collapse-widget">
                                                            <!-- Widget heading -->
                                                            <div class="widget-head">
                                                                <h4 class="heading"><?php echo $aci_text_carts_count_vs_days; ?></h4>
                                                            </div>

                                                            <div class="widget-body collapse">
                                                                <table class="form">
                                                                    <tr>
                                                                        <td class="name"><span>Range :</span>
                                                                            <i class="icon-question-sign" data-toggle="tooltip"  data-placement="top" data-original-title="Select the filter range fot the Graphs"></i>
                                                                        </td>
                                                                        <td>
                                                                            <select id="range" class="form-control"   >

                                                                                <option value="today">Today</option>
                                                                                <option value="yesterday">Yesterday</option>
                                                                                <option value="this_week" >This Week</option>
                                                                                <option value="last_week">Last Week</option>
                                                                                <option value="this_month" selected>This Month</option>
                                                                                <option value="last_month">Last Month</option>
                                                                                <option value="this_year">This Year</option>
                                                                                <option value="last_year">Last Year</option>



                                                                            </select>
                                                                        </td>


                                                                    </tr>
                                                                </table>
                                                                <table class="form">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="width: 8% !important;">
                                                                                <label style="border:0px! important;"><?php echo $aci_text_start_date; ?></label>
                                                                            </td>
                                                                            <td width="20%">
                                                            <?php if(!empty($start_date)) { ?>
                                                                                <div class="input-group date">
                                                                                    <input type="text" name="aci[analytic][start_date]" value= "<?php echo $start_date; ?>"   data-format="YYYY-MM-DD" id="date-start" class="form-control" />
                                                                                    <span class="input-group-btn">
                                                                                        <button type="button"  style="margin-left: 6px;
                                                                                                height: 40px;"  class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                                    </span></div>


                                                            <?php } else { ?>
                                                                                <div class="input-group date">
                                                                                    <input type="text" name="aci[analytic][start_date]"value= ""  data-format="YYYY-MM-DD" id="date-start" class="form-control" />
                                                                                    <span class="input-group-btn">
                                                                                        <button type="button"  style="margin-left: 6px;
                                                                                                height: 40px;"  class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                                    </span></div>
                                                            <?php } ?>
                                                                            </td>
                                                                            <td width="8%">                                                                
                                                                                <label style="border:0px! important;"><?php echo $aci_text_end_date; ?></label>
                                                                            </td>
                                                                            <td width="20%">
                                                            <?php if(!empty($end_date)) { ?>
                                                                                <div class="input-group date">
                                                                                    <input type="text" name="aci[analytic][end_date]" value= "<?php echo $end_date; ?>"  data-format="YYYY-MM-DD" id="date-end" class="form-control" />
                                                                                    <span class="input-group-btn">
                                                                                        <button type="button"  style="margin-left: 6px;
                                                                                                height: 40px;" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                                    </span></div>

                                                            <?php } else { ?>
                                                                                <div class="input-group date">
                                                                                    <input type="text" name="aci[analytic][end_date]"  value="" data-format="YYYY-MM-DD" id="date-end" class="form-control" />
                                                                                    <span class="input-group-btn">
                                                                                        <button type="button" style="margin-left: 6px;
                                                                                                height: 40px;" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                                                                    </span></div>
                                                            <?php } ?>
                                                                            </td>
                                                                            <!--<td width="10%">
                                                                                <label style="border:0px! important;"><?php echo $aci_text_group_by; ?></label>
                                                                            </td>-->
                                                                            <!--<td width="20%">
                                                            <?php if(!empty($group_by)) {?>
                                                                                <select name="aci[analytic][group_by]" class="form-control" id="aci_analytic_group_by">
                                                                                    <option value="years"><?php echo $aci_text_years; ?></option>
                                                                                    <option value="months"><?php echo $aci_text_months; ?></option>   
                                                                                        <option value="days" selected="selected"><?php echo $aci_text_days; ?></option>                                                        
                                                                                </select>
                                                            <?php } else { ?>
                                                                                <select name="aci[analytic][group_by]" class="form-control">
                                                                                    <option value="years" selected="selected"><?php echo $aci_text_years; ?></option>
                                                                                    <option value="months"><?php echo $aci_text_months; ?></option> 
                                                                                        <option value="days"><?php echo $aci_text_days; ?></option>                                                        
                                                                                </select>
                                                            <?php } ?>
                                                                            </td>-->
                                                                            <td width="20%">
                                                                                <a onClick="return validateStartEndDate();" class="btn btn-default"><?php echo $aci_text_filter; ?></a>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="7">


                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div id="carts_count" style="width: 95%; display:none; height: 500px;"></div>
                                                            </div>
                                                        </div> </div> </div></div></div>

                                        <!-- Carts count vs days Graph Ends -->



                                        <!-- Analytics Tab Ends -->
                                        <!-- Tab Content Ends -->
                                    </div>
                                </div>
                            </form>     
                        </div>
                    </div>
                </div>

                <!-- Content Form Ends -->
            </div>
            <div class="wait"><span></span></div>
        </div>

    </div>
</div>

<!-- Content Ends-->




<div class="clearfix"></div>

<!-- Sidebar Menu & Content Wrapper Ends -->

<!-- Main Container Fluid Ends -->
<script type="text/javascript">
            $('.date').datetimepicker({
    pickTime: false
            }, { dateFormat: 'yy-mm-dd' }); </script>
<script type='text/javascript'>
            var module_row = '<?php echo $module_row; ?>';
                        function addModule() {
                        html = '<tbody id="module-row' + module_row + '">';
                                html += '  <tr>';
                                html += '    <td class="center"><select name="aci[incentive][registered][row][' + module_row + '][aci_incentive_table_discount_type]" style="width:86%! important;" class="form-control">';
                                html += '     <option value="f" selected="selected"><?php echo $aci_entry_incentive_table_discount_type_fixed; ?></option> ';
                                html += '     <option value = "p"> <?php echo $aci_entry_incentive_table_discount_type_percentage; ?> </option> ';
                                html += '    </select></td>';
                                html += '    <td class="center"><input type="text" name="aci[incentive][registered][row][' + module_row + '][aci_incentive_table_discount]" value="" size="3" class="form-control" style="width:80%;" id="discount_' + module_row + '"/></td>';
                                html += '    <td class="center"><input type="text" name="aci[incentive][registered][row][' + module_row + '][aci_incentive_table_miminum_amount]" value="" size="3" class="form-control" style="width:80%;" id="min_amt_' + module_row + '"/></td>';
                                html += '    <td class="center"><input type="text" name="aci[incentive][registered][row][' + module_row + '][aci_incentive_table_coupon_active_time]" value="" size="3" class="form-control" style="width: 41%;margin-top: -2px;float: left;" id="coupon_active_' + module_row + '"/>\n\<p style="color:#A7A7A7; font-size: 15px; padding-top:10px;">&nbsp;day(s)</p></td>';
                                html += '    <td class="center"><select name="aci[incentive][registered][row][' + module_row + '][aci_incentive_table_status]" style="width:92%! important;" class="form-control">';
                                html += '      <option value="1" selected="selected"><?php echo $aci_entry_incentive_table_status_enabled; ?></option>';
                                html += '      <option value="0"><?php echo $aci_entry_incentive_table_status_disabled; ?></option>';
                                html += '    </select></td>';
                                html += '<td class="center"><select name="aci[incentive][registered][row][' + module_row + '][aci_incentive_table_delay_days]" style="margin-bottom:5px; width:38%! important;"class="form-control">';
                                for (i = 0; i <= 31; i++) {
                        html += '<option value="' + i + '">' + i + '</option>';
                        }
                        html += '</select>';
                                html += '<label style="color:#A7A7A7; float: right; margin-top: -30px;font-size:  15px; border:0px! important; padding-top:5px; font-weight:normal;">&nbsp;day(s)</label>';
                                html += '<select name="aci[incentive][registered][row][' + module_row + '][aci_incentive_table_delay]" style="width:38%! important;" class="form-control">';
                                for (i = 1; i <= 23; i++) {
                        html += '<option value="' + i + '">' + i + '</option>';
                        }
                        html += '</select>';
                                html += '<label style="color:#A7A7A7; float: right; margin-top: -38px;font-size:  15px; border:0px! important; padding-top:15px; font-weight:normal;">&nbsp;hour(s)</label></td>';
                                html += '    <td class="center"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="btn btn-default"><span><?php echo $aci_entry_incentive_table_remove; ?></span></a></td>';
                                html += '  </tr>';
                                $("#discount_" + module_row).focusout(function() {
                        //alert($.isNumeric(  $( "#discount_<?php echo $module_row; ?>" ).val() ));
                        if (!$.isNumeric($("#discount_" + module_row).val()))
                        {
                        //<?php $incentive_table_error = true;  ?>
                        $("#incentive_tab_error").val("1");
                        }

                        });
                                $("#min_amt_" + module_row).focusout(function() {
                        //alert($.isNumeric(  $( "#min_amt_<?php echo $module_row; ?>" ).val() ));
                        if (!$.isNumeric($("#min_amt_" + module_row).val()))
                        {
                        //<?php $incentive_table_error = true;  ?>
                        $("#incentive_tab_error").val("1");
                        }

                        });
                                $("#coupon_active_" + module_row).focusout(function() {
                        //alert($.isNumeric(  $( "#coupon_active_<?php echo $module_row; ?>" ).val() ));
                        if (!$.isNumeric($("#coupon_active_" + module_row).val()))
                        {
                        //<?php $incentive_table_error = true;  ?>
                        $("#incentive_tab_error").val("1");
                        }

                        });
                                html += '</tbody>';
                                $('#incentive-registered-table tfoot').before(html);
                                $('.selectpicker').selectpicker();
                                module_row++;
                        }
                function addGuestModule() {
                var guest_module_row = '<?php echo $guest_module_row; ?>';
                            html = '<tbody id="guest-module-row' + guest_module_row + '">';
                            html += '  <tr>';
                            html += '    <td class="left"><select name="aci[incentive][guest][row][' + guest_module_row + '][aci_incentive_table_discount_type]" style="width:86%! important;" class="form-control">';
                            html += '     <option value="F" selected="selected"><?php echo $aci_entry_incentive_table_discount_type_fixed; ?></option> ';
                            html += '     <option value = "P"> <?php echo $aci_entry_incentive_table_discount_type_percentage; ?> </option> ';
                            html += '    </select></td>';
                            html += '    <td class="left"><input type="text" name="aci[incentive][guest][row][' + guest_module_row + '][aci_incentive_table_discount]" value="" size="3" class="form-control" style="width:80%;" id="discount_' + guest_module_row + '"/></td>';
                            html += '    <td class="left"><input type="text" name="aci[incentive][guest][row][' + guest_module_row + '][aci_incentive_table_miminum_amount]" value="" size="3" class="form-control" style="width:80%;" id="min_amt_' + guest_module_row + '"/></td>';
                            html += '    <td class="left"><input type="text" name="aci[incentive][guest][row][' + guest_module_row + '][aci_incentive_table_coupon_active_time]" value="" size="3" class="form-control" style="width:80%; width: 41%;margin-top: -2px;float: left;" id="coupon_active_' + guest_module_row + '"/>\n\  <p style="color:#A7A7A7; font-size: 15px; padding-top:10px;">&nbsp;day(s)</p></td>';
                            html += '    <td class="left"><select name="aci[incentive][guest][row][' + guest_module_row + '][aci_incentive_table_status]" style="width:92%! important;" class="form-control">';
                            html += '      <option value="1" selected="selected"><?php echo $aci_entry_incentive_table_status_enabled; ?></option>';
                            html += '      <option value="0"><?php echo $aci_entry_incentive_table_status_disabled; ?></option>';
                            html += '    </select></td>';
                            html += '<td class="center"><select name="aci[incentive][guest][row][' + guest_module_row + '][aci_incentive_table_delay_days]" style="margin-bottom:5px; width:38%! important;" class="form-control">';
                            for (i = 0; i <= 31; i++) {
                    html += '<option value="' + i + '">' + i + '</option>';
                    }
                    html += '</select>';
                            html += '<label style="color:#A7A7A7; float: right; margin-top: -30px;font-size:  15px; border:0px! important; padding-top:5px; font-weight:normal;">&nbsp;day(s)</label>';
                            html += '<select name="aci[incentive][guest][row][' + guest_module_row + '][aci_incentive_table_delay]" style="width:38%! important;" class="form-control">';
                            for (i = 1; i <= 23; i++) {
                    html += '<option value="' + i + '">' + i + '</option>';
                    }
                    html += '</select>';
                            html += '<label style="color:#A7A7A7; float: right; margin-top: -38px;font-size:  15px; border:0px! important; padding-top:15px; font-weight:normal;">&nbsp;hour(s)</label></td>';
                            html += '    <td class="left"><a onclick="$(\'#guest-module-row' + guest_module_row + '\').remove();" class="btn btn-default"><span><?php echo $aci_entry_incentive_table_remove; ?></span></a></td>';
                            html += '  </tr>';
                            $("#discount_" + guest_module_row).focusout(function() {
                    //alert($.isNumeric(  $( "#discount_<?php echo $module_row; ?>" ).val() ));
                    if (!$.isNumeric($("#discount_" + guest_module_row).val()))
                    {
                    //<?php $incentive_table_error = true;  ?>
                    $("#incentive_tab_error").val("1");
                    }

                    });
                            $("#min_amt_" + guest_module_row).focusout(function() {
                    //alert($.isNumeric(  $( "#min_amt_<?php echo $module_row; ?>" ).val() ));
                    if (!$.isNumeric($("#min_amt_" + guest_module_row).val()))
                    {
                    //<?php $incentive_table_error = true;  ?>
                    $("#incentive_tab_error").val("1");
                    }

                    });
                            $("#coupon_active_" + guest_module_row).focusout(function() {
                    //alert($.isNumeric(  $( "#coupon_active_<?php echo $module_row; ?>" ).val() ));
                    if (!$.isNumeric($("#coupon_active_" + guest_module_row).val()))
                    {
                    //<?php $incentive_table_error = true;  ?>
                    $("#incentive_tab_error").val("1");
                    }

                    });
                            html += '</tbody>';
                            $('#incentive-guest-table tfoot').before(html);
                            $('.selectpicker').selectpicker();
                            guest_module_row++;
                    }
</script>

<script>
    var d1 = [];
            var d2 = [];
            var d3 = [];
            var d4 = [];
            var ds = new Array();
            var charts =
    {
    initCharts: function()
    {
    // init chart_carts_count_vs_days
    this.chart_carts_count_vs_days.init();
            // init chart_total_amount_vs_days
            this.chart_total_amount_vs_days.init();
            // init total amount pie chart
            this.chart_total_amount_pie.init();
            // init carts pie chart
            this.chart_carts_pie.init();
    },
            // utility class
            utility:
    {
    chartColors: [ themerPrimaryColor, "#444", "#777", "#999", "#DDD", "#EEE" ],
            chartBackgroundColors: ["#fff", "#fff"],
            applyStyle: function(that)
            {
            that.options.colors = charts.utility.chartColors;
                    that.options.grid.backgroundColor = { colors: charts.utility.chartBackgroundColors };
                    that.options.grid.borderColor = charts.utility.chartColors[0];
                    that.options.grid.color = charts.utility.chartColors[0];
            },
            // generate random number for charts
            randNum: function()
            {
            return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
            }
    },
            chart_carts_count_vs_days:
    {
    // chart data
    data: null,
            // will hold the chart object
            plot: null,
            // chart options
            options:
    {

    grid: {
    show: true,
            aboveData: false,
            color: "#3f3f3f",
            labelMargin: 5,
            axisMargin: 0,
            borderWidth: 0,
            borderColor:null,
            minBorderMargin: 5,
            clickable: true,
            hoverable: true,
            autoHighlight: true,
            mouseActiveRadius: 20,
            backgroundColor : { }
    },
            series: {
            grow: { active:false },
            },
            legend: {
            noColumns: 0,
                    labelBoxBorderColor: "#000000",
                    position: "ne"
            },
            colors: [],
            shadowSize:1,
            tooltip: true,
            tooltipOpts: {
            content: "%s : %x : %y1",
                    dateFormat: "%m-%d-%y",
                    shifts: {
                    x: - 30,
                            y: - 50
                    },
                    defaultTheme: false
            },
            xaxes: [
            {
            mode: "time",
                    timeformat: "%m/%d/%Y",
                    tickSize:[1, "day"],
                    position: 'bottom',
                    axisLabel: "Date",
                    axisLabelUseCanvas:true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    labelWidth: 30,
                    min: (new Date(2014, 4, 24)).getTime(),
                    max: (new Date(2014, 5, 24)).getTime(),
                    reserveSpace: true,
                    alignTicksWithAxis: 1

            }
            ],
            yaxes: [
            {
            position: 'left',
                    axisLabel: "Carts Count",
                    tickSize:1,
                    axisLabelUseCanvas:true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 25
            },
            {
            position: 'right',
                    axisLabel: "Amount",
                    axisLabelUseCanvas:true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 25

            }
            ]

    },
            // initialize
            init: function()
            {
            // apply styling
            charts.utility.applyStyle(this);
                    //some data


                    //for (var i = 0; i <= 10; i += 1) 
                    //{
                    //    d1.push([i, parseInt(Math.random() * 40)]);
                    //    d2.push([i, parseInt(Math.random() * 40)]);
                    //    d3.push([i, parseInt(Math.random() * 4000)]);
                    //    d4.push([i, parseInt(Math.random() * 4000)]);
                    //}

                    <?php if(!empty($monthly_ab_orders)) { foreach($monthly_ab_orders as $ab_cart) { 
                        $date = $ab_cart['date_modified'];
                        $day = date('d' , strtotime($date));
                        $month = date('m' , strtotime($date));
                        $year = date('Y' , strtotime($date)); ?>
                                            d1.push([gd(<?php echo $year; ?> , <?php echo $month; ?> ,<?php echo $day; ?> ),<?php echo $ab_cart['abandoned_count']; ?> ]);
                    <?php  } } ?>
                <?php if(!empty($monthly_converted_orders)) {  foreach($monthly_converted_orders as $converted_orders) { 
                        $date = $converted_orders['date_modified'];
                        $day = date('d' , strtotime($date));
                        $month = date('m' , strtotime($date));
                        $year = date('Y' , strtotime($date));
                        ?>
                                            d2.push([gd(<?php echo $year; ?> , <?php echo $month; ?> ,<?php echo $day; ?> ), 1]);     <?php } } ?>
                    <?php if(!empty($monthly_ab_cart)) {   foreach($monthly_ab_cart as $ab_cart) { 
                        $date = $ab_cart['date_modified'];
                        $day = date('d' , strtotime($date));
                        $month = date('m' , strtotime($date));
                        $year = date('Y' , strtotime($date)); ?>
                                            d3.push([gd(<?php echo $year; ?> , <?php echo $month; ?> ,<?php echo $day; ?> ),<?php echo $ab_cart['abandoned_amount']; ?> ]);
                    <?php  } } ?>
                    <?php if(!empty($monthly_converted_cart)) {   foreach($monthly_converted_cart as $converted_cart) { 
                        $date = $converted_cart['date_modified'];
                        $day = date('d' , strtotime($date));
                        $month = date('m' , strtotime($date));
                        $year = date('Y' , strtotime($date));
                        ?>
                                            d4.push([gd(<?php echo $year; ?> , <?php echo $month; ?> ,<?php echo $day; ?> ),<?php echo $converted_cart['converted_amount']; ?> ]);
                    <?php } } ?>
                                        ds.push({
                                        label: "Abandoned Carts",
                                                data:d1,
                                                bars:
                                        {
                                        show:true,
                                                barWidth: 0.5,
                                                fill:1,
                                                order: 1,
                                                lineWidth: 5
                                        },
                                                yaxis: 1
                                        });
                                        ds.push({
                                        label: "Converted Carts",
                                                data:d2,
                                                bars:
                                        {
                                        show:true,
                                                barWidth: 0.5,
                                                fill: 1,
                                                order: 2,
                                                lineWidth: 5
                                        },
                                                yaxis: 1
                                        });
                                        ds.push({
                                        label: "Abandoned Amount",
                                                data: d3,
                                                lines: { show:true, fill: false, lineWidth: 4, steps: false, fillColor:"#fff8f2" },
                                                points: { show:true, radius: 5, symbol: "circle", fill: true, borderColor: "#fff", fillColor: "#fff" },
                                                yaxis: 2
                                        });
                                        ds.push({
                                        label: "Converted Amount",
                                                data: d4,
                                                lines: { show:true, fill: false, lineWidth: 4, steps: false, fillColor:"rgba(0,0,0,0.1)" },
                                                points: { show:true, radius: 5, symbol: "circle", fill: true, borderColor: "#fff", fillColor: "#fff" },
                                                yaxis: 2
                                        });
                                        this.data = ds;
                                        //this.plot = $.plot($("#chart_carts_count_vs_days"), this.data, this.options);
                                }
        },
                                // chart_total_amount_vs_days
                                chart_total_amount_vs_days:
                        {
                        // chart data
                        data: null,
                                // will hold the chart object
                                plot: null,
                                // chart options
                                options:
                        {
                        bars: {
                        show:true,
                                barWidth: 0.2,
                                fill:1,
                                linewidth:1
                        },
                                grid: {
                                show: true,
                                        aboveData: true,
                                        color: "#3f3f3f",
                                        labelMargin: 5,
                                        axisMargin: 0,
                                        borderWidth: 0,
                                        borderColor:null,
                                        minBorderMargin: 5,
                                        clickable: true,
                                        hoverable: true,
                                        autoHighlight: false,
                                        mouseActiveRadius: 20,
                                        backgroundColor : { }
                                },
                                series: {
                                grow: { active:false },
                                        stack: 1
                                },
                                legend: { position: "ne" },
                                colors: [],
                                shadowSize:1,
                                tooltip: true,
                                tooltipOpts: {
                                content: "%s : %y.0",
                                        shifts: {
                                        x: - 30,
                                                y: - 50
                                        },
                                        defaultTheme: false
                                }
                        },
                                // initialize
                                init: function()
                                {
                                // apply styling
                                charts.utility.applyStyle(this);
                                        //some data
                                        var d1 = [];
                                        for (var i = 0; i <= 10; i += 1)
                                        d1.push([i, parseInt(Math.random() * 30)]);
                                        var d2 = [];
                                        for (var i = 0; i <= 10; i += 1)
                                        d2.push([i, parseInt(Math.random() * 30)]);
                                        var ds = new Array();
                                        ds.push({
                                        label: "Abandoned Order",
                                                data:d1,
                                                bars: { order: 1 }
                                        });
                                        ds.push({
                                        label: "Converted Order",
                                                data:d2,
                                                bars: { order: 2 }
                                        });
                                        this.data = ds;
                                        //this.plot = $.plot($("#chart_total_amount_vs_days"), this.data, this.options);
                                }
                        },
                                // pie chart
                                chart_total_amount_pie:
                        {
                        // chart data
                        data: [
                        { label: "Abandoned Amount", data: parseInt('<?php if(isset($total_ab_amount)) { echo $total_ab_amount; } ?>') },
                            { label: "Converted Amount", data: parseInt('<?php if(isset($total_converted_amount)) {  echo $total_converted_amount; } ?>')  }
                                ],
                                        // will hold the chart object
                                        plot: null,
                                        // chart options
                                        options:
                                {
                                series: {
                                pie: {
                                show: true,
                                        highlight: {
                                        opacity: 0.1
                                        },
                                        radius: 1,
                                        stroke: {
                                        color: '#fff',
                                                width: 2
                                        },
                                        startAngle: 2,
                                        combine: {
                                        color: '#353535',
                                                threshold: 0.05
                                        },
                                        label: {
                                        show: true,
                                                radius: 2 / 4,
                                                formatter: function(label, series){
                                                return '<div class="label label-inverse">' + label + '&nbsp;' + Math.round(series.percent) + '%</div>';
                                                }
                                        }
                                },
                                        grow: { active: false}
                                },
                                        colors: [],
                                        legend:{ show:true },
                                        grid: {
                                        hoverable: true,
                                                clickable: true,
                                                backgroundColor : { }
                                        },
                                        tooltip: true,
                                        tooltipOpts: {
                                        content: "%s : %y.1" + "%",
                                                shifts: {
                                                x: - 30,
                                                        y: - 50
                                                },
                                                defaultTheme: false
                                        }
                                },
                                        // initialize
                                        init: function()
                                        {
                                        // apply styling
                                        charts.utility.applyStyle(this);
                                                //this.plot = $.plot($("#chart_total_amount_pie"), this.data, this.options);
                                        }
                                },
                                        // pie chart     
                                        chart_carts_pie:
                                {

                                // chart data
                                data: [
                                { label:"Abandoned Carts", data: parseInt('<?php if(isset($total_ab_carts)) { echo $total_ab_carts; } ?>') },
                                    { label: "Converted Carts", data: parseInt('<?php if(isset($total_converted_carts)) { echo $total_converted_carts; }?>') }
                                        ],
                                                // will hold the chart object
                                                plot: null,
                                                // chart options
                                                options:
                                        {
                                        series: {
                                        pie: {
                                        show: true,
                                                highlight: {
                                                opacity: 0.1
                                                },
                                                radius: 1,
                                                stroke: {
                                                color: '#fff',
                                                        width: 2
                                                },
                                                startAngle: 2,
                                                combine: {
                                                color: '#353535',
                                                        threshold: 0.05
                                                },
                                                label: {
                                                show: true,
                                                        radius: 2 / 4,
                                                        formatter: function(label, series){
                                                        return '<div class="label label-inverse">' + label + '&nbsp;' + Math.round(series.percent) + '%</div>';
                                                        }
                                                }
                                        },
                                                grow: { active: false}
                                        },
                                                colors: [],
                                                legend:{ show:true },
                                                grid: {
                                                hoverable: true,
                                                        clickable: true,
                                                        backgroundColor : { }
                                                },
                                                tooltip: true,
                                                tooltipOpts: {
                                                content: "%s : %y.1" + "%",
                                                        shifts: {
                                                        x: - 30,
                                                                y: - 50
                                                        },
                                                        defaultTheme: false
                                                }
                                        },
                                                // initialize
                                                init: function()
                                                {
                                                // apply styling
                                                charts.utility.applyStyle(this);
                                                        //this.plot = $.plot($("#chart_carts_pie"), this.data, this.options);
                                                }
                                        }

                                        };
                                                $(function()
                                                        {
                                                        // initialize charts
                                                        if (typeof charts !== 'undefined')
                                                                charts.initCharts();
                                                                });
                                                function gd(year, month, day) {
                                                return new Date(year, month - 1, day).getTime();
                                                        }

</script>
<script type='text/javascript' src='http://www.google.com/jsapi'></script>
<script type='text/javascript'>
    google.load('visualization', '1', { 'packages':['corechart'] });
            //google.setOnLoadCallback(drawChart);
                    //google.setOnLoadCallback(drawTotalCartsPie);
                            //google.setOnLoadCallback(drawTotalAmountDonut);

                                    function drawTotalCartsPie() {
                                    //var data = new google.visualization.DataTable();
                                    var data = google.visualization.arrayToDataTable([
                                            ['Cart', 'Cart Count'],
                                            ['Abandoned Carts', <?php if(isset($total_ab_carts)) { echo $total_ab_carts; } ?> ],
                                                                                        ['Converted Carts', <?php if(isset($total_converted_carts)) { echo $total_converted_carts; }?> ],
                                                                                                                            ]);
                                                                                                                                    var options = {
                                                                                                                                    title: 'Abandoned vs Converted Carts',
                                                                                                                                            width: '700',
                                                                                                                                            height: '500',
                                                                                                                                    };
                                                                                                                                    var chart = new google.visualization.PieChart(document.getElementById('chart_total_carts_pie'));
                                                                                                                                    chart.draw(data, options);
                                                                                                                            }
                                                                                                                    function drawTotalAmountDonut() {
                                                                                                                    //var data = new google.visualization.DataTable();
                                                                                                                    var data = google.visualization.arrayToDataTable([
                                                                                                                            ['Cart', 'Amount '],
                                                                                                                            ['Abandoned Amount', <?php if(isset($total_ab_amount)) { echo $total_ab_amount; } ?> ],
                                                                                                                                                                ['Converted Amount', <?php if(isset($total_converted_amount)) {  echo $total_converted_amount; } ?> ],
                                                                                                                                                                                            ]);
                                                                                                                                                                                                    var options = {
                                                                                                                                                                                                    title: 'Abandoned vs Converted Amount',
                                                                                                                                                                                                        width: '700',
                                                                                                                                                                                                        height: '500',
                                                                                                                                                                                                        pieHole: 0.4
                                                                                                                                                                                                    };
                                                                                                                                                                                                    var chart = new google.visualization.PieChart(document.getElementById('chart_total_amount_pie'));
                                                                                                                                                                                                    chart.draw(data, options);
                                                                                                                                                                                            }


                                                                                                                                                                                            function drawBarChart(){

                                                                                                                                                                                            var data = new google.visualization.DataTable();
                                                                                                                                                                                                    data.addColumn('date', 'Date');
                                                                                                                                                                                                    data.addColumn('number', 'Abandoned Carts');
                                                                                                                                                                                                    data.addColumn('number', 'Converted Carts');
                                                                                                                                                                                                    data.addColumn('number', 'Abandoned Amount');
                                                                                                                                                                                                    data.addColumn('number', 'Converted Amount');
    <?php 
       if(isset($finalarray))
       foreach($finalarray as $cart) { 
            $date = $cart['date_modified'];
            $day = date('d' , strtotime($date));
            $month = date('m' , strtotime($date));
            $year = date('Y' , strtotime($date)); ?>
                                                data.addRows([
                                                        [new Date(<?php echo $year; ?> , <?php echo $month-1; ?> , <?php echo $day; ?> ),<?php echo $cart['abandoned_count']; ?> , <?php echo $cart['converted_count']; ?> , <?php echo round($cart['abandoned_amount'], 2); ?> , <?php echo round($cart['converted_amount'],2); ?> ]
                                                                                            ]);
    <?php  }  ?>
                                        var options = {
                                        title: 'Daily Carts Count',
                                                width: '1000',
                                                height: '500',
                                                hAxis: { title: 'Date', format:'MMM d, y', slantedText:true, titleTextStyle: { color: 'red' }},
                                                vAxis: [{ title: 'Carts', titleTextStyle: { color: 'red' }, minValue: 0, format:'#'}, // Left axis
                                                { title: 'Amount', titleTextStyle: { color: 'red' }} // Right axis
                                                ],
                                                seriesType:"bars",
                                                series:{
                                                0:{ targetAxisIndex: 0 },
                                                        1:{ targetAxisIndex: 0 },
                                                        2:{ targetAxisIndex: 1, type:"line" },
                                                        3:{ targetAxisIndex: 1, type:"line" }
                                                }
                                        };
                                        var chart = new google.visualization.ColumnChart(document.getElementById('carts_count'));
                                        chart.draw(data, options);
                                }

                                // validate start and end date


</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

                            $(document).ready(function() {


                            $.ajax({
                            type: "POST",
                                    url:'<?php echo $action_generate_graph; ?>',
                                                                        data: "range=" + 'this_month',
                                                                        dataType: 'json',
                                                                        success: function(q) {

                                                                        drawChart(q);
                                                                        }
                                                                });
                                                                        $("#range").bind("change", function() {
                                                                $("#carts_count").show();
//                    $("#errormsg").hide();
                                                                        $.ajax({
                                                                        type: "POST",
                                                                                url:'<?php echo $action_generate_graph; ?>',
                                                                                                                            data: "range=" + $("#range").val(),
                                                                                                                            dataType: 'json',
                                                                                                                            success: function(q) {

                                                                                                                            drawChart(q);
                                                                                                                            }
                                                                                                                    });
                                                                                                            });
                                                                                                            });                        </script>
<script type="text/javascript">
                                    google.load("visualization", "1", {packages:["corechart"]});
                                    //google.setOnLoadCallback(drawChart);

                                            function drawChart(chartdata) {


                                            var data = new google.visualization.DataTable();
                                                    data.addColumn('string', 'Date');
                                                    data.addColumn('number', 'Abandoned Carts');
                                                    data.addColumn('number', 'Converted Carts');
                                                    data.addColumn('number', 'Abandoned Amount');
                                                    data.addColumn('number', 'Converted Amount');
                                                    for (var i in chartdata){

                                            data.addRow([String(chartdata[i]['date_modified']), parseFloat(chartdata[i]['abandoned_count']), parseFloat(chartdata[i]['converted_count']), parseFloat(chartdata[i]['abandoned_amount']), parseFloat(chartdata[i]['converted_amount'])]);
                                                    //data.push([chartdata[i][0], chartdata[i][1]]);
                                            }
                                            //var cdata = google.visualization.arrayToDataTable(data);

                                            var options = {
                                            title: 'Daily Carts Count',
                                                    width: '1200',
                                                    height: '500',
                                                    hAxis: { title: 'Date', slantedText:true, titleTextStyle: { color: 'red' }},
                                                    vAxis: [{ title: 'Carts', titleTextStyle: { color: 'red' }, viewWindow: {min: 0}, format:'#'}, // Left axis
                                                    { title: 'Amount', titleTextStyle: { color: 'red' }, viewWindow: {min: 0}} // Right axis
                                                    ],
                                                    seriesType:"bars",
                                                    series:{
                                                    0:{ targetAxisIndex: 0 },
                                                            1:{ targetAxisIndex: 0 },
                                                            2:{ targetAxisIndex: 1, type:"line" },
                                                            3:{ targetAxisIndex: 1, type:"line" }
                                                    }};
                                                    var chart = new google.visualization.ColumnChart(document.getElementById('carts_count'));
                                                    chart.draw(data, options);
                                            }

</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

                                    $(document).ready(function() {


                                    $.ajax({
                                    type: "POST",
                                            url:'<?php echo $action_generate_graph; ?>',
                                                                                        data: "range=" + 'this_month',
                                                                                        dataType: 'json',
                                                                                        success: function(q) {

                                                                                        drawChart(q);
                                                                                        }
                                                                                });
                                                                                        $("#range").bind("change", function() {
                                                                                $("#carts_count").show();
//                    $("#errormsg").hide();
                                                                                        $.ajax({
                                                                                        type: "POST",
                                                                                                url:'<?php echo $action_generate_graph; ?>',
                                                                                                                                                    data: "range=" + $("#range").val(),
                                                                                                                                                    dataType: 'json',
                                                                                                                                                    success: function(q) {

                                                                                                                                                    drawChart(q);
                                                                                                                                                    }
                                                                                                                                            });
                                                                                                                                    });
                                                                                                                                    });                        </script>
<script type="text/javascript">
                                            google.load("visualization", "1", {packages:["corechart"]});
                                            //google.setOnLoadCallback(drawChart);

                                                    function drawChart(chartdata) {


                                                    var data = new google.visualization.DataTable();
                                                            data.addColumn('string', 'Date');
                                                            data.addColumn('number', 'Abandoned Carts');
                                                            data.addColumn('number', 'Converted Carts');
                                                            data.addColumn('number', 'Abandoned Amount');
                                                            data.addColumn('number', 'Converted Amount');
                                                            for (var i in chartdata){

                                                    data.addRow([String(chartdata[i]['date_modified']), parseFloat(chartdata[i]['abandoned_count']), parseFloat(chartdata[i]['converted_count']), parseFloat(chartdata[i]['abandoned_amount']), parseFloat(chartdata[i]['converted_amount'])]);
                                                            //data.push([chartdata[i][0], chartdata[i][1]]);
                                                    }
                                                    //var cdata = google.visualization.arrayToDataTable(data);

                                                    var options = {
                                                    title: 'Daily Carts Count',
                                                            width: '1200',
                                                            height: '500',
                                                            hAxis: { title: 'Date', slantedText:true, titleTextStyle: { color: 'red' }},
                                                            vAxis: [{ title: 'Carts', titleTextStyle: { color: 'red' }, viewWindow: {min: 0}, format:'#'}, // Left axis
                                                            { title: 'Amount', titleTextStyle: { color: 'red' }, viewWindow: {min: 0}} // Right axis
                                                            ],
                                                            seriesType:"bars",
                                                            series:{
                                                            0:{ targetAxisIndex: 0 },
                                                                    1:{ targetAxisIndex: 0 },
                                                                    2:{ targetAxisIndex: 1, type:"line" },
                                                                    3:{ targetAxisIndex: 1, type:"line" }
                                                            }};
                                                            var chart = new google.visualization.ColumnChart(document.getElementById('carts_count'));
                                                            chart.draw(data, options);
                                                    }

</script>
<script>
                                            function validateStartEndDate()
                                            {

                                            var startDate = document.getElementById("date-start").value; ;
                                                    var endDate = document.getElementById("date-end").value; ;
                                                    if (startDate == "")
                                            {
                                            bootbox.alert("Please Enter Start date!")
                                                    return false; }
                                            else if (endDate == "") {
                                            bootbox.alert("Please Enter End date")
                                                    return false;
                                            }
                                            else  if (startDate > endDate) {
                                            bootbox.alert("Start date cannot be greater than end date!")

                                                    return false;
                                            }
                                            else{

                                            $.ajax({
                                            type: "POST",
                                                    url:'<?php echo $action_generate_graph; ?>',
                                                                                                        data: "from=" + $("#date-start").val() + "&to=" + $("#date-end").val() + "&submit=" + $("#aci_button_graph").val(),
                                                                                                        dataType: 'json',
                                                                                                        success: function(q) {
                                                                                                        $("#carts_count").show();
                                                                                                                drawChart(q);
                                                                                                        }
                                                                                                });
                                                                                                        }
                                                                                                }

</script>
<?php echo $footer; ?>

