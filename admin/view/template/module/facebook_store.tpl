<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-fspro" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
	  </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($update) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $update; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-fspro" class="form-horizontal">
			<ul class="nav nav-tabs" id="tabs">
				<li class="active"><a href="#tab-setting" data-toggle="tab"><i class="fa fa-fw fa-wrench"></i> <?php echo $tab_setting; ?></a></li>
				<li><a href="#tab-help" data-toggle="tab"><i class="fa fa-fw fa-question"></i> <?php echo $tab_help; ?></a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tab-setting"> 
					<div class="alert alert-info"><i class="fa fa-fw fa-info-circle"></i> <?php echo $text_requirements; ?></div>
					
					<div class="form-group required">
						<label class="col-sm-2 control-label" for="input-app-id"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_app_id; ?>"><?php echo $entry_app_id; ?></span></label>
						<div class="col-sm-10">
							<input type="text" name="facebook_store_app_id" value="<?php echo $facebook_store_app_id; ?>" placeholder="<?php echo $entry_app_id; ?>" id="input-app-id" class="form-control" />
							<?php if ($error_app_id) { ?>
							<div class="text-danger"><?php echo $error_app_id; ?></div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-logo"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_logo; ?>"><?php echo $entry_logo; ?></label>
						<div class="col-sm-10"><a href="" id="thumb-logo" data-toggle="image" class="img-thumbnail"><img src="<?php echo $logo; ?>" alt="" title="" data-placeholder="" class="img-responsive" /></a>
						  <input type="hidden" name="facebook_store_logo" value="<?php echo $facebook_store_logo; ?>" id="input-logo" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-template"><?php echo $entry_template; ?></label>
						<div class="col-sm-10">
							<select name="facebook_store_template" id="input-template" class="form-control">
								<?php foreach($templates as $template) { ?>
								<?php if ($template == $facebook_store_template) { ?>
								<option value="<?php echo $template; ?>" selected="selected"><?php echo $template; ?></option>
								<?php } else { ?>
								<option value="<?php echo $template; ?>"><?php echo $template; ?></option>
								<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>						
					
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-redirect-checkout"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_redirect_checkout; ?>"><?php echo $entry_redirect_checkout; ?></span></label>
						<div class="col-sm-10">
							<select name="facebook_store_redirect_checkout" id="input-redirect-checkout" class="form-control">
								<?php if ($facebook_store_redirect_checkout) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>	
					
					<div class="form-group">
						<label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_code; ?>"><?php echo $entry_code; ?></span></label>
						<div class="col-sm-10">
							<div class="well well-sm"><?php echo $generated_code; ?></div>
						</div>
					</div>					
					
				</div>			
				
				<div class="tab-pane" id="tab-help">
					<div class="tab-content">
						Change Log and HELP Guide is available : <a href="http://www.oc-extensions.com/Facebook-Store-Pro-Opencart-2.x" target="blank">HERE</a><br /><br />
						If you need support email us at <strong>support@oc-extensions.com</strong> (Please first read help guide) 				
					</div>
				</div>
			</div>
		</form>	
    </div>
  </div>
<script type="text/javascript"><!--
//--></script></div>
<?php echo $footer; ?>