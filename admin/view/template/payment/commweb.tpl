<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-commweb" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-commweb" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" ><span data-toggle="tooltip"><?php echo $entry_merchant_id; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="commweb_merchant_id" value="<?php echo $commweb_merchant_id; ?>" placeholder="<?php echo $entry_merchant_id; ?>"  class="form-control" />
			  
			  <?php if ($error_merchant_id) { ?>
                  <div class="text-danger"><?php echo $error_merchant_id; ?></div>
                  <?php } ?>
			  
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_access_code; ?></label>
            <div class="col-sm-10">
              <input type="text" name="commweb_access_code" value="<?php echo $commweb_access_code; ?>" placeholder="<?php echo $entry_access_code; ?>"  class="form-control" />
			  <?php if ($error_access_code) { ?>
                  <div class="text-danger"><?php echo $error_access_code; ?></div>
                  <?php } ?>
			  
            </div>
          </div>
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_secure_hash; ?></label>
            <div class="col-sm-10">
              <input type="text" name="commweb_secure_hash" value="<?php echo $commweb_secure_hash; ?>" placeholder="<?php echo $entry_secure_hash; ?>"  class="form-control" />
			  <?php if ($error_secure_hash) { ?>
                  <div class="text-danger"><?php echo $error_secure_hash; ?></div>
                  <?php } ?>
			  
            </div>
          </div>
		  
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-geo-zone"><?php echo $entry_test; ?></label>
            <div class="col-sm-10">
              <select name="commweb_test" id="input-geo-zone" class="form-control">
                <option value="1" <?php echo ($commweb_test == '1' ? ' selected="selected"' : '')?>> live</option>
                
                <option value="0" <?php echo ($commweb_test == '0' ? ' selected="selected"' : '')?>> demo</option>
                
              </select>
            </div>
          </div>
		  <div class="form-group ">
            <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_transaction_method; ?></label>
            <div class="col-sm-10">
             <select name="commweb_method" class="form-control">
                <?php if (!$commweb_method) { ?>
                <option value="0" selected="selected"><?php echo $text_transaction_method; ?></option>
                <?php } else { ?>
                <option value="0"><?php echo $text_transaction_method; ?></option>
                <?php } ?>
                
              </select>
			 
			  
            </div>
          </div>
		  
		  <div class="form-group ">
            <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_total; ?></label>
            <div class="col-sm-10">
              <input type="text" name="commweb_total" value="<?php echo $commweb_total; ?>" placeholder=""  class="form-control" />
			  
			  
            </div>
          </div>
		  
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_order_status; ?></label>
            <div class="col-sm-10">
              <select name="commweb_order_status_id" id="input-status" class="form-control">
                 <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $commweb_order_status_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_geo_zone; ?></label>
            <div class="col-sm-10">
              <select name="commweb_geo_zone_id" id="input-status" class="form-control">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $commweb_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected">
				     <?php echo $geo_zone['name']; ?>
			    </option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="commweb_status" id="input-status" class="form-control">
                  <?php if ($commweb_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
		  
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="commweb_sort_order" value="<?php echo $commweb_sort_order; ?>" placeholder="<?php echo $commweb_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?> 