<?php echo $header; ?><?php echo $column_left; ?>
    <div id="content">
        <div class="page-header">
            <div class="container-fluid">
                <div class="pull-right">
                    <button type="submit" form="form-zipmoney" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <h1><?php echo $heading_title; ?></h1>
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ($error_warning) { ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
                </div>
                <div class="panel-body">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-zipMoney" class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="zipMoney-status">Status</label>
                            <div class="col-sm-10">
                                <select name="zipmoney_status" id="zipMoney-status" class="form-control">
                                    <?php foreach (array(1 => 'Enable', 0 => 'Disable') as $status_value => $status_text) { ?>
                                        <option value="<?php echo $status_value ?>" <?php echo $status_value == $zipmoney_status ? 'selected="selected"' : '' ?>><?php echo $status_text ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="zipMoney-title">Title</label>
                            <div class="col-sm-10">
                                <input type="text" name="zipmoney_title" value="<?php echo $zipmoney_title; ?>" placeholder="Title" id="zipMoney-title" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="zipMoney-mode">Mode</label>
                            <div class="col-sm-10">
                                <select name="zipmoney_mode" id="zipMoney-mode" class="form-control">
                                    <?php foreach (array('sandbox' => 'Sandbox', 'live' => 'Live') as $mode_value => $mode_text) {?>
                                        <option value="<?php echo $mode_value?>" <?php echo $mode_value == $zipmoney_mode ? 'selected="selected"':''?>><?php echo $mode_text?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="zipMoney-sandbox-merchant-public-key">Sandbox Merchant Public Key</label>
                            <div class="col-sm-10">
                                <input type="text" name="zipmoney_sandbox_merchant_public_key" value="<?php echo $zipmoney_sandbox_merchant_public_key; ?>" placeholder="Sandbox Merchant Public Key" id="zipMoney-sandbox-merchant-public-key" class="form-control" />
                                <?php if ($error_zipmoney_sandbox_merchant_public_key) { ?>
                                    <div class="text-danger"><?php echo $error_zipmoney_sandbox_merchant_public_key; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="zipMoney-sandbox-merchant-private-key">Sandbox Merchant Private Key</label>
                            <div class="col-sm-10">
                                <input type="text" name="zipmoney_sandbox_merchant_private_key" value="<?php echo $zipmoney_sandbox_merchant_private_key; ?>" placeholder="Sandbox Merchant Private Key" id="zipMoney-sandbox-merchant-private-key" class="form-control" />
                                <?php if ($error_zipmoney_sandbox_merchant_private_key) { ?>
                                    <div class="text-danger"><?php echo $error_zipmoney_sandbox_merchant_private_key; ?></div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="zipMoney-live-merchant-public-key">Live Merchant Public Key</label>
                            <div class="col-sm-10">
                                <input type="text" name="zipmoney_live_merchant_public_key" value="<?php echo $zipmoney_live_merchant_public_key; ?>" placeholder="Live Merchant Public Key" id="zipMoney-live-merchant-public-key" class="form-control" />
                                <?php if ($error_zipmoney_live_merchant_public_key) { ?>
                                    <div class="text-danger"><?php echo $error_zipmoney_live_merchant_public_key; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="zipMoney-live-merchant-private-key">Live Merchant Private Key</label>
                            <div class="col-sm-10">
                                <input type="text" name="zipmoney_live_merchant_private_key" value="<?php echo $zipmoney_live_merchant_private_key; ?>" placeholder="Live Merchant Private Key" id="zipMoney-live-merchant-private-key" class="form-control" />
                                <?php if ($error_zipmoney_live_merchant_private_key) { ?>
                                    <div class="text-danger"><?php echo $error_zipmoney_live_merchant_private_key; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="zipMoney-product"><span data-toggle="tooltip" title="zipPay/zipMoney">Product</span></label>
                            <div class="col-sm-10">
                                <select name="zipmoney_product" id="zipMoney-product" class="form-control">
                                    <?php foreach (array('zipPay', 'zipMoney') as $product) { ?>
                                        <option value="<?php echo $product ?>" <?php echo $zipmoney_product == $product ? 'selected="selected"' : '' ?>><?php echo $product ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="zipMoney-charge-capture-option">Charge Capture Option</span></label>
                            <div class="col-sm-10">
                                <select name="zipmoney_charge_capture_option" id="zipMoney-charge-capture-option" class="form-control">
                                    <?php foreach (array(1 => 'Capture immediately', 2 => 'Put payment status to Authorized and capture later') as $capture_charge_option_value => $capture_charge_option_text) { ?>
                                        <option value="<?php echo $capture_charge_option_value ?>" <?php echo $zipmoney_charge_capture_option == $capture_charge_option_value ? 'selected="selected"' : '' ?>><?php echo $capture_charge_option_text ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="zipMoney-log-message-level">Log message level</span></label>
                            <div class="col-sm-10">
                                <select name="zipmoney_log_message_level" id="zipMoney-log-message-level" class="form-control">
                                    <?php foreach (array(1 => 'All', 2 => 'None') as $log_message_level_value => $log_message_level_text) { ?>
                                        <option value="<?php echo $log_message_level_value ?>" <?php echo $zipmoney_log_message_level == $log_message_level_value ? 'selected="selected"' : '' ?>><?php echo $log_message_level_text ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="zipMoney-iframe-checkout">In-context flow checkout</span></label>
                            <div class="col-sm-10">
                                <select name="zipmoney_iframe_checkout" id="zipMoney-iframe-checkout" class="form-control">
                                    <?php foreach (array(1 => 'Enable', 0 => 'Disable') as $iframe_checkout_value => $iframe_checkout_text) { ?>
                                        <option value="<?php echo $iframe_checkout_value ?>" <?php echo $zipmoney_iframe_checkout == $iframe_checkout_value ? 'selected="selected"' : '' ?>><?php echo $iframe_checkout_text ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group required">
                            <label class="col-sm-2 control-label" for="zipMoney-minimum-order-total">Minimum order total</label>
                            <div class="col-sm-10">
                                <input type="text" name="zipmoney_minimum_order_total" value="<?php echo $zipmoney_minimum_order_total; ?>" placeholder="Minimum order total" id="zipMoney-minimum-order-total" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="zipMoney-sort-order">Sort Order</label>
                            <div class="col-sm-10">
                                <input type="text" name="zipmoney_sort_order" value="<?php echo $zipmoney_sort_order; ?>" placeholder="Sort Order" id="zipMoney-sort-order" class="form-control" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            jQuery('#zipMoney-mode').change(function () {
                var sandbox = jQuery('#zipMoney-sandbox-merchant-public-key, #zipMoney-sandbox-merchant-private-key').closest('div.form-group');
                var production = jQuery('#zipMoney-live-merchant-public-key, #zipMoney-live-merchant-private-key').closest('div.form-group');

                if (jQuery(this).val() == 'sandbox') {
                    sandbox.show();
                    production.hide();
                } else {
                    sandbox.hide();
                    production.show();
                }
            }).change();


            $('input[name=\'zipmoney_ipn_token\']').change(function () {
                $('#input-ipn-url').val('<?php echo HTTPS_CATALOG; ?>index.php?route=payment/zipmoney/ipn&token=' + $(this).val());
            });
        </script>
    </div>
<?php echo $footer; ?>