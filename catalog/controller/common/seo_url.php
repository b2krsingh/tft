<?php
class ControllerCommonSeoUrl extends Controller {
	public function index() {
		// Add rewrite to url class
		if ($this->config->get('config_seo_url')) {
			$this->url->addRewrite($this);
		}

		// Decode URL
		if (isset($this->request->get['_route_'])) {
			$parts = explode('/', $this->request->get['_route_']);

			// remove any empty arrays from trailing
			if (utf8_strlen(end($parts)) == 0) {
				array_pop($parts);
			}

			foreach ($parts as $part) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($part) . "'");

				if ($query->num_rows) {
					$url = explode('=', $query->row['query']);

					if ($url[0] == 'product_id') {
						$this->request->get['product_id'] = $url[1];
					}

					if ($url[0] == 'category_id') {
						if (!isset($this->request->get['path'])) {
							$this->request->get['path'] = $url[1];
						} else {
							$this->request->get['path'] .= '_' . $url[1];
						}
					}

					if ($url[0] == 'manufacturer_id') {
						$this->request->get['manufacturer_id'] = $url[1];
					}

					if ($url[0] == 'information_id') {
						$this->request->get['information_id'] = $url[1];
					}

					if ($query->row['query'] && $url[0] != 'information_id' && $url[0] != 'manufacturer_id' && $url[0] != 'category_id' && $url[0] != 'product_id') {
						$this->request->get['route'] = $query->row['query'];
					}
				} else {
					$this->request->get['route'] = 'error/not_found';

					break;
				}
			}

			if (strpos($this->request->get['_route_'], 'filtered/') !== false) {
					$this->request->get['route'] = 'extension/filter';
					
					foreach ($parts as $part) {
						if (strpos($part, ']')) {
							
							$explode = explode(']', str_replace('[', '', $part));
							$id_data = explode('-', $explode[0]);
							$text_values = explode(':', $explode[1]);
							
							if (count($id_data) == 1) {
								$this->request->get['attribute'][$id_data[0]] = $text_values[1];
							} elseif (count($id_data) == 2) {
								$this->request->get['option'][$id_data[0]] = $id_data[1];
							} elseif (count($id_data) == 3) {
								$this->request->get['filter'][$id_data[1]] = $id_data[2];
							}
							
						} elseif (strpos($part, 'category:') === 0) {
							
							$category_ids = array();
							$category_keywords = explode(';', str_replace('category:', '', $part));
							
							foreach ($category_keywords as $category_keyword) {
								if (version_compare(VERSION, '3.0', '<')) {
									$seo_keyword_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape($category_keyword) . "'");
								} else {
									$seo_keyword_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE keyword = '" . $this->db->escape($category_keyword) . "' AND store_id = " . (int)$this->config->get('config_store_id') . " AND language_id = " . (int)$this->config->get('config_language_id'));
								}
								
								if ($seo_keyword_query->num_rows) {
									$category_ids[] = str_replace('category_id=', '', $seo_keyword_query->row['query']);
								}
							}
							
							$this->request->get['category_id'] = implode(';', $category_ids);
							
						} elseif (strpos($part, 'manufacturer:') === 0) {
							
							if (version_compare(VERSION, '3.0', '<')) {
								$seo_keyword_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE keyword = '" . $this->db->escape(str_replace('manufacturer:', '', $part)) . "'");
							} else {
								$seo_keyword_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE keyword = '" . $this->db->escape(str_replace('manufacturer:', '', $part)) . "' AND store_id = " . (int)$this->config->get('config_store_id') . " AND language_id = " . (int)$this->config->get('config_language_id'));
							}
							
							if ($seo_keyword_query->num_rows) {
								$this->request->get['manufacturer_id'] = str_replace('manufacturer_id=', '', $seo_keyword_query->row['query']);
							}
							
						} else {
							
							$prefix = (version_compare(VERSION, '3.0', '<')) ? '' : 'module_';
							if ($this->config->get($prefix . 'smartsearch_status') && strpos($part, 'search:') === 0) {
								$this->request->get['route'] = 'product/search';
							}
							
							$explode = explode(':', $part, 2);
							if (isset($explode[1])) {
								$this->request->get[$explode[0]] = $explode[1];
							}
							
						}
					}
				}

			if (!isset($this->request->get['route'])) {
				if (isset($this->request->get['product_id'])) {
					$this->request->get['route'] = 'product/product';
				} elseif (isset($this->request->get['path'])) {
					$this->request->get['route'] = 'product/category';
				} elseif (isset($this->request->get['manufacturer_id'])) {
					$this->request->get['route'] = 'product/manufacturer/info';
				} elseif (isset($this->request->get['information_id'])) {
					$this->request->get['route'] = 'information/information';
				}
			}

			if (isset($this->request->get['route'])) {
				return new Action($this->request->get['route']);
			}
		}
	}

	public function rewrite($link) {
		$url_info = parse_url(str_replace('&amp;', '&', $link));

		$url = '';

		$data = array();

		parse_str($url_info['query'], $data);

		if (isset($data['category_id'])) {
			$data['path'] = implode('_', explode(';', $data['category_id']));
		}

		foreach ($data as $key => $value) {
			if (isset($data['route'])) {
				if (($data['route'] == 'product/product' && $key == 'product_id') || (($data['route'] == 'product/manufacturer/info' || $data['route'] == 'product/product') && $key == 'manufacturer_id') || ($data['route'] == 'information/information' && $key == 'information_id')) {
					$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = '" . $this->db->escape($key . '=' . (int)$value) . "'");

					if ($query->num_rows && $query->row['keyword']) {
						$url .= '/' . $query->row['keyword'];

						unset($data[$key]);
					}
				} elseif ($key == 'path') {
					$categories = explode('_', $value);

					foreach ($categories as $category) {
						$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'category_id=" . (int)$category . "'");

						if ($query->num_rows && $query->row['keyword']) {
							$url .= '/' . $query->row['keyword'];
						} else {
							$url = '';

							break;
						}
					}

					unset($data[$key]);
				}
			}
		}

		$route = (isset($data['route'])) ? $data['route'] : '';
				
		if ($route == 'extension/filter' || $route == 'product/search') {
			$url .= '/filtered';
			
			foreach ($data as $key => $value) {
				$unset = true;
				
				if ($key == 'attribute') {
					
					foreach ($value as $k => $v) {
						$attribute_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute_description WHERE attribute_id = " . (int)$k);
						if ($attribute_query->num_rows) {
							$url .= '/[' . $k . ']' . $attribute_query->row['name'] . ':' . $v;
						}
					}
					
				} elseif ($key == 'option') {
					
					foreach ($value as $k => $v) {
						$option_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_description WHERE option_id = " . (int)$k);
						$option_values = array();
						
						foreach (explode(';', $v) as $option_value_id) {
							$option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value_description WHERE option_value_id = " . (int)$option_value_id);
							if ($option_value_query->num_rows) {
								$option_values[] = $option_value_query->row['name'];
							}
						}
						
						if ($option_query->num_rows && $option_values) {
							$url .= '/[' . $k . '-' . $v . ']' . $option_query->row['name'] . ':' . implode(';', $option_values);
						}
					}
					
				} elseif ($key == 'filter') {
					
					foreach ($value as $k => $v) {
						$filter_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_group_description WHERE filter_group_id = " . (int)$k);
						$filter_values = array();
						
						foreach (explode(';', $v) as $filter_id) {
							$filter_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_description WHERE filter_id = " . (int)$filter_id);
							if ($filter_query->num_rows) {
								$filter_values[] = $filter_query->row['name'];
							}
						}
						
						if ($filter_group_query->num_rows && $filter_values) {
							$url .= '/[f-' . $k . '-' . $v . ']' . $filter_group_query->row['name'] . ':' . implode(';', $filter_values);
						}
					}
					
				} elseif ($key == 'manufacturer_id') {
					
					if (version_compare(VERSION, '3.0', '<')) {
						$seo_keyword_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE `query` = 'manufacturer_id=" . (int)$value . "'");
					} else {
						$seo_keyword_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_url WHERE `query` = 'manufacturer_id=" . (int)$value . "' AND store_id = " . (int)$this->config->get('config_store_id') . " AND language_id = " . (int)$this->config->get('config_language_id'));
					}
					
					if (!empty($seo_keyword_query->row['keyword'])) {
						$url .= '/manufacturer:' . $seo_keyword_query->row['keyword'];
					}
					
				} elseif ($key == 'list' || $key == 'module_id' || $key == 'price' || $key == 'rating' || $key == 'search' || $key == 'stock_status') {
					
					$url .= '/' . $key . ':' . $value;
					
				} elseif ($key != 'category_id') {
					
					$unset = false;
					
				}
				
				if ($unset) unset($data[$key]);
			}
			
			$url = str_replace(' ', '_', $url);
		}

		if ($url) {
			unset($data['route']);

			$query = '';

			if ($data) {
				foreach ($data as $key => $value) {
					$query .= '&' . rawurlencode((string)$key) . '=' . rawurlencode((is_array($value) ? http_build_query($value) : (string)$value));
				}

				if ($query) {
					$query = '?' . str_replace('&', '&amp;', trim($query, '&'));
				}
			}

			return $url_info['scheme'] . '://' . $url_info['host'] . (isset($url_info['port']) ? ':' . $url_info['port'] : '') . str_replace('/index.php', '', $url_info['path']) . $url . $query;
		} else {
			return $link;
		}
	}
}
