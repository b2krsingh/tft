<?php
class ControllerFacebookStoreAccount extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('facebook_store/account', '', 'SSL');

			$this->response->redirect($this->url->link('facebook_store/login', '', 'SSL'));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('facebook_store/home', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('facebook_store/account', '', 'SSL')
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('facebook_store/edit', '', 'SSL');
		$data['password'] = $this->url->link('facebook_store/password', '', 'SSL');
		$data['address'] = $this->url->link('facebook_store/address', '', 'SSL');
		$data['wishlist'] = $this->url->link('facebook_store/wishlist');
		$data['order'] = $this->url->link('facebook_store/order', '', 'SSL');
		$data['download'] = $this->url->link('facebook_store/download', '', 'SSL');
		$data['return'] = $this->url->link('facebook_store/return', '', 'SSL');
		$data['transaction'] = $this->url->link('facebook_store/transaction', '', 'SSL');
		$data['newsletter'] = $this->url->link('facebook_store/newsletter', '', 'SSL');
		$data['recurring'] = $this->url->link('facebook_store/recurring', '', 'SSL');

		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('facebook_store/reward', '', 'SSL');
		} else {
			$data['reward'] = '';
		}

		$data['column_left'] = $this->load->controller('facebook_store/column_left');
		$data['column_right'] = false;
		$data['content_top'] = $this->load->controller('facebook_store/content_top');
		$data['content_bottom'] = $this->load->controller('facebook_store/content_bottom');
		$data['footer'] = $this->load->controller('facebook_store/footer');
		$data['header'] = $this->load->controller('facebook_store/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/account.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/account.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('facebook_store_default/template/facebook_store/account.tpl', $data));
		}
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}