<?php
class ControllerFacebookStoreAccountSuccess extends Controller {
	public function index() {
		$this->load->language('account/success');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('facebook_store/home', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('facebook_store/account', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('facebook_store/success', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($this->config->get('config_customer_group_id'));

		if ($customer_group_info && !$customer_group_info['approval']) {
			$data['text_message'] = sprintf($this->language->get('text_message'), $this->url->link('facebook_store/contact', '', 'SSL'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_approval'), $this->config->get('config_name'), $this->url->link('facebook_store/contact', '', 'SSL'));
		}

		$data['button_continue'] = $this->language->get('button_continue');

		if ($this->cart->hasProducts()) {
			$data['continue'] = $this->url->link('facebook_store/cart', '', 'SSL');
		} else {
			$data['continue'] = $this->url->link('facebook_store/account', '', 'SSL');
		}

		$data['column_left'] = $this->load->controller('facebook_store/column_left');
		$data['column_right'] = false;
		$data['content_top'] = $this->load->controller('facebook_store/content_top');
		$data['content_bottom'] = $this->load->controller('facebook_store/content_bottom');
		$data['footer'] = $this->load->controller('facebook_store/footer');
		$data['header'] = $this->load->controller('facebook_store/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('facebook_store_default/template/facebook_store/success.tpl', $data));
		}
	}
}