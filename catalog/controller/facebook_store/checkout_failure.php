<?php
class ControllerFacebookStoreCheckoutFailure extends Controller {
	public function index() {
		$this->load->language('checkout/failure');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('facebook_store/home', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('facebook_store/cart', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('facebook_store/checkout', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_failure'),
			'href' => $this->url->link('facebook_store/failure', '', 'SSL')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = sprintf($this->language->get('text_message'), $this->url->link('information/contact'));

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('facebook_store/home', '', 'SSL');

		$data['column_left'] = $this->load->controller('facebook_store/column_left');
		$data['column_right'] = false;
		$data['content_top'] = $this->load->controller('facebook_store/content_top');
		$data['content_bottom'] = $this->load->controller('facebook_store/content_bottom');
		$data['footer'] = $this->load->controller('facebook_store/footer');
		$data['header'] = $this->load->controller('facebook_store/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('facebook_store_default/template/facebook_store/success.tpl', $data));
		}
	}
}