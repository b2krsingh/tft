<?php
class ControllerFacebookStoreFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('facebook_store/information', 'information_id=' . $result['information_id'], 'SSL')
				);
			}
		}

		$data['contact'] = $this->url->link('facebook_store/contact' , '', 'SSL');
		$data['return'] = $this->url->link('facebook_store/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('facebook_store/sitemap', '', 'SSL');
		$data['manufacturer'] = $this->url->link('facebook_store/manufacturer', '', 'SSL');
		$data['voucher'] = $this->url->link('facebook_store/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('facebook_store/account', '', 'SSL');
		$data['special'] = $this->url->link('facebook_store/special', 'SSL');
		$data['account'] = $this->url->link('facebook_store/account', '', 'SSL');
		$data['order'] = $this->url->link('facebook_store/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('facebook_store/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('facebook_store/newsletter', '', 'SSL');

		$data['store_name'] = $this->config->get('config_name');
		$data['store_address'] = $this->config->get('config_address');
		$data['store_email'] = $this->config->get('config_email');
		$data['store_telephone'] = $this->config->get('config_telephone');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/footer.tpl')) {
			return $this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/footer.tpl', $data);
		} else {
			return $this->load->view('facebook_store_default/template/facebook_store/footer.tpl', $data);
		}
	}
}