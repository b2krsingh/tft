<?php
class ControllerFacebookStoreHeader extends Controller {
	public function index() {
        
		// Analytics		
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
			}
		}
		
		$server = $this->config->get('config_ssl');		
		
		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}		
		
		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');
		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('facebook_store_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('facebook_store_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		$data['text_home'] = $this->language->get('text_home');
		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}
		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('facebook_store/home', '', 'SSL');
		$data['wishlist'] = $this->url->link('facebook_store/wishlist', '', 'SSL');
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('facebook_store/account', '', 'SSL');
		$data['register'] = $this->url->link('facebook_store/register', '', 'SSL');
		$data['login'] = $this->url->link('facebook_store/login', '', 'SSL');
		$data['order'] = $this->url->link('facebook_store/order', '', 'SSL');
		$data['transaction'] = $this->url->link('facebook_store/transaction', '', 'SSL');
		$data['download'] = $this->url->link('facebook_store/download', '', 'SSL');
		$data['logout'] = $this->url->link('facebook_store/logout', '', 'SSL');
		$data['shopping_cart'] = $this->url->link('facebook_store/cart', '', 'SSL');
		$data['checkout'] = $this->url->link('facebook_store/checkout', '', 'SSL');
		$data['contact'] = $this->url->link('facebook_store/contact', '', 'SSL');
		$data['telephone'] = $this->config->get('config_telephone');
		
		if ($this->config->get('facebook_store_redirect_checkout')) {
			$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		}

		$status = true;

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}

		$data['language'] = $this->load->controller('facebook_store/language');
		$data['currency'] = $this->load->controller('facebook_store/currency'); 
		$data['search'] = $this->load->controller('facebook_store/search_bar');
		$data['cart'] = $this->load->controller('facebook_store/cart_bar');
		$data['facebook_tool'] = $this->load->controller('facebook_store/facebook_tool');
		
		$data['redirect_checkout'] = $this->config->get('facebook_store_redirect_checkout');
		$data['product_count'] = $this->config->get('config_product_count');
		
		$data['fb_app_id'] = $this->config->get('facebook_store_app_id');
		$data['fb_login_button'] = $this->config->get('fb_login_button_name_' . $this->config->get('config_language_id'));
		
		if ($this->config->get('fb_login_app_id')) {  // => fb login is installed
			$data['fblogin_installed'] = true;
		} else {
			$data['fblogin_installed'] = false;
		}

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'facebook-store-home';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/header.tpl')) {
			return $this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/header.tpl', $data);
		} else {
			return $this->load->view('facebook_store_default/template/facebook_store/header.tpl', $data);
		}
	}
}