<?php
class ControllerFacebookStoreHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink(HTTP_SERVER, 'canonical');
		}

		$data['column_left'] = $this->load->controller('facebook_store/column_left');
		$data['column_right'] = false;
		$data['content_top'] = $this->load->controller('facebook_store/content_top');
		$data['content_bottom'] = $this->load->controller('facebook_store/content_bottom');
		$data['footer'] = $this->load->controller('facebook_store/footer');
		$data['header'] = $this->load->controller('facebook_store/header');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/home.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/home.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('facebook_store_default/template/facebook_store/home.tpl', $data));
		}
	}
}