<?php
class ControllerFacebookStoreInformation extends Controller {
	public function index() {
		$this->load->language('information/information');

		$this->load->model('catalog/information');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('facebook_store/home', '', 'SSL')
		);

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$this->document->setTitle($information_info['meta_title']);
			$this->document->setDescription($information_info['meta_description']);
			$this->document->setKeywords($information_info['meta_keyword']);

			$data['breadcrumbs'][] = array(
				'text' => $information_info['title'],
				'href' => $this->url->link('facebook_store/information', 'information_id=' .  $information_id)
			);

			$data['heading_title'] = $information_info['title'];

			$data['button_continue'] = $this->language->get('button_continue');

			$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

			$data['continue'] = $this->url->link('facebook_store/home', '', 'SSL');

			$data['column_left'] = $this->load->controller('facebook_store/column_left');
			$data['column_right'] = false;
			$data['content_top'] = $this->load->controller('facebook_store/content_top');
			$data['content_bottom'] = $this->load->controller('facebook_store/content_bottom');
			$data['footer'] = $this->load->controller('facebook_store/footer');
			$data['header'] = $this->load->controller('facebook_store/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/information.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/information.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/facebook_store/information.tpl', $data));
			}
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('facebook_store/information', 'information_id=' . $information_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('facebook_store/home', '', 'SSL');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('facebook_store/column_left');
			$data['column_right'] = false;
			$data['content_top'] = $this->load->controller('facebook_store/content_top');
			$data['content_bottom'] = $this->load->controller('facebook_store/content_bottom');
			$data['footer'] = $this->load->controller('facebook_store/footer');
			$data['header'] = $this->load->controller('facebook_store/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('facebook_store_default/template/facebook_store/not_found.tpl', $data));
			}
		}
	}

	public function agree() {
		$this->load->model('catalog/information');

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$output = '';

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$output .= html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->setOutput($output);
	}
}