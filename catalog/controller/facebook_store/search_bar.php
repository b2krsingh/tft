<?php
class ControllerFacebookStoreSearchBar extends Controller {
	public function index() {
		$this->load->language('common/search');
		$this->load->language('facebook_store/extra');

		$data['text_search'] = $this->language->get('text_search');
		$data['text_all_categories'] = $this->language->get('text_all_categories');

		if (isset($this->request->get['search'])) {
			$data['search'] = $this->request->get['search'];
		} else {
			$data['search'] = '';
		}
		
		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'category_id' => $child['category_id'],
						'name'        => $child['name'],
						'href'        => $this->url->link('facebook_store/category', 'path=' . $category['category_id'] . '_' . $child['category_id'], 'SSL')
					);
				}

				// Level 1
				$data['categories'][] = array(
					'category_id' => $category['category_id'],
					'name'     	  => $category['name'],
					'children'    => $children_data,
					'column'      => $category['column'] ? $category['column'] : 1,
					'href'        => $this->url->link('facebook_store/category', 'path=' . $category['category_id'], 'SSL')
				);
			}
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/facebook_store/search_bar.tpl')) {
			return $this->load->view($this->config->get('facebook_store_template') . '/template/facebook_store/search_bar.tpl', $data);
		} else {
			return $this->load->view('facebook_store_default/template/facebook_store/search_bar.tpl', $data);
		}
	}
}