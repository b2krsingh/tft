<?php

/*
 * ***********************************************************************************
 * Abandoned Cart Module -                                                          **
 * Provide Coupons to the customers whose carts left abandoned.                     **
 * By Velocity Software Solutions Pvt. Ltd.                                         **
 * ***********************************************************************************
 */


class ControllerModuleAbandonedCartIncentive extends Controller
{

    // Generate Random Coupon Code and send email to all valid entries in Abandoned Cart
    public function sendCouponMail()
    {
        // Store
        $store = $this->config->get('config_store_id');
        if(isset($store)){
            $store_id     = $this->config->get('config_store_id');
        }else{
            $store_id     = 0;
        }
        
        $catalog_link = $this->config->get('config_url');
        
        // Load Modals
        $this->load->model('module/abandonedcartincentive');
        $this->load->model('setting/setting');
        $this->load->model('tool/image');

        // Get settings from database
        $result = $this->model_setting_setting->getSetting('aci', $store_id);
      
        if (!empty($result))
        {
            $result = $result['aci'];
            if ($result['general']['enable'] && $result['general']['scheduled_task'] == 'yes')
            {
                //$delay = $result['general']['delay'];
                $days = $result['general']['delay1'];
                $hours = $result['general']['delay'];
                $delay=(24*$days)+$hours;
                $totalIncentives = $result['incentive'];
                if(isset($totalIncentives['registered']))
                {
                    $customerType = $totalIncentives['registered'];
                    if(isset($customerType['row'] )){
                    foreach ($customerType['row'] as $incentive)
                    {
                        if ($incentive['aci_incentive_table_status'])
                        {
                            $incentive['aci_incentive_table_delay']=($incentive['aci_incentive_table_delay_days']*24)+$incentive['aci_incentive_table_delay'];
                            $totalDelayTime = date("Y-m-d H:i:s", strtotime('-' . ($delay + $incentive['aci_incentive_table_delay']) . ' hours'));
                            $startDelayTime = date("Y-m-d H:i:s", strtotime("-30 minutes", strtotime($totalDelayTime)));
                            //$endDelayTime   = date("Y-m-d H:i:s", strtotime("+30 minutes", strtotime($totalDelayTime))); commented by Nitin Jain
			    $endDelayTime   = $totalDelayTime;                            
                            $abandonedCarts = $this->model_module_abandonedcartincentive->getAbandonedCarts($startDelayTime, $endDelayTime, $store_id, 1);                            
                            if (!empty($abandonedCarts))
                            {
                                foreach ($abandonedCarts as $ab)
                                {
                                    $ab['customer'] = unserialize($ab['customer_info']);
                                    if (!empty($ab['customer']['email']))
                                    {

                                        // Disable previous coupons
                                        $couponName = 'ACI['.$ab['customer']['email'].']';
                                        $coupons = $this->model_module_abandonedcartincentive->getCoupons($couponName);
                                        if(!empty($coupons)) {
                                            foreach($coupons as $coupon){
                                                $couponResult = $this->model_module_abandonedcartincentive->getCouponsWithCouponHistory($coupon['coupon_id']);
                                                if (!$couponResult){
                                                        $this->model_module_abandonedcartincentive->disableCoupon($coupon['coupon_id']);
                                                }
                                            }
                                        }
                                        
                                        $ab['cart'] = unserialize($ab['cart']);
                                        $mailSubject    = $result['email']['subject'];
                                        $mailBody       = html_entity_decode($result['email']['body']);
                                        $couponCode     = $this->generateCoupon();
                                        $couponValidity = date('Y-m-d', time() + $incentive['aci_incentive_table_coupon_active_time'] * 24 * 60 * 60);
                                        $couponInfo     = array('name'          => 'ACI[' . $ab['restore_id'] . ']',
                                            'code'          => $couponCode,
                                            'discount'      => $incentive['aci_incentive_table_discount'],
                                            'type'          => $incentive['aci_incentive_table_discount_type'],
                                            'total'         => $incentive['aci_incentive_table_miminum_amount'],
                                            'logged'        => '1',
                                            'shipping'      => '0',
                                            'date_start'    => date('Y-m-d', time()),
                                            'date_end'      => $couponValidity,
                                            'uses_total'    => '1',
                                            'uses_customer' => '1',
                                            'status'        => '1');
                                        $this->model_module_abandonedcartincentive->saveCoupon($couponInfo);

                                        $this->language->load('product/product');
                                        $data['text_price'] = $this->language->get('text_price');
                                        $data['text_qty']   = $this->language->get('text_qty');

                                        $cartContent = '<table width="100%">';
                                        $cartContent .= '<thead>
                                                        <tr class="table-header">
                                                          <td class="left" width="70%"><strong>Product</strong></td>
                                                          <td class="left" width="15%"><strong>' . $data['text_qty'] . '</strong></td>
                                                          <td class="left" width="15%"><strong>' . $data['text_price'] . '</strong></td>
                                                        </tr>
                                                    </thead>';
                                        //print_r($ab['cart']);
                                        foreach ($ab['cart'] as $product)
                                        {
                                            if ($product['image'])
                                            {
                                                $image_thumb = $this->model_tool_image->resize($product['image'], 60, 60);
												$image_thumb = str_replace(" ", "%20", $image_thumb);
                                            }
                                            else
                                            {
                                                $image = false;
                                            }
                                            $cartContent .='<tr>';
                                            $cartContent .='<td class="name"><div id="picture" style="float:left;padding-right:3px;"><a href="' . $catalog_link . 'index.php?route=product/product&product_id=' . $product['product_id'] . '" target="_blank"><img src="' . $image_thumb . '" /></a></div> <a href="' . $catalog_link . 'index.php?route=product/product&product_id=' . $product['product_id'] . '" target="_blank">' . $product['name'] . '</a><br />';
                                           foreach ($product['option'] as $option)
                                            {
                                                $cartContent .= '- <small>' . $option['name'] . ' </small><br />';
                                            }
                                            $cartContent .= '</td>
								  <td class="quantity">x&nbsp;' . $product['quantity'] . '</td>
								  <td class="price">' . ($this->currency->format($product['price'], $this->session->data['currency'])) . '</td>
								</tr>';
                                        }
                                        $cartContent .='</table>';

                                        $placeHolderName    = array();
                                        $placeHolderName[0] = '{firstname}';
                                        $placeHolderName[1] = '{lastname}';
                                        $placeHolderName[2] = '{cart_content}';
                                        $placeHolderName[3] = '{discount_code}';
                                        $placeHolderName[4] = '{discount_value}';
                                        $placeHolderName[5] = '{total_amount}';
                                        $placeHolderName[6] = '{date_end}';

                                        $placeHolderValues    = array();
                                        $placeHolderValues[0] = (empty($ab['customer']['firstname'])) ? '' : $ab['customer']['firstname'];
                                        $placeHolderValues[1] = (empty($ab['customer']['lastname'])) ? '' : $ab['customer']['lastname'];
                                        $placeHolderValues[2] = $cartContent;
                                        $placeHolderValues[3] = $couponCode;
										if($incentive['aci_incentive_table_discount_type'] == "f" || $incentive['aci_incentive_table_discount_type'] =="F") {
                                        	$placeHolderValues[4] = $this->currency->format($incentive['aci_incentive_table_discount'], $this->session->data['currency']);
										} else {
											$placeHolderValues[4] = $incentive['aci_incentive_table_discount']."%";
										}
                                        $placeHolderValues[5] = $incentive['aci_incentive_table_miminum_amount'];
                                        $placeHolderValues[6] = date("d-M-Y", strtotime($couponValidity));

                                        $mailContentHTML = str_replace($placeHolderName, $placeHolderValues, $mailBody);

                                        $mailContent = array(
                                            'email'   => $ab['customer']['email'],
                                            'message' => $mailContentHTML,
                                            'subject' => $mailSubject);

                                        $emailResult = $this->model_module_abandonedcartincentive->sendMail($mailContent);
                                        if ($emailResult)
                                        {
                                            echo 'Success';
                                        }
                                        else
                                        {
                                            echo 'Not sent';
                                        }
                                    }
                                    else
                                    {
                                        echo 'No Email id.';
                                    }
                                }
                            }
                            else
                            {
                                echo 'No valid record found to send coupon codes. <br/><br/>';
                            }
                        }
                        else
                        {
                            echo 'This incentive is disabled by the admin';
                        }
                    }
                }
                }
               
                
                
                if(isset($totalIncentives['guest']))
                {
                    $customerType = $totalIncentives['guest'];
                    if(isset($customerType['row'] )){
                    foreach ($customerType['row'] as $incentive)
                    {
                        if ($incentive['aci_incentive_table_status'])
                        {
                            $incentive['aci_incentive_table_delay']=($incentive['aci_incentive_table_delay_days']*24)+$incentive['aci_incentive_table_delay'];
                            $totalDelayTime = date("Y-m-d H:i:s", strtotime('-' . ($delay + $incentive['aci_incentive_table_delay']) . ' hours'));
                            $startDelayTime = date("Y-m-d H:i:s", strtotime("-30 minutes", strtotime($totalDelayTime)));
                            $endDelayTime   = date("Y-m-d H:i:s", strtotime("+30 minutes", strtotime($totalDelayTime)));
                            
                            $abandonedCarts = $this->model_module_abandonedcartincentive->getAbandonedCarts($startDelayTime, $endDelayTime, $store_id, 0);
                            
                            if (!empty($abandonedCarts))
                            {
                                foreach ($abandonedCarts as $ab)
                                {
                                    $ab['customer'] = unserialize($ab['customer_info']);
                                    if (!empty($ab['customer']['email']))
                                    {

                                        // Disable previous coupons
                                        $couponName = 'ACI['.$ab['customer']['email'].']';
                                        $coupons = $this->model_module_abandonedcartincentive->getCoupons($couponName);
                                        if(!empty($coupons)) {
                                            foreach($coupons as $coupon){
                                                $couponResult = $this->model_module_abandonedcartincentive->getCouponsWithCouponHistory($coupon['coupon_id']);
                                                if (!$couponResult){
                                                        $this->model_module_abandonedcartincentive->disableCoupon($coupon['coupon_id']);
                                                }
                                            }
                                        }
                                        
                                        $ab['cart'] = unserialize($ab['cart']);
                                        $mailSubject    = $result['email']['subject'];
                                        $mailBody       = html_entity_decode($result['email']['body']);
                                        $couponCode     = $this->generateCoupon();
                                        $couponValidity = date('Y-m-d', time() + $incentive['aci_incentive_table_coupon_active_time'] * 24 * 60 * 60);
                                        $couponInfo     = array('name'          => 'ACI[' . $ab['restore_id'] . ']',
                                            'code'          => $couponCode,
                                            'discount'      => $incentive['aci_incentive_table_discount'],
                                            'type'          => $incentive['aci_incentive_table_discount_type'],
                                            'total'         => $incentive['aci_incentive_table_miminum_amount'],
                                            'logged'        => '1',
                                            'shipping'      => '0',
                                            'date_start'    => date('Y-m-d', time()),
                                            'date_end'      => $couponValidity,
                                            'uses_total'    => '1',
                                            'uses_customer' => '1',
                                            'status'        => '1');
                                        $this->model_module_abandonedcartincentive->saveCoupon($couponInfo);

                                        $this->language->load('product/product');
                                        $data['text_price'] = $this->language->get('text_price');
                                        $data['text_qty']   = $this->language->get('text_qty');

                                        $cartContent = '<table width="100%">';
                                        $cartContent .= '<thead>
                                                        <tr class="table-header">
                                                          <td class="left" width="70%"><strong>Product</strong></td>
                                                          <td class="left" width="15%"><strong>' . $data['text_qty'] . '</strong></td>
                                                          <td class="left" width="15%"><strong>' . $data['text_price'] . '</strong></td>
                                                        </tr>
                                                    </thead>';
                                        foreach ($ab['cart'] as $product)
                                        {
                                            if ($product['image'])
                                            {
                                                $image_thumb = $this->model_tool_image->resize($product['image'], 60, 60);
                                            }
                                            else
                                            {
                                                $image = false;
                                            }
                                            $cartContent .='<tr>';
                                            $cartContent .='<td class="name"><div id="picture" style="float:left;padding-right:3px;"><a href="' . $catalog_link . 'index.php?route=product/product&product_id=' . $product['product_id'] . '" target="_blank"><img src="' . $image_thumb . '" /></a></div> <a href="' . $catalog_link . 'index.php?route=product/product&product_id=' . $product['product_id'] . '" target="_blank">' . $product['name'] . '</a><br />';
                                            foreach ($product['option'] as $option)
                                            {
                                                $cartContent .= '- <small>' . $option['name'] . ' ' . $option['option_value'] . '</small><br />';
                                            }
                                            $cartContent .= '</td>
								  <td class="quantity">x&nbsp;' . $product['quantity'] . '</td>
								  <td class="price">' . ($this->currency->format($product['price'], $this->session->data['currency'])) . '</td>
								</tr>';
                                        }
                                        $cartContent .='</table>';

                                        $placeHolderName    = array();
                                        $placeHolderName[0] = '{firstname}';
                                        $placeHolderName[1] = '{lastname}';
                                        $placeHolderName[2] = '{cart_content}';
                                        $placeHolderName[3] = '{discount_code}';
                                        $placeHolderName[4] = '{discount_value}';
                                        $placeHolderName[5] = '{total_amount}';
                                        $placeHolderName[6] = '{date_end}';

                                        $placeHolderValues    = array();
                                        $placeHolderValues[0] = (empty($ab['customer']['firstname'])) ? '' : $ab['customer']['firstname'];
                                        $placeHolderValues[1] = (empty($ab['customer']['lastname'])) ? '' : $ab['customer']['lastname'];
                                        $placeHolderValues[2] = $cartContent;
                                        $placeHolderValues[3] = $couponCode;
										if($incentive['aci_incentive_table_discount_type'] == 'f') {
                                        	$placeHolderValues[4] = $this->currency->format($incentive['aci_incentive_table_discount'], $this->session->data['currency']);
										} else {
											$placeHolderValues[4] = trim($incentive['aci_incentive_table_discount'])."%";
										}
                                        $placeHolderValues[5] = $incentive['aci_incentive_table_miminum_amount'];
                                        $placeHolderValues[6] = date('d-M-Y', strtotime($couponValidity));

                                        $mailContentHTML = str_replace($placeHolderName, $placeHolderValues, $mailBody);

                                        $mailContent = array(
                                            'email'   => $ab['customer']['email'],
                                            'message' => $mailContentHTML,
                                            'subject' => $mailSubject);

                                        $emailResult = $this->model_module_abandonedcartincentive->sendMail($mailContent);
                                        if ($emailResult)
                                        {
                                            echo 'Success';
                                        }
                                        else
                                        {
                                            echo 'Not sent';
                                        }
                                    }
                                    else
                                    {
                                        echo 'No Email id.';
                                    }
                                }
                            }
                            else
                            {
                                echo 'No valid record found to send coupon codes. <br/><br/>';
                            }
                        }
                        else
                        {
                            echo 'This incentive is disabled by the admin';
                        }
                    }
                }
                }
            }
            else
            {
                echo 'Not configured to send emails automatically';
            }
        }
        else
        {
            echo 'DB config missing.';
        }
    }

    /**
      * Generate distict coupon code
      * return the coupon code
      */
    private function generateCoupon()
    {
        $this->load->model('module/abandonedcartincentive');
        $length    = 8;
        $code      = "";
        $chars     = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ0123456789";
        $maxlength = strlen($chars);
        if ($length > $maxlength)
        {
            $length = $maxlength;
        }
        $i = 0;
        while ($i < $length)
        {
            $char = substr($chars, mt_rand(0, $maxlength - 1), 1);
            if (!strstr($code, $char))
            {
                $code .= $char;
                $i++;
            }
        }
	// Check if coupon code alredy exist or not
        if ($this->model_module_abandonedcartincentive->distinctCode($code))
        {
            return $code;
        }
        else
        {
            return $this->generateCoupon();
        }

        return $code;
    }

}
