<?php
class ControllerModuleFSBanner extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.transitions.css');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');
				
		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}
		
		$data['slide_time'] = $setting['slide_time'] * 1000;

		$data['module'] = $module++ . '-' . mt_rand(1000, 2000);

		$template_file = 'fs_banner.tpl';
		
		if ($data['banners']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/module/' . $template_file)) {
				return $this->load->view($this->config->get('facebook_store_template') . '/template/module/' . $template_file, $data);
			} else {
				return $this->load->view('facebook_store_default/template/module/' . $template_file, $data);
			}
		}
	}
}