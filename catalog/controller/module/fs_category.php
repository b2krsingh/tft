<?php
class ControllerModuleFSCategory extends Controller {
	public function index() {
		
		if (isset($this->request->get['fbcpath'])) {
			$parts = explode('_', (string)$this->request->get['fbcpath']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			$children_data = array();

			if ($category['category_id'] == $data['category_id']) {
				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach($children as $child) {
					$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

					$children_data[] = array(
						'category_id'   => $child['category_id'], 
						'name'          => $child['name'],
						'product_count' => ($this->config->get('config_product_count') ? $this->model_catalog_product->getTotalProducts($filter_data) : ''), 
						'href'          => $this->url->link('facebook_store/category', 'fbcpath=' . $category['category_id'] . '_' . $child['category_id'], 'SSL')
					);
				}
			}

			$filter_data = array(
				'filter_category_id'  => $category['category_id'],
				'filter_sub_category' => true
			);

			$data['categories'][] = array(
				'category_id'   => $category['category_id'],
				'name'          => $category['name'],
				'product_count' => ($this->config->get('config_product_count') ?  $this->model_catalog_product->getTotalProducts($filter_data) : ''),
				'children'      => $children_data,
				'href'          => $this->url->link('facebook_store/category', 'fbcpath=' . $category['category_id'], 'SSL')
			);
		}
		
		$data['show_product_count'] = $this->config->get('config_product_count');

		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/module/fs_category.tpl')) {
			return $this->load->view($this->config->get('facebook_store_template') . '/template/module/fs_category.tpl', $data);
		} else {
			return $this->load->view('facebook_store_default/template/module/fs_category.tpl', $data);
		}
	}
}