<?php
class ControllerModuleFSLatest extends Controller {
	public function index($setting) {
		static $module = 0; 
		
		$this->document->addScript('catalog/view/javascript/jquery/jquery.bxslider.js');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/stylesheet/jquery.bxslider.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('facebook_store_template') . '/stylesheet/jquery.bxslider.css');
		} else {
			$this->document->addStyle('catalog/view/theme/facebook_store_default/stylesheet/jquery.bxslider.css');
		}
		
		$heading_titles = $setting['title'];		
		
		if (isset($heading_titles[$this->config->get('config_language_id')])) {
			$data['heading_title'] = $heading_titles[$this->config->get('config_language_id')];
		} else {
			$data['heading_title'] = '';
		}
		
		$data['button_cart'] = $this->language->get('button_cart');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$results = $this->model_catalog_product->getLatestProducts($setting['limit']);

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('facebook_store/product', 'product_id=' . $result['product_id'], 'SSL'),
				);
			}
			
			$data['slider_width'] = $setting['width'] + 5;
			$data['random_start'] = $setting['random_start'];
			$data['auto_slider'] = $setting['auto_slider'];
			
			if ($setting['auto_slider']) {
				$data['slide_time'] = $setting['slide_time'] * 1000;
			} else {
				$data['slide_time'] = 0;
			}	
			
			$data['module'] = $module++ . '-' . mt_rand(1000, 2000);

			$template_file = 'fs_latest_' . $setting['display_type'] . '.tpl';
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/module/' . $template_file)) {
				return $this->load->view($this->config->get('facebook_store_template') . '/template/module/' . $template_file, $data);
			} else {
				return $this->load->view('facebook_store_default/template/module/' . $template_file, $data);
			}
		}
	}
}