<?php
class ControllerModuleFSSlider extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');
		
		$this->document->addScript('catalog/view/javascript/jquery/jquery.easing.1.3.js');
		$this->document->addScript('catalog/view/javascript/jquery/jquery.camera.js');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/stylesheet/jquery.camera.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('facebook_store_template') . '/stylesheet/jquery.camera.css');
		} else {
			$this->document->addStyle('catalog/view/theme/facebook_store_default/stylesheet/jquery.camera.css');
		}

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}
		
		$data['height'] = $setting['height'];
		$data['slide_time'] = $setting['slide_time'] * 1000;
		$data['transition_time'] = $setting['transition_time'] * 1000;

		$data['module'] = $module++ . '-' . mt_rand(1000, 2000);

		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/module/fs_slider.tpl')) {
			return $this->load->view($this->config->get('facebook_store_template') . '/template/module/fs_slider.tpl', $data);
		} else {
			return $this->load->view('facebook_store_default/template/module/fs_slider.tpl', $data);
		}
	}
}