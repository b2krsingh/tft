<?php
class ControllerModuleFSSpecial extends Controller {
	public function index($setting) {
		static $module = 0; 
		
		$this->document->addScript('catalog/view/javascript/jquery/jquery.bxslider.js');
		$this->document->addScript('catalog/view/javascript/jquery/jquery.mb-comingsoon.js');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/stylesheet/jquery.bxslider.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('facebook_store_template') . '/stylesheet/jquery.bxslider.css');
		} else {
			$this->document->addStyle('catalog/view/theme/facebook_store_default/stylesheet/jquery.bxslider.css');
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/stylesheet/jquery.mb-comingsoon.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('facebook_store_template') . '/stylesheet/jquery.mb-comingsoon.css');
		} else {
			$this->document->addStyle('catalog/view/theme/facebook_store_default/stylesheet/jquery.mb-comingsoon.css');
		}
		
		$language_texts = $setting['language_texts'][$this->config->get('config_language_id')];		
		
		$data['heading_title'] = $language_texts['title'];
		$data['text_days'] = $language_texts['days'];
		$data['text_hours'] = $language_texts['hours'];
		$data['text_minutes'] = $language_texts['minutes'];
		$data['text_seconds'] = $language_texts['seconds'];
		
		$data['button_cart'] = $this->language->get('button_cart');

		$this->load->model('catalog/product');
		$this->load->model('module/fs_special');

		$this->load->model('tool/image');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => 'p.sort_order',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit']
		);
		
		$results = $this->model_catalog_product->getProductSpecials($filter_data);

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
				
				$show_counter = false;
				$date_end_year = false;
				$date_end_month = false;
				$date_end_day = false;
				
				$special_end_date = $this->model_module_fs_special->getSpecialEndDate($result['product_id']);
				
				if ($special_end_date) {
					if ($special_end_date != '0000-00-00') {
						$show_counter = true;
						
						$date_elements = explode("-", $special_end_date);
						$date_end_year = $date_elements[0]; 
						$date_end_month = $date_elements[1];
						$date_end_day = $date_elements[2];	
					}	
				}

				$data['products'][] = array(
					'product_id'     => $result['product_id'],
					'thumb'          => $image,
					'name'           => $result['name'],
					'description'    => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'          => $price,
					'special'        => $special,
					'tax'            => $tax,
					'rating'         => $rating,
					'href'           => $this->url->link('facebook_store/product', 'product_id=' . $result['product_id'], 'SSL'),
					'show_counter'   => $show_counter,
					'date_end_year'  => $date_end_year,
					'date_end_month' => $date_end_month,
					'date_end_day'   => $date_end_day
				);
			}
			
			$data['slider_width'] = $setting['width'] + 5;
			$data['random_start'] = $setting['random_start'];
			$data['auto_slider'] = $setting['auto_slider'];
			
			if ($setting['auto_slider']) {
				$data['slide_time'] = $setting['slide_time'] * 1000;
			} else {
				$data['slide_time'] = 0;
			}	
			
			$data['module'] = $module++ . '-' . mt_rand(1000, 2000);

			$template_file = 'fs_special_list.tpl';
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('facebook_store_template') . '/template/module/' . $template_file)) {
				return $this->load->view($this->config->get('facebook_store_template') . '/template/module/' . $template_file, $data);
			} else {
				return $this->load->view('facebook_store_default/template/module/' . $template_file, $data);
			}
		}
	}
}