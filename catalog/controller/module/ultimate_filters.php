<?php
//==============================================================================
// Ultimate Filters Module v3
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

class ControllerModuleUltimateFilters extends Controller {
	private $type = 'module';
	private $name = 'ultimate_filters';
	
	public function index($settings) {
		return $this->load->controller('extension/' . $this->type . '/' . $this->name, $settings);
	}
}
?>