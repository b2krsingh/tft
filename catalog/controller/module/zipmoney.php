<?php
class ControllerModuleZipmoney extends Controller {

    public function index($setting) {

        $this->document->addScript('https://d3k1w8lx8mqizo.cloudfront.net/lib/js/zm-widget-js/dist/zipmoney-widgets-v1.min.js');

        $zipmoney_mode = $this->config->get('zipmoney_mode');

        $environment = ($zipmoney_mode == 'sandbox') ? 'sandbox':'production';

        $merchant_public_key = ($zipmoney_mode == 'sandbox') ? $this->config->get('zipmoney_sandbox_merchant_public_key') : $this->config->get('zipmoney_live_merchant_public_key');

        $setting['environment'] = $environment;
        $setting['merchant_public_key'] = $merchant_public_key;

        return $this->load->view($this->config->get('config_template') .'/template/module/zipmoney_widget.tpl', $setting);
    }

    protected function validate() {
        return true;
    }
}