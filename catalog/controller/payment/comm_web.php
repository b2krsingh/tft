<?php
class ControllerPaymentCommWeb extends Controller {
	public function index() {
		$this->language->load('payment/comm_web');

		$data['text_credit_card'] = $this->language->get('text_credit_card');
		$data['text_start_date'] = $this->language->get('text_start_date');
		$data['text_issue'] = $this->language->get('text_issue');
		$data['text_wait'] = $this->language->get('text_wait');

		$data['entry_cc_owner'] = $this->language->get('entry_cc_owner');
		$data['entry_cc_type'] = $this->language->get('entry_cc_type');
		$data['entry_cc_number'] = $this->language->get('entry_cc_number');
		$data['entry_cc_start_date'] = $this->language->get('entry_cc_start_date');
		$data['entry_cc_expire_date'] = $this->language->get('entry_cc_expire_date');
		$data['entry_cc_cvv2'] = $this->language->get('entry_cc_cvv2');
		$data['entry_cc_issue'] = $this->language->get('entry_cc_issue');

		$data['button_confirm'] = $this->language->get('button_confirm');

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		$data['owner'] = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];

		$data['cards'] = array();

		$data['cards'][] = array(
			'text'  => 'Visa',
			'value' => '0'
		);

		$data['cards'][] = array(
			'text'  => 'MasterCard',
			'value' => '1'
		);

		$data['cards'][] = array(
			'text'  => 'Maestro',
			'value' => '9'
		);

		$data['cards'][] = array(
			'text'  => 'Solo',
			'value' => 'S'
		);

		$data['months'] = array();

		for ($i = 1; $i <= 12; $i++) {
			$data['months'][] = array(
				'text'  => strftime('%B', mktime(0, 0, 0, $i, 1, 2000)),
				'value' => sprintf('%02d', $i)
			);
		}

		$today = getdate();

		$data['year_valid'] = array();

		for ($i = $today['year'] - 10; $i < $today['year'] + 1; $i++) {
			$data['year_valid'][] = array(
				'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)),
				'value' => strftime('%y', mktime(0, 0, 0, 1, 1, $i))
			);
		}

		$data['year_expire'] = array();

		for ($i = $today['year']; $i < $today['year'] + 11; $i++) {
			$data['year_expire'][] = array(
				'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)),
				'value' => strftime('%y', mktime(0, 0, 0, 1, 1, $i))
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/comm_web.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/comm_web.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/comm_web.tpl', $data);
		}
	}
 
	public function send() {
		$this->language->load('payment/comm_web');

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		if (!$this->config->get('comm_web_transaction')) {
			$payment_type = 'A';
		} else {
			$payment_type = 'S';
		}
$url2 = "https://paymentgateway.commbank.com.au/api/nvp/version/41";

	 
	/*	$xmldata = "<request>
<terminalid>178001</terminalid>
<password>Password1!</password>
<action>1</action>
<card>". $this->request->post['cc_number']."</card>
<cvv2>". $this->request->post['cc_cvv2'] ."</cvv2>
<expYear>".$this->request->post['cc_expire_date_year']."</expYear>
<expMonth>".$this->request->post['cc_expire_date_month']."</expMonth>
<member>". $this->request->post['cc_owner'] ."</member>
<currencyCode>MUR</currencyCode>
<address>" . $order_info['payment_address_1']."</address>
<city>". $order_info['payment_city']."</city>
<statecode>".    $order_info['payment_zone'] ."</statecode>
<zip>" . $order_info['payment_postcode'] ."</zip>
<CountryCode>".  $order_info['payment_iso_code_2'] ."</CountryCode>
<email>". $order_info['email']."</email>
<amount>".$this->currency->format($order_info['total'], $order_info['currency_code'], false, false)."</amount>
<trackid>".$this->session->data['order_id']."</trackid>
 
</request>";*/
$request = "order.id=".$this->session->data['order_id']."&transaction.id=".$this->session->data['order_id']."&apiOperation=PAY&sourceOfFunds.type=CARD&sourceOfFunds.provided.card.number=". $this->request->post['cc_number']."&sourceOfFunds.provided.card.expiry.month=".$this->request->post['cc_expire_date_month']."&sourceOfFunds.provided.card.expiry.year=".$this->request->post['cc_expire_date_year']."&sourceOfFunds.provided.card.securityCode=". $this->request->post['cc_cvv2'] ."&order.amount=".$this->currency->format($order_info['total'], $order_info['currency_code'], false, false)."&order.currency=AUD&merchant=TFPPTYCOM201&apiPassword=b57bd1018535406f2a7c9cf4bc0e4c5e&apiUsername=merchant.TFPPTYCOM201";
//echo $request;
// verify cc

 $validcard = 1;
    $cardtype = array(
        "visa"       => "/^4[0-9]{12}(?:[0-9]{3})?$/",
        "mastercard" => "/^5[1-5][0-9]{14}$/",
        "amex"       => "/^3[47][0-9]{13}$/",
        "discover"   => "/^6(?:011|5[0-9]{2})[0-9]{12}$/",
    );

    if (preg_match($cardtype['visa'],$this->request->post['cc_number']))
    {
	 
       $validcard = 1;
	
    }
    else if (preg_match($cardtype['mastercard'],$this->request->post['cc_number']))
    {
	 
     $validcard = 1;
    }
    else if (preg_match($cardtype['amex'],$this->request->post['cc_number']))
    {
	 
      $validcard = 1;
	
    }
    else if (preg_match($cardtype['discover'],$this->request->post['cc_number']))
    {
	 
       $validcard = 1;
    }
    else
    {
     $validcard =0;
    } 

// verify cc
//curl code
// echo $xmldata;
$json = array();
/*if(empty($this->request->post['cc_owner']) || empty($this->request->post['cc_number'])  || empty($this->request->post['cc_cvv2']) ){
			$json['error'] = "You have left a field empty";
}
else if( $validcard  == 0){
		$json['error'] = "You have Added An InValid Card Number";
}
else*/ if( (strlen($this->request->post['cc_cvv2']) > 4 ) || !is_numeric($this->request->post['cc_cvv2'])){
		$json['error'] = "CVV code is invalid";
}
/*else if( $this->request->post['cc_expire_date_month'] < date('m') ){
	//	$json['error'] = "Expiry date is invalid";
}*/
else {
 $ch = curl_init();
    curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
    
    curl_setopt($ch, CURLOPT_URL, "https://paymentgateway.commbank.com.au/api/nvp/version/41");
	 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($request)));

    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded;charset=UTF-8"));
  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_POST, 1);
    // this is used for debugging only. This would not be used in your integration, as DEBUG should be set to FALSE
   
     curl_setopt($ch, CURLOPT_HEADER, TRUE);
      curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE); 
     
    $response = curl_exec($ch);
     // this is used for debugging only. This would not be used in your integration, as DEBUG should be set to FALSE
 
    $requestHeaders = curl_getinfo($ch);
     $response = $requestHeaders["request_header"] . $response; 
  
    // assigns the cURL error to response if something went wrong so the caller can echo the error
    if (curl_error($ch))
      $response = "cURL Error: " . curl_errno($ch) . " - " . curl_error($ch);

    // respond with the transaction result, or a cURL error message if it failed
   // echo  str_replace("\n", "<br/>", $response);
	 
//
 
 if (empty($response)) {

 // some kind of an error happened

 die(curl_error($ch));

 curl_close($ch); // close cURL handler

 } else {

 $info = curl_getinfo($ch);

 curl_close($ch); // close cURL handler



 if (empty($info['http_code'])) {

 die("No HTTP code was returned");

 }



 else {

 // load the HTTP codes

 //$http_codes = parse_ini_file("response.inc");



 // echo responses

 //echo "The server responded: \n";

 //echo $info['http_code'] . " " . $http_codes[$info['http_code']];

 }

 }

  //echo "RESULT: $response"; //contains response from server

//curl code
	 
	 	if (!$response) {
			$this->log->write('DoDirectPayment failed: ' . curl_error($curl) . '(' . curl_errno($curl) . ')');
		}
 
		$responseArray = array();


if (strstr($response, "cURL Error") != FALSE) {
  print("Communication failed. Please review payment server return response (put code into debug mode).");
  die();
}
if (strlen($response) != 0) {
  $pairArray = explode("&", $response);
  foreach ($pairArray as $pair) {
    $param = explode("=", $pair);
    $responseArray[urldecode($param[0])] = urldecode($param[1]);
  }
}
// [Snippet] howToDecodeResponse - end

// [Snippet] howToParseResponse - start
if (array_key_exists("result", $responseArray))
  $title = $result = $responseArray["result"];
  // [Snippet] howToParseResponse - end

// Form error string if error is triggered
if ($result == "FAIL" || $result == "FAILURE") {
  if (array_key_exists("failureExplanation", $responseArray)) {
   $json['error'] =  $errorMessage = rawurldecode($responseArray["failureExplanation"]);
  }
  else if (array_key_exists("supportCode", $responseArray)) {
   $json['error'] =   $errorMessage = rawurldecode($responseArray["supportCode"]);
  }
  else {
   $json['error'] =   $errorMessage = "Reason unspecified.";
  }

  if (array_key_exists("failureCode", $responseArray)) {
    $json['error'] =  $errorCode = "Error (" . $responseArray["failureCode"] . ")";
  }
  else {
   $json['error'] =   $errorCode = "Error (UNSPECIFIED)";
  }
}

else {
  if (array_key_exists("response.gatewayCode", $responseArray))
   $json['error'] =   $gatewayCode = rawurldecode($responseArray["response.gatewayCode"]);
  else
    $gatewayCode = "Response not received.";
}

 if (array_key_exists("transaction.receipt", $responseArray))
  $payid = $responseArray["transaction.receipt"];
 
  
		
		if(!empty($targetUrl) && !empty($payid)){
 			 $targetUrl = $targetUrl.$payid;
 	//for 3d cards only   	 
 
  
	     $json['success'] = $targetUrl;
	 
		}
		
 		else if ( $title == 'SUCCESS')    {
			$message = '';
 
			if (isset($responseArray["transaction.receipt"])) {
				$message .= 'TRANSACTIONID: ' .$responseArray["transaction.receipt"] . "\n";
			}

			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('comm_web_order_status_id'), $message, false);

			$json['success'] = $this->url->link('checkout/success');
		} 
		else {
			 
				     	$json['error'] =  "Transaction Declined";
			 
		}
 
	}	
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json)); 
	 
	}
}