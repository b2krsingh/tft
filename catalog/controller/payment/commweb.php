<?php
class ControllerPaymentCommweb extends Controller {
	public function index() {
    	$this->language->load('payment/commweb');
		
		$data['text_credit_card'] = $this->language->get('text_credit_card');
		$data['text_start_date'] = $this->language->get('text_start_date');
		$data['text_issue'] = $this->language->get('text_issue');
		$data['text_wait'] = $this->language->get('text_wait');
		
		$data['entry_cc_type'] = $this->language->get('entry_cc_type');
		$data['entry_cc_number'] = $this->language->get('entry_cc_number');
		$data['entry_expiry'] = $this->language->get('entry_expiry');
		
		$data['entry_cc_cvv2'] = $this->language->get('entry_cc_cvv2');
		$data['entry_cc_issue'] = $this->language->get('entry_cc_issue');
		
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['confirm'] = $this->url->link('checkout/cart');
		
		$data['cards'] = array();

		$data['cards'][] = array(
			'text'  => 'Visa', 
			'value' => 'VISA'
		);

		$data['cards'][] = array(
			'text'  => 'MasterCard', 
			'value' => 'MASTERCARD'
		);

		$data['cards'][] = array(
			'text'  => 'Discover Card', 
			'value' => 'DISCOVER'
		);
		
		$data['cards'][] = array(
			'text'  => 'American Express', 
			'value' => 'AMEX'
		);

	/*	$data['cards'][] = array(
			'text'  => 'Maestro', 
			'value' => 'SWITCH'
		);
		
		$data['cards'][] = array(
			'text'  => 'Solo', 
			'value' => 'SOLO'
		);		*/
	
		$data['months'] = array();
		
		for ($i = 1; $i <= 12; $i++) {
			$data['months'][] = array(
				'text'  => strftime('%B', mktime(0, 0, 0, $i, 1, 2000)), 
				'value' => sprintf('%02d', $i)
			);
		}
		
		$today = getdate();
		
		$data['year_valid'] = array();
		
		for ($i = $today['year']; $i < $today['year'] + 20; $i++) {	
			$data['year_valid'][] = array(
				'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)), 
				'value' => strftime('%Y', mktime(0, 0, 0, 1, 1, $i))
			);
		}

		$data['year_expire'] = array();

		for ($i = $today['year']; $i < $today['year'] + 20; $i++) {
			$data['year_expire'][] = array(
				'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)),
				'value' => strftime('%y', mktime(0, 0, 0, 1, 1, $i)) 
			);
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/commweb.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/commweb.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/commweb.tpl', $data);
		}
		
			
		$this->render();		
	}

	public function send() {
		
		if (!$this->config->get('commweb_method')) {
			$payment_type = 'Pay';
		} else {
			$payment_type = '';
		}
		
		$this->load->model('checkout/order');
		
		$order_id = $this->session->data['order_id'];
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$vpc_AccessCode = $this->config->get('commweb_access_code');
		$vpc_Merchant = $this->config->get('commweb_merchant_id');
		$access_code = $this->config->get('commweb_access_code');
		
		$num1 = number_format($order_info['total'], 2, ".", "");
		$vpc_Amount = $num1 * 100; //multiply 100 for cent 00
		
		$vpc_MerchTxnRef = rand();
		$vpc_OrderInfo = "Order by - ".$order_info['payment_firstname']." ".$order_info['payment_lastname'];
		$vpc_CardNum = $this->request->post['cc_type'];
		$vpc_Cardtype = $this->request->post['vpc_CardNum'];
		$exp_month=$this->request->post['cc_expire_date_month'];
		$exp_year=$this->request->post['cc_expire_date_year'];
		$vpc_CardExp = $exp_year.$exp_month;
		
		
		$vpcURL = "https://migs.mastercard.com.au/vpcdps";
		
		$postData = "vpc_Version=1&vpc_Command=pay&vpc_AccessCode=$vpc_AccessCode&vpc_MerchTxnRef=$vpc_MerchTxnRef&vpc_Merchant=$vpc_Merchant&vpc_OrderInfo=$vpc_OrderInfo&vpc_Amount=$vpc_Amount&vpc_CardNum=$vpc_CardNum&vpc_CardExp=$vpc_CardExp";
		
		
		// Get a HTTPS connection to VPC Gateway and do transaction
		// turn on output buffering to stop response going to browser
		ob_start();
		
		// initialise Client URL object
		$ch = curl_init();
		
		// set the URL of the VPC
		curl_setopt ($ch, CURLOPT_URL, $vpcURL);
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $postData);
		
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		
		curl_exec ($ch);
		
		// get response
		$response = ob_get_contents();
		
		// turn output buffering off.
		ob_end_clean();
		
		// set up message paramter for error outputs
		$message = "";
		
		
		
		$json = array();
			
		$pairArray = explode("&", $response);
		foreach ($pairArray as $pair) {
			$param = explode("=", $pair);
			$map[urldecode($param[0])] = urldecode($param[1]);
		}
		
	
		if($map["vpc_Message"]=="Approved")
		{
						
			$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get('config_order_status_id'));
			$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get('commweb_order_status_id'), '', true);
				
			$json['success'] = $this->url->link('checkout/success');
			$this->response->setOutput(json_encode($json));
			
		}else{
			$json['error'] = $map["vpc_Message"];
			$this->response->setOutput(json_encode($json));		
		}
		
		
	}
	
	function null2unknown($map, $key) {
		if (array_key_exists($key, $map)) {
			if (!is_null($map[$key])) {
				return $map[$key];
			}
		} 
		return "No Value Returned";
	} 
}
?>