<?php

require_once(modification(DIR_SYSTEM . 'commweb_direct/configuration.php'));

require_once(modification(DIR_SYSTEM . 'commweb_direct/connection.php'));



class ControllerPaymentCommweb3dsecure extends Controller {

	public function index() {

    	

		$this->language->load('payment/commweb_3dsecure');

		

		$data['text_credit_card'] = $this->language->get('text_credit_card');

		$data['text_start_date'] = $this->language->get('text_start_date');

		$data['text_issue'] = $this->language->get('text_issue');

		$data['text_wait'] = $this->language->get('text_wait');

		

		$data['entry_cc_type'] = $this->language->get('entry_cc_type');

		$data['entry_cc_number'] = $this->language->get('entry_cc_number');

		$data['entry_expiry'] = $this->language->get('entry_expiry');

		

		$data['entry_cc_cvv2'] = $this->language->get('entry_cc_cvv2');

		$data['entry_cc_issue'] = $this->language->get('entry_cc_issue');

		

		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['confirm'] = $this->url->link('checkout/cart');

		

		$data['cards'] = array();



		$data['cards'][] = array(

			'text'  => 'Visa', 

			'value' => 'VISA'

		);



		$data['cards'][] = array(

			'text'  => 'MasterCard', 

			'value' => 'MASTERCARD'

		);



		$data['cards'][] = array(

			'text'  => 'Discover Card', 

			'value' => 'DISCOVER'

		);

		

		$data['cards'][] = array(

			'text'  => 'American Express', 

			'value' => 'AMEX'

		);



	/*	$data['cards'][] = array(

			'text'  => 'Maestro', 

			'value' => 'SWITCH'

		);

		

		$data['cards'][] = array(

			'text'  => 'Solo', 

			'value' => 'SOLO'

		);		*/

	

		$data['months'] = array();

		

		for ($i = 1; $i <= 12; $i++) {

			$data['months'][] = array(

				'text'  => strftime('%B', mktime(0, 0, 0, $i, 1, 2000)), 

				'value' => sprintf('%02d', $i)

			);

		}

		

		$today = getdate();

		

		$data['year_valid'] = array();

		

		for ($i = $today['year']; $i < $today['year'] + 20; $i++) {	

			$data['year_valid'][] = array(

				'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)), 

				'value' => strftime('%y', mktime(0, 0, 0, 1, 1, $i))

			);

		}



		$data['year_expire'] = array();



		for ($i = $today['year']; $i < $today['year'] + 20; $i++) {

			$data['year_expire'][] = array(

				'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)),

				'value' => strftime('%y', mktime(0, 0, 0, 1, 1, $i)) 

			);

		}

		

		$data['vpc_Merchant'] = $this->config->get('commweb_3dsecure_merchant_id');

		

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/commweb_3dsecure.tpl')) {

			return $this->load->view('default/template/payment/commweb_3dsecure.tpl', $data);

		} else {

			return $this->load->view('default/template/payment/commweb_3dsecure.tpl', $data);

		}

		

			

		$this->render();		

	}



	public function send() {

		

		if (!$this->config->get('commweb_3dsecure_method')) {

			$payment_type = 'PAY';

		} else {

			$payment_type = $this->config->get('commweb_3dsecure_method');

		}

		

		$this->session->data['card_data']=$this->request->post;

		

		$this->load->model('checkout/order');

		

		$order_id = $this->session->data['order_id'];

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		

		//$vpc_AccessCode = $this->config->get('commweb_access_code');

		 $vpc_Merchant = $this->config->get('commweb_3dsecure_merchant_id');

		

		 $password = $this->config->get('commweb_3dsecure_access_code');

		 

		 $card_number = $this->session->data['card_data']['vpc_CardNum'];

		 $expiry_month = $this->session->data['card_data']['cc_expire_date_month'];

		 $expiry_year = $this->session->data['card_data']['cc_expire_date_year'];

		 $ccv = $this->session->data['card_data']['vpc_CardSecurityCode'];

		 

		 $tprice=$this->currency->format($order_info['total'], $this->session->data['currency']);

		

		$left_symbol=$this->currency->getSymbolLeft($this->session->data['currency']);

		$right_symbol=$this->currency->getSymbolRight($this->session->data['currency']);

		

		$tprice=str_replace(",","",str_replace(array($left_symbol,$right_symbol),"",$tprice));

		

		 $price=$this->currency->convert($tprice,$this->session->data['currency'],'AUD');

		

		 $num1 = number_format($price, 2, ".", "");

		//$vpc_Amount = $num1 * 100; //multiply 100 for cent 00

		

		
		

				// possible values:

		// FALSE = disable verification

		// TRUE = enable verification

		$configArray["certificateVerifyPeer"] = FALSE;

		

		// possible values:

		// 0 = do not check/verify hostname

		// 1 = check for existence of hostname in certificate

		// 2 = verify request hostname matches certificate hostname

		$configArray["certificateVerifyHost"] = 2;

		

		

		// Base URL of the Payment Gateway. Do not include the version.

		$configArray["gatewayUrl"] = "https://paymentgateway.commbank.com.au/api/rest";

		

		// Merchant ID supplied by your payments provider

		$configArray["merchantId"] = $vpc_Merchant;

		

		// API username in the format below where Merchant ID is the same as above

		$configArray["apiUsername"] = "merchant.".$vpc_Merchant;

		

		// API password which can be configured in Merchant Administration

		$configArray["password"] = $password;

		

		

		// The debug setting controls displaying the raw content of the request and 

		// response for a transaction.

		// In production you should ensure this is set to FALSE as to not display/use

		// this debugging information

		$configArray["debug"] = FALSE;

		

		// Version number of the API being used for your integration

		// this is the default value if it isn't being specified in process.php

		$configArray["version"] = "48";

		

		$method = "PUT";



		// The following section allows the example code to setup the custom/changing components to the URI

		// In your integration, you should never pass these in, but set the values here based on your requirements

		$customUri = "";

		

		$customUri .= "/3DSecureId/" . $order_id;

		

		

		// Creates the Merchant Object from config. If you are using multiple merchant ID's,

		// you can pass in another configArray each time, instead of using the one from configuration.php

		$merchantObj = new Merchant($configArray);

		

		// The Parser object is used to process the response from the gateway and handle the connections

		$parserObj = new Parser($merchantObj);

		

		// In your integration, you should never pass this in, but store the value in configuration

		// If you wish to use multiple versions, you can set the version as is being done below

		 $merchantObj->SetVersion(48);

		

		$callback_url=$this->url->link('payment/commweb_3dsecure/callback', '', true);

		

		// form transaction request

		$request = '{ "apiOperation": "CHECK_3DS_ENROLLMENT", "order": { "amount": "'.$num1.'", "currency": "AUD" },"sourceOfFunds":{"provided":{"card":{"number":"'.$card_number.'","expiry":{"month":"'.$expiry_month.'","year":"'.$expiry_year.'"}}}}, "3DSecure": { "authenticationRedirect": { "pageGenerationMode": "CUSTOMIZED", "responseUrl": "'.$callback_url.'" } } }';

		

		//$request = $parserObj->ParseRequest($_POST);

		

		

		// forms the requestUrl and assigns it to the merchantObj gatewayUrl member

		// returns what was assigned to the gatewayUrl member for echoing if in debug mode

		$requestUrl = $parserObj->FormRequestUrl($merchantObj, $customUri);

		

		// attempt sending of transaction

		// $response is used in receipt page, do not change variable name

		 $response = $parserObj->SendTransaction($merchantObj, $request, $method);

		

		

		$response_data=json_decode($response, TRUE);

		

		if(isset($response_data['response']['gatewayRecommendation']) && $response_data['response']['gatewayRecommendation']=='PROCEED'){
          
		  if(isset($response_data['3DSecure']['authenticationRedirect']['customized']['acsUrl']) && $response_data['3DSecure']['authenticationRedirect']['customized']['acsUrl']!=''){
		  $form_data='<form name="3dsRedirect" id="3dsRedirect" action="'.$response_data['3DSecure']['authenticationRedirect']['customized']['acsUrl'].'" method="POST" accept-charset="UTF-8"><input type="hidden" name="PaReq" value="'.$response_data['3DSecure']['authenticationRedirect']['customized']['paReq'].'"/><input type="hidden" name="TermUrl" value="'.$callback_url.'"/><input type="hidden" name="MD" value="'.$response_data['3DSecureId'].'"/><div class="form-group"><label class="col-md-7 control-label" for="process"></label><div class="col-md-7"><button id="process" name="process" class="btn btn-primary">Click Here to Continue</button></div></div></form>';

		  

		  $json['success'] = $form_data;

		  echo json_encode($json);

		  //$this->response->addHeader('Content-Type: application/json');

		  //$this->response->setOutput(json_encode($json));

		  exit;

		}else{
		  $json['error'] = 'Invalid card details';

		  echo json_encode($json);
		  exit;
		}  

		}else{

		

		  $json['error'] = $response_data['error']['explanation'];

		  echo json_encode($json);

		  //$this->response->addHeader('Content-Type: application/json');

		  //$this->response->setOutput(json_encode($json));		

		  exit;

		}

		

		

		

	}

	

	function callback(){

	    $this->load->model('checkout/order');

		

		$order_id = $this->session->data['order_id'];

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

		

		//$vpc_AccessCode = $this->config->get('commweb_access_code');

		 $vpc_Merchant = $this->config->get('commweb_3dsecure_merchant_id');

		

		 $password = $this->config->get('commweb_3dsecure_access_code');

		 

		 $card_number = $this->session->data['card_data']['vpc_CardNum'];

		 $expiry_month = $this->session->data['card_data']['cc_expire_date_month'];

		 $expiry_year = $this->session->data['card_data']['cc_expire_date_year'];

		 $ccv = $this->session->data['card_data']['vpc_CardSecurityCode'];

		 

		  $tprice=$this->currency->format($order_info['total'], $this->session->data['currency']);

		

		$left_symbol=$this->currency->getSymbolLeft($this->session->data['currency']);

		$right_symbol=$this->currency->getSymbolRight($this->session->data['currency']);

		

		$tprice=str_replace(",","",str_replace(array($left_symbol,$right_symbol),"",$tprice));

		

		$price=$this->currency->convert($tprice,$this->session->data['currency'],'AUD');

		

		$num1 = number_format($price, 2, ".", "");

		

		$configArray["certificateVerifyPeer"] = FALSE;

		

		// possible values:

		// 0 = do not check/verify hostname

		// 1 = check for existence of hostname in certificate

		// 2 = verify request hostname matches certificate hostname

		$configArray["certificateVerifyHost"] = 2;

		

		

		// Base URL of the Payment Gateway. Do not include the version.

		$configArray["gatewayUrl"] = "https://paymentgateway.commbank.com.au/api/rest";

		

		// Merchant ID supplied by your payments provider

		$configArray["merchantId"] = $vpc_Merchant;

		

		// API username in the format below where Merchant ID is the same as above

		$configArray["apiUsername"] = "merchant.".$vpc_Merchant;

		

		// API password which can be configured in Merchant Administration

		$configArray["password"] = $password;

		

		

		// The debug setting controls displaying the raw content of the request and 

		// response for a transaction.

		// In production you should ensure this is set to FALSE as to not display/use

		// this debugging information

		$configArray["debug"] = FALSE;

		

		// Version number of the API being used for your integration

		// this is the default value if it isn't being specified in process.php

		$configArray["version"] = "48";

		

		//process ACS Result code

		$method = "POST";

		

		$customUri = "";

		

		$customUri .= "/3DSecureId/" . $_REQUEST["MD"];

		

		$merchantObj = new Merchant($configArray);

		

		// The Parser object is used to process the response from the gateway and handle the connections

		$parserObj = new Parser($merchantObj);

		

		// In your integration, you should never pass this in, but store the value in configuration

		// If you wish to use multiple versions, you can set the version as is being done below

		$merchantObj->SetVersion('48');

		

		$request_arr=array("apiOperation"=>"PROCESS_ACS_RESULT","3DSecure"=>array("paRes"=>$_REQUEST["PaRes"]));

		

		$request=json_encode($request_arr);

		

		

		$requestUrl = $parserObj->FormRequestUrl($merchantObj, $customUri);

		

		$response = $parserObj->SendTransaction($merchantObj, $request, $method);

		

		$data2=json_decode($response, true);

		

		

		

		// get paymentOptionsInquiry code

		$customUri_new='';

		$customUri_new .= "/paymentOptionsInquiry";

		

		$merchantObj1 = new Merchant($configArray);

		

		// The Parser object is used to process the response from the gateway and handle the connections

		$parserObj1 = new Parser($merchantObj1);

		$merchantObj1->SetVersion('48');

		

		$requestUrl_new = $parserObj1->FormRequestUrl($merchantObj1, $customUri_new);

		

		

		$response_new = $parserObj1->SendTransaction($merchantObj1, '', "GET");

		$data=json_decode($response_new, true);

		

		$transactionMode = $data['transactionMode'];

		if ($transactionMode === 'PURCHASE')

			$apiOperation= 'PAY';

		else

			$apiOperation=  'AUTHORIZE';

		

				

		//create payment option

		$customUri1='';

		$customUri1 .= "/order/".$_REQUEST["MD"]."/transaction/".$_REQUEST["MD"];

		

		$merchantObj2 = new Merchant($configArray);

		

		// The Parser object is used to process the response from the gateway and handle the connections

		$parserObj2 = new Parser($merchantObj2);

		

		

		// In your integration, you should never pass this in, but store the value in configuration

		// If you wish to use multiple versions, you can set the version as is being done below

		$merchantObj2->SetVersion('48');

		

		//$request_arr_new=array("3DSecureId"=>$_REQUEST["MD"],"apiOperation"=>$apiOperation,"order"=>array("amount"=>$num1,"currency"=>"AUD"),"session"=>array("id"=>$this->session->data['sessionid']));

		

		//$request_new=json_encode($request_arr_new);

		

		$request_new='{"3DSecureId":"'.$_REQUEST["MD"].'","apiOperation":"'.$apiOperation.'","order":{"amount":"'.$num1.'","currency":"AUD"},"sourceOfFunds":{"type":"CARD","provided":{"card":{"number":"'.$card_number.'","expiry":{"month":"'.$expiry_month.'","year":"'.$expiry_year.'"},"securityCode":"'.$ccv.'"}}}}';

		file_put_contents(DIR_IMAGE.'commweblog.txt',$request_new);

		$requestUrl = $parserObj2->FormRequestUrl($merchantObj2, $customUri1);

		

		$response_pay = $parserObj2->SendTransaction($merchantObj2, $request_new, "PUT");

		

		$response_data=json_decode($response_pay, TRUE);

		

	

		if(isset($response_data['result']) && $response_data['result']=='SUCCESS'){

		    $this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get('config_order_status_id'));

			$this->model_checkout_order->addOrderHistory($order_info['order_id'], $this->config->get('commweb_3dsecure_order_status_id'), '', true);

			unset($this->session->data['card_data']);

			$success_page = $this->url->link('checkout/success', '', true);

			$this->response->redirect($success_page);

		}else{

		     $this->log->write($response_data['error']['explanation']);

			 $error=$response_data['error']['explanation'];

			 if(empty($error)){

			   $error=$response_data['response']['acquirerMessage'];

			 }

		     $success_page = $this->url->link('checkout/checkout&pay_resp='.$error, '', true);

			 $this->response->redirect($success_page);

		}

		

	}

	

	function null2unknown($map, $key) {

		if (array_key_exists($key, $map)) {

			if (!is_null($map[$key])) {

				return $map[$key];

			}

		} 

		return "No Value Returned";

	} 

}

?>