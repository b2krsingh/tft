<?php
// Tab
$_['tab_review']                   = 'Reviews';
$_['tab_related']                  = 'Related products';

// Text
$_['text_all_categories']          = 'All Categories';
$_['text_create_new_account']      = 'Create new account';

// Legend
$_['legend_review']                = 'In Store Reviews (%s)';
$_['legend_fb_review']             = 'Facebook Reviews';
