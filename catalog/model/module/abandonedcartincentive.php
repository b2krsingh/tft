<?php

/*
 * ***********************************************************************************
 * Abandoned Cart Module -                                                          **
 * Provide Coupons to the customers whose carts left abandoned.                     **
 * By Velocity Software Solutions Pvt. Ltd.                                         **
 * ***********************************************************************************
 */


class ModelModuleAbandonedCartIncentive extends Model
{

    /** Function to get all abandoned cart between the specified time
      * $startDelay time from, abandoned cart to be fetched
      * $endDelay timt to, abandoned cart will be fetched
      * $store_id, store of which the data to be fetched 
      * return the array of abandoned carts 
      */
    public function getAbandonedCarts($startDelay, $endDelay, $store_id = 0, $isRegistered = 1)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcartincentive`
			WHERE (`date_modified` BETWEEN '" . $startDelay . "' AND '" . $endDelay . "' ) AND `store_id`='" . $store_id . "' AND `autoEmail` = '0' AND `isOrderCompleted` = '0' AND `isRegistered` = '".$isRegistered."'");

        if($query->num_rows > 0)
        {
            return $query->rows;
        }
        else
        {
            return;
        }
    }

    public function saveCoupon($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "coupon SET name = '" . $this->db->escape($data['name']) . "', code = '" . $this->db->escape($data['code']) . "', discount = '" . (float) $data['discount'] . "', type = '" . $this->db->escape($data['type']) . "', total = '" . (float) $data['total'] . "', logged = '" . (int) $data['logged'] . "', shipping = '" . (int) $data['shipping'] . "', date_start = '" . $this->db->escape($data['date_start']) . "', date_end = '" . $this->db->escape($data['date_end']) . "', uses_total = '" . (int) $data['uses_total'] . "', uses_customer = '" . (int) $data['uses_customer'] . "', status = '" . (int) $data['status'] . "', date_added = NOW()");

        $coupon_id = $this->db->getLastId();

        if (isset($data['coupon_product']))
        {
            foreach ($data['coupon_product'] as $product_id)
            {
                $this->db->query("INSERT INTO " . DB_PREFIX . "coupon_product SET coupon_id = '" . (int) $coupon_id . "', product_id = '" . (int) $product_id . "'");
            }
        }

        if (isset($data['coupon_category']))
        {
            foreach ($data['coupon_category'] as $category_id)
            {
                $this->db->query("INSERT INTO " . DB_PREFIX . "coupon_category SET coupon_id = '" . (int) $coupon_id . "', category_id = '" . (int) $category_id . "'");
            }
        }
    }

    public function deleteCoupon($restoreCartID)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "coupon` WHERE `name` = 'ACI[" . $restoreCartID . "]'");
    }

    public function distinctCode($coupon)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code = '" . $this->db->escape($coupon) . "'");
        if ($query->num_rows == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function sendMail($data = array())
    {
    	
    	
    	$this->load->language('mail/customer');
        
        if(version_compare(VERSION, '2.0.2.0', '<')) {
            $mail = new Mail($this->config->get('config_mail'));

            $mail->setTo($data['email']);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender($this->config->get('config_name'));	
            $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($data['message']);
            $mail->send();
        }
        else{
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($data['email']);
            $mail->setFrom($this->config->get('config_email'));                
            $mail->setSender($this->config->get('config_name'));                
            $mail->setSubject(html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($data['message']);
            $mail->send();
        }
				       
        if ($mail)
            return true;
        else
            return false;
    }
    
    public function getCouponsWithCouponHistory($coupon_id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` c1 LEFT JOIN `" . DB_PREFIX . "coupon_history` c2 ON c1.coupon_id = c2.coupon_id WHERE c2.`coupon_id` = '" . $coupon_id . "'");
        if($query->num_rows > 0){
            return true;
        }else{
            return false;
        }
        
    }

    public function getCoupons($couponName)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE `name` = '" . $couponName . "' ORDER BY `date_added` DESC");
        return $query->rows;
    }
    
     public function disableCoupon($coupon_id)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "coupon SET status = '0' WHERE coupon_id = '".$coupon_id."'");
        return;
    }

}
