<?php
class ModelPaymentZipmoney extends Model {

    public function __construct($registry)
    {
        parent::__construct($registry);

        require_once(DIR_SYSTEM . 'library/zipmoney_util.php');

        ZipmoneyUtil::initialMerchantApi($this->config);
    }

    public function getWidgetJs() {
        return 'https://static.zipmoney.com.au/checkout/checkout-v1.js';
    }

    /**
     * The method will be called on the checkout page
     *
     * @param $address
     * @param $total
     * @return array
     */
    public function getMethod($address, $total) {

        if(strtoupper($this->session->data['currency']) != 'AUD'){
            return array();
        }

        if($total < $this->config->get('zipmoney_minimum_order_total')){
            return array();
        }

        $method_data = array(
            'code'       => 'zipmoney',
            'terms'      => '',
            'title'      => $this->config->get('zipmoney_title'),
            'sort_order' => $this->config->get('zipmoney_sort_order')
        );

        return $method_data;
    }

    public function initChargeApi()
    {
        return new \zipMoney\Api\ChargesApi();
    }


    public function createCharge($checkoutId, \zipMoney\Api\ChargesApi $chargesApi)
    {
        $response = array(
            'success' => false,
            'message' => ''
        );

        $this->load->model('checkout/order');

        $zipmoneyOrder = ZipmoneyUtil::getZipMoneyObjectById($this->db, $checkoutId);

        $checkoutObject = json_decode($zipmoneyOrder['checkout_object'], true);

        if(empty($zipmoneyOrder) || empty($checkoutObject)){
            //return null if it can't find anything
            return null;
        }

        ZipmoneyUtil::log('Creating charge ...');

        try{
            $body = self::prepareRequestForCharge($checkoutObject);

            ZipmoneyUtil::log('Request creating charge: ' . json_encode(\zipMoney\ObjectSerializer::sanitizeForSerialization($body)));

            $charge = $chargesApi->chargesCreate($body, ZipmoneyUtil::get_uuid());

            ZipmoneyUtil::log('Received charge response: ' . json_encode(\zipMoney\ObjectSerializer::sanitizeForSerialization($charge)));

            //update the charge_id
            ZipmoneyUtil::setChargeIdToCheckout($this->db, $checkoutId, $charge->getId());

            if($charge->getState() == 'captured'){
                $this->model_checkout_order->addOrderHistory(
                    $zipmoneyOrder['order_id'],
                    ZipmoneyUtil::getOrderStatusId($this->db, ZipmoneyUtil::ORDER_STATUS_PROCESSING),
                    sprintf("Checkout id: %s, Charge id: %s", $checkoutId, $charge->getId())
                );

                //Create a transaction
                ZipmoneyUtil::createTransaction(
                    $this->db,
                    $zipmoneyOrder['order_id'],
                    ZipmoneyUtil::TRANSACTION_TYPE_CHARGE,
                    $charge->getId(),
                    $charge->getAmount()
                );

                $response['success'] = true;
            } elseif($charge->getState() == 'authorised'){
                $this->model_checkout_order->addOrderHistory(
                    $zipmoneyOrder['order_id'],
                    ZipmoneyUtil::getOrderStatusId($this->db, ZipmoneyUtil::ORDER_STATUS_AUTHORIZED),
                    sprintf("Checkout id: %s, Charge id: %s", $checkoutId, $charge->getId())
                );

                $response['success'] = true;
            } else {
                $response['message'] = 'Unable to create charge. The charge state is: ' . $charge->getState();
            }
        } catch (\zipMoney\ApiException $exception) {
            ZipmoneyUtil::log($exception->getCode() . $exception->getMessage());
            ZipmoneyUtil::log($exception->getResponseBody());

            $response['message'] = ZipmoneyUtil::handleCreateChargeApiException($exception);
        } catch (Exception $exception) {
            ZipmoneyUtil::log($exception->getCode() . $exception->getMessage());

            $response['message'] = $exception->getMessage();
        }

        return $response;
    }


    private function prepareRequestForCharge($checkout)
    {
        //get the charge order
        $chargeOrder = self::createChargeOrder($checkout);

        //get authority
        $authority = new \zipMoney\Model\Authority(
            array(
                'type' => 'checkout_id',
                'value' => $checkout['id']
            )
        );

        $shouldCapture = $this->config->get('zipmoney_charge_capture_option') == ZipmoneyUtil::CHARGE_OPTION_CAPTURE_IMMEDIATELY ? true :false;

        return new \zipMoney\Model\CreateChargeRequest(array(
            'authority' => $authority,
            'amount' => ZipmoneyUtil::roundNumber($checkout['order']['amount']),
            'currency' => strtoupper($checkout['order']['currency']),
            'order' => $chargeOrder,
            'capture' => $shouldCapture
        ));
    }


    private function createChargeOrder($checkout)
    {
        //shipping address
        $shipping_array = $checkout['order']['shipping']['address'];
        $shippingAddress = new \zipMoney\Model\Address(
            array(
                'line1' => $shipping_array['line1'],
                'line2' => $shipping_array['line2'],
                'city' => $shipping_array['city'],
                'state' => $shipping_array['state'],
                'postal_code' => $shipping_array['postal_code'],
                'country' => $shipping_array['country'],
                'first_name' => $shipping_array['first_name'],
                'last_name' => $shipping_array['last_name']
            )
        );
        $orderShipping = new \zipMoney\Model\OrderShipping(
            array(
                'address' => $shippingAddress
            )
        );

        //order item
        $items = array();
        foreach($checkout['order']['items'] as $item){
            $items[] = new \zipMoney\Model\OrderItem($item);
        }

        return new \zipMoney\Model\ChargeOrder(array(
            'shipping' => $orderShipping,
            'items' => $items
        ));
    }

    public function initCheckoutApi(){
        return new \zipMoney\Api\CheckoutsApi();
    }


    /**
     * Create the checkout for API request
     *
     * @param $sessionData
     * @param $cart
     * @param \zipMoney\Api\CheckoutsApi $checkoutsApi
     * @return null|\zipMoney\Model\Checkout
     */
    public function createCheckout($sessionData, $cart, \zipMoney\Api\CheckoutsApi $checkoutsApi)
    {
        ZipmoneyUtil::log('Creating checkout...');

        $body = self::prepareRequestForCheckout($sessionData, $cart);

        ZipmoneyUtil::log('Request checkout: ' . json_encode(\zipMoney\ObjectSerializer::sanitizeForSerialization($body)));

        try {
            $checkout = $checkoutsApi->checkoutsCreate($body);

            ZipmoneyUtil::log('Receive checkout: ' . json_encode(\zipMoney\ObjectSerializer::sanitizeForSerialization($checkout)));

            //set checkout id to zipmoney table
            ZipmoneyUtil::setCheckoutIdToOrder(
                $this->db,
                $sessionData['order_id'],
                $checkout->getId(),
                json_encode(\zipMoney\ObjectSerializer::sanitizeForSerialization($checkout))
            );

            return $checkout;

        } catch (\zipMoney\ApiException $exception) {
            ZipmoneyUtil::log($exception->getCode() . $exception->getMessage());
            ZipmoneyUtil::log($exception->getResponseBody());
        } catch (Exception $exception) {
            ZipmoneyUtil::log($exception->getCode() . $exception->getMessage());
        }

        return null;
    }

    private function prepareRequestForCheckout($sessionData, $cart)
    {
        //config object
        $checkoutConfiguration = new \zipMoney\Model\CheckoutConfiguration(array(
            'redirect_uri' => $this->url->link('payment/zipmoney/complete', '', true)
        ));

        //Create checkout request
        return new \zipMoney\Model\CreateCheckoutRequest(
            array(
                'shopper' => self::createShopper($sessionData),
                'order' => self::createCheckoutOrder($sessionData, $cart),
                'config' => $checkoutConfiguration
            )
        );
    }


    private function createCheckoutOrder($sessionData, $cart)
    {
        $shippingAddress = empty($sessionData['shipping_address']) ? $sessionData['payment_address'] : $sessionData['shipping_address'];

        $orderShipping = new \zipMoney\Model\OrderShipping(array(
            'address' => self::createAddress($shippingAddress),
            'pickup' => false
        ));

        //create checkout order
        $order_items = self::getOrderItems($sessionData, $cart);

        $total = self::getOrderTotalInTotal($sessionData['order_id']);

        $checkoutOrder = new \zipMoney\Model\CheckoutOrder(array(
            'amount' => ZipmoneyUtil::roundNumber($total['value']),
            'currency' => strtoupper($sessionData['currency']),
            'shipping' => $orderShipping,
            'items' => $order_items
        ));

        return $checkoutOrder;
    }

    private function getOrderItems($sessionData, $cart)
    {
        $orderItems = array();

        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('total/coupon');

        $cartProducts = $cart->getProducts();

        //get order item
        foreach ($cartProducts as $cartProduct) {
            $product = $this->model_catalog_product->getProduct($cartProduct['product_id']);

            $orderItemData = array(
                'name' => $product['name'],
                'amount' => $cartProduct['price'],
                'description' => trim(strip_tags(htmlspecialchars_decode($product['description']))),
                'quantity' => $cartProduct['quantity'],
                'type' => 'sku',
                'item_uri' => $this->url->link('product/product', array('product_id' => $cartProduct['product_id']), true),
                'product_code' => strval($cartProduct['product_id'])
            );

            if (!empty($product['image'])) {

                if ($this->request->server['HTTPS']) {
                    $orderItemData['image_uri'] = $this->config->get('config_ssl') . 'image/' . $product['image'];
                } else {
                    $orderItemData['image_uri'] = $this->config->get('config_url') . 'image/' . $product['image'];
                }
            }

            $orderItems[] = new \zipMoney\Model\OrderItem($orderItemData);
        }

        //get discount
        $coupons = self::getCouponInTotal($sessionData['order_id']);
        if (!empty($coupons)) {
            foreach($coupons as $coupon){
                $orderItems[] = new \zipMoney\Model\OrderItem(
                    array(
                        'name' => $coupon['title'],
                        'amount' => floatval($coupon['value']),
                        'quantity' => 1,
                        'type' => 'discount'
                    )
                );
            }
        }

        //get voucher
        $vouchers = self::getVouchersInTotal($sessionData['order_id']);
        if(!empty($vouchers)){
            foreach($vouchers as $voucher){
                $orderItems[] = new \zipMoney\Model\OrderItem(
                    array(
                        'name' => $voucher['title'],
                        'amount' => floatval($voucher['value']),
                        'quantity' => 1,
                        'type' => 'discount'
                    )
                );
            }
        }

        //get shipping
        if (!empty($sessionData['shipping_method'])) {
            $orderItems[] = new \zipMoney\Model\OrderItem(
                array(
                    'name' => $sessionData['shipping_method']['title'],
                    'amount' => floatval($sessionData['shipping_method']['cost']),
                    'quantity' => 1,
                    'type' => 'shipping'
                )
            );
        }

        //get Tax for products
        $taxes = $cart->getTaxes();
        if (!empty($taxes)) {
            foreach ($cart->getTaxes() as $taxValue) {
                $orderItems[] = new \zipMoney\Model\OrderItem(
                    array(
                        'name' => 'Product Tax',
                        'amount' => floatval($taxValue),
                        'quantity' => 1,
                        'type' => 'tax'
                    )
                );
            }
        }

        //get Tax for Shipping tax
        if ($this->session->data['shipping_method']['tax_class_id']) {
            $tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

            $shippingTax = 0;

            foreach ($tax_rates as $tax_rate) {
                $shippingTax += $tax_rate['amount'];
            }

            $orderItems[] = new \zipMoney\Model\OrderItem(
                array(
                    'name' => 'Shipping Tax',
                    'amount' => $shippingTax,
                    'quantity' => 1,
                    'type' => 'tax'
                )
            );
        }

        return $orderItems;
    }


    /**
     * Create shopper object
     *
     * @param $sessionData
     * @return \zipMoney\Model\Shopper
     */
    private function createShopper($sessionData)
    {
        $data = array(
            'first_name' => $sessionData['payment_address']['firstname'],
            'last_name' => $sessionData['payment_address']['lastname']
        );

        if($this->customer->isLogged()){
            $phone = $this->customer->getTelephone();
            $data['email'] = $this->customer->getEmail();
        } else {
            $phone = $this->customer->getTelephone();
            $data['email'] = $sessionData['guest']['email'];
        }

        if(!empty($phone)){
            $data['phone'] = $phone;
        }

        $data['billing_address'] = self::createAddress($sessionData['payment_address']);

        return new \zipMoney\Model\Shopper($data);
    }


    /**
     * Create billing address object
     *
     * @param $billingAddress
     * @return \zipMoney\Model\Address
     */
    private function createAddress($billingAddress)
    {
        return new \zipMoney\Model\Address(array(
            'line1' => $billingAddress['address_1'],
            'line2' => $billingAddress['address_2'],
            'city' => $billingAddress['city'],
            'state' => $billingAddress['zone_code'],
            'postal_code' => $billingAddress['postcode'],
            'country' => $billingAddress['iso_code_2'],
            'first_name' => $billingAddress['firstname'],
            'last_name' => $billingAddress['lastname']
        ));
    }

    private function getCouponInTotal($orderId)
    {
        return $this->db->query(sprintf("SELECT * FROM `%sorder_total` WHERE `order_id` = %s AND code = 'coupon'", DB_PREFIX, $orderId))->rows;
    }

    private function getOrderTotalInTotal($orderId)
    {
        return $this->db->query(sprintf("SELECT * FROM `%sorder_total` WHERE `order_id` = %s AND code = 'total'", DB_PREFIX, $orderId))->row;
    }

    private function getVouchersInTotal($orderId)
    {
        return $this->db->query(sprintf("SELECT * FROM `%sorder_total` WHERE `order_id` = %s AND code = 'voucher'", DB_PREFIX, $orderId))->rows;
    }


}
