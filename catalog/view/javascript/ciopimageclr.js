function ciopImageclr(ciopimageclr, ciopimageclrpopup) {
  var index = 0;
  if(!ciopimageclr ||  !ciopimageclrpopup) {
    // ciopimageclr = $('.ciopimageclr-thumb').find('img').attr('data-ciopclrsrc');
    // ciopimageclrpopup = $('.ciopimageclr-thumb').attr('data-ciopclrhref');

    ciopimageclr = $('a[data-ciopclrhref]').first().find('img').attr('data-ciopclrsrc');
    ciopimageclrpopup = $('a[data-ciopclrhref]').attr('data-ciopclrhref');
  }

  $('a[data-ciopclrhref]').attr('href',ciopimageclrpopup);
  $('a[data-ciopclrhref]').find('img').attr('src',ciopimageclr);

  if ($.fn.elevateZoom) {
    $('a[data-ciopclrhref]').find('img').data('zoom-image',ciopimageclrpopup);
    $('a[data-ciopclrhref]').find('img').attr('data-zoom-image',ciopimageclrpopup);
    $('a[data-ciopclrhref]').find('img').elevateZoom('swaptheimage');
  }

  if(typeof Journal != 'undefined' && typeof Journal.changeProductImage != 'undefined') {
    Journal.changeProductImage(ciopimageclr, ciopimageclrpopup, index);
  }

  // reinitialize magnificpop so popup pic new image
  $('.thumbnails').magnificPopup({
    type:'image',
    delegate: 'a',
    gallery: {
    enabled:true
    }
  });
}
$(document).ready(function() {

  if ($.fn.elevateZoom) {
    $('#image-additional-carousel a').on('click', function() {
      var zoom_image = $(this).attr('data-zoom-image');
      $('#image').data('zoom-image',zoom_image);
      $('#image').attr('data-zoom-image',zoom_image);
      $('#image').elevateZoom('swaptheimage');
    });
  }

  $('.codi-radio').delegate('input[type=\'radio\']', 'change', function() 
  {
    $(this).parent().parent().parent().find('.ciradio-background').removeClass('ciradio-active');

    var $this = $(this);
    var el = '';
    var eltype = '';
    var action = '';

    if(($this.attr("type")=="radio") && $this.is("input") && $this.prop("checked")) {
      
       el = $this;
       eltype = $this.attr("type");
       action = 'click';
    } else {
      // check if there is any other radio is selected
      
      $.each($('.codi-radio').find('input[type=\'radio\']'), function() {

        var $thiss = $(this);
        if(($thiss.attr("type")=="radio")  && $thiss.is("input") && $thiss.prop("checked") ) {
          var ciopimageclr = $thiss.attr('data-ciopimageclr');
          if(ciopimageclr) {
            el = $thiss;
            eltype = $thiss.attr("type");
            action = 'refresh';
          }
        }
      });

    }

    var ciopimageclr = '', ciopimageclrpopup = '';
    if(el) {
      ciopimageclr = el.attr('data-ciopimageclr');
      ciopimageclrpopup = el.attr('data-ciopimageclrpopup');

      if(ciopimageclr) {
        el.parent().find('.ciradio-background').addClass('ciradio-active');
        ciopImageclr(ciopimageclr, ciopimageclrpopup);
      } else if(eltype != 'checkbox' && action == 'click') {
        if(ciopimageclr) {
          ciopImageclr(ciopimageclr, ciopimageclrpopup);
        }
      }
    } else {
      ciopImageclr(ciopimageclr, ciopimageclrpopup);  
    }

  });

});