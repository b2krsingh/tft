<footer> <?php echo $footertop; ?>
  <div id="footer" class="container test">
    <div class="row"> <?php echo $footerleft; ?>
      <?php /*?><?php if ($informations) { ?>       <div class="col-sm-3 column">         <h5><?php echo $text_information; ?></h5>         <ul class="list-unstyled">           <?php foreach ($informations as $information) { ?>           <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>           <?php } ?>         </ul>       </div>       <?php } ?>       <div class="col-sm-3 column">         <h5><?php echo $text_service; ?></h5>         <ul class="list-unstyled">                      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>         </ul>       </div><?php */?>
      <!--<?php echo $footercenter; ?>       <div class="col-sm-3 column right">         <h5><?php echo $text_extra; ?></h5>         <ul class="list-unstyled">           <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>           <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>           <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>           <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li> 		  <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>         </ul>       </div>-->
      <div class="col-sm-3 column right">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>" title="My Account"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $contact; ?>" title="Contact Us"><?php echo $text_contact; ?></a></li>
          <li><a href="https://thefurniturepeople.com.au/about_us" title="About Us">About Us</a></li>
<li><a href="https://thefurniturepeople.com.au/faq" title="Opening Hours">Opening Hours</a></li>
<li><a href="https://thefurniturepeople.com.au/faq" title="Delivery Info">Delivery Info</a></li>
        </ul>
      </div>
    </div>
    <?php echo $footerbottom; ?>
    <p><?php echo $powered; ?></p>
  </div>
</footer>


<!-- compare layout -->

<div class="__expanded " style="bottom: 0px;">

<div class="__hide-pin up-down"><i class="fa fa-angle-down"></i> Hide </div>
<ul class="__products">

<!----><li class="__product ng-star-inserted">
<div class="__product-box-full"><img width="70px" height="70px" src="assets/images/l-blog-img1.png">
<div class="__product-info"><div class="__title"><span>Fisher &amp; Paykel </span><span>RF522BRPX6 519L </span><span style="display: inline-block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">ActiveSmart Bottom Mount Fridge</span></div><div class="__price">$1,417</div></div>
<div class="button-cross-container top-right"><a class="button-cross sm blue" href="javascript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a></div></div></li><!---->

<li class="__product ng-star-inserted"><div class="__product-box-full"><img width="70px" height="70px" src="assets/images/l-blog-img1.png">
<div class="__product-info"><div class="__title"><span>Fisher &amp; Paykel </span><span>RF522BRPX6 519L </span><span style="display: inline-block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">ActiveSmart Bottom Mount Fridge</span></div><div class="__price">$1,417</div></div>
<div class="button-cross-container top-right"><a class="button-cross sm blue" href="javascript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a></div></div><div class="__product-box-empty"><div class="__description"><div class="__desktop">Add another product by ticking</div><div class="__desktop">'Add to Compare' above</div><div class="__mobile"><div class="__text">No product</div><div class="__text">selected</div></div></div><div class="__checkbox"><div class="__box"></div>Compare </div></div></li>

<li class="__product ng-star-inserted"><div class="__product-box-full"><img width="70px" height="70px" src="assets/images/l-blog-img1.png">
<div class="__product-info"><div class="__title"><span>Fisher &amp; Paykel </span><span>RF522BRPX6 519L </span><span style="display: inline-block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 100%;">ActiveSmart Bottom Mount Fridge</span></div><div class="__price">$1,417</div></div>
<div class="button-cross-container top-right"><a class="button-cross sm blue" href="javascript:void(0)"><i class="fa fa-times-circle" aria-hidden="true"></i></a></div></div><div class="__product-box-empty"><div class="__description"><div class="__desktop">Add another product by ticking</div><div class="__desktop">'Add to Compare' above</div><div class="__mobile"><div class="__text">No product</div><div class="__text">selected</div></div></div><div class="__checkbox"><div class="__box"></div>Compare </div></div></li>
</ul>

<div class="__buttons"><!----><!----><a class="cmp_slct" href="" >Compare Selected</a></div> 

</div>



<div id="popup1" class="overlay1">
	<div class="popup1">
		<span class="close1" ><i class="fa fa-times" aria-hidden="true"></i></span>
<div class="product-compare-info-dialog"><h2 class="product-compare-full-heading">Too many products</h2><p class="product-compare-full-text">You've already selected 3 products to compare. That's the maximum. To compare this product, please remove another first.</p></div>
	</div>
</div>


<script>


$(document).ready(function(){
$('.compare_button').click(function() {
var $this = $(this);
$(this).each(function(){
$this.toggleClass('btn-active');
$('.__expanded').addClass('__expanded-active');
});



$( ".__products" ).find('li').each(function(){
var item = $(this).find('.__product-box-full').length;
if(item >= 1){
$(this).addClass('active');
}
});

var n = $( ".__products" ).find('li.active').length;
if(n >= 2)
{
$('#popup1').css('opacity','1');
$('#popup1').css('visibility','visible');
}
else
{
$('#popup1').hide();
}

if(n >= 2){
    $('.__buttons a').addClass('btn-active-cmp');
}
else {
    $('.__buttons a').removeClass('btn-active-cmp');
}

})

$('.close1').click(function() {
$('#popup1').css('opacity','0');
$('#popup1').css('visibility','hidden');
});

$('.up-down').click(function() {  
    var $this = $(this);
    $this.toggleClass('minus');
	$this.parents().find('.__expanded').removeClass('__expanded-active');	
})

$('.__product-box-empty').prev('.__product-box-full').next().hide();


});



</script>



<!-- OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation. Please donate via PayPal to donate@opencart.com //--> <!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk --> 


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MC6CNW7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


 <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2gTTz1CHLRTolR5gTJ6pcuiKc3gMi25F";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!-- {literal} -->
<script>
function myFunction() {
    var x = document.getElementById('myDIV');
    if (x.style.display === 'none') {
        x.style.display = 'block';
        btnshow.style.display = 'none';
    } else {
        x.style.display = 'none';
		  btnshow.style.display = 'block';
    }
}
</script>
<!--End of Zopim Live Chat Script-->


<script type="text/javascript">
  window._mfq = window._mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/f7a0e0da-c38f-48d0-b823-77965a3f2228.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>


</body></html>