<footer> <?php echo $footertop; ?>
  <div id="footer" class="container">
    <div class="row"> <?php echo $footerleft; ?>
      <?php /*?><?php if ($informations) { ?>       <div class="col-sm-3 column">         <h5><?php echo $text_information; ?></h5>         <ul class="list-unstyled">           <?php foreach ($informations as $information) { ?>           <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>           <?php } ?>         </ul>       </div>       <?php } ?>       <div class="col-sm-3 column">         <h5><?php echo $text_service; ?></h5>         <ul class="list-unstyled">                      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>         </ul>       </div><?php */?>
      <!--<?php echo $footercenter; ?>       <div class="col-sm-3 column right">         <h5><?php echo $text_extra; ?></h5>         <ul class="list-unstyled">           <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>           <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>           <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>           <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li> 		  <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>         </ul>       </div>-->
      <div class="col-sm-3 column right">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>" title="My Account"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $contact; ?>" title="Contact Us"><?php echo $text_contact; ?></a></li>
          <li><a href="https://thefurniturepeople.com.au/about_us" title="About Us">About Us</a></li>
<li><a href="https://thefurniturepeople.com.au/faq" title="Opening Hours">Opening Hours</a></li>
<li><a href="https://thefurniturepeople.com.au/faq" title="Delivery Info">Delivery Info</a></li>
        </ul>
      </div>
    </div>
    <?php echo $footerbottom; ?>
    <p><?php echo $powered; ?></p>
  </div>
</footer>




<!-- compare layout -->
<?php $counter = 0; ?> 
<?php foreach ($products as $product) { ?>
    <?php $counter = $counter+1; ?> 
<?php } ?>


<div class="__expanded <?php if($counter >=1) { echo '__expanded-active'; } ?>" style="bottom: 0px;">
    <div class="__hide-pin up-down">
        <i class="fa fa-angle-down"></i> Hide 
    </div>
    <ul id="open_ender_output" class="__products">
        <?php $count = 0; ?>
        <?php foreach ($products as $product) { ?>
            <li class="__product ng-star-inserted">
                <div class="__product-box-full">
                    <img width="70px" height="70px" src="<?php echo $product['thumb']; ?>">
                    <div class="__product-info">
                        <div class="__title">
                            <span><?php echo $product['name']; ?></span>  
                        </div>
                        <div class="__price"><?php if($product['special']) { echo $product['special']; } else { echo $product['price']; } ?></div>
                    </div>
                    <div class="button-cross-container top-right"><a id="compaire-remove-<?php echo $product['product_id']; ?>" class="button-cross sm blue" href="<?php echo $product['remove']; ?>"><i class="fa fa-times-circle" aria-hidden="true"></i></a></div>';    
                </div>
            </li>
            <?php $count = $count+1; ?>
        <?php } ?>
        <?php if($count == 0) { ?>
            <li id="__product-ng-star-inserted1" class="__product ng-star-inserted">
                <div id="__product-box-empty" class="__product-box-empty">
                    <div class="__description">
                        <div class="__desktop">Add another product by ticking</div>
                        <div class="__desktop">'Add to Compare' above</div>
                        <div class="__mobile">
                            <div class="__text">No product</div>
                            <div class="__text">selected</div>
                        </div>   
                    </div>
                    <div class="__checkbox">
                        <div class="__box"></div>Compare 
                    </div>   
                </div>
            </li>

            <li id="__product-ng-star-inserted2" class="__product ng-star-inserted">
                <div class="__product-box-empty">
                    <div class="__description">
                        <div class="__desktop">Add another product by ticking</div>
                        <div class="__desktop">'Add to Compare' above</div>
                        <div class="__mobile">
                            <div class="__text">No product</div>
                            <div class="__text">selected</div> 
                        </div>   
                    </div>
                    <div class="__checkbox">
                    <div class="__box"></div>Compare </div>       
                </div>
            </li>
        <?php } ?>    
    </ul>
    <div class="__buttons">
        <a class="cmp_slct <?php if($count >= 2){ echo 'cmp_slct_active'; } ?>" href="<?php echo $base; ?>index.php?route=product/compare">Compare Selected</a>
    </div> 
</div>



<div id="popup1" class="overlay1">
    <div class="popup1">
        <span class="close1" ><i class="fa fa-times" aria-hidden="true"></i></span>
        <div class="product-compare-info-dialog">
            <h2 class="product-compare-full-heading">Too many products</h2>
            <p class="product-compare-full-text">You've already selected 3 products to compare. That's the maximum. To compare this product, please remove another first.</p>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
       
        
    });
</script>



<!-- OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation. Please donate via PayPal to donate@opencart.com //--> <!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk --> 


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MC6CNW7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


 <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2gTTz1CHLRTolR5gTJ6pcuiKc3gMi25F";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!-- {literal} -->
<script>
function myFunction() {
    var x = document.getElementById('myDIV');
    if (x.style.display === 'none') {
        x.style.display = 'block';
        btnshow.style.display = 'none';
    } else {
        x.style.display = 'none';
		  btnshow.style.display = 'block';
    }
}
</script>
<!--End of Zopim Live Chat Script-->


<script type="text/javascript">
  window._mfq = window._mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/f7a0e0da-c38f-48d0-b823-77965a3f2228.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>


</body></html>