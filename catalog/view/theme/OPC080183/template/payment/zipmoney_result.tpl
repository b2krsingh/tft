<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div class="container"><?php echo $content_top; ?>
  <h2><?php echo $title; ?></h2>
  <p><?php echo $content; ?></p>
  <?php echo $content_bottom; ?>
</div>
<?php echo $footer; ?>