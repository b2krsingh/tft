<?php echo $header; ?>
<div class="container category-container">
    <ul class="breadcrumb">
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
    <div class="row">
        <?php if($category_id == 62 || $category_id == 74 || $category_id == 134 || $category_id == 79 || $category_id == 77 || $category_id == 77) { ?>
            <?php echo $column_left; ?>
        <?php } else { ?>
            <script type="text/javascript">
                $("body").removeClass( "layout-2 left-col" );
            </script>    
        <?php } ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php if($category_id == 62 || $category_id == 74 || $category_id == 134 || $category_id == 79 || $category_id == 77 || $category_id == 77) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>    
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
    <div id="content" class="<?php echo $class; ?> category-page"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
     <div class="col-sm-10 category_description"><?php if($heading_title == "Clearance Sales" ) { ?><img src="https://thefurniturepeople.com.au/image/catsalebanner.jpg" style="width:100%;" /><?php } else { ?><img src="https://thefurniturepeople.com.au/image/catbanner.jpg" style="width:100%;"  alt="Melbourne Furniture Stores" /><?php }  ?></div>
      <?php if ($thumb || $description) { ?>
      <div class="row category_thumb">
	   <?php if ($description) { ?>
        <div class="col-sm-10 category_description"><br />
        <?php echo $description; ?>
        </div>
        <?php } ?>
              
 <div class="col-sm-10 category_description"><p></p></div>
        <?php if ($thumb) { ?>
        <div class="col-sm-2 category_img"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3 category_list">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php if ($products) { ?>
      <div class="row category_filter">
        <div class="col-md-4 btn-list-grid">
          <div class="btn-group">
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"></button>
			<button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"></button>
          </div>
        </div>
		<div class="compare-total">
		<a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a>
		</div>
		<div class="pagination-right">
		<div class="sort-by-wrapper">
        <div class="col-md-2 text-right sort-by">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div>
        <div class="col-md-3 text-right sort">
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
		</div>
		<div class="show-wrapper">
        <div class="col-md-1 text-right show">
          <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
        </div>
        <div class="col-md-2 text-right limit">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
		</div>
		</div>
      </div>
	  <hr>
      <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list col-xs-12">
          <div class="product-block product-thumb">
            <div class="product-block-inner ">
	  	
		<div class="image">
			<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
	  <?php if (!$product['special']) { ?>       
			 <?php } else { ?>
			<span class="saleicon sale"> <?php   
         $sprice = str_replace(',','', trim($product['special'], '$' )); 
$nprice = str_replace(',','', trim($product['price'], '$' )); 

echo trim(round(( ($sprice - $nprice ) / $nprice  * 100) ),'-').'%';
         
          ?></span>         
			 <?php } ?>	
			 <div class="hover_block">
					<div class="actions">
                    	<button class="cart_button" type="button" onclick="document.location.href='<?php echo $product['href']; ?>';"><span class="hidden-xs hidden-sm hidden-md">View Details</span></button>
						<!--<button class="cart_button" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>-->
						<button class="wishlist_button" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></button>
                                                <button id="compare_button_<?php echo $product['product_id']; ?>" class="compare_button <?php if (in_array($product['product_id'], $product['compaire'])) { echo 'btn-active'; } ?>" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>', '<?php echo $product['name']; ?>', '<?php echo $product['thumb']; ?>', '<?php if($product['special']) { echo $product['special']; } else { echo $product['price']; } ?>');"></button>
						<div class="rating">
						  <?php for ($i = 1; $i <= 5; $i++) { ?>
						  <?php if ($product['rating'] < $i) { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
						  <?php } else { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
						  <?php } ?>
						  <?php } ?>
						</div>
						</div>
       			 </div>
		</div>
		<div class="caption">
 			       	<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
					
					<div class="list-rating">
						<div class="rating">
						  <?php for ($i = 1; $i <= 5; $i++) { ?>
						  <?php if ($product['rating'] < $i) { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
						  <?php } else { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
						  <?php } ?>
						  <?php } ?>
						</div>
						</div>
			 <p><?php echo $product['description']; ?></p>
			<?php if ($product['price']) { ?>
			<p class="price">
			  <?php if (!$product['special']) { ?>
			  Price: <?php echo $product['price']; ?>
			  <?php } else { ?>
              <span class="price-old">Price: <?php echo $product['price']; ?></span><br />
              			  <span class="price-new">Promo Price: <?php echo $product['special']; ?></span> 

			  
              <?php } ?>
			  <?php if ($product['tax']) { ?>
			  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
			  <?php } ?>
			</p>
			<?php } ?>
			<button class="cart_button_res" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
			<div class="res-actions">
						<button class="res-cart_button" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
						<button class="res-wishlist_button" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_wishlist; ?></span></button>
        				<button class="res-compare_button" type="button" onclick="compare.add('<?php echo $product['product_id']; ?>', '<?php echo $product['name']; ?>', '<?php echo $product['thumb']; ?>', '<?php if($product['special']) { echo $product['special']; } else { echo $product['price']; } ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_compare; ?></span></button>
						</div>
        </div> 	    
		</div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right pages"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>