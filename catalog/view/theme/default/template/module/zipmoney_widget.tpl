
<div data-zm-merchant="<?php echo $merchant_public_key?>" data-env="<?php echo $environment?>" data-require-checkout="false"></div>

<script type="text/javascript">
    <?php
        if(!empty($zipmoney_page_script)){
            echo htmlspecialchars_decode($zipmoney_page_script);
        }
    ?>
</script>
