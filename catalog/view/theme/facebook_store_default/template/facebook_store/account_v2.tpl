<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <legend><?php echo $text_my_account; ?></legend>
      <div class="account-buttons-container">
        <a href="<?php echo $edit; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_edit; ?>"><i class="fa fa-5x fa-user"></i></a>
		<a href="<?php echo $password; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_password; ?>"><i class="fa fa-5x fa-key"></i></a>
        <a href="<?php echo $address; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_address; ?>"><i class="fa fa-5x fa-location-arrow"></i></a>
      </div>
      <legend><?php echo $text_my_orders; ?></legend>
      <div class="account-buttons-container">
        <a href="<?php echo $order; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_order; ?>"><i class="fa fa-5x fa-shopping-cart"></i></a>
        <a href="<?php echo $download; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_download; ?>"><i class="fa fa-5x fa-download"></i></a>
        <?php if ($reward) { ?>
        <a href="<?php echo $reward; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_reward; ?>"><i class="fa fa-5x fa-gift"></i></a>
        <?php } ?>
        <a href="<?php echo $transaction; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_transaction; ?>"><i class="fa fa-5x fa-trophy"></i></a>
        <a href="<?php echo $recurring; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_recurring; ?>"><i class="fa fa-5x fa-credit-card"></i></a>
      </div>
      <legend><?php echo $text_my_newsletter; ?></legend>
      <div class="account-buttons-container">
        <a href="<?php echo $newsletter; ?>" class="btn btn-lg" data-toggle="tooltip" data-html="true" title="<?php echo $text_newsletter; ?>"><i class="fa fa-5x fa-envelope"></i></a>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>