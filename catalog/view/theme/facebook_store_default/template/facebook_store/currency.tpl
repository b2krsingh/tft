<?php if (count($currencies) > 1) { ?>
    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
    <?php foreach ($currencies as $currency) { ?>
    <?php if ($currency['code'] == $code) { ?>
    <?php echo $currency['code']; ?>
    <?php } ?>
    <?php } ?>
    <i class="fa fa-caret-down"></i></a>
    <ul class="dropdown-menu dropdown-menu-right">
      <?php foreach ($currencies as $currency) { ?>
      <li><a class="currency-select btn btn-link btn-block" type="button" name="<?php echo $currency['code']; ?>"><?php echo strtoupper($currency['code']); ?></a></li>
      <?php } ?>
    </ul>

   <input type="hidden" name="code" value="" />
   <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
<?php } ?>
