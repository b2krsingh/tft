<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<meta property="fb:app_id" content="<?php echo $fb_app_id; ?>"/>
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:extralight,light,regular,bold&amp;subset=latin" rel="stylesheet" type="text/css"/>
<link href="catalog/view/theme/facebook_store_default/stylesheet/facebook_store_stylesheet.css" rel="stylesheet">
<link href="catalog/view/theme/facebook_store_default/stylesheet/facebook_store_chosen.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/jquery/jquery.slimscroll.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.chosen.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.browser-detection.js" type="text/javascript"></script>
<script src="catalog/view/javascript/facebook_store_common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php if ($fblogin_installed) { ?>
<link href="catalog/view/theme/facebook_store_default/stylesheet/fb_login.css" rel="stylesheet">
<link href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/jquery.fblogin.js" type="text/javascript"></script>

<script type="text/javascript"><!--
window.fbAsyncInit = function() {
	FB.init({
		appId      : <?php echo $fb_app_id; ?>, // App ID
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true  // parse XFBML
	});
};

// Load the SDK Asynchronously
(function(d){
	var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	d.getElementsByTagName('head')[0].appendChild(js);
}(document));
//--></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<div id="fb-root"></div>
<input type="hidden" name="facebook_store_redirect_checkout"  id="facebook-store-redirect-checkout" value="<?php echo $redirect_checkout; ?>" />
<input type="hidden" name="facebook_store_product_count"  id="facebook-store-product-count" value="<?php echo $product_count; ?>" />

<div id="slim-scroll-container">
	<nav id="top" class="navbar-fixed-top">
	  <div class="container">
		<div class="pull-left">
			<div id="home"><a href="<?php echo $home; ?>"><i class="fa fa-home"></i></a></div>
			<?php if ($fblogin_installed) { ?>
			<div id="fb-login"><a class="ocx-fb-login-trigger ocx-stay-here" href="javascript:void(0);"><i class="fa"></i><?php echo $fb_login_button; ?></a></div>
			<?php } ?>
		</div>
		<div class="pull-right">
			<div id="top-links" class="nav">
			  <ul class="list-inline">
				<li class="dropdown" id="language"><?php echo $language; ?></li>		  
				<li class="dropdown" id="currency"><?php echo $currency; ?></li>
				<li class="dropdown"><a href="<?php echo $account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
				  <ul class="dropdown-menu dropdown-menu-right">
					<?php if ($logged) { ?>
					<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
					<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
					<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
					<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
					<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
					<?php } else { ?>
					<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
					<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
					<?php } ?>
				  </ul>
				</li>     
				<li id="cart" class="dropdown"><?php echo $cart; ?></li>
			  </ul>		  
			</div>
			<div id="top-links-checkout"><a href="<?php echo $checkout; ?>" class="btn btn-success redirect-checkout"><i class="fa fa-share"></i> <span><?php echo $text_checkout; ?></span></a></div>
		</div>	
	  </div>
	</nav>
	<header>
	  <div class="container">
		<div id="logo">
			<a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
		</div>	
	  </div>
	</header>
	<div id="menu">
		<div class="row">
			<div class="col-sm-12"><?php echo $search; ?></div>
		</div>
	</div>