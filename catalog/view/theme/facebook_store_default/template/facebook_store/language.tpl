<?php if (count($languages) > 1) { ?>
    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
    <?php foreach ($languages as $language) { ?>
    <?php if ($language['code'] == $code) { ?>
    <?php echo strtoupper($language['code']); ?>
    <?php } ?>
    <?php } ?>
    <i class="fa fa-caret-down"></i></a>
    <ul class="dropdown-menu dropdown-menu-right">
      <?php foreach ($languages as $language) { ?>
      <li><a class="language-select btn btn-link btn-block" type="button" name="<?php echo $language['code']; ?>"><?php echo strtoupper($language['code']); ?></a></li>
      <?php } ?>
    </ul>

   <input type="hidden" name="code" value="" />
   <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
<?php } ?>
