<div id="search" class="input-group">
	<input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control" />
	<?php if ($categories) { ?>
	<div id="inside-search">
		<select id="filter_category_id" name="filter_category_id" class="chosen-select">
			<option value="*"><?php echo $text_all_categories; ?></option>
			<?php foreach ($categories as $category) { ?>
			<option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
			<?php if ($category['children']) { ?>
			<?php foreach($category['children'] as $child) { ?>
			<option value="<?php echo $child['category_id']; ?>" class="child-category"><?php echo $child['name']; ?></option>
			<?php } ?>
			<?php } ?>
			<?php } ?>
		</select>
	</div>
	<?php } ?>
	<span class="input-group-btn">
		<button type="button" class="btn btn-default search-trigger"><i class="fa fa-search"></i></button>
	</span>
</div>