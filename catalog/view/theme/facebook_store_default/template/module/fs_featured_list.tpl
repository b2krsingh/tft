<div class="box-heading">
	<span><?php echo $heading_title; ?></span>
	<div class="pull-right">
		<div id="fs-featured-<?php echo $module;?>-prev" class="bxslider-custom-prev-next"></div>
		<div id="fs-featured-<?php echo $module;?>-next" class="bxslider-custom-prev-next"></div>
	</div>
</div>
<div class="product-list-container" id="fs-featured-<?php echo $module; ?>">
    <?php foreach ($products as $product) { ?>
    <div class="product-list">
		<div class="product-thumb transition">
			<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
			<div>
				<div class="caption">
					<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
					<p class="description"><?php echo $product['description']; ?></p>
					<?php if ($product['rating']) { ?>
					<div class="rating">
					  <?php for ($i = 1; $i <= 5; $i++) { ?>
					  <?php if ($product['rating'] < $i) { ?>
					  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } else { ?>
					  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } ?>
					  <?php } ?>
					</div>
					<?php } ?> 
					<?php if ($product['price']) { ?>
					<p class="price">
					  <?php if (!$product['special']) { ?>
					  <?php echo $product['price']; ?>
					  <?php } else { ?>
					  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
					  <?php } ?>
					</p>
					<?php } ?>
				</div>
				<div class="button-group">
					<button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span><?php echo $button_cart; ?></span></button>
				</div>
				<div class="added-in-cart added-in-cart-<?php echo $product['product_id']; ?>"></div> 
			</div>	
		</div>
    </div>
  <?php } ?>
</div>

<?php if (count($products)) { ?>
<script type="text/javascript"><!--
$('#fs-featured-<?php echo $module; ?>').bxSlider({
	speed: 500,
	<?php if ($auto_slider) { ?>
	pause: <?php echo $slide_time; ?>,
	auto: true,
	autoHover:true,
	<?php } else { ?>
	auto: false,
	<?php } ?>
	<?php if ($random_start) { ?>
	randomStart: true,
	<?php } else { ?>
	randomStart: false,
	<?php } ?>
	infiniteLoop: true,
	touchEnabled: true,
	pager: false,
	prevSelector: '#fs-featured-<?php echo $module; ?>-prev',
	nextSelector: '#fs-featured-<?php echo $module; ?>-next',
	prevText: '<i class="fa fa-chevron-circle-left"></i>',
	nextText: '<i class="fa fa-chevron-circle-right"></i>',
});
--></script>
<?php } ?>
