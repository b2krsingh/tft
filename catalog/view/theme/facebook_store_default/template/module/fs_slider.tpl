<div class="fs-slider-container">
	<div id="fs-slider-<?php echo $module; ?>" class="camera_wrap">
	  <?php foreach ($banners as $banner) { ?>
	  <?php if ($banner['link']) { ?>
		<div data-src="<?php echo $banner['image']; ?>" data-link="<?php echo $banner['link']; ?>"></div>
	  <?php } else { ?>
	    <div data-src="<?php echo $banner['image']; ?>"></div>
	  <?php } ?>
	  <?php } ?>
	</div>
</div>

<script type="text/javascript"><!--
$('#fs-slider-<?php echo $module; ?>').camera({
	height: '<?php echo $height; ?>px',
	loader: 'bar',
	loaderBgColor: '#FFFFFF',
	loaderColor: '#0883bb',
	pagination: false
});
--></script>