jQuery(document).ready(function(){
	//Right Click Protection
	//$(document).bind("contextmenu",function(e){ return false;});
	
	//Content Area Mods
	$(".pagecontainer > table:eq(1) tr:first td:first").addClass("estore-background");
	$(".estore-background table:eq(1)").addClass("estore-content");
	$(".estore-content>tbody>tr:nth-child(2)>td:nth-child(3)").attr('id', 'rightarea');
	$( ".estore-content>tbody>tr:nth-child(2)>td:nth-child(2)" ).remove();
	
	//Getting and Setting Store Categories
	if($("#estore-categories").length > 0) {
		if($("#LeftPanel .lcat").length > 0) {
			$("#estore-categories").html($("#LeftPanel .lcat").html());
		}
		$("#estore-categories ul[class=lev1]").find("li:last").addClass("estore-lastitem");
	}
	
	if($("#estore-categories1").length > 0) {
		if($("#LeftPanel .lcat").length > 0) {
			$("#estore-categories1").html($("#LeftPanel .lcat").html());
		}
		$("#estore-categories1 ul[class=lev1]").find("li:last").addClass("estore-lastitem");
	}
	
	//Search Box
	var stxt = $("#estore-search #estore-input").find("input[class=v4sbox]").val();
	$("#estore-search #estore-input").find("input[class=v4sbox]").focus(function(){
		if($("#estore-search #estore-input").find("input[class=v4sbox]").val() == stxt) {
			$("#estore-search #estore-input").find("input[class=v4sbox]").val("");
		}
	});
	$("#estore-search #estore-input").find("input[class=v4sbox]").blur(function(){
		if($("#estore-search #estore-input").find("input[class=v4sbox]").val() == "") {
			$("#estore-search #estore-input").find("input[class=v4sbox]").val(stxt);
		}
	});
	$("#estore-search #estore-submit").find("input").click(function(){
		if($("#estore-search #estore-input").find("input[class=v4sbox]").val() == stxt) {
			$("#estore-search #estore-input").find("input[class=v4sbox]").val("");
		}
	});

	

	
	//Footer
	var d = new Date();
	var footer = '\n\r <footer><div class="ftr-comn"><div class="container container1"><div class="row"><div class="col-sm-4"><div class="ftr-01"> <img src="https://thefurniturepeople.com.au/ebay/images/ftr-pay.png" class="img-responsive center-block hidden-xs" alt=""> <img src="https://thefurniturepeople.com.au/ebay/images/ftr-pay1.png" class="img-responsive center-block visible-xs" alt=""></div></div><div class="col-sm-4"><div class="ftr-02"> <img src="https://thefurniturepeople.com.au/ebay/images/waranty.png" class="img-responsive center-block" alt=""></div></div><div class="col-sm-4"><div class="ftr-03"> <img src="https://thefurniturepeople.com.au/ebay/images/ftr-truck.png" class="img-responsive center-block hidden-xs" alt=""> <img src="https://thefurniturepeople.com.au/ebay/images/ftr-truck1.png" class="img-responsive center-block visible-xs" alt=""></div></div></div></div></div><div class="ftr-links"><div class="container container1"><ul><li><a href="https://www.ebaystores.com.au/tfpaustralia">Home</a></li><li><a href="https://www.ebaystores.com.au/tfpaustralia/about">About us</a></li><li><a href="https://www.ebaystores.com.au/tfpaustralia/i.html">our Products</a></li><li><a href="https://www.ebaystores.com.au/tfpaustralia/payment">Payments</a></li><li><a href="https://www.ebaystores.com.au/tfpaustralia/shipping">Shipping</a></li><li><a href="https://www.ebaystores.com.au/tfpaustralia/return">returns</a></li><li><a href="https://signin.ebay.com.au/ws/eBayISAPI.dll?SignIn&UsingSSL=1&pUserId=&co_partnerId=2&siteid=15&ru=https%3A%2F%2Fcontact.ebay.com.au%2Fws%2FeBayISAPI.dll%3FFindAnswers%26_trksid%3Dp2545226.m2531.l4583%26rt%3Dnc%26%26requested%3Dthefurniturepeople%26guest%3D1&pageType=5410">Contact Us</a></li></ul></div></div><div class="lower-footer"><div class="container container1"><div class="col-sm-8"><p>Copyright © 2019 THE FURNITURE PEOPLE. All Rights Reserved.</p></div><div class="col-sm-4"> <a href="https://www.estoreseller.com/"><img src="https://thefurniturepeople.com.au/ebay/images/estore.png" title="Design By:estore" alt=""></a></div></div></div> </footer>';
	
	if(pageName != "PageAboutMeViewStore") {
		if($(".estore-content").length > 0) {
			$(".estore-content").after(footer);			
		}
	}
});

