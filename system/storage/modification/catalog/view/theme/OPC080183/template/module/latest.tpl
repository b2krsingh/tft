<div class="box">

<?php if (isset($ee_tracking) && $ee_tracking && $ee_impression) { ?>
<script type="text/javascript"><!--
  $(document).ready(function() {
    setIntervalEE(function() {
      var ee_data = JSON.parse('<?php echo $ee_impression_data; ?>');
      ee_data['url'] = window.location.href;
      ee_data['title'] = document.title;
      $.ajax({
        url: 'index.php?route=module/ee_tracking/listview',
        type: 'post',
        data: ee_data,
        dataType: 'json',
        success: function(json) {
          if (json) {
            console.log(json);
          }
        },
      <?php if ($ee_impression_log) { ?>
        error: function(xhr, exc, error) {
          $.post('index.php?route=module/ee_tracking/listviewlog', { 'error': error + ' (exc: ' + exc + ' status: ' + xhr.statusText + ')', 'url': window.location.href }, function( logs ) {
            console.log(logs);
          });
        }
      <?php } ?>
    });
    }, <?php echo $ee_ga_callback ? $ee_ga_callback : 0; ?>, <?php echo $ee_generate_cid ? $ee_generate_cid : 0; ?>);
  });
  //--></script>
<?php } ?>
            
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
	<?php 
		$sliderFor = 5;
		$productCount = sizeof($products); 
	?>
	<?php if ($productCount >= $sliderFor): ?>
		<div class="customNavigation">
			<a class="btn prev">&nbsp;</a>
			<a class="btn next">&nbsp;</a>
		</div>	
	<?php endif; ?>	
	
	<div class="box-product <?php if ($productCount >= $sliderFor){?>product-carousel<?php }else{?>productbox-grid<?php }?>" id="<?php if ($productCount >= $sliderFor){?>latest-carousel<?php }else{?>latest-grid<?php }?>">
  <?php foreach ($products as $product) { ?>
  <div class="<?php if ($productCount >= $sliderFor){?>slider-item<?php }else{?>product-items<?php }?>">
    <div class="product-block product-thumb transition">
	  <div class="product-block-inner ">
	  	
		<div class="image">
			<a href="<?php echo $product['href']; ?>" <?php if (isset($ee_tracking) && $ee_tracking && $ee_click) { ?>onclick="ee_product.click('<?php echo $product['product_id']; ?>', '<?php echo isset($product['ee_position']) ? $product['ee_position'] : ''; ?>', '<?php echo isset($ee_type) ? $ee_type : ''; ?>')"<?php } ?>><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
	  <?php if (!$product['special']) { ?>       
			 <?php } else { ?>
			<span class="saleicon sale">Sale</span>         
			 <?php } ?>	
			 <div class="hover_block">
					<div class="actions">
						<button class="cart_button" type="button" <?php if (isset($ee_tracking) && $ee_tracking && $ee_cart) { ?>data-eet-position="<?php echo isset($product['ee_position']) ? $product['ee_position'] : ''; ?>" data-eet-type="<?php echo isset($ee_type) ? $ee_type : ''; ?>"<?php } ?> onclick="cart.add('<?php echo $product['product_id']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
						<button class="wishlist_button" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></button>
        				<button class="compare_button" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"></button>
						<div class="rating">
						  <?php for ($i = 1; $i <= 5; $i++) { ?>
						  <?php if ($product['rating'] < $i) { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
						  <?php } else { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
						  <?php } ?>
						  <?php } ?>
						</div>
						</div>
       			 </div>
		</div>
		<div class="caption">
 			       	<h4><a href="<?php echo $product['href']; ?>" <?php if (isset($ee_tracking) && $ee_tracking && $ee_click) { ?>onclick="ee_product.click('<?php echo $product['product_id']; ?>', '<?php echo isset($product['ee_position']) ? $product['ee_position'] : ''; ?>', '<?php echo isset($ee_type) ? $ee_type : ''; ?>')"<?php } ?>><?php echo $product['name']; ?></a></h4>
			<?php /*?> <p><?php echo $product['description']; ?></p><?php */?>
			<?php if ($product['price']) { ?>
			<p class="price">
			  <?php if (!$product['special']) { ?>
			  <?php echo $product['price']; ?>
			  <?php } else { ?>
			  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
			  <?php } ?>
			  <?php if ($product['tax']) { ?>
			  <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
			  <?php } ?>
			</p>
			<?php } ?>
			<button class="cart_button_res" type="button" <?php if (isset($ee_tracking) && $ee_tracking && $ee_cart) { ?>data-eet-position="<?php echo isset($product['ee_position']) ? $product['ee_position'] : ''; ?>" data-eet-type="<?php echo isset($ee_type) ? $ee_type : ''; ?>"<?php } ?> onclick="cart.add('<?php echo $product['product_id']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
        </div> 	    
		</div>
	</div>
</div>
  
  <?php } ?>
</div>
  </div>
</div>
<span class="latest_default_width" style="display:none; visibility:hidden"></span>
