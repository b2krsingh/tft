<?php
//==============================================================================
// Ultimate Filters Module v303.1
// 
// Author: Clear Thinking, LLC
// E-mail: johnathan@getclearthinking.com
// Website: http://www.getclearthinking.com
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

class ControllerExtensionModuleUltimateFilters extends Controller {
	private $type = 'module';
	private $name = 'ultimate_filters';
	
	public function index($settings) {
		if (empty($settings) || empty($settings['status'])) {
			return;
		}
		
		// Load correct module_id if present
		if (!empty($this->request->get['module_id'])) {
			if (version_compare(VERSION, '3.0', '<')) {
				$this->load->model('extension/module');
				$settings = $this->model_extension_module->getModule($this->request->get['module_id']);
			} else {
				$this->load->model('setting/module');
				$settings = $this->model_setting_module->getModule($this->request->get['module_id']);
			}
		}
		
		// Load needed data
		$store_id = $this->config->get('config_store_id');
		$language = $this->session->data['language'];
		$customer_group_id = (int)$this->customer->getGroupId();
		$currency = $this->session->data['currency'];
		
		// Restrictions check
		if (!array_intersect(array($store_id), $settings['stores']) ||
			!array_intersect(array($language), $settings['languages']) ||
			!array_intersect(array($customer_group_id), $settings['customer_groups']) ||
			!array_intersect(array($currency), $settings['currencies'])
		) {
			return;
		}
		
		// Load general data
		$data['type'] = $this->type;
		$data['name'] = $this->name;
		$data['settings'] = $settings;
		$data['language'] = $language;
		
		$data = array_merge($data, $this->load->language('product/search'));
		
		$prefix = (version_compare(VERSION, '3.0', '<')) ? '' : 'module_';
		$data['smartsearch'] = (int)$this->config->get($prefix . 'smartsearch_status');
		
		$this->load->model('tool/image');
		
		// Set Clear Filters return page
		if (empty($this->request->get['module_id'])) {
			$this->session->data['clear_filters'] = $this->request->server['REQUEST_URI'];
		}
		$data['clear_filters'] = (isset($this->session->data['clear_filters'])) ? $this->session->data['clear_filters'] : '';
		
		// Create cache hash
		$data['get'] = $this->request->get;
		unset($data['get']['sort']);
		unset($data['get']['order']);
		unset($data['get']['page']);
		unset($data['get']['limit']);
		
		if (isset($data['get']['path'])) {
			$paths = explode('_', $this->request->get['path']);
			$data['get']['category_id'] = ($settings['category_filter'] == 'checkbox' || $settings['category_filter'] == 'radio') ? implode(';', $paths) : array_pop($paths);
		}
		
		$data['get']['caching'] = $settings['caching'];
		$hash = md5(http_build_query($data['get']));
		
		if ($settings['category_subcategories']) {
			$data['get']['sub_category'] = 1;
		}
		
		// Get relevant values
		$data['check_relevant_values'] = false;
		
		foreach (array('attribute', 'category', 'filter', 'manufacturer', 'option') as $filter_type) {
			if ($settings[$filter_type . '_choices'] == 'relevant') {
				$data['check_relevant_values'] = true;
			}
			$data['get'][$filter_type . '_choices'] = $settings[$filter_type . '_choices'];
		}
		
		$this->load->model('extension/module/filter');
		$relevant_values = ($data['check_relevant_values']) ? $this->model_extension_module_filter->getRelevantValues($data['get']) : array();
		
		$get = array_merge(array('return_total' => true), $data['get']);
		
		// Get filter sort order
		$data['filter_sorting'] = array();
		
		foreach (array('attribute', 'category', 'filter', 'manufacturer', 'option', 'price', 'rating', 'search', 'stock') as $filter_type) {
			if ($settings[$filter_type . '_filter'] && $settings[$filter_type . '_filter'] != 'hide') {
				$data['filter_sorting'][$settings[$filter_type . '_sort_order'] . '.' . $filter_type] = $filter_type;
			}
		}
		ksort($data['filter_sorting']);
		
		// Check cache	
		$cached_data = $this->cache->get('filter.choices.' . $hash);
		
		if (!$cached_data || !$settings['caching']) {
			$cached_data = array();
			
			// Load attributes
			if ($settings['attribute_filter']) {
				$module_attributes = array();
				foreach ($settings as $key => $value) {
					if (strpos($key, 'attribute_filter_') === 0 && $value != 'hide') {
						$explode = explode('_', $key);
						$module_attributes[] = (int)$explode[2];
					}
				}
				
				$cached_data['attributes'] = array();
				$cached_data['product_attributes'] = array();
				
				if (!empty($module_attributes)) {
					$cached_data['attributes'] = $this->db->query("SELECT * FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE ad.language_id = " . (int)$this->config->get('config_language_id') . " AND (a.attribute_id = " . implode(" OR a.attribute_id = ", $module_attributes) . ") ORDER BY a.sort_order, ad.name")->rows;
					
					$product_attributes = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE language_id = " . (int)$this->config->get('config_language_id') . " AND (attribute_id = " . implode(" OR attribute_id = ", $module_attributes) . ") GROUP BY attribute_id, text ORDER BY attribute_id, text")->rows;
					
					foreach ($product_attributes as $product_attribute) {
						foreach (explode(',', $product_attribute['text']) as $attribute_value) {
							$attribute_value = trim($attribute_value);
							if (empty($attribute_value)) continue;
							
							if (isset($relevant_values['attribute'])) {
								if (empty($relevant_values['attribute'][$product_attribute['attribute_id']]) || !in_array($attribute_value, $relevant_values['attribute'][$product_attribute['attribute_id']])) {
									continue;
								}
							}
							
							if (!empty($settings['attribute_count'])) {
								$temp_get = $get;
								$temp_get['attribute'][$product_attribute['attribute_id']] = $attribute_value;
								$product_count = $this->model_extension_module_filter->getProducts($temp_get);
								$attribute_value .= ' (' . $product_count . ')';
							}
							
							$cached_data['product_attributes'][$product_attribute['attribute_id']][] = html_entity_decode($attribute_value, ENT_QUOTES, 'UTF-8');
						}
						if (!empty($cached_data['product_attributes'][$product_attribute['attribute_id']])) {
							$cached_data['product_attributes'][$product_attribute['attribute_id']] = array_unique($cached_data['product_attributes'][$product_attribute['attribute_id']]);
							sort($cached_data['product_attributes'][$product_attribute['attribute_id']], defined('SORT_NATURAL') ? SORT_NATURAL : SORT_REGULAR);
						}
					}
				}
			}
			
			// Load categories
			if ($settings['category_filter'] != 'hide') {
				$this->load->model('catalog/category');
				$cached_data['categories'] = array();
				$non_relevants = array();
				$non_links = array();
				
				// note: at some point, rewrite this to use a recursive function
				foreach ($this->model_catalog_category->getCategories(0) as $category_1) {
					if (isset($relevant_values['category'])) {
						if (empty($relevant_values['category']) || !in_array($category_1['category_id'], $relevant_values['category'])) {
							$non_relevants[$category_1['category_id']] = $category_1['category_id'];
						}
					}
					if ($settings['category_filter'] == 'links') {
						if (!empty($data['get']['category_id']) && !in_array($category_1['category_id'], explode(';', $data['get']['category_id']))) {
							$non_links[$category_1['category_id']] = $category_1['category_id'];
						}
					}
					if (!empty($category_1['image']) && $settings['category_images'] && $settings['category_filter'] != 'select') {
						$category_1['name'] = '<img src="' . $this->model_tool_image->resize($category_1['image'], $settings['category_images'], $settings['category_images']) . '" width="' . $settings['category_images'] . '" height="' . $settings['category_images'] . '" /> ' . $category_1['name'];
					}
					$cached_data['categories'][] = array(
						'category_id'	=> $category_1['category_id'],
						'name'			=> $category_1['name'],
					);
					foreach ($this->model_catalog_category->getCategories($category_1['category_id']) as $category_2) {
						if (isset($relevant_values['category'])) {
							if (empty($relevant_values['category']) || !in_array($category_2['category_id'], $relevant_values['category'])) {
								$non_relevants[$category_2['category_id']] = $category_2['category_id'];
							} else {
								unset($non_relevants[$category_1['category_id']]);
							}
						}
						if ($settings['category_filter'] == 'links') {
							if (empty($data['get']['category_id']) || ($data['get']['category_id'] != $category_1['category_id'] && !in_array($category_2['category_id'], explode(';', $data['get']['category_id'])))) {
								$non_links[$category_2['category_id']] = $category_2['category_id'];
							} else {
								unset($non_links[$category_1['category_id']]);
							}
						}
						if (!empty($category_2['image']) && $settings['category_images'] && $settings['category_filter'] != 'select') {
							$category_2['name'] = '<img src="' . $this->model_tool_image->resize($category_2['image'], $settings['category_images'], $settings['category_images']) . '" width="' . $settings['category_images'] . '" height="' . $settings['category_images'] . '" /> ' . $category_2['name'];
						}
						$cached_data['categories'][] = array(
							'category_id'	=> $category_2['category_id'],
							'name'			=> ' &nbsp; &nbsp; ' . $category_2['name'],
						);
						foreach ($this->model_catalog_category->getCategories($category_2['category_id']) as $category_3) {
							if (isset($relevant_values['category'])) {
								if (empty($relevant_values['category']) || !in_array($category_3['category_id'], $relevant_values['category'])) {
									$non_relevants[$category_3['category_id']] = $category_3['category_id'];
								} else {
									unset($non_relevants[$category_1['category_id']]);
									unset($non_relevants[$category_2['category_id']]);
								}
							}
							if ($settings['category_filter'] == 'links') {
								if (empty($data['get']['category_id']) || ($data['get']['category_id'] != $category_2['category_id'] && !in_array($category_3['category_id'], explode(';', $data['get']['category_id'])))) {
									$non_links[$category_3['category_id']] = $category_3['category_id'];
								} else {
									unset($non_links[$category_1['category_id']]);
									unset($non_links[$category_2['category_id']]);
								}
							}
							if (!empty($category_3['image']) && $settings['category_images'] && $settings['category_filter'] != 'select') {
								$category_3['name'] = '<img src="' . $this->model_tool_image->resize($category_3['image'], $settings['category_images'], $settings['category_images']) . '" width="' . $settings['category_images'] . '" height="' . $settings['category_images'] . '" /> ' . $category_3['name'];
							}
							$cached_data['categories'][] = array(
								'category_id'	=> $category_3['category_id'],
								'name'			=> ' &nbsp; &nbsp; &nbsp; &nbsp; ' . $category_3['name'],
							);
							foreach ($this->model_catalog_category->getCategories($category_3['category_id']) as $category_4) {
								if (isset($relevant_values['category'])) {
									if (empty($relevant_values['category']) || !in_array($category_4['category_id'], $relevant_values['category'])) {
										$non_relevants[$category_4['category_id']] = $category_4['category_id'];
									} else {
										unset($non_relevants[$category_1['category_id']]);
										unset($non_relevants[$category_2['category_id']]);
										unset($non_relevants[$category_3['category_id']]);
									}
								}
								if ($settings['category_filter'] == 'links') {
									if (empty($data['get']['category_id']) || ($data['get']['category_id'] != $category_3['category_id'] && !in_array($category_4['category_id'], explode(';', $data['get']['category_id'])))) {
										$non_links[$category_4['category_id']] = $category_4['category_id'];
									} else {
										unset($non_links[$category_1['category_id']]);
										unset($non_links[$category_2['category_id']]);
										unset($non_links[$category_3['category_id']]);
									}
								}
								if (!empty($category_4['image']) && $settings['category_images'] && $settings['category_filter'] != 'select') {
									$category_4['name'] = '<img src="' . $this->model_tool_image->resize($category_4['image'], $settings['category_images'], $settings['category_images']) . '" width="' . $settings['category_images'] . '" height="' . $settings['category_images'] . '" /> ' . $category_4['name'];
								}
								$cached_data['categories'][] = array(
									'category_id'	=> $category_4['category_id'],
									'name'			=> ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ' . $category_4['name'],
								);
								foreach ($this->model_catalog_category->getCategories($category_4['category_id']) as $category_5) {
									if (isset($relevant_values['category'])) {
										if (empty($relevant_values['category']) || !in_array($category_5['category_id'], $relevant_values['category'])) {
											$non_relevants[$category_5['category_id']] = $category_5['category_id'];
										} else {
											unset($non_relevants[$category_1['category_id']]);
											unset($non_relevants[$category_2['category_id']]);
											unset($non_relevants[$category_3['category_id']]);
											unset($non_relevants[$category_4['category_id']]);
										}
									}
									if ($settings['category_filter'] == 'links') {
										if (empty($data['get']['category_id']) || ($data['get']['category_id'] != $category_4['category_id'] && !in_array($category_5['category_id'], explode(';', $data['get']['category_id'])))) {
											$non_links[$category_5['category_id']] = $category_5['category_id'];
										} else {
											unset($non_links[$category_1['category_id']]);
											unset($non_links[$category_2['category_id']]);
											unset($non_links[$category_3['category_id']]);
											unset($non_links[$category_4['category_id']]);
										}
									}
									if (!empty($category_5['image']) && $settings['category_images'] && $settings['category_filter'] != 'select') {
										$category_5['name'] = '<img src="' . $this->model_tool_image->resize($category_5['image'], $settings['category_images'], $settings['category_images']) . '" width="' . $settings['category_images'] . '" height="' . $settings['category_images'] . '" /> ' . $category_5['name'];
									}
									$cached_data['categories'][] = array(
										'category_id'	=> $category_5['category_id'],
										'name'			=> ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ' . $category_5['name'],
									);
								}
							}
						}
					}
				}
				
				$top_parent = 0;
				
				if (isset($data['get']['category_id'])) {
					$top_parent_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE category_id = " . (int)$data['get']['category_id'] . " AND `level` = 0");
					if ($top_parent_query->num_rows) {
						$top_parent = $top_parent_query->row['path_id'];
					}
				}
				
				foreach ($cached_data['categories'] as $key => $value) {
					if (in_array($value['category_id'], $non_relevants) || in_array($value['category_id'], $non_links) || (!empty($settings['categories']) && !in_array($value['category_id'], $settings['categories']))) {
						if ($settings['category_links'] == 'top') {
							$same_top_parent = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_path WHERE category_id = " . (int)$value['category_id'] . " AND path_id = " . (int)$top_parent);
							if ($same_top_parent->num_rows) continue;
						}
						unset($cached_data['categories'][$key]);
					} elseif (!empty($settings['category_count'])) {
						$temp_get = array_merge($get, array('category_id' => $value['category_id']));
						$product_count = $this->model_extension_module_filter->getProducts($temp_get);
						$cached_data['categories'][$key]['name'] .= ' (' . $product_count . ')';
					}
				}
			}
			
			// Load filters
			if ($settings['filter_filter']) {
				$module_filters = array();
				foreach ($settings as $key => $value) {
					if (strpos($key, 'filter_filter_') === 0 && $value != 'hide') {
						$explode = explode('_', $key);
						$module_filters[] = (int)$explode[2];
					}
				}
				
				$cached_data['filters'] = array();
				$cached_data['filter_values'] = array();
				
				if (!empty($module_filters)) {
					$cached_data['filters'] = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter_group fg LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE fgd.language_id = " . (int)$this->config->get('config_language_id') . " AND (fg.filter_group_id = " . implode(" OR fg.filter_group_id = ", $module_filters) . ") ORDER BY fg.sort_order, fgd.name")->rows;
					
					$filter_values = $this->db->query("SELECT * FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE fd.language_id = " . (int)$this->config->get('config_language_id') . " AND (f.filter_group_id = " . implode(" OR f.filter_group_id = ", $module_filters) . ") ORDER BY f.sort_order, fd.name")->rows;
					
					foreach ($filter_values as $filter_value) {
						if (isset($relevant_values['filter'])) {
							if (empty($relevant_values['filter']) || !in_array($filter_value['filter_id'], $relevant_values['filter'])) {
								continue;
							}
						}
						/*
						if ($data['get']['category_id']) {
							$category_ids = array_map('intval', explode(';', $data['get']['category_id']));
							$category_filter = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_filter WHERE (category_id = " . implode(" OR category_id = ", $category_ids) . ") AND filter_id = " . (int)$filter_value['filter_id']);
							if (!$category_filter->num_rows) continue;
						}
						*/
						if (!empty($settings['filter_count'])) {
							$temp_get = $get;
							$temp_get['filter'][$filter_value['filter_group_id']] = $filter_value['filter_id'];
							$product_count = $this->model_extension_module_filter->getProducts($temp_get);
							$filter_value['name'] .= ' (' . $product_count . ')';
						}
						$cached_data['filter_values'][$filter_value['filter_group_id']][$filter_value['filter_id']] = trim(html_entity_decode($filter_value['name'], ENT_QUOTES, 'UTF-8'));
					}
				}
			}
			
			// Load manufacturers
			if ($settings['manufacturer_filter'] != 'hide') {
				$this->load->model('catalog/manufacturer');
				$cached_data['manufacturers'] = array();
				
				foreach ($this->model_catalog_manufacturer->getManufacturers() as $manufacturer) {
					if (isset($relevant_values['manufacturer'])) {
						if (empty($relevant_values['manufacturer']) || !in_array($manufacturer['manufacturer_id'], $relevant_values['manufacturer'])) {
							continue;
						}
					}
					if (!empty($manufacturer['image']) && $settings['manufacturer_images'] && $settings['manufacturer_filter'] != 'select') {
						$manufacturer['name'] = '<img src="' . $this->model_tool_image->resize($manufacturer['image'], $settings['manufacturer_images'], $settings['manufacturer_images']) . '" width="' . $settings['manufacturer_images'] . '" height="' . $settings['manufacturer_images'] . '" /> ' . $manufacturer['name'];
					}
					if (!empty($settings['manufacturer_count'])) {
						$temp_get = array_merge($get, array('manufacturer_id' => $manufacturer['manufacturer_id']));
						$product_count = $this->model_extension_module_filter->getProducts($temp_get);
						$manufacturer['name'] .= ' (' . $product_count . ')';
					}
					$cached_data['manufacturers'][] = array(
						'manufacturer_id'	=> $manufacturer['manufacturer_id'],
						'name'				=> $manufacturer['name'],
					);
				}
			}
			
			// Load options
			if ($settings['option_filter']) {
				$module_options = array();
				foreach ($settings as $key => $value) {
					if (strpos($key, 'option_filter_') === 0 && $value != 'hide') {
						$explode = explode('_', $key);
						$module_options[] = (int)$explode[2];
					}
				}
				
				$cached_data['options'] = array();
				$cached_data['option_values'] = array();
				
				if (!empty($module_options)) {
					$cached_data['options'] = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE od.language_id = " . (int)$this->config->get('config_language_id') . " AND (o.option_id = " . implode(" OR o.option_id = ", $module_options) . ") ORDER BY o.sort_order, od.name")->rows;
					
					$option_values = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ovd.language_id = " . (int)$this->config->get('config_language_id') . " AND (ov.option_id = " . implode(" OR ov.option_id = ", $module_options) . ") ORDER BY ov.sort_order, ovd.name")->rows;
					
					foreach ($option_values as $option_value) {
						if (isset($relevant_values['option'])) {
							if (empty($relevant_values['option'][$option_value['option_id']]) || !in_array($option_value['option_value_id'], $relevant_values['option'][$option_value['option_id']])) {
								continue;
							}
						}
						if (!empty($option_value['image']) && $settings['option_images'] && $settings['option_filter_' . $option_value['option_id']] != 'select') {
							$option_value['name'] = '<img src="' . $this->model_tool_image->resize($option_value['image'], $settings['option_images'], $settings['option_images']) . '" width="' . $settings['option_images'] . '" height="' . $settings['option_images'] . '" /> ' . $option_value['name'];
						}
						if (!empty($settings['option_count'])) {
							$temp_get = $get;
							$temp_get['option'][$option_value['option_id']] = $option_value['option_value_id'];
							$product_count = $this->model_extension_module_filter->getProducts($temp_get);
							$option_value['name'] .= ' (' . $product_count . ')';
						}
						$cached_data['option_values'][$option_value['option_id']][$option_value['option_value_id']] = trim(html_entity_decode($option_value['name'], ENT_QUOTES, 'UTF-8'));
					}
				}
			}
			
			// Load price ranges
			if ($settings['price_filter'] != 'hide') {
				$cached_data['price_ranges'] = array();
				$price_points = explode(',', preg_replace('/\s+/', '', $settings['price_range_' . $currency]));
				
				if ($price_points[0] != '') {
					$i = 0;
					foreach ($price_points as $price) {
						$text = $this->currency->format($price, $currency, 1, true);
						if ($i == 0) {
							$cached_data['price_ranges']['0-' . $price] = str_replace('[price]', $text, $settings['price_bottom_text_' . $data['language']]);
						} elseif ($i < count($price_points)) {
							$cached_data['price_ranges'][$last_price . '-' . $price] = str_replace(array('[from]', '[to]'), array($last_text, $text), $settings['price_middle_text_' . $data['language']]);
						}
						$last_price = $price;
						$last_text = $text;
						$i++;
					}
					$cached_data['price_ranges'][$last_price . '-'] = str_replace('[price]', $last_text, $settings['price_top_text_' . $data['language']]);
				}
				
				if (!empty($settings['price_count'])) {
					foreach ($cached_data['price_ranges'] as $price_range => &$price_text) {
						$temp_get = $get;
						$temp_get['price'] = $price_range;
						$product_count = $this->model_extension_module_filter->getProducts($temp_get);
						$price_text .= ' (' . $product_count . ')';
					}
				}
				
				if (!empty($settings['price_flexible'])) {
					$max_price_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product ORDER BY price DESC LIMIT 1");
					$cached_data['max_price'] = $max_price_query->row['price'];
					
					$cached_data['lower'] = '';
					$cached_data['upper'] = '';
					
					if (!empty($this->request->get['price'])) {
						$price_ranges = explode(';', $this->request->get['price']);
						$last_price_range = array_pop($price_ranges);
						$price_range = explode('-', $last_price_range);
						
						$lower = $price_range[0];
						$upper = (isset($price_range[1])) ? $price_range[1] : '';
						
						if ($settings['price_flexible'] == 'slider' || !in_array($lower . '-' . $upper, array_keys($cached_data['price_ranges']))) {
							$cached_data['lower'] = $lower;
							$cached_data['upper'] = $upper;
						}
					}
				}
				
				$cached_data['left_symbol'] = $this->currency->getSymbolLeft($currency);
				$cached_data['right_symbol'] = $this->currency->getSymbolRight($currency);
			}
			
			// Load ratings data
			if ($settings['rating_filter'] != 'hide' && !empty($settings['rating_count'])) {
				for ($rating = 1; $rating <= 5; $rating++) {
					$temp_get = $get;
					$temp_get['rating'] = $rating;
					$product_count = $this->model_extension_module_filter->getProducts($temp_get);
					$cached_data['rating_product_count'][$rating] = ' (' . $product_count . ')';
				}
			}
			
			// Load stock status data
			if ($settings['stock_filter'] != 'hide') {
				if ($settings['outofstock_text_' . $data['language']]) {
					$cached_data['stock_statuses'] = array(array('stock_status_id' => 'out', 'name' => $settings['outofstock_text_' . $data['language']]));
				} else {
					$cached_data['stock_statuses'] = $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_status WHERE language_id = " . (int)$this->config->get('config_language_id') . " ORDER BY name")->rows;
				}
				array_unshift($cached_data['stock_statuses'], array('stock_status_id' => 'in', 'name' => $settings['instock_text_' . $data['language']]));
				
				if (!empty($settings['stock_count'])) {
					foreach ($cached_data['stock_statuses'] as &$stock_status) {
						$temp_get = $get;
						$temp_get['stock_status'] = $stock_status['stock_status_id'];
						$product_count = $this->model_extension_module_filter->getProducts($temp_get);
						$stock_status['name'] .= ' (' . $product_count . ')';
					}
				}
			}
			
			$this->cache->set('filter.choices.' . $hash, $cached_data);
		}
		
		// Merge cached data
		$data = array_merge($data, $cached_data);
		
		// Load page type
		if (isset($this->request->get['route'])) {
			$route = explode('/', $this->request->get['route']);
			if (isset($this->request->get['list'])) {
				$data['list'] = $this->request->get['list'];
			} elseif ($route[1] == 'list' && $route[2] != 'all') {
				$data['list'] = $route[2];
			} elseif ($route[1] == 'special') {
				$data['list'] = 'special';
			}
		}
		
		// Load third-party libraries
		if ($settings['price_flexible'] == 'slider') {
			$this->document->addLink('catalog/view/javascript/nouislider/nouislider.min.css', 'stylesheet');
			$this->document->addScript('catalog/view/javascript/nouislider/nouislider.min.js');
		}
		
		// Render
		$theme = (version_compare(VERSION, '2.2', '<')) ? $this->config->get('config_template') : $this->config->get('theme_default_directory');
		$template = (file_exists(DIR_TEMPLATE . $theme . '/template/extension/' . $this->type . '/' . $this->name . '.twig')) ? $theme : 'default';
		$template_file = DIR_TEMPLATE . $template . '/template/extension/' . $this->type . '/' . $this->name . '.twig';
		
		if (version_compare(VERSION, '3.0', '>=')) {
			$override_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "theme WHERE theme = '" . $this->db->escape($theme) . "' AND route = 'extension/" . $this->type . "/" . $this->name . "'");
			if ($override_query->num_rows) {
				$cache_file = DIR_CACHE . $this->name . '.twig.' . strtotime($override_query->row['date_added']);
				
				if (!file_exists($cache_file)) {
					$old_files = glob(DIR_CACHE . $this->name . '.twig.*');
					foreach ($old_files as $old_file) unlink($old_file);
					file_put_contents($cache_file, html_entity_decode($override_query->row['code'], ENT_QUOTES, 'UTF-8'));
				}
				
				$template_file = $cache_file;
			}
		}
		
		if (is_file($template_file)) {
			extract($data);
			
			ob_start();
			require(class_exists('VQMod') ? VQMod::modCheck(modification($template_file)) : modification($template_file));
			$output = ob_get_clean();
			
			return $output;
		} else {
			return 'Error loading template file: ' . $template_file;
		}
	}
	
	//==============================================================================
	// rewrite()
	//==============================================================================
	public function rewrite() {
		$explode = explode('?route=', $this->request->post['url']);
		$url = explode('&amp;', $explode[1], 2);
		$link = $this->url->link($url[0], str_replace('&amp;', '&', $url[1]));
		echo str_replace('&amp;', '&', $link);
	}
}
?>