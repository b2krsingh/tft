<?php
####################################################
#    AutoSearch 1.11 for Opencart 2.x by AlexDW    #
####################################################
class ControllerModuleAutosearch extends Controller {

	private $error = array(); 

	public function index() {   
		$this->load->language('module/autosearch');

		$this->document->setTitle(strip_tags($this->language->get('heading_title')));

		$this->load->model('extension/module');
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('autosearch', $this->request->post);		

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$text_strings = array(
				'heading_title',
				'text_module',
				'text_edit',
				'text_search',
				'text_show',
				'text_enabled',
				'text_disabled',
				'text_sort_date',
				'text_viewall',
				'text_code_variant1',
				'text_code_variant2',
				'text_code_variant3',
				'text_sort_name',
				'entry_show',
				'entry_show_model',
				'entry_show_price',
				'entry_show_quantity',
				'entry_limit',
				'entry_symbol',
				'entry_status',
				'entry_tag',
				'entry_model',
				'entry_sku',
				'entry_upc',
				'entry_ean',
				'entry_jan',
				'entry_isbn',
				'entry_mpn',
				'entry_location',
				'entry_attr',
				'entry_sort',
				'entry_codepage',
				'entry_viewall',
				'entry_asr_image',
				'button_save',
				'button_cancel',
				'button_add_module',
				'button_remove'
		);

		foreach ($text_strings as $text) {
			$data[$text] = $this->language->get($text);
		}

		$config_data = array(
				'autosearch_status',
				'autosearch_show',
				'autosearch_show_model',
				'autosearch_show_price',
				'autosearch_show_quantity',
				'autosearch_tag',
				'autosearch_model',
				'autosearch_sku',
				'autosearch_upc',
				'autosearch_ean',
				'autosearch_jan',
				'autosearch_isbn',
				'autosearch_mpn',
				'autosearch_location',
				'autosearch_attr',
				'autosearch_asr_image',
				'autosearch_limit',
				'autosearch_sort',
				'autosearch_codepage',
				'autosearch_viewall',
				'autosearch_symbol'
		);

		foreach ($config_data as $conf) {
			if (isset($this->request->post[$conf])) {
				$data[$conf] = $this->request->post[$conf];
			} else {
				$data[$conf] = $this->config->get($conf);
			}
		}

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/autosearch', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/autosearch', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/autosearch.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/autosearch')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}
?>