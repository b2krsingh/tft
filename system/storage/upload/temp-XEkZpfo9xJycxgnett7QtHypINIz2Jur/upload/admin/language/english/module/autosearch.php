<?php
####################################################
#    AutoSearch 1.11 for Opencart 2.x by AlexDW    #
####################################################

$_['heading_title']    = '<i class="fa fa-cubes fa-lg" style="color: #5BC0DE"></i> AutoSearch 2X - AJAX Instant search [2000-2200]';

// Text
$_['text_module']      = 'Modules';
$_['text_edit']        = 'Edit AutoSearch Module';
$_['text_search']      = 'Search in';
$_['text_show']        = 'Show in results';
$_['text_sort_date']   = 'Date available';
$_['text_sort_name']   = 'Name';
$_['text_viewall']     = 'view all results';
$_['text_code_variant1']   = 'default';
$_['text_code_variant2']   = 'variant 1';
$_['text_code_variant3']   = 'variant 2';
$_['text_success']     = 'Success: You have modified module AutoSearch!';

// Entry
$_['entry_show']          = 'Image';
$_['entry_show_model']    = 'Model';
$_['entry_show_price']    = 'Price';
$_['entry_show_quantity'] = 'Quantity / status';
$_['entry_limit']         = 'View results limit:';
$_['entry_symbol']        = 'Symbol for autosearch:';
$_['entry_status']        = 'Module status:';
$_['entry_tag']   	  = 'Tag';
$_['entry_model']     = 'Model';
$_['entry_sku']   	  = 'SKU';
$_['entry_upc']   	  = 'UPC';
$_['entry_ean']   	  = 'EAN';
$_['entry_jan']   	  = 'JAN';
$_['entry_isbn']   	  = 'ISBN';
$_['entry_mpn']   	  = 'MPN';
$_['entry_location']  = 'Location';
$_['entry_attr']  	  = 'Attributes';
$_['entry_sort']   	  = 'Sort results by';
$_['entry_codepage']   	  = 'Charset page:';
$_['entry_viewall']   	  = 'View all results:';
$_['entry_asr_image'] 	  = 'Image size';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify module AutoSearch!';
?>