<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row">
   <div class="col-sm-10 category_description"><img src="https://thefurniturepeople.com.au/image/pbanner.jpg" style="width:100%;"/></div>
 <div class="col-sm-10 category_description"><p></p></div> 
  <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="productpage <?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
        
		
		<?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } ?>
        
		<div class="<?php echo $class; ?>">
		<div class="product-info">
         <?php if ($thumb || $images) { ?>
    <ul class="left product-image thumbnails">
      <?php if ($thumb) { ?>      
	  <!-- Megnor Cloud-Zoom Image Effect Start -->
	  	<li class="image"><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li> 
      <?php } ?>
      <?php if ($images) { ?>	  
	  	 <?php 
			$sliderFor = 3;
			$imageCount = sizeof($images); 
		 ?>			
		
	 <!--vertical slider star-->
	 <li>
		  <ul id="vertical_latest_product1"">
		  <li class="prev-button"><a class="buttons prev" href="#"><i class="fa fa-chevron-up"></i></a></li>
		  <li class="viewport">
	    	 <ul class="overview" id="block_newproducts">
				<!-- Megnor End-->	
				<li class="product-block item">
				<div class="product-block-inner">
				  			<a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="thumbnail" data-image="<?php echo $thumb; ?>"><img  src="<?php echo $thumb; ?>" width="74" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
					
					</div>
			</li>
    			   <?php foreach ($images as $image) { ?>
	  				<li class="product-block item">
	  					<div class="product-block-inner">		
        			<a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="thumbnail elevatezoom-gallery" data-image="<?php echo $image['thumb']; ?>" data-zoom-image="<?php echo $image['popup']; ?>"><img id="cloud-zoom" src="<?php echo $image['thumb']; ?>"  title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
				</div>
	  				</li>
	   			  <?php } ?>
    		</ul>
		</li>
		<li class="next-button"><a class="buttons next" href="#"><i class="fa fa-chevron-down"></i></a></li>

		</ul> 
		</li>
		<!--vertical slider end--> 

		 <div class="additional-carousel">	
		  <?php if ($imageCount >= $sliderFor): ?>
		  	<div class="customNavigation">
				<span class="btn prev">&nbsp;</span>
			<span class="btn next">&nbsp;</span>
			</div> 
		  <?php endif; ?>	      
		  <div id="additional-carousel" class="image-additional <?php if ($imageCount >= $sliderFor){?>product-carousel<?php }?>">
    	    
			<div class="slider-item">
				<div class="product-block">
				<li>		
        			<a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="thumbnail" data-image="<?php echo $thumb; ?>"><img  src="<?php echo $thumb; ?>" width="74" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
					
					</li>
				</div>
			</div>
			  
			<?php foreach ($images as $image) { ?>
				<div class="slider-item">
				<div class="product-block">		
        			<a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="thumbnail elevatezoom-gallery" data-image="<?php echo $image['thumb']; ?>" data-zoom-image="<?php echo $image['popup']; ?>"><img id="cloud-zoom" src="<?php echo $image['thumb']; ?>"  title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
				</div>
				</div>		
	        <?php } ?>				
    	  </div>
		  <span class="additional_default_width" style="display:none; visibility:hidden"></span>
		  </div>
		<?php } ?>		  	  

	<!-- Megnor Cloud-Zoom Image Effect End-->
    </ul>
    <?php } ?>
	</div>
        </div>
	
       <?php if ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <h2><?php echo $heading_title; ?></h2>
		  <h4><?php echo $heading_title; ?></h4>
		  
		  <?php if ($review_status) { ?>
          <div class="rating">
            <p>
              <?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($rating < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
              <?php } ?>
              <?php } ?>
              <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a> / <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $text_write; ?></a></p>
          </div>
          <?php } ?>
		  
          <ul class="list-unstyled prod-price">
		  <?php if (!$special) { ?>
            <li>
              <h3><?php echo $price; ?></h3>
            </li>
            <?php } else { ?>
            <li><span style="text-decoration: line-through;"><?php echo $price; ?></span>
              <h3><?php echo $special; ?></h3>
			  </li>
            <?php } ?>
            <?php if ($manufacturer) { ?>
            <li><?php echo $text_manufacturer; ?> <a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
            <?php } ?>
            <li><?php echo $text_model; ?> <?php	/* Related Options PRO / Связанные опции PRO */	echo "<font id='product_model'>".$model."</font>"; /* >> Related Options PRO / Связанные опции PRO */ ?>
      </li>
            <?php if ($reward) { ?>
            <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
            <?php } ?>
            <li><?php echo $text_stock; ?> <?php	/* Related Options PRO / Связанные опции PRO */	echo "<font id='product_stock'>".$stock."</font>"; /* >> Related Options PRO / Связанные опции PRO */ ?>
      </li>
          </ul>
          <?php if ($price) { ?>
          <ul class="list-unstyled">
            <?php if ($tax) { ?>
            <li><?php echo $text_tax; ?> <?php echo $tax; ?></li>
            <?php } ?>
            <?php if ($points) { ?>
            <li><?php echo $text_points; ?> <?php echo $points; ?></li>
            <?php } ?>
            <?php if ($discounts) { ?>
            <?php foreach ($discounts as $discount) { ?>
            <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
            <?php } ?>
            <?php } ?>
          </ul>
          <?php } ?>
          <div id="product">
            <?php if ($options) { ?>
            <hr>
            <h3><?php echo $text_option; ?></h3>
            <?php foreach ($options as $option) { ?>
            <?php if ($option['type'] == 'select') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                <?php if ($option_value['price']) { ?>
                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                <?php } ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'radio') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'checkbox') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'image') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <div id="input-option<?php echo $option['product_option_id']; ?>">
                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                <div class="radio">
                  <label>
                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                    <?php if ($option_value['price']) { ?>
                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                    <?php } ?>
                  </label>
                </div>
                <?php } ?>
              </div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'text') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'textarea') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'file') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label"><?php echo $option['name']; ?></label>
              <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
              <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'date') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group date">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'datetime') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group datetime">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php if ($option['type'] == 'time') { ?>
            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
              <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
              <div class="input-group time">
                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php if ($recurrings) { ?>
            <hr>
            <h3><?php echo $text_payment_recurring ?></h3>
            <div class="form-group required">
              <select name="recurring_id" class="form-control">
                <option value=""><?php echo $text_select; ?></option>
                <?php foreach ($recurrings as $recurring) { ?>
                <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                <?php } ?>
              </select>
              <div class="help-block" id="recurring-description"></div>
            </div>
            <?php } ?>
            <div class="form-group qty">
              <label class="control-label" for="input-quantity"><?php echo $entry_qty; ?></label>
              <input type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
              <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
              <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary btn-lg btn-block"><?php echo $button_cart; ?></button>
            </div>
            <?php if ($minimum > 1) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
            <?php } ?>
          </div>
		  
		    <div class="btn-group">
            <button type="button" class="btn btn-default" id="wishlist" onclick="wishlist.add('<?php echo $product_id; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_wishlist; ?></span></button>
            <button type="button" class="btn btn-default" id="compare" onclick="compare.add('<?php echo $product_id; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_compare; ?></span></button>
          </div>
		  <hr>
            <!-- AddThis Button BEGIN -->
            <div class="addthis_toolbox addthis_default_style"><a class="addthis_button_facebook_like" fb:like:layout="button_count"></a> <a class="addthis_button_tweet"></a> <a class="addthis_button_pinterest_pinit"></a> <a class="addthis_counter addthis_pill_style"></a></div>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515eeaf54693130e"></script> 
            <!-- AddThis Button END --> 
        </div>
		<?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
				<ul class="nav nav-tabs">
            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
            <?php if ($attribute_groups) { ?>
            <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <?php } ?>
            <?php if ($review_status) { ?>
            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
            <?php } ?> 
            <li><a href="#additional-info" data-toggle="tab">Additional Information</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
            
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
              <table class="table table-bordered">
                <?php foreach ($attribute_groups as $attribute_group) { ?>
                <thead>
                  <tr>
                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                  <tr>
                    <td><?php echo $attribute['name']; ?></td>
                    <td><?php echo $attribute['text']; ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <?php } ?>
            <?php if ($review_status) { ?>
            <div class="tab-pane" id="tab-review">
              <form class="form-horizontal" id="form-review">
                <div id="review"></div>
                <h3><?php echo $text_write; ?></h3>
				<?php if ($review_guest) { ?>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                    <input type="text" name="name" value="" id="input-name" class="form-control" />
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                    <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                    <div class="help-block"><?php echo $text_note; ?></div>
                  </div>
                </div>
                <div class="form-group required">
                  <div class="col-sm-12">
                    <label class="control-label"><?php echo $entry_rating; ?></label>
                    &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                    <input type="radio" name="rating" value="1" />
                    &nbsp;
                    <input type="radio" name="rating" value="2" />
                    &nbsp;
                    <input type="radio" name="rating" value="3" />
                    &nbsp;
                    <input type="radio" name="rating" value="4" />
                    &nbsp;
                    <input type="radio" name="rating" value="5" />
                    &nbsp;<?php echo $entry_good; ?></div>
                </div>
                 <?php echo $captcha; ?><div class="buttons clearfix">
                  <div class="pull-right">
                    <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                  </div>
                </div>
				 <?php } else { ?>
                <?php echo $text_login; ?>
                <?php } ?>
              </form>
            </div>
            <?php } ?>
            <div class="tab-pane" id="additional-info">
              <div class="quick_view">
                  <!--<a href="#" class="download_catlog">Download Catlog</a>-->
                  
                  
                 
                  <dl>
                    <dt>Product</dt>
                    <dd><?php echo $heading_title; ?></dd>
                  </dl>
                  
                  <dl>
                    <dt>Size (WxDxH)</dt>
                    <dd><?php echo $size; ?></dd>
                  </dl>
                  
                  <!--<dl>
                    <dt>Youtube Link</dt>
                    <dd><?php echo $youtube; ?></dd>
                  </dl>
                  
                  <dl>
                    <dt>Polish</dt>
                    <dd><?php //echo $polish; ?></dd>
                  </dl>-->
                  
                  <dl>
                    <dt>Seat Cushion Shade</dt>
                    <dd><?php echo $seat_cushion_shade; ?></dd>
                  </dl>
                  
                  <!--<dl>
                    <dt>Production time </dt>
                    <dd><?php //echo $production_time; ?></dd>
                  </dl>-->
                  
                  <dl>
                    <dt>Number of Seats </dt>
                    <dd><?php echo $number_of_seats; ?></dd>
                  </dl>
                  <dl>
                    <dt>Frame Material </dt>
                    <dd><?php echo $frame_material; ?></dd>
                  </dl>
                  <dl>
                    <dt>Leg Material</dt>
                    <dd><?php echo $leg_material; ?></dd>
                  </dl>
                  <dl>
                    <dt>Upholstery</dt>
                    <dd><?php echo $upholstery; ?></dd>
                  </dl>
                  <dl>
                    <dt>Seating Support</dt>
                    <dd><?php echo $seating_support; ?></dd>
                  </dl>
                  <dl>
                    <dt>Shades Available </dt>
                    <dd><?php echo $shades_available; ?></dd>
                  </dl>
                  <dl>
                    <dt>Warranty</dt>
                    <dd><?php echo $warranty; ?></dd>
                  </dl>
                  
                  <div class="clear"></div>
                </div>
            </div>
          </div>
		</div>
      </div>
      <?php if ($products) { ?>
      
	  <div class="box">
	  
	   <div class="box-heading"><?php echo $text_related; ?></div>
		<div id="products-related" class="related-products">
			<?php 
				$sliderFor = 5;
				$productCount = sizeof($products); 
			?>
			
				<?php if ($productCount >= $sliderFor): ?>
					<div class="customNavigation">
						<a class="btn prev">&nbsp;</a>
						<a class="btn next">&nbsp;</a>
					</div>	
				<?php endif; ?>	
				
				<div class="box-product <?php if ($productCount >= $sliderFor){?>product-carousel<?php }else{?>productbox-grid<?php }?>" id="<?php if ($productCount >= $sliderFor){?>related-carousel<?php }else{?>related-grid<?php }?>">
				
      		  <?php foreach ($products as $product) { ?>
				<div class="<?php if ($productCount >= $sliderFor){?>slider-item<?php }else{?>product-items<?php }?>">
					 <div class="product-block product-thumb transition">
	  					<div class="product-block-inner">
					<div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
					<div class="hover_block">
					<div class="actions">
						<button class="cart_button" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
						<button class="wishlist_button" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"></button>
        				<button class="compare_button" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"></button>
						
						<div class="rating">
						  <?php for ($i = 1; $i <= 5; $i++) { ?>
						  <?php if ($product['rating'] < $i) { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i></span>
						  <?php } else { ?>
						  <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star fa-stack-1x"></i></span>
						  <?php } ?>
						  <?php } ?>
						</div>
						
						</div>
       			 </div>
					</div>
					<div class="caption">
					  <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
					<?php /*?>  <p><?php echo $product['description']; ?></p><?php */?>
					  <?php if ($product['price']) { ?>
					  <p class="price">
						<?php if (!$product['special']) { ?>
						<?php echo $product['price']; ?>
						<?php } else { ?>
						<span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
						<?php } ?>
						<?php if ($product['tax']) { ?>
						<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
						<?php } ?>
					  </p>
					  <?php } ?>
					</div>
					<span class="related_default_width" style="display:none; visibility:hidden"></span>
					<!-- Megnor Related Products Start -->	
				  </div>
				  </div>
				</div>
				<?php } ?>
				</div>
		</div>
	  </div>
	  </div>
      <?php } ?>
      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>


<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			
			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
// << Related Options PRO / Связанные опции PRO
	<?php
		
		if ( isset($ro_installed) && $ro_installed ) {
			if (isset($ro_settings['stock_control']) && $ro_settings['stock_control'] && isset($ro_data) && $ro_data ) {
	?>
	
		if (!$('#button-cart').attr('allow_add_to_cart')) {
			ro_stock_control(1);
			return;
		}
		$('#button-cart').attr('allow_add_to_cart','');
		
	<?php
			}
		}
	?>
			
// >> Related Options PRO / Связанные опции PRO 
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));
						
						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}
				
				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}
				
				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}
			
			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
				
				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>' + '<i class="fa fa-angle-down"></i>');
				
				$('html, body').animate({ scrollTop: 0 }, 'slow');
				
				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);				 

		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;
	
	$('#form-upload').remove();
	
	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
	
	$('#form-upload input[name=\'file\']').trigger('click');
	
	$('#form-upload input[name=\'file\']').on('change', function() {
		$.ajax({
			url: 'index.php?route=tool/upload',
			type: 'post',
			dataType: 'json',
			data: new FormData($(this).parent()[0]),
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function() {
				$(node).button('loading');
			},
			complete: function() {
				$(node).button('reset');
			},
			success: function(json) {
				$('.text-danger').remove();
				
				if (json['error']) {
					$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
				}
				
				if (json['success']) {
					alert(json['success']);
					
					$(node).parent().find('input').attr('value', json['code']);
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	});
});
//--></script> 
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
  e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>

			<?php // << Live Price ?>
			<?php if (isset($liveprice_installed) && $liveprice_installed) { ?>

			<script type="text/javascript">
				
				var liveprice_last_request = 0;
				
				function liveprice_recalc() {
					
					liveprice_last_request = Math.random();
					//console.debug(liveprice_last_request);
					
					$.ajax({
						type: 'POST',
						url: 'index.php?route=module/liveprice/get_price&product_id=<?php echo $product_id; ?>&quantity='+$('input#input-quantity,input#qty-input,input#qty[name="quantity"],#product .quantity-adder input[name="quantity"], #quantity_wanted').val()+'&rnd='+liveprice_last_request,
						data: $('select[name^="'+lp_option_prefix+'["], input[name^="'+lp_option_prefix+'["][type=\'radio\']:checked, input[name^="'+lp_option_prefix+'["][type=\'checkbox\']:checked, textarea[name^="'+lp_option_prefix+'["], input[name^="'+lp_option_prefix+'["][type="text"]'),
						dataType: 'json',
						//dataType: 'text',
						beforeSend: function() {},
						complete: function() {},
						success: function(json) {
							//console.debug(json);
							
							if (json && json.rnd && json.rnd == liveprice_last_request) {
								if (json.ct == 'monster') {
									$('#product div.priceArea p.newprice').replaceWith(json.htmls.html);
									
								} else if (json.ct == 'smarti' || json.ct == 'beamstore') {
									$('.product-info .price').html(json.htmls.html);
									
								} else if (json.ct == 'pav_styleshop') {
									$('.product-info .price').html(json.htmls.html);
									
								} else if (json.ct == 'pav_digitalstore') {
									$('.product-info .price').html(json.htmls.html);
									
								} else if (json.ct == 'megashop') {
									$('.product-info .price').html(json.htmls.html);
									
								} else if (json.ct == 'bigshop') {
									$('.product-info ul.price-box').html(json.htmls.html);	
									
								} else if (json.ct == 'pav_fashion') {
									$('.product-info .price ul.list-unstyled:first').replaceWith(json.htmls.html1);
									$('.product-info ul.description').html(json.htmls.html2);
									
								} else if (json.ct == 'pav_dress_store') {
									$('.product-info .price ul.list-unstyled:first').html(json.htmls.html);	
									
								} else if (json.ct == 'theme516') {
									$('.product-info .price-section').html(json.htmls.html);
									
								} else if (json.ct == 'theme519') { // Beauty
									$('.product-info .price-section').html(json.htmls.html); 	
									
								} else if (json.ct == 'theme531') {
									$('.product-info .price-section').html(json.htmls.html);
									
								} else if (json.ct == 'theme511') {
									$('.product-info .price-section').html(json.htmls.html);
									
								} else if (json.ct == 'theme546') {
									$('#product .price-section').html(json.htmls.html);		
									
								} else if (json.ct == 'cosyone') {
									$('#product .cart .price').html(json.htmls.html1);
									$('#product').find('.minimum, .reward, .discount').remove();
									$('#product .cart').after(json.htmls.html2);	
									
								} else if (json.ct == 'OPC080183') {
									$('#product').siblings('.prod-price').find('li:first').replaceWith(json.htmls.html1);
									$('#product').siblings('.prod-price').next().replaceWith(json.htmls.html2);
									
								} else if (json.ct == 'sellegance') {
									
									var stock_info = $('#content .price .stock').html();
									$('#content .price').html(json.htmls.html.replace('%stock%', stock_info));
								
									//var stock_info = $('.product-info .price .stock').html();
									//$('.product-info .price').html(json.htmls.html.replace('%stock%', stock_info));
									
								} else if (json.ct == 'glade') {
									$('#product .price').html(json.htmls.html);
									
								} else if (json.ct == 'lamby') {
									$('.product-shop .list-unstyled.price-box').html(json.htmls.html);
									
								} else if (json.ct == 'journal2') {
									$('#product .list-unstyled.price').html(json.htmls.html);
								
								} else if (json.ct == 'fortuna') {
									$('#product div.price').replaceWith(json.htmls.html);	
								
								} else if (json.ct == 'rgen-opencart') {
									$('#content .product-info .buying-info .price-wrp').html(json.htmls.html);
									$('#content .product-info .buying-info1').html(json.htmls.html1);
									
								} else if (json.ct == 'bt_comohos') {
									$('.product-info .price_info').replaceWith(json.htmls.html);
									
								} else if (json.ct == 'amazecart') {
									$('#product').parent().find('div.prdmf').each(function() {
										if ( $(this).find('.list-unstyled').length == 2 ) {
											$($(this).find('.list-unstyled')[1]).replaceWith(json.htmls.html);		
										} else if ( $(this).find('.list-unstyled').length == 1 ) {
											$($(this).find('.list-unstyled')[0]).replaceWith(json.htmls.html);		
										}
									})
									
								} else if (json.ct == 'ntl' || json.ct == 'bt_claudine') { 
									$('.product-info .price_info').replaceWith(json.htmls.html);
									
								} else if (json.ct == 'allure') { 
									$('#product ul.list-unstyled[itemprop="offerDetails"]').html(json.htmls.html);	
									
								} else if (json.ct == 'OPC080185_3') { // Glorious
									var lp_infos = $('#product').siblings('.list-unstyled:first').replaceWith(json.htmls.html);
									
								} else if (json.ct == 'kingstorepro') {
									$('#product').parent().find('div.price').replaceWith(json.htmls.html);
									
								} else if (json.ct == 'coloring') {
									$('#product div.price').html(json.htmls.html);
									
								} else if (json.ct == 'OPC080178') {
									$('div.price ul').html(json.htmls.html);
									
								} else if (json.ct == 'tt_petsyshop') {
									$('#product').siblings('div.price:first').html(json.htmls.html);	
									
								} else {
									var lp_infos = $('#product').parent().find('.list-unstyled');
									if (lp_infos.length >= 2 ) {
										$(lp_infos[1]).replaceWith(json.htmls.html);
									}
								}
							}
						},
						error: function(error) {
							console.log(error);
						}
					});
					
				}
				
				//$(document).ready( function() {
					var lp_theme_name = '<?php echo isset($lp_theme_name) ? $lp_theme_name : ''; ?>';
					
					
				
					var lp_option_prefix = "option";
					if ($('#mijoshop').length && $('[name^="option_oc["]').length) { // 
						lp_option_prefix = "option_oc";
					}
				
					$('select[name^="'+lp_option_prefix+'["]').on('change', function(){
						liveprice_recalc();
					});
					
					$('input[name^="'+lp_option_prefix+'["]').on('change', function(){
						liveprice_recalc();
					});
					
					$('textarea[name^="'+lp_option_prefix+'["]').on('change', function(){
						liveprice_recalc();
					});
					
					$('#input-quantity, #qty-input, input#qty[name="quantity"], #product .quantity-adder input[name="quantity"], #quantity_wanted').on('input propertychange change', function(){
						liveprice_recalc();
					});
					
					<?php if ($lp_theme_name == 'journal2') { ?>
						$('#product .options ul li[data-value]').click(function(){
							setTimeout(function(){
								liveprice_recalc();
							}, 50);
						});
					<?php } ?>
					
					<?php if (isset($custom_price) && $custom_price) { ?>
						$('input[name^="'+lp_option_prefix+'["]').on('propertychange input', function(){
							liveprice_recalc();
						});
					<?php } ?>
					
					$('#button-cart').on('click', function(){
						setTimeout( function(){
							liveprice_recalc();
						}, 200);
					});
					
					if ($('#input-quantity').length && $('#input-quantity').val() && $('#input-quantity').val() > 1) {
						liveprice_recalc();
					}
					
					
					if (lp_theme_name == 'mobile') {
						$(document).ready( function() {
							setTimeout( function(){
								$(document).off('click','.spinner .btn:first-of-type');
								$(document).on('click','.spinner .btn:first-of-type', function(){
									$('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
									$('.spinner input').change();
								});
								$(document).off('click','.spinner .btn:last-of-type');
								$(document).on('click','.spinner .btn:last-of-type', function(){
									$('.spinner input').val( Math.max(parseInt($('.spinner input').val(), 10) - 1, 1));
									$('.spinner input').change();
								});
							}, 100);
						});
					}
					
					if (lp_theme_name == 'tt_petsyshop') {
						$('#input-quantity').siblings('#plus, #minus').click(function() {
							setTimeout(function () {
								liveprice_recalc();
							}, 100);
						});
					}
					
					//pav_digitalstore
					$('#product .quantity-adder').find('span.add-up, span.add-down').click(function() {
						setTimeout(function () {
							liveprice_recalc();
						}, 100);
					});
					
					//bt_comohos
					$('#input-quantity').siblings('.increase, .decrease').click(function() {
						setTimeout(function () {
							$('#input-quantity').change();
						}, 100);
					});
					
					//rgen-opencart
					$('#content .product-info .buying-info .buy-btn-wrp a.qty-handle').click(function() {
						setTimeout(function () {
							$('#input-quantity').change();
						}, 100);
					});
					
					//fortuna
					$('#product .qty-minus, #product .qty-plus').click(function() {
						setTimeout(function () {
							$('#qty-input').change();
						}, 100);
					});
					
					//cosyone
					$('#product .cart .quantity_button').click(function() {
						setTimeout(function () {
							$('#input-quantity').change();
						}, 100);
					});
					
					// pav styleshop
					$('#product .quantity-adder .quantity-wrapper span').click(function() {
						setTimeout(function () {
							$('#input-quantity').change();
						}, 100);
					});
					
					// sellegance
					$('#product .input-qty .qty-minus, #product .input-qty .qty-plus').click(function() {
						setTimeout(function () {
							$('#qty-input').change();
						}, 100);
					});
					
					//journal2
					$('#product .qty .journal-stepper').click(function() {
						setTimeout(function () {
							$('#input-quantity').change();
						}, 100);
					});
					
					//kingstorepro & megashop
					$('#product #q_up, #product #q_down').click(function() {
						setTimeout(function () {
							$('#quantity_wanted').change();
						}, 100);
					});
					
					//bigshop
					$('.product-info .cart .qty .qtyBtn').click(function() {
						setTimeout(function () {
							$('#input-quantity').change();
						}, 100);
					});
					
				//});
				
			</script>
			<?php } ?>
	
	
			<?php // >> Live Price ?>
			
<!-- Related Options PRO / Связанные опции PRO << -->

			<script type="text/javascript">
			
				<?php
					// << Product Image Option DropDown compatibility
					if ( isset($text_select_your) && isset($options) && is_array($options) ) {
						echo "var ro_piodd_select_texts = [];\n";
						foreach ($options as $option) {
							if ($option['type'] == 'image') {
								echo "ro_piodd_select_texts[".$option['product_option_id']."] = '".$text_select_your.$option['name']."';\n";
							}
						}
					
					}
					// >> Product Image Option DropDown compatibility
				?>
			
				// << Product Image Option DropDown compatibility
				function ro_piodd_set_value(product_option_id, value) {
				
					var radio_elems = $('input[type=radio][name="'+option_prefix+'['+product_option_id+']"]');
					if (radio_elems.length) {
						var piodd_option_div = radio_elems.closest('div[id^=option-]').find('[id^=image-option]');
						if (piodd_option_div.length) {
							
							piodd_option_div.find('a.dd-option').removeClass('dd-option-selected');
							piodd_option_div.find('input.dd-selected-value').val(value);
							if (value) {
								var piodd_selected_a = piodd_option_div.find('input.dd-option-value[value='+value+']').parent().addClass('dd-option-selected');
								piodd_option_div.find('a.dd-selected').html('');
								piodd_option_div.find('a.dd-selected').append( piodd_selected_a.find('img').clone().removeClass('dd-option-image').addClass('dd-selected-image') );
								piodd_option_div.find('a.dd-selected').append( piodd_selected_a.find('label').clone().removeClass('dd-option-text').addClass('dd-selected-text') );
							} else {
								if (ro_piodd_select_texts[product_option_id]) {
									piodd_option_div.find('a.dd-selected').html(ro_piodd_select_texts[product_option_id]);
								}
							}
						}
					}
					
				}
				// >> Product Image Option DropDown compatibility
			
			
				function ro_clear_options() {
				
					// << Product Image Option DropDown compatibility
					options = $('input[type=radio][name^="'+option_prefix+'"]:checked');
					for (i=0;i<options.length;i++) {

						var product_option_id = ro_get_product_option_id_from_name($(options[i]).attr('name'));
						ro_piodd_set_value(product_option_id, '');
					}
					// >> Product Image Option DropDown compatibility
					
					$('input[type=radio][name^="'+option_prefix+'"]').attr('checked', false);
					
					$('select[name^="'+option_prefix+'"]').val('');
					
					$('textarea[name^="'+option_prefix+'"]').val('')
					
					$('input[type=text][name^="'+option_prefix+'"]').val('');
					
					$('input[type=checkbox][name^="'+option_prefix+'"]').attr('checked', false);
					
					$('input[type=hidden][name^="'+option_prefix+'"]').val('')
					
					ro_options_values_access();
					ro_set_block_options();
					ro_set_journal2_options();
					
					$('#input-quantity').trigger('change');
					
					<?php if (isset($ro_theme_name) && $ro_theme_name=='journal2') { ?>
					if (Journal.updatePrice) {
						Journal.updateProductPrice();
					}
					<?php } ?>
					
					return false;
				}
				
				// Product Block Option & Product Color Option compatibility
				// make option block selected (the same as in original input/select)
				function ro_set_block_options() {
					if (use_block_options) {
					
						// Product Block Option & Product Color Option text clear
						$('.option span[id^="option-text-"]').html('');
						
						$('select[name^="'+option_prefix+'["]').find('option').each( function () {
							var poid = ro_get_product_option_id_from_name($(this).parent().attr('name'));
							//$(this).parent().attr('name').substr(7, $(this).parent().attr('name').length-8);
							
							// Product Block Option
							// disable all SELECT blocks
							$('a[id^="block-"][option-text-id$="-'+poid+'"]').removeClass('block-active');
							if ($(this).parent().val()) {
								$('a[id^="block-"][option-text-id$="-'+poid+'"][option-value="'+$(this).parent().val()+'"]').addClass('block-active').click();
							}
							
							// Product Color Option
							$('a[id^="color-"][option-text-id$="-'+poid+'"]').removeClass('color-active');
							if ($(this).parent().val()) {
								$('a[id^="color-"][option-text-id$="-'+poid+'"][optval="'+$(this).parent().val()+'"]').addClass('color-active').click();
							}
							
						});
						
						// block options use RADIOs for images
						$('input[type=radio][name^="'+option_prefix+'["]').each( function () {
							var poid = ro_get_product_option_id_from_name($(this).attr('name'));
							//$(this).attr('name').substr(7, $(this).attr('name').length-8);
							
							// Product Block Option
							// disable only current RADIO block
							$('a[id^="block-"][option-text-id$="-'+poid+'"][option-value="'+$(this).val()+'"]').removeClass('block-active');
							if ($(this).is(':checked')) {
								$('a[id^="block-"][option-text-id$="-'+poid+'"][option-value="'+$(this).val()+'"]').addClass('block-active').click();
							}
							
							// Product Color Option
							$('a[id^="color-"][option-text-id$="-'+poid+'"][optval="'+$(this).val()+'"]').removeClass('color-active');
							if ($(this).is(':checked')) {
								$('a[id^="color-"][option-text-id$="-'+poid+'"][optval="'+$(this).val()+'"]').addClass('color-active').click();
								
							}
							
						});
						
					}
					
				}
				
			</script>
			
			
			
			<?php 
			
			//if (	$this->model_module_related_options->installed() ) {
			if ( isset($ro_installed) &&	$ro_installed && isset($ro_data) && $ro_data ) {
				
				?>
				
				<style>
					.ro_option_disabled { color: #e1e1e1; }
				</style>
			
				<?php if ( $ro_theme_name == 'theme638' ) { ?>
					<script src="catalog/view/theme/theme638/js/jquery.selectbox-0.2.min.js" type="text/javascript"></script>
					<style>
						.sbDisabled { padding-left:10px; padding-top:8px; padding-bottom:8px; opacity:0.4; line-height:37px; }
					</style>
				<?php } ?>
				
				<script type="text/javascript">
				
				var hide_inaccessible = <?php if (isset($ro_settings['hide_inaccessible']) && $ro_settings['hide_inaccessible']) echo "true"; else echo "false"; ?>;
				var options_types = [];
				var ro_data = <?php echo json_encode($ro_data); ?>;
				var ro_product_options = <?php echo json_encode($ro_product_options); ?>;
				var ro_step_by_step = <?php if (isset($ro_settings['step_by_step']) && $ro_settings['step_by_step']) echo "true"; else echo "false"; ?>;
				var auto_select_last = <?php if (isset($ro_settings['select_first']) && $ro_settings['select_first'] == 2) echo "true"; else echo "false"; ?>;
				var use_block_options = ($('a[id^=block-option][option-value]').length || $('a[id^=block-image-option][option-value]').length || $('a[id^=color-][optval]').length);
				var use_journal2 = <?php echo (isset($ro_theme_name) && $ro_theme_name=='journal2') ? 'true' : 'false'; ?>;
				var ro_theme_name = "<?php echo $ro_theme_name; ?>";
				var ro_settings = <?php echo json_encode($ro_settings); ?>;
				
				var option_prefix = "option";
				if ($('#mijoshop').length && $('[name^="option_oc["]').length) { // 
					option_prefix = "option_oc";
				}
				var option_prefix_length = option_prefix.length;
				
				var all_select_ov = {};
				$('select[name^="'+option_prefix+'["]').each( function (si, sel_elem) {
					all_select_ov[sel_elem.name] = [];
					
					$.each(sel_elem.options, function (oi, op_elem) {
						all_select_ov[sel_elem.name].push(op_elem.value);
					});
					
				} );
				
				
				var ro_options_steps = ro_get_options_steps();
				var ro_all_options_values = [];
				for (var i=0; i < ro_options_steps.length; i++) {
					ro_all_options_values[ro_options_steps[i]] = ro_get_all_possible_option_values(ro_options_steps[i]);
				}
				
				
				<?php
				
				if (isset($options) && is_array($options)) {
					foreach ($options as $option) {
						echo "options_types[".$option['product_option_id']."]='".$option['type']."';\n";
					}
				}
				
				if ( isset($ros_to_select) && $ros_to_select ) {
					echo "var ros_to_select = ".json_encode($ros_to_select).";";
					echo "var filter_name = false;";
				} elseif (isset($_GET['filter_name'])) {
					echo "var filter_name=\"".$_GET['filter_name']."\";";
				} elseif (isset($_GET['search'])) {
					echo "var filter_name=\"".$_GET['search']."\";";
				} else {
					echo "var filter_name = false;";
				}
				
				
				?>
				
				(function ($) {
					$.fn.toggleOption = function (value, show) {
						/// <summary>Show or hide the desired option</summary>
						return this.filter('select').each(function () {
							var select = $(this);
							if (typeof show === 'undefined') {
								show = select.find('option[value="' + value + '"]').length == 0;
							}
							if (show) {
								select.showOption(value);
							}
							else {
								select.hideOption(value);
							}
						});
					};
					$.fn.showOption = function (value) {
						/// <summary>Show the desired option in the location it was in when hideOption was first used</summary>
						return this.filter('select').each(function () {
							var select = $(this);
							var found = select.find('option[value="' + value + '"]').length != 0;
							if (found) return; // already there
				
							var info = select.data('opt' + value);
							if (!info) return; // abort... hideOption has not been used yet
				
							var targetIndex = info.data('i');
							var options = select.find('option');
							var lastIndex = options.length - 1;
							if (lastIndex == -1) {
								select.prepend(info);
							}
							else {
								options.each(function (i, e) {
									var opt = $(e);
									if (opt.data('i') > targetIndex) {
										opt.before(info);
										return false;
									}
									else if (i == lastIndex) {
										opt.after(info);
										return false;
									}
								});
							}
							return;
						});
					};
					$.fn.hideOption = function (value) {
						/// <summary>Hide the desired option, but remember where it was to be able to put it back where it was</summary>
						return this.filter('select').each(function () {
							var select = $(this);
							var opt = select.find('option[value="' + value + '"]').eq(0);
							if (!opt.length) return;
				
							if (!select.data('optionsModified')) {
								// remember the order
								select.find('option').each(function (i, e) {
									$(e).data('i', i);
								});
								select.data('optionsModified', true);
							}
				
							select.data('opt' + value, opt.detach());
							return;
						});
					};
				})(jQuery);					
				
				function ro_stock_control(add_to_cart) {
				
					<?php  if (!isset($ro_settings['stock_control']) || !$ro_settings['stock_control']) { ?>
					if (add_to_cart) {
						$('#button-cart').attr('allow_add_to_cart','allow_add_to_cart');
						$('#button-cart').click();
					}
					return;
					<?php } ?>
				
					var erros_msg = '<?php echo $entry_stock_control_error; ?>';
					
					var options_values = ro_get_options_values([]);
					var roids = ro_get_current_ro_ids(options_values);
					
					$('.alert-warning, .alert-warning').remove();
					
					if (roids.length) {
					
						$.ajax({
								url: '<?php echo HTTP_SERVER; ?>index.php?route=module/related_options/get_ro_free_quantity',
								data: {roids: roids},
								dataType : "text",         
								success: function (data) { 
								
									if (parseInt(data) < parseInt( $('input[type=text][name=quantity]').val() )) {
										$('.alert-warning, .alert-warning').remove();
										$('#input-quantity').parent().after('<div class="alert alert-warning">' + erros_msg.replace('%s',parseInt(data)) + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
										
									} else {
																				
										if (add_to_cart) {
											$('#button-cart').attr('allow_add_to_cart','allow_add_to_cart');
											$('#button-cart').click();
										}
									}
								},
								error: function(error) {
									console.log(error);
								}
						});
					} else { // if there's no selected related options combination - use standard algorithm
						if (add_to_cart) {
							$('#button-cart').attr('allow_add_to_cart','allow_add_to_cart');
							$('#button-cart').click();
						}
					
					}
					
				}
				
				function ro_get_current_ro_ids(options_values) {
				
					var ro_ids = [];
				
					for (var i in ro_data) {
						if((ro_data[i] instanceof Function) ) { continue; }
					
						var ro_dt = ro_data[i];
				
						var all_ok;
						for(var ro_id in ro_dt['ro']) {
							if((ro_dt['ro'][ro_id] instanceof Function) ) { continue; }
							
							all_ok = true;
							for(var product_option_id in ro_dt['ro'][ro_id]['options']) {
								if((ro_dt['ro'][ro_id]['options'][product_option_id] instanceof Function) ) { continue; }
								
								if (!(product_option_id in options_values && options_values[product_option_id]==ro_dt['ro'][ro_id]['options'][product_option_id])) {
									all_ok = false;
								}
							}
							if (all_ok) ro_ids.push(ro_id); 
						}
						
					}
					return ro_ids;
				}
				
				
				
				function ro_arrays_intersection(arr1, arr2) {
					var new_arr = [];
					
					for (var i in arr1) {
						if ($.inArray(arr1[i], arr2) != -1) {
							new_arr.push(arr1[i]);
						}
					}
					
					return new_arr;
					
				}
				
				function ro_get_array_copy(arr) {
				
					var new_arr = [];
					
					for (var i in arr) {
						if ( $.isArray(arr[i]) ) {
							new_arr[i] = ro_get_array_copy(arr[i]);
						} else {
							new_arr[i] = arr[i];
						}
					}
					
					return new_arr;
				}
				
				
				function ro_get_all_possible_option_values(product_option_id) {
					
					var values = [];
					
					if ($('select[name="'+option_prefix+'['+product_option_id+']"]').length) {
					
						var select_options = all_select_ov[option_prefix+"["+product_option_id+"]"];
						for (var i=0;i<select_options.length;i++) {
							if (select_options[i]) {
								values.push(select_options[i]);
							}
						}
					
					} else if ($('input[type=radio][name="'+option_prefix+'['+product_option_id+']"]').length) {
						
						$('input[type=radio][name="'+option_prefix+'['+product_option_id+']"]').each(function(){
							values.push($(this).val());
						});
						
					}
					
					return values;
					
				}
				
				// get available options values
				// option_id - опция (product_option_id)
				// param_options_values - current options values (selected) - only for related options
				// param_skip_ropv_ids - don't make values addition for this related options combinations
				function ro_get_accessible_option_values(option_id, param_options_values, param_skip_options) {
				
					// make copies od arrays
					var options_values = ro_get_array_copy(param_options_values);
					var skip_options = ro_get_array_copy(param_skip_options);
					skip_options.push(option_id);
					
					for (var current_option_id in options_values) {
						if ( !options_values[current_option_id].length && current_option_id != option_id) {
							if ( $.inArray(current_option_id, skip_options) == -1 ) {
								options_values[current_option_id] = ro_get_accessible_option_values(current_option_id, options_values, skip_options);
							}
						}
					}
					
					var common_accessible_values = false;
					
					var possible_current_option_values = ro_all_options_values[option_id];
					
					for (var i in ro_data) {
					
						if ($.inArray(option_id,ro_data[i]['options_ids'])==-1) {
							continue;
						}
						
						var accessible_values = [];
					
						var ro_array = ro_data[i]['ro'];
						
						var options_for_check = []; // optimization
						for(var current_option_id in options_values) {
							if (current_option_id != option_id && options_values[current_option_id].length
							&& $.inArray(current_option_id, skip_options) == -1 && $.inArray(current_option_id,ro_data[i]['options_ids'])!=-1) {
								options_for_check.push(current_option_id);
							}
						}
						
						if (!options_for_check.length) {
							if (ro_data[i]['options_ids'].length == 1) { // combination contains only one option (this option)
								for(var ro_id in ro_array) {
									if( !ro_array.hasOwnProperty(ro_id) ) continue;
									if ( ro_array[ro_id]['options'][option_id] && $.inArray(ro_array[ro_id]['options'][option_id], accessible_values) == -1 ) {
										accessible_values.push(ro_array[ro_id]['options'][option_id]);
									}
								}
							} else {
								accessible_values = ro_get_array_copy(possible_current_option_values);
							}
						} else {
							for(var ro_id in ro_array) {
								if((ro_array[ro_id] instanceof Function) ) { continue; }
								
								all_ok = true;
								
								for (var j =0; j < options_for_check.length; j++) {
								
									var current_option_id = options_for_check[j];
								
								//for(var current_option_id in options_values) {
									
									//if (current_option_id != option_id && options_values[current_option_id].length
									//&& $.inArray(current_option_id, skip_options) == -1 && $.inArray(current_option_id,ro_data[i]['options_ids'])!=-1) {
										
										if ( $.inArray(ro_array[ro_id]['options'][current_option_id], options_values[current_option_id]) == -1  ) {
											all_ok = false;
										}
										
									//}
									
									if (!all_ok) {
										break;
									}
									
								}
								
								if (all_ok && ($.inArray(ro_array[ro_id]['options'][option_id], accessible_values) == -1 )) {
									accessible_values.push(ro_array[ro_id]['options'][option_id]);
									if (possible_current_option_values.length == accessible_values.length) { // optimization
										break;
									}
								}
								
							}
						}
						
						if (common_accessible_values === false) {
							common_accessible_values = accessible_values;
						} else {
							common_accessible_values = ro_arrays_intersection(common_accessible_values, accessible_values);
						}
						
					}
					
					return common_accessible_values;
					
				}
				
				// only for options with values
				// returns array of accessible values
				function ro_option_values_access(param_options_values, option_id) {
					
					var options_values = [];
					for (var product_option_id in param_options_values) {
						options_values[product_option_id] = [];
						if (param_options_values[product_option_id]) {
							options_values[product_option_id].push(param_options_values[product_option_id]);
						}
					}
					
					var skip_ropv_ids = [];
					var accessible_values = ro_get_accessible_option_values(option_id, options_values, skip_ropv_ids)
					
					ro_set_accessible_values(option_id, accessible_values);
					
					return accessible_values;
				}
				
				function ro_toggle_option_block(option_id, toggle_flag) {
						
					$('#input-option'+option_id).parent().toggle(toggle_flag);
					
				}
				
				function ro_option_is_available(option_id) {
					
					if ($('select[name="'+option_prefix+'['+option_id+']"]').length) {
						return $('select[name="'+option_prefix+'['+option_id+']"] option[value][value!=""]:not(:disabled)').length ? true : false;
					} else if ($('input[type=radio][name="'+option_prefix+'['+option_id+']"]').length) {
						return $('input[type=radio][name="'+option_prefix+'['+option_id+']"]:not(:disabled)').length ? true : false;
					}
					
				}
				
				function ro_hide_unavailable_option(option_id) {

					if (ro_settings && ro_settings['hide_option']) {
						ro_toggle_option_block(option_id, ro_option_is_available(option_id));
					}
				}
				
				function ro_unavailable_options_not_required(option_id) {
					
					if (ro_settings && ro_settings['unavailable_not_required']) {
						var current_ids = [];
						if ($('#ro_not_required').length) {
							current_ids = $('#ro_not_required').val().split(',');
						} else {
							$('#product').append('<input type="hidden" name="ro_not_required" id="ro_not_required" value="">');
						}
						var new_ids = [];
						for (var i in current_ids) {
							if (current_ids[i] != option_id) {
								new_ids.push(current_ids[i]);
							}
						}
						if (!ro_option_is_available(option_id)) {
							new_ids.push(option_id);
						}
						$('#ro_not_required').val( new_ids.toString());
					}
					
				}
				
				function ro_set_accessible_values(option_id, accessible_values) {
				
					var current_value = ($('input[type=radio][name="'+option_prefix+'['+option_id+']"]:checked').val() || $('select[name="'+option_prefix+'['+option_id+']"]').val());
				
					if ($('select[name="'+option_prefix+'['+option_id+']"]').length) {
					
						if (current_value && $.inArray(current_value, accessible_values)==-1) {
							$('select[name="'+option_prefix+'['+option_id+']"]').val("");
						}
						
						if (hide_inaccessible) {
						
							select_options = all_select_ov[option_prefix+"["+option_id+"]"];
							for (var i=0;i<select_options.length;i++) {
								if (select_options[i]) {
									option_value_disabled = ($.inArray(select_options[i],accessible_values) == -1);
									// hiding options for IE
									$('select[name="'+option_prefix+'['+option_id+']"]').toggleOption(select_options[i], !option_value_disabled);
									
								}
							}
							
						} else {
						
							select_options = $('select[name="'+option_prefix+'['+option_id+']"]')[0].options;
							for (var i=0;i<select_options.length;i++) {
								if (select_options[i].value) {
									option_value_disabled = ($.inArray(select_options[i].value,accessible_values) == -1);
									select_options[i].disabled = option_value_disabled;
									if (option_value_disabled) {
										$(select_options[i]).addClass('ro_option_disabled');
									} else {
										$(select_options[i]).removeClass('ro_option_disabled');
									}
								}
							}
							
						}
						
						if ( ro_theme_name == 'theme638' ) { // VIVA theme
							$('select[name="'+option_prefix+'['+option_id+']"]').selectbox("detach");
							
							$('select[name="'+option_prefix+'['+option_id+']"]').selectbox({
								effect: "slide",
								speed: 400
							});
							
						}
						
					} else if ($('input[type=radio][name="'+option_prefix+'['+option_id+']"]').length) {	
						
						if (current_value && $.inArray(current_value, accessible_values)==-1) {
							$('input[type=radio][name="'+option_prefix+'['+option_id+']"]:checked')[0].checked=false;
							
							// << Product Image Option DropDown compatibility
							var piodd_option_div = $('#image-option-'+option_id);
							if (piodd_option_div.length) {
								ro_piodd_set_value(option_id, '');
							}
							// >> Product Image Option DropDown compatibility
						}
						
						radio_options = $('input[type=radio][name="'+option_prefix+'['+option_id+']"]');
						for (var i=0;i<radio_options.length;i++) {
							option_value_disabled = ($.inArray(radio_options[i].value,accessible_values) == -1);
							$(radio_options[i]).prop('disabled', option_value_disabled); // hidden should be disabled too
							
							if (hide_inaccessible) {
							
								//$(radio_options[i]).attr('disabled', option_value_disabled); // disable anyway
								
								if ( ro_theme_name == 'theme638' ) { // VIVA theme
									$(radio_options[i]).parent().toggle(!option_value_disabled);
									
									// style change for padding change
									if (i == 0) {
										if (option_value_disabled) {
											if ($(radio_options[i]).parent().hasClass('radio')) {
												$(radio_options[i]).parent().removeClass('radio');
												$(radio_options[i]).parent().addClass('_radio_ro');
											}
										} else {
											if ($(radio_options[i]).parent().hasClass('_radio_ro')) {
												$(radio_options[i]).parent().removeClass('_radio_ro');
												$(radio_options[i]).parent().addClass('radio');
											}
										}
									}
								} else {
									$(radio_options[i]).parent().parent().toggle(!option_value_disabled);
									$(radio_options[i]).toggle(!option_value_disabled);
									
									// style change for padding change
									if (i == 0) {
										if (option_value_disabled) {
											if ($(radio_options[i]).parent().parent().hasClass('radio')) {
												$(radio_options[i]).parent().parent().removeClass('radio');
												$(radio_options[i]).parent().parent().addClass('_radio_ro');
											}
										} else {
											if ($(radio_options[i]).parent().parent().hasClass('_radio_ro')) {
												$(radio_options[i]).parent().parent().removeClass('_radio_ro');
												$(radio_options[i]).parent().parent().addClass('radio');
											}
										}
									}
								} 
								
								
								
								
								// << Product Image Option DropDown compatibility
								var piodd_option_div = $('#image-option-'+option_id);
								var piodd_value = piodd_option_div.find('ul.dd-options input.dd-option-value[value='+radio_options[i].value+']');
								if (piodd_value.length) {
									piodd_value.parent().toggle(!option_value_disabled);
								}
								// >> Product Image Option DropDown compatibility
							
							} else {
								
								if (option_value_disabled) {
									$(radio_options[i]).parent().fadeTo("fast", 0.2);
								} else {
									$(radio_options[i]).parent().fadeTo("fast", 1);
								}
								
								// << Product Image Option DropDown compatibility
								// make copies of unavailable elements, originals hide in hidden div, when element became available again - place it back
								var piodd_option_div = $('#image-option-'+option_id);
								
								if ( piodd_option_div.find('ul.dd-options').length ) {
									
									var ro_hidden_div_id = piodd_option_div.attr('id')+'-ro-hidden';
									
									if ( !$('#'+ro_hidden_div_id).length ) {
										piodd_option_div.after('<div id="'+ro_hidden_div_id+'" style="display: none;"></div>');
									}
									var ro_hidden_div = $('#'+ro_hidden_div_id);
									
									
									var clone_id = 'clone_'+radio_options[i].value;
									if (option_value_disabled) {
									
										var piodd_value = piodd_option_div.find('ul.dd-options input.dd-option-value[value='+radio_options[i].value+']');
										
										if (piodd_value.length) {
									
											if ( !piodd_option_div.find('[clone_id='+clone_id+']').length ) {
												var ro_clone = piodd_value.parent().clone(true, true).appendTo(ro_hidden_div);
												ro_clone.clone().insertAfter(piodd_value.parent()).attr('clone_id', clone_id).fadeTo('fast', 0.2);
												piodd_value.parent().remove();
											}
										}
										
									} else {
										if (ro_hidden_div.find('[value='+radio_options[i].value+']').length) {
											ro_hidden_div.find('[value='+radio_options[i].value+']').parent().clone(true, true).insertAfter(piodd_option_div.find('[clone_id='+clone_id+']'));
											ro_hidden_div.find('[value='+radio_options[i].value+']').parent().remove();
											piodd_option_div.find('[clone_id='+clone_id+']').remove();
										}
									}
									
								}
								// >> Product Image Option DropDown compatibility
								
							}
							
							
						}
						
						
						
					}
					
					ro_hide_unavailable_option(option_id);
					ro_unavailable_options_not_required(option_id);
				
				}
				
				function ro_get_options_values(options_keys) {
				
					var options_values = {};
				
					$('select[name^="'+option_prefix+'["], input[type=radio][name^="'+option_prefix+'["]').each(function(){
						option_id = ro_get_product_option_id_from_name( $(this).attr('name') );
						
						if ($.inArray(option_id,ro_product_options) != -1) {
							
							if ($.inArray(option_id,options_keys) == -1) {
								options_values[option_id] = 0;
								options_keys.push(option_id);
							}
							
							if ( $(this).find('option[value]').length ) { // select
								options_values[option_id] = $(this).val();
							} else { // radio
								if ( $(this).is(':checked') ) {
									options_values[option_id] = $(this).val();
								}
							}
							
							
						}
					});
					
					return options_values;
				
					
				}
				
				// correct auto selection for options some values already selected
				function ro_use_first_values(set_anyway) {
					
					var options_values = ro_get_options_values([]);
					
					var selected_options = [];
					var has_selected = false;
					for (var optkey in options_values) {
						if((options_values[optkey] instanceof Function) ) { continue; }
						if (options_values[optkey]) {
							has_selected = true;
							selected_options.push(optkey);
							//break;
						}
					}
					
					if (has_selected || set_anyway) {
					
						for (var i in ro_options_steps) {
							
							var product_option_id = ro_options_steps[i];
							
							if ( $.inArray(product_option_id, ro_product_options) != -1 ) {
							
								var product_option_value_id = false;
								
								if ($('select[name="'+option_prefix+'['+product_option_id+']"]').length) {
									var product_option_value_id = $('select[name="'+option_prefix+'['+product_option_id+']"] option[value][value!=""]:not(:disabled)').val();
								} else if ($('input[type=radio][name="'+option_prefix+'['+product_option_id+']"]').length > 0) {
									var product_option_value_id = $('input[type=radio][name="'+option_prefix+'['+product_option_id+']"]:not(:disabled):first').val();
								}
								
								if (product_option_value_id && ($.inArray(product_option_id, selected_options) != -1 || set_anyway) ) {
									ro_set_option_value(product_option_id, product_option_value_id);
									ro_options_values_access();
									ro_set_block_options();
									ro_set_journal2_options();
								}
								
							}
							
						}
					
					}
					
					
				}
				
				function ro_journal2_set_value(product_option_value_id) {
					if (use_journal2 && $('li[data-value="'+product_option_value_id+'"]').length) {
						$('li[data-value="'+product_option_value_id+'"]').siblings('li').removeClass('selected');
						$('li[data-value="'+product_option_value_id+'"]').removeClass('selected').addClass('selected');
					}
				}
				
				function ro_set_option_value(product_option_id, product_option_value_id) {
					
					if ($('select[name="'+option_prefix+'['+product_option_id+']"]').length > 0) {
						$('[name="'+option_prefix+'['+product_option_id+']"]').val(product_option_value_id);
						
						ro_journal2_set_value(product_option_value_id);
						
					} else if ($('input[type=radio][name="'+option_prefix+'['+product_option_id+']"]').length > 0) {
						$('input[type=radio][name="'+option_prefix+'['+product_option_id+']"]').prop('checked', false);
						$('input[type=radio][name="'+option_prefix+'['+product_option_id+']"][value='+product_option_value_id+']').prop('checked', true);
						
						// << Product Image Option DropDown compatibility
						ro_piodd_set_value(product_option_id, product_option_value_id);
						// >> Product Image Option DropDown compatibility
						
						ro_journal2_set_value(product_option_value_id);
						
					}
					
				}
				
				function ro_set_selected_combination(ro_id, skip_access) {
				
					if (ro_data) {
						for (var i in ro_data) {
							if (ro_data[i]['ro'][ro_id]) {
								
								for (var product_option_id in ro_data[i]['ro'][ro_id]['options']) {
									ro_set_option_value(product_option_id, ro_data[i]['ro'][ro_id]['options'][product_option_id]);
								}
								
								break;
							}
						}
					}
				
					if ( typeof(skip_access) == 'undefined' || !skip_access ) {

						ro_options_values_access();
						ro_set_block_options();
						ro_set_journal2_options();
						
					}
					
				}
				
				function ro_get_product_option_id_from_name(name) {
					return name.substr(option_prefix_length+1, name.indexOf(']')-(option_prefix_length+1) )
				}
				
				// for step-by-step way
				function ro_get_options_steps() {
					var options_steps = [];
					var product_option_id = "";
					
					//for (var i=0;i<$('input[name^="'+option_prefix+'["]').length;i++) {
					$('input[name^="'+option_prefix+'["], select[name^="'+option_prefix+'["]').each(function(){
					
						product_option_id = ro_get_product_option_id_from_name($(this).attr('name'));
						//if (!isNaN(product_option_id)) product_option_id = parseInt(product_option_id);
						
						if ($.inArray(product_option_id, ro_product_options) != -1) {
							if ($.inArray(product_option_id, options_steps) == -1) {
								options_steps.push(product_option_id);
							}
						}
						
					});
					
					return options_steps;
				}
				
				function ro_options_values_access() {
					
					if (!ro_data || !Object.keys(ro_data).length) return;
					
					if (ro_step_by_step) {
					
						var prev_options_values = {};
						var prev_options = [];
						var option_accessible_values = [];
						var one_prev_value_is_not_set = false;
						
						for (var i=0;i<ro_options_steps.length;i++) {
							if (i>0) {
								// if previous option value is selected or if previous option has no available values
								//if (prev_options[i-1] || (one_prev_value_is_set && (!prev_option_accessible_values || prev_option_accessible_values.length==0) ) ) {
								if (!one_prev_value_is_not_set) {
									// limitaion on previous
									option_accessible_values = ro_option_values_access(prev_options_values, ro_options_steps[i]);
									
								} else {
									// disable all
									ro_set_accessible_values(ro_options_steps[i], []);
									option_accessible_values = [];
								}
							}
							prev_options.push( ($('input[type=radio][name="'+option_prefix+'['+ro_options_steps[i]+']"]:checked').val() || $('select[name="'+option_prefix+'['+ro_options_steps[i]+']"]').val()) );
							//one_prev_value_is_set = one_prev_value_is_set || prev_options[i];
							prev_options_values[ro_options_steps[i]] = prev_options[prev_options.length-1];
							
							if ((option_accessible_values.length || i==0) && !prev_options[i] ) { // option has available values, but none of them is selected
								one_prev_value_is_not_set = true;
							}
							
						}
					
					} else {
					
						var options_keys = [];
						var options_values = ro_get_options_values(options_keys);
						
						for (var i=0;i<options_keys.length;i++) {
							ro_option_values_access(options_values, options_keys[i]);
						}
						
					}
					
					ro_stock_control(0);
					
					
					ro_check_block_options();
					
					ro_check_auto_select();
					
					<?php if (isset($ro_settings['spec_model']) && $ro_settings['spec_model']) { ?>
						ro_set_model();
					<?php } ?>
					
					<?php if (isset($ro_settings['spec_ofs']) && $ro_settings['spec_ofs']) { ?>
						ro_set_stock();
					<?php } ?>
					
				}
				
				// autoselection for last available option value
				function ro_check_auto_select() {
				
					if (auto_select_last) {
					
						for (var i in ro_options_steps) {
							
							var product_option_id = ro_options_steps[i];
							
							if ( $('select[name="'+option_prefix+'['+product_option_id+']"]').length ) {
								
								var options_elems = $('select[name="'+option_prefix+'['+product_option_id+']"]').find('option[value][value!=""]:not(:disabled)');
								
								if (options_elems.length == 1 && !$(options_elems[0]).is(':selected')) {
								
									var product_option_value_id = $(options_elems[0]).val();
									
									ro_set_option_value(product_option_id, product_option_value_id);
									$(options_elems[0]).parent().change();
									return;
								}
								
							} else if ( $('input:radio[name="'+option_prefix+'['+product_option_id+']"]').length ) {
							
								var radio_elems = $('input:radio[name="'+option_prefix+'['+product_option_id+']"]:not(:disabled):visible');
								
								if (radio_elems.length == 1 && !$(radio_elems[0]).is(':checked')) {
									
									var product_option_value_id = $(radio_elems[0]).val();
									
									ro_set_option_value(product_option_id, product_option_value_id);
									
									$(radio_elems[0]).change();
									return;
									
								}
							
							}
							
						}
					
					}
				
				}
				
				// autorelection for first values
				function ro_auto_select_first() {
					
					if (ro_settings && ro_settings['select_first'] && ro_settings['select_first'] == 1) {
						
						for (var i in ro_options_steps) {
							if (!ro_options_steps.hasOwnProperty(i)) continue;
							
							var product_option_id = ro_options_steps[i];
							
							if ( $('select[name="'+option_prefix+'['+product_option_id+']"]').length ) {
								
								var elem = $('select[name="'+option_prefix+'['+product_option_id+']"]');
								if ( !elem.val() ) {
									
									var elem_option = elem.find('option[value][value!=""]:not(:disabled)');
									if (elem_option.length) {
										elem.val(elem_option.val());
										elem.change();
									}
									
								}
								
							} else if ( $('input:radio[name="'+option_prefix+'['+product_option_id+']"]').length ) {
								
								if ( !$('input:radio[name="'+option_prefix+'['+product_option_id+']"]:checked').length ) {
									var elem = $('input:radio[name="'+option_prefix+'['+product_option_id+']"]:not(:disabled):first');
									if (elem.length) {
										elem.prop('checked', true);
										elem.change();
										
									}
								}
								
							}
							
						}
						
					}
				
				}
				
				function ro_set_model() {
				
					var options_values = ro_get_options_values([]);
					var ro_ids = ro_get_current_ro_ids(options_values);
					var product_model = "<?php echo addslashes($model); ?>";
					var model = "";
					
					if (ro_ids.length) {
						for (var i in ro_data) {
							var ro_dt = ro_data[i];
							
							for (var j in ro_ids) {
								var ro_id = ro_ids[j];
								if (ro_dt['ro'][ro_id] && ro_dt['ro'][ro_id]['model']) {
									if (ro_settings['spec_model'] == 1) {
										model = ro_dt['ro'][ro_id]['model'];
									} else if (ro_settings['spec_model'] == 2 || ro_settings['spec_model'] == 3) {
										model+= ro_dt['ro'][ro_id]['model'];
									}
								}
							}
						}
					}
					
					if (model) {
						if (ro_settings['spec_model'] == 3) {
							model = product_model + model;
						}
					} else {
						model = product_model;
					}
					$('#product_model').html(model);
				}
				
				function ro_set_stock() {
				
				
					var stock_text = "<?php echo addslashes($text_stock); ?>";
					var stock = "<?php echo addslashes($stock); ?>";
					if (use_journal2) {
						var journal2_stock_status = "<?php echo isset($stock_status) ? $stock_status : ''; ?>";
					}
					
					var options_values = ro_get_options_values([]);
					var ro_ids = ro_get_current_ro_ids(options_values);
					
					if (ro_ids.length) {
						for (var i in ro_data) {
							var ro_dt = ro_data[i];
							
							for (var j in ro_ids) {
								var ro_id = ro_ids[j];
							
								if (ro_dt['ro'][ro_id] && ro_dt['ro'][ro_id]['stock']) {
									var stock = ro_dt['ro'][ro_id]['stock'];
									
									if (use_journal2) {
										var journal2_stock_status = ro_prices[roid]['in_stock'] ? 'instock' : 'outofstock';
									}
									
									break;
								}
							}
						}
					}
					
					if (use_journal2) {
						//journal2 uses specific price and stock update, but it's slow and doesn't swith block class (style)
						$('#product .p-stock .journal-stock').removeClass('instock, outofstock').addClass(journal2_stock_status);
						$('#product .p-stock .journal-stock').html(stock);
					} else {
						$('#product_stock').html(stock);
					}
					
					
				}
				
				function ro_set_combination_by_model(model) {
				
					if (model && ro_data) {
						for (var i in ro_data) {
							for (var ro_id in ro_data[i]['ro']) {
								if (ro_data[i]['ro'][ro_id][model] && ro_data[i]['ro'][ro_id][model].toLowerCase() == model.toLowerCase()) {
									ro_set_selected_combination(ro_id);
									return true;
								}
							}
						}
					}
				
					return false;
				}
				
				// Block Option & journal2 compatibility
				// show/hide enable/disable options block
				function ro_check_block_options() {
			
					if (use_block_options || use_journal2) {
						
						var available_values = [];
						
						// block options use SELECTs for select & radio
						$('select[name^="'+option_prefix+'["]').find('option').each( function () {
							
							if ($(this).val()) {
								if (hide_inaccessible) {
									available_values.push( $(this).val() );
								} else {
									if (! $(this).attr('disabled')) {
										available_values.push( $(this).val() );
									}
								}
							}
							
						});
						
						// block options use RADIOs for images
						$('input[type=radio][name^="'+option_prefix+'["]').each( function () {
							
							if (hide_inaccessible) {
								if ($(this)[0].style.display != 'none') {
									available_values.push( $(this).val() );
								}
							} else {
								if (!$(this).attr('disabled')) {
									available_values.push( $(this).val() );
								}
							}
							
						});
						
						// Product Block Option Module
						if (use_block_options) {
							$('a[id^=block-option],a[id^=block-image-option]').each( function () {
								if ($.inArray($(this).attr('option-value'), available_values) == -1) {
									$(this).removeClass('block-active');
									if (hide_inaccessible) {
										$(this).hide();
									} else {
										if (!$(this).attr('disabled')) {
											$(this).attr('disabled', true);
											$(this).fadeTo("fast", 0.2);
										}
									}
								} else {
									if (hide_inaccessible) {
										$(this).show();
									} else {
										if ($(this).attr('disabled')) {
											$(this).attr('disabled', false);
											$(this).fadeTo("fast", 1);
										}
									}
								}
								
							} );
						}
						
						// Journal2
						if (use_journal2) {
						
							$('#product').find('li[data-value]').each(function() {
							
								if ($.inArray($(this).attr('data-value'), available_values) == -1) {
									$(this).removeClass('selected');
									if (hide_inaccessible) {
										$(this).hide();
									} else {
										if (!$(this).attr('disabled')) {
											$(this).attr('disabled', true);
											$(this).fadeTo("fast", 0.2);
										}
									}
								} else {
									if (hide_inaccessible) {
										$(this).show();
									} else {
										if ($(this).attr('disabled')) {
											$(this).attr('disabled', false);
											$(this).fadeTo("fast", 1);
										}
									}
								}
								
								// change standart Journal2 function
								$(this).unbind('click');
								
								
								$(this).click(function () {
									if ($(this).attr('disabled')) {
										return;
									}
									$(this).siblings().removeClass('selected');
									$(this).addClass('selected');
									$(this).parent().siblings('select').find('option[value=' + $(this).attr('data-value') + ']').attr('selected', 'selected');
									$(this).parent().siblings('select').trigger('change');
									
									$(this).parent().parent().find('.radio input[type=radio][name^="'+option_prefix+'"]').attr('checked', false);
									$(this).parent().parent().find('.radio input[type=radio][name^="'+option_prefix+'"][value='+$(this).attr('data-value')+']').attr('checked', true).trigger('change');
									
									if (Journal.updatePrice) {
										Journal.updateProductPrice();
									}
									
								});
								
								
							});
							
						}
						
					}
					
				}
				
				// Journal2 compatibility
				// make option block selected (the same as in original input/select)
				function ro_set_journal2_options() {
					
					if (use_journal2) {
						$('select[name^="'+option_prefix+'["]').find('option').each( function () {
							var poid = $(this).parent().attr('name').substr(7, $(this).parent().attr('name').length-8);
							
							// выключаем все блоки для SELECT
							//$(this).parent().parent().find('li[data-value]').removeClass('selected');
							
							if ($(this).parent().val()) {
								$(this).parent().parent().find('li[data-value='+$(this).parent().val()+']').removeClass('selected').addClass('selected');
							} else {
								$(this).parent().parent().find('li[data-value]').removeClass('selected');
							}
							
						});
						
						// block options use RADIOs for images
						$('input[type=radio][name^="'+option_prefix+'["]').each( function () {
							var poid = $(this).attr('name').substr(7, $(this).attr('name').length-8);
							
							// turn off only current block for RADIO
							//$(this).parent().find('li[data-value]').removeClass('selected');

							if ($(this).is(':checked')) {
							
								$('#input-option'+poid).parent().find('li[data-value='+$(this).val()+']').removeClass('selected').addClass('selected');
							} else {
							
								$('#input-option'+poid).parent().find('li[data-value]').removeClass('selected');
							}
							
						});
						
					}
					
				}
				
				
				
				$('select[name^="'+option_prefix+'"]').change(function(){
					ro_options_values_access();
				})
				
				$('input[type=radio][name^="'+option_prefix+'"]').change(function(){
					ro_options_values_access();
				})
				
				$("input[type=text][name=quantity]").change(function(){
					ro_stock_control(0);
				})
				
				
				ro_options_values_access();
				
				$(document).ready( function() {
				
					if ( typeof(ros_to_select) != 'undefined' && ros_to_select && ros_to_select.length) {
					
						var ro_id = false;
						for (var i in ros_to_select) {
							if (ros_to_select.hasOwnProperty(i)) {
								var ro_id = ros_to_select[i];
								ro_set_selected_combination(ro_id, true); // without limitaions
							}
						}
						if (ro_id) {
							ro_set_selected_combination(ro_id); /// with limitations
						}
					
					} else {
				
						// if there's filter and it's equal to related options model - this related options combination should be selected
						if (!ro_set_combination_by_model(filter_name)) { // if there's not filter relevant to related options 
							// if any value is selected - all values should be reselected (to be relevant to available related options)
							//if (ro_default !== false) {
							//	ro_set_selected_combination(ro_default);
							//} else {
								<?php if (!isset($poip_ov)) { ?>
									setTimeout(function () { ro_use_first_values(); }, 1); // if any combination is selected (may be other extension), check it and change if it's not relevant to available related options
								<?php } ?>
							//}
						}
						ro_options_values_access();
						ro_auto_select_first();
					}
					
					
				
				});
				
				<?php
				
					if (isset($ro_settings) && isset($ro_settings['show_clear_options']) && $ro_settings['show_clear_options']) {
						if ((int)$ro_settings['show_clear_options'] == 1) { ?>
							$(document).ready( function() {
								$('#product').find('h3').after('<div class="form-group"><a href="#" id="clear_options"><?php echo $text_ro_clear_options; ?></a></div>');
							});
						<?php } else { ?>
							$(document).ready( function() {
								<?php if ($ro_theme_name=='journal2') { ?>
									$('#product .options').append('<div class="form-group"><a href="#" id="clear_options" ><?php echo $text_ro_clear_options; ?></a></div>');
									//$('#product').find('h3:first').parent().append('<div class="form-group"><a href="#" id="clear_options" ><?php echo $text_ro_clear_options; ?></a></div>');
								<?php } else { ?>
									$('#product #input-quantity').parent().before('<div class="form-group"><a href="#" id="clear_options"><?php echo $text_ro_clear_options; ?></a></div>');
								<?php } ?>
							});
						<?php } ?>
						
						$('#product').on('click', '#clear_options', function(e){
							e.preventDefault();
							ro_clear_options();
						});
						
				<?php		
					}
				?>
				
				if (use_journal2) { // compatibility for live price update with specials in related options
				
					var div_prod_opt = $('div.product-options');
				
					if (div_prod_opt.length == 1) {
						if ( div_prod_opt.find('div.price').find('span.product-price').length ) {
							div_prod_opt.find('div.price').find('span.product-price').after('<span class="price-old" style="display: none"></span><span class="price-new" style="display: none"></span>');
							
						} else if ($('div.price').find('span.price-old').length) {
							div_prod_opt.find('div.price').find('span.price-old').before('<span class="product-price" itemprop="price" style="display: none"></span>');
						}
						
						setInterval( function() {
						
							if ( div_prod_opt.find('div.price').find('span.product-price').html() && div_prod_opt.find('div.price').find('span.price-old').html() && div_prod_opt.find('div.price').find('span.price-new').html() ) {
							
								if ( div_prod_opt.find('div.price').find('span.price-old').html() == div_prod_opt.find('div.price').find('span.price-new').html()
									|| Number($('div.product-options').find('div.price').find('span.price-new').html().replace(/\D/g, '')) == 0 ) {
									div_prod_opt.find('div.price').find('span.price-old').hide();
									div_prod_opt.find('div.price').find('span.price-new').hide();
									div_prod_opt.find('div.price').find('span.product-price').show();
								} else {
									div_prod_opt.find('div.price').find('span.price-old').show();
									div_prod_opt.find('div.price').find('span.price-new').show();
									div_prod_opt.find('div.price').find('span.product-price').hide();
								}
							}
						}, 200 );
					
					}
					
				}
				
				</script>
				
			

			
			<?php } ?>

<!-- >> Related Options PRO / Связанные опции PRO -->
<?php echo $footer; ?>