<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
<li><a href="#tab-google" data-toggle="tab"><?php echo $tab_google; ?></a></li>
            
            <li><a href="#tab-links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
            <li><a href="#tab-attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
            <li><a href="#tab-option" data-toggle="tab"><?php echo $tab_option; ?></a></li>
            <li><a href="#tab-recurring" data-toggle="tab"><?php echo $tab_recurring; ?></a></li>
            <li><a href="#tab-discount" data-toggle="tab"><?php echo $tab_discount; ?></a></li>
            <li><a href="#tab-special" data-toggle="tab"><?php echo $tab_special; ?></a></li>
            <li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
            <li><a href="#tab-reward" data-toggle="tab"><?php echo $tab_reward; ?></a></li>
            <li><a href="#tab-design" data-toggle="tab"><?php echo $tab_design; ?></a></li>

        <!-- Related Options PRO / Связанные опции PRO << -->
				<?php if ($ro_installed) { ?>
				<li><a href="#tab-related_options" data-toggle="tab"><?php echo $related_options_title; ?></a></li>
				<?php } ?>
        <!-- >> Related Options PRO / Связанные опции PRO -->
      
      
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php foreach ($languages as $language) { ?>
                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_name[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="product_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                      <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                      <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                      <?php } ?>
                    </div>
                  </div>
                  <!--chm-->
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-size<?php echo $language['language_id']; ?>"><?php echo $entry_size; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][size]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['size'] : ''; ?>" placeholder="<?php echo $entry_size; ?>" id="input-size<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-youtube<?php echo $language['language_id']; ?>"><?php echo $entry_youtube; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][youtube]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['youtube'] : ''; ?>" placeholder="<?php echo $entry_youtube; ?>" id="input-youtube<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-wood<?php echo $language['language_id']; ?>"><?php echo $entry_wood; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][wood]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['wood'] : ''; ?>" placeholder="<?php echo $entry_wood; ?>" id="input-wood<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-polish<?php echo $language['language_id']; ?>"><?php echo $entry_polish; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][polish]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['polish'] : ''; ?>" placeholder="<?php echo $entry_polish; ?>" id="input-polish<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-seat_cushion_shade<?php echo $language['language_id']; ?>"><?php echo $entry_seat_cushion_shade; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][seat_cushion_shade]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['seat_cushion_shade'] : ''; ?>" placeholder="<?php echo $entry_seat_cushion_shade; ?>" id="input-seat_cushion_shade<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-production_time<?php echo $language['language_id']; ?>"><?php echo $entry_production_time; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][production_time]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['production_time'] : ''; ?>" placeholder="<?php echo $entry_production_time; ?>" id="input-production_time<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-number_of_seats<?php echo $language['language_id']; ?>"><?php echo $entry_number_of_seats; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][number_of_seats]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['number_of_seats'] : ''; ?>" placeholder="<?php echo $entry_number_of_seats; ?>" id="input-number_of_seats<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-frame_material<?php echo $language['language_id']; ?>"><?php echo $entry_frame_material; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][frame_material]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['frame_material'] : ''; ?>" placeholder="<?php echo $entry_frame_material; ?>" id="input-frame_material<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-leg_material<?php echo $language['language_id']; ?>"><?php echo $entry_leg_material; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][leg_material]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['leg_material'] : ''; ?>" placeholder="<?php echo $entry_leg_material; ?>" id="input-leg_material<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-upholstery<?php echo $language['language_id']; ?>"><?php echo $entry_upholstery; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][upholstery]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['upholstery'] : ''; ?>" placeholder="<?php echo $entry_upholstery; ?>" id="input-upholstery<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-seating_support<?php echo $language['language_id']; ?>"><?php echo $entry_seating_support; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][seating_support]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['seating_support'] : ''; ?>" placeholder="<?php echo $entry_seating_support; ?>" id="input-seating_support<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-shades_available<?php echo $language['language_id']; ?>"><?php echo $entry_shades_available; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][shades_available]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['shades_available'] : ''; ?>" placeholder="<?php echo $entry_shades_available; ?>" id="input-shades_available<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-warranty<?php echo $language['language_id']; ?>"><?php echo $entry_warranty; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][warranty]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['warranty'] : ''; ?>" placeholder="<?php echo $entry_warranty; ?>" id="input-warranty<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                  <!--chm-->
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                    <div class="col-sm-10">
                      <textarea name="product_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                    <div class="col-sm-10">
                      <textarea name="product_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-tag<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="<?php echo $help_tag; ?>"><?php echo $entry_tag; ?></span></label>
                    <div class="col-sm-10">
                      <input type="text" name="product_description[<?php echo $language['language_id']; ?>][tag]" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['tag'] : ''; ?>" placeholder="<?php echo $entry_tag; ?>" id="input-tag<?php echo $language['language_id']; ?>" class="form-control" />
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
            <div class="tab-pane" id="tab-data">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-image"><?php echo $entry_image; ?></label>
                <div class="col-sm-10">
                  <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                  <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" />
                </div>
              </div>
              <div class="form-group required">
                <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_model; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
                  <?php if ($error_model) { ?>
                  <div class="text-danger"><?php echo $error_model; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sku"><span data-toggle="tooltip" title="<?php echo $help_sku; ?>"><?php echo $entry_sku; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="sku" value="<?php echo $sku; ?>" placeholder="<?php echo $entry_sku; ?>" id="input-sku" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-upc"><span data-toggle="tooltip" title="<?php echo $help_upc; ?>"><?php echo $entry_upc; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="upc" value="<?php echo $upc; ?>" placeholder="<?php echo $entry_upc; ?>" id="input-upc" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-ean"><span data-toggle="tooltip" title="<?php echo $help_ean; ?>"><?php echo $entry_ean; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="ean" value="<?php echo $ean; ?>" placeholder="<?php echo $entry_ean; ?>" id="input-ean" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-jan"><span data-toggle="tooltip" title="<?php echo $help_jan; ?>"><?php echo $entry_jan; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="jan" value="<?php echo $jan; ?>" placeholder="<?php echo $entry_jan; ?>" id="input-jan" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-isbn"><span data-toggle="tooltip" title="<?php echo $help_isbn; ?>"><?php echo $entry_isbn; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="isbn" value="<?php echo $isbn; ?>" placeholder="<?php echo $entry_isbn; ?>" id="input-isbn" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                





                <label class="col-sm-2 control-label" for="input-location"><?php echo $entry_location; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="location" value="<?php echo $location; ?>" placeholder="<?php echo $entry_location; ?>" id="input-location" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="price" value="<?php echo $price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
                <div class="col-sm-10">
                  <select name="tax_class_id" id="input-tax-class" class="form-control">
                    <option value="0"><?php echo $text_none; ?></option>
                    <?php foreach ($tax_classes as $tax_class) { ?>
                    <?php if ($tax_class['tax_class_id'] == $tax_class_id) { ?>
                    <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-minimum"><span data-toggle="tooltip" title="<?php echo $help_minimum; ?>"><?php echo $entry_minimum; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="minimum" value="<?php echo $minimum; ?>" placeholder="<?php echo $entry_minimum; ?>" id="input-minimum" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-subtract"><?php echo $entry_subtract; ?></label>
                <div class="col-sm-10">
                  <select name="subtract" id="input-subtract" class="form-control">
                    <?php if ($subtract) { ?>
                    <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                    <option value="0"><?php echo $text_no; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_yes; ?></option>
                    <option value="0" selected="selected"><?php echo $text_no; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-stock-status"><span data-toggle="tooltip" title="<?php echo $help_stock_status; ?>"><?php echo $entry_stock_status; ?></span></label>
                <div class="col-sm-10">
                  <select name="stock_status_id" id="input-stock-status" class="form-control">
                    <?php foreach ($stock_statuses as $stock_status) { ?>
                    <?php if ($stock_status['stock_status_id'] == $stock_status_id) { ?>
                    <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_shipping; ?></label>
                <div class="col-sm-10">
                  <label class="radio-inline">
                    <?php if ($shipping) { ?>
                    <input type="radio" name="shipping" value="1" checked="checked" />
                    <?php echo $text_yes; ?>
                    <?php } else { ?>
                    <input type="radio" name="shipping" value="1" />
                    <?php echo $text_yes; ?>
                    <?php } ?>
                  </label>
                  <label class="radio-inline">
                    <?php if (!$shipping) { ?>
                    <input type="radio" name="shipping" value="0" checked="checked" />
                    <?php echo $text_no; ?>
                    <?php } else { ?>
                    <input type="radio" name="shipping" value="0" />
                    <?php echo $text_no; ?>
                    <?php } ?>
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                  <?php if ($error_keyword) { ?>
                  <div class="text-danger"><?php echo $error_keyword; ?></div>
                  <?php } ?>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-date-available"><?php echo $entry_date_available; ?></label>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <input type="text" name="date_available" value="<?php echo $date_available; ?>" placeholder="<?php echo $entry_date_available; ?>" data-date-format="YYYY-MM-DD" id="input-date-available" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-length"><?php echo $entry_dimension; ?></label>
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-sm-4">
                      <input type="text" name="length" value="<?php echo $length; ?>" placeholder="<?php echo $entry_length; ?>" id="input-length" class="form-control" />
                    </div>
                    <div class="col-sm-4">
                      <input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
                    </div>
                    <div class="col-sm-4">
                      <input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-length-class"><?php echo $entry_length_class; ?></label>
                <div class="col-sm-10">
                  <select name="length_class_id" id="input-length-class" class="form-control">
                    <?php foreach ($length_classes as $length_class) { ?>
                    <?php if ($length_class['length_class_id'] == $length_class_id) { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-weight"><?php echo $entry_weight; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="weight" value="<?php echo $weight; ?>" placeholder="<?php echo $entry_weight; ?>" id="input-weight" class="form-control" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-weight-class"><?php echo $entry_weight_class; ?></label>
                <div class="col-sm-10">
                  <select name="weight_class_id" id="input-weight-class" class="form-control">
                    <?php foreach ($weight_classes as $weight_class) { ?>
                    <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="status" id="input-status" class="form-control">
                    <?php if ($status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                <div class="col-sm-10">
                  <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                </div>
              </div>
            </div>
        <div class="tab-pane" id="tab-google">
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_on_google1"><span data-toggle="tooltip" title="<?php echo $help_p_on_google; ?>"><?php echo $entry_p_on_google; ?></span></label>
			<div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary<?php if ($g_on_google=='' || $g_on_google=='1') { ?> active<?php } ?>">
						<input type="radio" name="g_on_google" id="input-g_on_google1" value="1"<?php if ($g_on_google=='' || $g_on_google=='1') { ?> checked<?php } ?>><?php echo $text_yes; ?>
					</label>
					<label class="btn btn-primary<?php if ($g_on_google=='0') { ?> active<?php } ?>">
						<input type="radio" name="g_on_google" id="input-g_on_google2" value="0"<?php if ($g_on_google=='0') { ?> checked<?php } ?>><?php echo $text_no; ?>
					</label>
				</div>
			</div>
		</div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_promotion_id"><span data-toggle="tooltip" title="<?php echo $help_p_promotion_id; ?>"><?php echo $entry_p_promotion_id; ?></span></label>
			<div class="col-sm-10">
			  <input type="text" name="g_promotion_id" value="<?php echo $g_promotion_id; ?>" placeholder="<?php echo $entry_p_promotion_id; ?>" id="input-g_promotion_id" class="form-control" />
			</div>
		  </div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-p_expiry_date"><span data-toggle="tooltip" title="<?php echo $help_p_expiry_date; ?>"><?php echo $entry_p_expiry_date; ?></span></label>
			<div class="col-sm-3">
			  <div class="input-group expiry-date">
				<input type="text" name="g_expiry_date" value="<?php echo $g_expiry_date; ?>" placeholder="<?php echo $entry_p_expiry_date; ?>" data-date-format="YYYY-MM-DD" id="input-p_expiry_date" class="form-control" />
				<span class="input-group-btn">
				<button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
				</span></div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_identifier_exists1"><span data-toggle="tooltip" title="<?php echo $help_p_identifier_exists; ?>"><?php echo $entry_p_identifier_exists; ?></span></label>
			<div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary<?php if ($g_identifier_exists=='' || $g_identifier_exists=='1') { ?> active<?php } ?>">
						<input type="radio" name="g_identifier_exists" id="input-g_identifier_exists1" value="1"<?php if ($g_identifier_exists=='' || $g_identifier_exists=='1') { ?> checked<?php } ?>><?php echo $text_yes; ?>
					</label>
					<label class="btn btn-primary<?php if ($g_identifier_exists=='0') { ?> active<?php } ?>">
						<input type="radio" name="g_identifier_exists" id="input-g_identifier_exists2" value="0"<?php if ($g_identifier_exists=='0') { ?> checked<?php } ?>><?php echo $text_no; ?>
					</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_condition1"><span data-toggle="tooltip" title="<?php echo $help_p_condition; ?>"><?php echo $entry_p_condition; ?></span></label>
			<div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary<?php if ($g_condition=='new'||$g_condition=='') { ?> active<?php } ?>">
						<input type="radio" name="g_condition" id="input-g_condition1" value="new"<?php if ($g_condition=='new'||$g_condition=='') { ?> checked<?php } ?>><?php echo $text_condition_new; ?>
					</label>
					<label class="btn btn-primary<?php if ($g_condition=='used') { ?> active<?php } ?>">
						<input type="radio" name="g_condition" id="input-g_condition2" value="used"<?php if ($g_condition=='used') { ?> checked<?php } ?>><?php echo $text_condition_used; ?>
					</label>
					<label class="btn btn-primary<?php if ($g_condition=='refurbished') { ?> active<?php } ?>">
						<input type="radio" name="g_condition" id="input-g_condition3" value="refurbished"<?php if ($g_condition=='refurbished') { ?> checked<?php } ?>><?php echo $text_condition_ref; ?>
					</label>
				</div>
			</div>
		</div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_brand"><span data-toggle="tooltip" title="<?php echo $help_p_brand; ?>"><?php echo $entry_p_brand; ?></span></label>
			<div class="col-sm-10">
			  <input type="text" name="g_brand" value="<?php echo $g_brand; ?>" placeholder="<?php echo $entry_p_brand; ?>" id="input-g_brand" class="form-control" />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-mpn"><span data-toggle="tooltip" title="<?php echo $help_mpn; ?>"><?php echo $entry_mpn; ?></span></label>
			<div class="col-sm-10">
			  <input type="text" name="mpn" value="<?php echo $mpn; ?>" placeholder="<?php echo $entry_p_mpn; ?>" id="input-mpn" class="form-control"<?php echo ($config_mpn=='model'?' onclick="javascript:alert(\''.$warning_mpn_model.'\');"':($config_mpn=='location'?' onclick="javascript:alert(\''.$warning_mpn_location.'\');"':($config_mpn=='sku'?' onclick="javascript:alert(\''.$warning_mpn_sku.'\');"':''))); ?> />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_gtin"><span data-toggle="tooltip" title="<?php echo $help_p_gtin; ?>"><?php echo $entry_p_gtin; ?></span></label>
			<div class="col-sm-10">
			  <input type="text" name="g_gtin" value="<?php echo $g_gtin; ?>" placeholder="<?php echo $entry_p_gtin; ?>" id="input-g_gtin" class="form-control"<?php echo ($config_gtin=='default'?' onclick="javascript:alert(\''.$warning_gtin_default.'\');"':($config_gtin=='location'?' onclick="javascript:alert(\''.$warning_gtin_location.'\');"':($config_gtin=='sku'?' onclick="javascript:alert(\''.$warning_gtin_sku.'\');"':''))); ?> />
			</div>
		  </div>
		<div class="form-group">
            <label class="col-sm-2 control-label" for="select-gpc"><span data-toggle="tooltip" title="<?php echo $help_p_google_category; ?><br><br><?php echo $entry_choose_google_category_xml; ?>"><?php echo $entry_p_google_category; ?></span></label>
            <div class="col-sm-8">
			<?php $nogpc = 17; if($config_gpc_gb != ''){ ?>
			<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/gb.png" /></span><span class="input-group-addon"><img src="view/image/flags/in.png" /> <img src="view/image/flags/ch.png" /></span><input id="input-gpc" type="text" name="google_category_gb" placeholder="<?php echo $text_gpc_gb; ?>" value="<?php echo $google_category_gb; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_gb" value="" /><?php } ?>
            <?php if($config_gpc_us != ''){ ?>
			<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/us.png" /></span><span class="input-group-addon"><img src="view/image/flags/ca.png" /></span><input type="text" name="google_category_us" placeholder="<?php echo $text_gpc_us; ?>" value="<?php echo $google_category_us; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=en-US','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_us" value="" /><?php } ?>
            <?php if($config_gpc_au != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/au.png" /></span><input type="text" name="google_category_au" placeholder="<?php echo $text_gpc_au; ?>" value="<?php echo $google_category_au; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=en-AU','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_au" value="" /><?php } ?>
            <?php if($config_gpc_fr != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/fr.png" /></span><span class="input-group-addon"><img src="view/image/flags/be.png" /> <img src="view/image/flags/ca.png" /> <img src="view/image/flags/ch.png" /></span><input type="text" name="google_category_fr" placeholder="<?php echo $text_gpc_fr; ?>" value="<?php echo $google_category_fr; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=fr-FR','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_fr" value="" /><?php } ?>
            <?php if($config_gpc_de != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/de.png" /></span><span class="input-group-addon"><img src="view/image/flags/at.png" /> <img src="view/image/flags/ch.png" /></span><input type="text" name="google_category_de" placeholder="<?php echo $text_gpc_de; ?>" value="<?php echo $google_category_de; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=de-DE','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_de" value="" /><?php } ?>
            <?php if($config_gpc_it != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/it.png" /></span><span class="input-group-addon"><img src="view/image/flags/ch.png" /></span><input type="text" name="google_category_it" placeholder="<?php echo $text_gpc_it; ?>" value="<?php echo $google_category_it; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=it-IT','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_it" value="" /><?php } ?>
            <?php if($config_gpc_nl != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/nl.png" /></span><span class="input-group-addon"><img src="view/image/flags/be.png" /></span><input type="text" name="google_category_nl" placeholder="<?php echo $text_gpc_nl; ?>" value="<?php echo $google_category_nl; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=nl-NL','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_nl" value="" /><?php } ?>
            <?php if($config_gpc_es != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/es.png" /></span><span class="input-group-addon"><img src="view/image/flags/mx.png" /></span><input type="text" name="google_category_es" placeholder="<?php echo $text_gpc_es; ?>" value="<?php echo $google_category_es; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=es-ES','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_es" value="" /><?php } ?>
            <?php if($config_gpc_pt != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/br.png" /></span><input type="text" name="google_category_pt" placeholder="<?php echo $text_gpc_pt; ?>" value="<?php echo $google_category_pt; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=pt-BR','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_pt" value="" /><?php } ?>
            <?php if($config_gpc_cz != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/cz.png" /></span><input type="text" name="google_category_cz" placeholder="<?php echo $text_gpc_cz; ?>" value="<?php echo $google_category_cz; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=cs-CZ','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_cz" value="" /><?php } ?>
            <?php if($config_gpc_jp != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/jp.png" /></span><input type="text" name="google_category_jp" placeholder="<?php echo $text_gpc_jp; ?>" value="<?php echo $google_category_jp; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=ja-JP','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_jp" value="" /><?php } ?>
            <?php if($config_gpc_dk != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/dk.png" /></span><input type="text" name="google_category_dk" placeholder="<?php echo $text_gpc_da; ?>" value="<?php echo $google_category_dk; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=da-DK','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_dk" value="" /><?php } ?>
            <?php if($config_gpc_no != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/no.png" /></span><input type="text" name="google_category_no" placeholder="<?php echo $text_gpc_no; ?>" value="<?php echo $google_category_no; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=no-NO','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_no" value="" /><?php } ?>
            <?php if($config_gpc_pl != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/pl.png" /></span><input type="text" name="google_category_pl" placeholder="<?php echo $text_gpc_pl; ?>" value="<?php echo $google_category_pl; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=pl-PL','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_pl" value="" /><?php } ?>
            <?php if($config_gpc_ru != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/ru.png" /></span><input type="text" name="google_category_ru" placeholder="<?php echo $text_gpc_ru; ?>" value="<?php echo $google_category_ru; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=ru-RU','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_ru" value="" /><?php } ?>
            <?php if($config_gpc_sv != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/se.png" /></span><input type="text" name="google_category_sv" placeholder="<?php echo $text_gpc_sv; ?>" value="<?php echo $google_category_sv; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=sv-SE','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div><br>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_sv" value="" /><?php } ?>
            <?php if($config_gpc_tr != ''){ ?>
            <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/tr.png" /></span><input type="text" name="google_category_tr" placeholder="<?php echo $text_gpc_tr; ?>" value="<?php echo $google_category_tr; ?>" class="form-control" /><span class="input-group-addon"><a onclick="window.open('<?php echo $uksbhome; ?>taxonomy.php?lang=tr-TR','google');"><i data-toggle="tooltip" class="fa fa-plus-circle" style="cursor:pointer;" title="<?php echo $entry_choose_google_category; ?>"></i></a></span></div>
			<?php }else{ $nogpc --; ?><input type="hidden" id="input-gpc" name="google_category_tr" value="" /><?php } ?>
<?php if($nogpc === 0){ echo $warning_nogpc; } ?></div>
          </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_multipack"><span data-toggle="tooltip" title="<?php echo $help_p_multipack; ?>"><?php echo $entry_p_multipack; ?></span></label>
			<div class="col-sm-10">
			  <input type="text" name="g_multipack" value="<?php echo ($g_multipack?$g_multipack:''); ?>" placeholder="<?php echo $entry_p_multipack; ?>" id="input-g_multipack" class="form-control" />
			</div>
		  </div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_is_bundle1"><span data-toggle="tooltip" title="<?php echo $help_p_is_bundle; ?>"><?php echo $entry_p_is_bundle; ?></span></label>
			<div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary<?php if ($g_is_bundle=='1') { ?> active<?php } ?>">
						<input type="radio" name="g_is_bundle" id="input-g_is_bundle1" value="1"<?php if ($g_is_bundle=='1') { ?> checked<?php } ?>><?php echo $text_yes; ?>
					</label>
					<label class="btn btn-primary<?php if ($g_is_bundle=='' || $g_is_bundle=='0') { ?> active<?php } ?>">
						<input type="radio" name="g_is_bundle" id="input-g_is_bundle2" value="0"<?php if ($g_is_bundle=='' || $g_is_bundle=='0') { ?> checked<?php } ?>><?php echo $text_no; ?>
					</label>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_adult1"><span data-toggle="tooltip" title="<?php echo $help_p_adult; ?>"><?php echo $entry_p_adult; ?></span></label>
			<div class="col-sm-10">
				<div class="btn-group" data-toggle="buttons">
					<label class="btn btn-primary<?php if ($g_adult=='1') { ?> active<?php } ?>">
						<input type="radio" name="g_adult" id="input-g_adult1" value="1"<?php if ($g_adult=='1') { ?> checked<?php } ?>><?php echo $text_yes; ?>
					</label>
					<label class="btn btn-primary<?php if ($g_adult=='' || $g_adult=='0') { ?> active<?php } ?>">
						<input type="radio" name="g_adult" id="input-g_adult2" value="0"<?php if ($g_adult=='' || $g_adult=='0') { ?> checked<?php } ?>><?php echo $text_no; ?>
					</label>
				</div>
			</div>
		</div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-g_energy_efficiency_class"><span data-toggle="tooltip" title="<?php echo $help_p_energy_efficiency_class; ?>"><?php echo $entry_p_energy_efficiency_class; ?></span></label>
            <div class="col-sm-10">
              <select name="g_energy_efficiency_class" id="select-g_energy_efficiency_class" class="form-control">
                  <option value="0"<?php if (!$g_energy_efficiency_class||$g_energy_efficiency_class=='0') { ?> selected="selected"<?php } ?>><?php echo $text_none; ?></option>
                  <option value="A+++"<?php if ($g_energy_efficiency_class=='A+++') { ?> selected="selected"<?php } ?>>A+++</option>
                  <option value="A++"<?php if ($g_energy_efficiency_class=='A++') { ?> selected="selected"<?php } ?>>A++</option>
                  <option value="A+"<?php if ($g_energy_efficiency_class=='A+') { ?> selected="selected"<?php } ?>>A+</option>
                  <option value="A"<?php if ($g_energy_efficiency_class=='A') { ?> selected="selected"<?php } ?>>A</option>
                  <option value="B"<?php if ($g_energy_efficiency_class=='B') { ?> selected="selected"<?php } ?>>B</option>
                  <option value="C"<?php if ($g_energy_efficiency_class=='C') { ?> selected="selected"<?php } ?>>C</option>
                  <option value="D"<?php if ($g_energy_efficiency_class=='D') { ?> selected="selected"<?php } ?>>D</option>
                  <option value="E"<?php if ($g_energy_efficiency_class=='E') { ?> selected="selected"<?php } ?>>E</option>
                  <option value="F"<?php if ($g_energy_efficiency_class=='F') { ?> selected="selected"<?php } ?>>F</option>
                  <option value="G"<?php if ($g_energy_efficiency_class=='G') { ?> selected="selected"<?php } ?>>G</option>
              </select></div>
          </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_unit_pricing_measure"><span data-toggle="tooltip" title="<?php echo $help_p_unit_pricing_measure; ?>"><?php echo $entry_p_unit_pricing_measure; ?></span></label>
			<div class="col-sm-10">
			  <input type="text" name="g_unit_pricing_measure" value="<?php echo $g_unit_pricing_measure; ?>" placeholder="<?php echo $entry_p_unit_pricing_measure; ?>" id="input-g_unit_pricing_measure" class="form-control" />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_unit_pricing_base_measure"><span data-toggle="tooltip" title="<?php echo $help_p_unit_pricing_base_measure; ?>"><?php echo $entry_p_unit_pricing_base_measure; ?></span></label>
			<div class="col-sm-10">
			  <input type="text" name="g_unit_pricing_base_measure" value="<?php echo $g_unit_pricing_base_measure; ?>" placeholder="<?php echo $entry_p_unit_pricing_base_measure; ?>" id="input-g_unit_pricing_base_measure" class="form-control" />
			</div>
		  </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-g_gender"><?php echo $entry_p_gender; ?></label>
            <div class="col-sm-10">
              <select name="g_gender" id="select-g_gender" class="form-control">
                  <option value="0"<?php if ($g_gender=='0'||$g_gender=='') { ?> selected="selected"<?php } ?>><?php echo $text_none; ?></option>
                  <option value="male"<?php if ($g_gender=='male') { ?> selected="selected"<?php } ?>><?php echo $text_male; ?></option>
                  <option value="female"<?php if ($g_gender=='female') { ?> selected="selected"<?php } ?>><?php echo $text_female; ?></option>
                  <option value="unisex"<?php if ($g_gender=='unisex') { ?> selected="selected"<?php } ?>><?php echo $text_unisex; ?></option>
              </select></div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-g_age_group"><?php echo $entry_p_age_group; ?></label>
            <div class="col-sm-10">
              <select name="g_age_group" id="select-g_age_group" class="form-control">
                  <option value="0"<?php if ($g_age_group=='0'||$g_age_group=='') { ?> selected="selected"<?php } ?>><?php echo $text_none; ?></option>
                  <option value="newborn"<?php if ($g_age_group=='newborn') { ?> selected="selected"<?php } ?>><?php echo $text_newborn; ?></option>
                  <option value="infant"<?php if ($g_age_group=='infant') { ?> selected="selected"<?php } ?>><?php echo $text_infant; ?></option>
                  <option value="toddler"<?php if ($g_age_group=='toddler') { ?> selected="selected"<?php } ?>><?php echo $text_toddler; ?></option>
                  <option value="kids"<?php if ($g_age_group=='kids') { ?> selected="selected"<?php } ?>><?php echo $text_kids; ?></option>
                  <option value="adult"<?php if ($g_age_group=='adult') { ?> selected="selected"<?php } ?>><?php echo $text_adult; ?></option>
              </select></div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-size_type"><span data-toggle="tooltip" title="<?php echo $help_p_size_type; ?>"><?php echo $entry_p_size_type; ?></span></label>
            <div class="col-sm-10">
              <select name="g_size_type" id="select-g_size_type" class="form-control">
                  <option value="0"<?php if (!$g_size_type||$g_size_type=='0') { ?> selected="selected"<?php } ?>><?php echo $text_none; ?></option>
                  <option value="regular"<?php if ($g_size_type=='regular') { ?> selected="selected"<?php } ?>><?php echo $text_regular; ?></option>
                  <option value="petite"<?php if ($g_size_type=='petite') { ?> selected="selected"<?php } ?>><?php echo $text_petite; ?></option>
                  <option value="plus"<?php if ($g_size_type=='plus') { ?> selected="selected"<?php } ?>><?php echo $text_plus; ?></option>
                  <option value="big and tall"<?php if ($g_size_type=='big and tall') { ?> selected="selected"<?php } ?>><?php echo $text_big_and_tall; ?></option>
                  <option value="maternity"<?php if ($g_size_type=='maternity') { ?> selected="selected"<?php } ?>><?php echo $text_maternity; ?></option>
              </select></div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="select-size_system"><span data-toggle="tooltip" title="<?php echo $help_p_size_system; ?>"><?php echo $entry_p_size_system; ?></span></label>
            <div class="col-sm-10">
              <select name="g_size_system" id="select-g_size_system" class="form-control">
                  <option value="0"<?php if (!$g_size_system||$g_size_system=='0') { ?> selected="selected"<?php } ?>><?php echo $text_none; ?></option>
                  <option value="US"<?php if ($g_size_system=='US') { ?> selected="selected"<?php } ?>>US</option>
                  <option value="UK"<?php if ($g_size_system=='UK') { ?> selected="selected"<?php } ?>>UK</option>
                  <option value="EU"<?php if ($g_size_system=='EU') { ?> selected="selected"<?php } ?>>EU</option>
                  <option value="DE"<?php if ($g_size_system=='DE') { ?> selected="selected"<?php } ?>>DE</option>
                  <option value="FR"<?php if ($g_size_system=='FR') { ?> selected="selected"<?php } ?>>FR</option>
                  <option value="JP"<?php if ($g_size_system=='JP') { ?> selected="selected"<?php } ?>>JP</option>
                  <option value="CN (China)"<?php if ($g_size_system=='CN (China)') { ?> selected="selected"<?php } ?>>CN (China)</option>
                  <option value="IT"<?php if ($g_size_system=='IT') { ?> selected="selected"<?php } ?>>IT</option>
                  <option value="BR"<?php if ($g_size_system=='BR') { ?> selected="selected"<?php } ?>>BR</option>
                  <option value="MEX"<?php if ($g_size_system=='MEX') { ?> selected="selected"<?php } ?>>MEX</option>
                  <option value="AU"<?php if ($g_size_system=='AU') { ?> selected="selected"<?php } ?>>AU</option>
              </select></div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-10"><h2><?php echo $entry_variant_section; ?></h2></div>
		  </div>
              <div class="table-responsive">
                <table id="variants" class="table table-striped table-bordered table-hover">
                  <tbody>
                    <?php $variant_row = 0; ?>
                    <?php foreach ($variants as $variant) { ?>
                    <tr id="variant-row<?php echo $variant_row; ?>">
                      <td class="col-sm-3 text-left">
					  <label class="control-label" for="variant[<?php echo $variant_row; ?>][g_size]"><?php echo $entry_p_size; ?></label><br>
					  <input type="text" id="variant[<?php echo $variant_row; ?>][g_size]" name="variant[<?php echo $variant_row; ?>][g_size]" value="<?php echo $variant['g_size']; ?>" placeholder="<?php echo $entry_p_size; ?>" class="form-control" /><br><br>
					  <label class="control-label" for="variant[<?php echo $variant_row; ?>][v_mpn]"><?php echo $entry_v_mpn; ?></label><br>
					  <input type="text" id="variant[<?php echo $variant_row; ?>][v_mpn]" name="variant[<?php echo $variant_row; ?>][v_mpn]" value="<?php echo $variant['v_mpn']; ?>" placeholder="<?php echo $entry_v_mpn; ?>" class="form-control" /><br><br>
					  <label class="control-label" for="variant[<?php echo $variant_row; ?>][g_material]"><?php echo $entry_p_material; ?></label><br>
					  <input type="text" id="variant[<?php echo $variant_row; ?>][g_material]" name="variant[<?php echo $variant_row; ?>][g_material]" value="<?php echo $variant['g_material']; ?>" placeholder="<?php echo $entry_p_material; ?>" class="form-control" /></td>
                      <td class="col-sm-3 text-left">
					  <label class="control-label" for="variant[<?php echo $variant_row; ?>][g_colour]"><?php echo $entry_p_colour; ?></label><br>
					  <input type="text" id="variant[<?php echo $variant_row; ?>][g_colour]" name="variant[<?php echo $variant_row; ?>][g_colour]" value="<?php echo $variant['g_colour']; ?>" placeholder="<?php echo $entry_p_colour; ?>" class="form-control" /><br><br>
					  <label class="control-label" for="variant[<?php echo $variant_row; ?>][v_gtin]"><?php echo $entry_v_gtin; ?></label><br>
					  <input type="text" id="variant[<?php echo $variant_row; ?>][v_gtin]" name="variant[<?php echo $variant_row; ?>][v_gtin]" value="<?php echo $variant['v_gtin']; ?>" placeholder="<?php echo $entry_v_gtin; ?>" class="form-control" /><br><br>
					  <label class="control-label" for="variant[<?php echo $variant_row; ?>][g_pattern]"><?php echo $entry_p_pattern; ?></label><br>
					  <input type="text" id="variant[<?php echo $variant_row; ?>][g_pattern]" name="variant[<?php echo $variant_row; ?>][g_pattern]" value="<?php echo $variant['g_pattern']; ?>" placeholder="<?php echo $entry_p_pattern; ?>" class="form-control" /></td>
                      <td class="col-sm-3 text-left">
					  <label class="control-label" for="variant[<?php echo $variant_row; ?>][v_prices]"><?php echo $entry_v_prices; ?></label><br><br><?php echo $help_v_prices; ?><br><br>
					  <input type="text" id="variant[<?php echo $variant_row; ?>][v_prices]" name="variant[<?php echo $variant_row; ?>][v_prices]" value="<?php echo $variant['v_prices']; ?>" placeholder="<?php echo $entry_v_prices; ?>" class="form-control" /></td>
                      <td class="col-sm-2 text-left"><a href="" id="thumb-image<?php echo $variant_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo ($variant['thumb']!=''?$variant['thumb']:$placeholder); ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="variant[<?php echo $variant_row; ?>][v_images]" value="<?php echo $variant['v_images']; ?>" id="v_images<?php echo $variant_row; ?>" /></td>
                      <td class="text-left"><button type="button" onclick="$('#variant-row<?php echo $variant_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $variant_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="4"></td>
                      <td class="col-sm-1 text-left"><button type="button" onclick="addVariant();" data-toggle="tooltip" title="<?php echo $button_add_variant; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
          <div class="form-group">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-10"><h2><?php echo $entry_adwords_section; ?></h2></div>
		  </div>
          <div class="form-group">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-10"><?php echo $help_p_custom_label; ?></div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_custom_label_0"><?php echo $entry_p_custom_label_0; ?></label>
			<div class="col-sm-10">
			  <input type="text" name="g_custom_label_0" value="<?php echo $g_custom_label_0; ?>" placeholder="<?php echo $entry_p_custom_label_0; ?>" id="input-g_custom_label_0" class="form-control" />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_custom_label_1"><?php echo $entry_p_custom_label_1; ?></label>
			<div class="col-sm-10">
			  <input type="text" name="g_custom_label_1" value="<?php echo $g_custom_label_1; ?>" placeholder="<?php echo $entry_p_custom_label_1; ?>" id="input-g_custom_label_1" class="form-control" />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_custom_label_2"><?php echo $entry_p_custom_label_2; ?></label>
			<div class="col-sm-10">
			  <input type="text" name="g_custom_label_2" value="<?php echo $g_custom_label_2; ?>" placeholder="<?php echo $entry_p_custom_label_2; ?>" id="input-g_custom_label_2" class="form-control" />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_custom_label_3"><?php echo $entry_p_custom_label_3; ?></label>
			<div class="col-sm-10">
			  <input type="text" name="g_custom_label_3" value="<?php echo $g_custom_label_3; ?>" placeholder="<?php echo $entry_p_custom_label_3; ?>" id="input-g_custom_label_3" class="form-control" />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_custom_label_4"><?php echo $entry_p_custom_label_4; ?></label>
			<div class="col-sm-10">
			  <input type="text" name="g_custom_label_4" value="<?php echo $g_custom_label_4; ?>" placeholder="<?php echo $entry_p_custom_label_4; ?>" id="input-g_custom_label_4" class="form-control" />
			</div>
		  </div>
		  <div class="form-group">
			<label class="col-sm-2 control-label" for="input-g_adwords_redirect"><span data-toggle="tooltip" title="<?php echo $help_p_adwords_redirect; ?>"><?php echo $entry_p_adwords_redirect; ?></span></label>
			<div class="col-sm-10">
			  <input type="text" name="g_adwords_redirect" value="<?php echo $g_adwords_redirect; ?>" placeholder="<?php echo $entry_p_adwords_redirect; ?>" id="input-g_adwords_redirect" class="form-control" />
			</div>
		  </div>
          <div class="form-group">
            <div class="col-sm-2">&nbsp;</div>
            <div class="col-sm-10"><h2><?php echo $entry_help_section; ?></h2><p><?php echo $help_google_help; ?></p></div>
		  </div>
		  <div class="form-group">
            <div class="col-sm-2">&nbsp;</div>
			<div class="col-sm-10"><button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button></div>
		  </div>		  
        </div>
            <div class="tab-pane" id="tab-links">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-manufacturer"><span data-toggle="tooltip" title="<?php echo $help_manufacturer; ?>"><?php echo $entry_manufacturer; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="manufacturer" value="<?php echo $manufacturer ?>" placeholder="<?php echo $entry_manufacturer; ?>" id="input-manufacturer" class="form-control" />
                  <input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>" />
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-category"><span data-toggle="tooltip" title="<?php echo $help_category; ?>"><?php echo $entry_category; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="category" value="" placeholder="<?php echo $entry_category; ?>" id="input-category" class="form-control" />
                  <div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_categories as $product_category) { ?>
                    <div id="product-category<?php echo $product_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_category['name']; ?>
                      <input type="hidden" name="product_category[]" value="<?php echo $product_category['category_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="<?php echo $help_filter; ?>"><?php echo $entry_filter; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="filter" value="" placeholder="<?php echo $entry_filter; ?>" id="input-filter" class="form-control" />
                  <div id="product-filter" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_filters as $product_filter) { ?>
                    <div id="product-filter<?php echo $product_filter['filter_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_filter['name']; ?>
                      <input type="hidden" name="product_filter[]" value="<?php echo $product_filter['filter_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_store; ?></label>
                <div class="col-sm-10">
                  <div class="well well-sm" style="height: 150px; overflow: auto;">
                    <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $product_store)) { ?>
                        <input type="checkbox" name="product_store[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="product_store[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $product_store)) { ?>
                        <input type="checkbox" name="product_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="product_store[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-download"><span data-toggle="tooltip" title="<?php echo $help_download; ?>"><?php echo $entry_download; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="download" value="" placeholder="<?php echo $entry_download; ?>" id="input-download" class="form-control" />
                  <div id="product-download" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_downloads as $product_download) { ?>
                    <div id="product-download<?php echo $product_download['download_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_download['name']; ?>
                      <input type="hidden" name="product_download[]" value="<?php echo $product_download['download_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                  <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                    <?php foreach ($product_relateds as $product_related) { ?>
                    <div id="product-related<?php echo $product_related['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_related['name']; ?>
                      <input type="hidden" name="product_related[]" value="<?php echo $product_related['product_id']; ?>" />
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-attribute">
              <div class="table-responsive">
                <table id="attribute" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_attribute; ?></td>
                      <td class="text-left"><?php echo $entry_text; ?></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $attribute_row = 0; ?>
                    <?php foreach ($product_attributes as $product_attribute) { ?>
                    <tr id="attribute-row<?php echo $attribute_row; ?>">
                      <td class="text-left" style="width: 40%;"><input type="text" name="product_attribute[<?php echo $attribute_row; ?>][name]" value="<?php echo $product_attribute['name']; ?>" placeholder="<?php echo $entry_attribute; ?>" class="form-control" />
                        <input type="hidden" name="product_attribute[<?php echo $attribute_row; ?>][attribute_id]" value="<?php echo $product_attribute['attribute_id']; ?>" /></td>
                      <td class="text-left"><?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                          <textarea name="product_attribute[<?php echo $attribute_row; ?>][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"><?php echo isset($product_attribute['product_attribute_description'][$language['language_id']]) ? $product_attribute['product_attribute_description'][$language['language_id']]['text'] : ''; ?></textarea>
                        </div>
                        <?php } ?></td>
                      <td class="text-left"><button type="button" onclick="$('#attribute-row<?php echo $attribute_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $attribute_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="<?php echo $button_attribute_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-option">
              <div class="row">
                <div class="col-sm-2">
                  <ul class="nav nav-pills nav-stacked" id="option">
                    <?php $option_row = 0; ?>
                    <?php foreach ($product_options as $product_option) { ?>
                    <li><a href="#tab-option<?php echo $option_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-option<?php echo $option_row; ?>\']').parent().remove(); $('#tab-option<?php echo $option_row; ?>').remove(); $('#option a:first').tab('show');"></i> <?php echo $product_option['name']; ?></a></li>
                    <?php $option_row++; ?>
                    <?php } ?>
                    <li>
                      <input type="text" name="option" value="" placeholder="<?php echo $entry_option; ?>" id="input-option" class="form-control" />
                    </li>
                  </ul>
                </div>
                <div class="col-sm-10">
                  <div class="tab-content">
                    <?php $option_row = 0; ?>
                    <?php $option_value_row = 0; ?>
                    <?php foreach ($product_options as $product_option) { ?>
                    <div class="tab-pane" id="tab-option<?php echo $option_row; ?>">
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_id]" value="<?php echo $product_option['product_option_id']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][name]" value="<?php echo $product_option['name']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][option_id]" value="<?php echo $product_option['option_id']; ?>" />
                      <input type="hidden" name="product_option[<?php echo $option_row; ?>][type]" value="<?php echo $product_option['type']; ?>" />
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-required<?php echo $option_row; ?>"><?php echo $entry_required; ?></label>
                        <div class="col-sm-10">
                          <select name="product_option[<?php echo $option_row; ?>][required]" id="input-required<?php echo $option_row; ?>" class="form-control">
                            <?php if ($product_option['required']) { ?>
                            <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                            <option value="0"><?php echo $text_no; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_yes; ?></option>
                            <option value="0" selected="selected"><?php echo $text_no; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <?php if ($product_option['type'] == 'text') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'textarea') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <textarea name="product_option[<?php echo $option_row; ?>][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control"><?php echo $product_option['value']; ?></textarea>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'file') { ?>
                      <div class="form-group" style="display: none;">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'date') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-3">
                          <div class="input-group date">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'time') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <div class="input-group time">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'datetime') { ?>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                        <div class="col-sm-10">
                          <div class="input-group datetime">
                            <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                        </div>
                      </div>
                      <?php } ?>
                      <?php if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') { ?>
                      <div class="table-responsive">
                        <table id="option-value<?php echo $option_row; ?>" class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                              <td class="text-left"><?php echo $entry_option_value; ?></td>
                              <td class="text-right"><?php echo $entry_quantity; ?></td>
                              <td class="text-left"><?php echo $entry_subtract; ?></td>
                              <td class="text-right"><?php echo $entry_price; ?></td>
                              <td class="text-right"><?php echo $entry_option_points; ?></td>
                              <td class="text-right"><?php echo $entry_weight; ?></td>

				<?php if (!empty($product_option['type']) && $product_option['type'] == 'radio') { ?>
				<td class="text-right"><?php echo $text_cioptioncolor; ?></td>
				<?php } ?>
			
                              <td></td>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
                            <tr id="option-value-row<?php echo $option_value_row; ?>">
                              <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]" class="form-control">
                                  <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                  <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                  <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                  <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
                                  <?php } else { ?>
                                  <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                  <?php } ?>
                                  <?php } ?>
                                  <?php } ?>
                                </select>
                                <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" /></td>
                              <td class="text-right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][quantity]" value="<?php echo $product_option_value['quantity']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>
                              <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][subtract]" class="form-control">
                                  <?php if ($product_option_value['subtract']) { ?>
                                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                  <option value="0"><?php echo $text_no; ?></option>
                                  <?php } else { ?>
                                  <option value="1"><?php echo $text_yes; ?></option>
                                  <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                  <?php } ?>
                                </select></td>
                              <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price_prefix]" class="form-control">
                                  <?php if ($product_option_value['price_prefix'] == '+') { ?>
                                  <option value="+" selected="selected">+</option>
                                  <?php } else { ?>
                                  <option value="+">+</option>
                                  <?php } ?>

			<?php // << Live Price
			
			if ( isset($liveprice_installed) && $liveprice_installed ) {
				echo '<option value="%" '.(($product_option_value['price_prefix']=='%')?('selected'):('')).'>%</option>';
				echo '<option value="*" '.(($product_option_value['price_prefix']=='*')?('selected'):('')).'>*</option>';
				echo '<option value="/" '.(($product_option_value['price_prefix']=='/')?('selected'):('')).'>/</option>';
				echo '<option value="=" '.(($product_option_value['price_prefix']=='=')?('selected'):('')).'>=</option>';
			}
			
			// >> Live Price ?>
			
                                  <?php if ($product_option_value['price_prefix'] == '-') { ?>
                                  <option value="-" selected="selected">-</option>
                                  <?php } else { ?>
                                  <option value="-">-</option>
                                  <?php } ?>
                                </select>
                                <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price]" value="<?php echo $product_option_value['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                              <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points_prefix]" class="form-control">
                                  <?php if ($product_option_value['points_prefix'] == '+') { ?>
                                  <option value="+" selected="selected">+</option>
                                  <?php } else { ?>
                                  <option value="+">+</option>
                                  <?php } ?>
                                  <?php if ($product_option_value['points_prefix'] == '-') { ?>
                                  <option value="-" selected="selected">-</option>
                                  <?php } else { ?>
                                  <option value="-">-</option>
                                  <?php } ?>
                                </select>
                                <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points]" value="<?php echo $product_option_value['points']; ?>" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>
                              <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight_prefix]" class="form-control">
                                  <?php if ($product_option_value['weight_prefix'] == '+') { ?>
                                  <option value="+" selected="selected">+</option>
                                  <?php } else { ?>
                                  <option value="+">+</option>
                                  <?php } ?>
                                  <?php if ($product_option_value['weight_prefix'] == '-') { ?>
                                  <option value="-" selected="selected">-</option>
                                  <?php } else { ?>
                                  <option value="-">-</option>
                                  <?php } ?>
                                </select>
                                <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight]" value="<?php echo $product_option_value['weight']; ?>" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>

			  	<?php if($product_option['type'] == 'radio') { ?>
				<td>
                  <div class="ciopimageclr-container"><a href="" id="thumb-ciopimageclr-<?php echo $option_row; ?>-<?php echo $option_value_row; ?>" data-toggle="image" data-placement="bottom" class="img-thumbnail"><img src="<?php echo $product_option_value['ciopimageclr_thumb']; ?>"  alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][ciopimageclr]" value="<?php echo $product_option_value['ciopimageclr']; ?>" id="input-ciopimageclr-<?php echo $option_row; ?>-<?php echo $option_value_row; ?>" /></div>
                  <div class="input-group colorpicker"><input class="form-control" type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][ciopcolor]" value="<?php echo $product_option_value['ciopcolor']; ?>" id="input-ciopcolor-<?php echo $option_row; ?>-<?php echo $option_value_row; ?>" /><span class="input-group-addon"><i></i></span></div>
                </td>
              	<?php } ?>
			
                              <td class="text-left"><button type="button" onclick="$(this).tooltip('destroy');$('#option-value-row<?php echo $option_value_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                            </tr>
                            <?php $option_value_row++; ?>
                            <?php } ?>
                          </tbody>
                          <tfoot>
                            <tr>
                              <td colspan="6"></td>
                              <td class="text-left"><button type="button" onclick="addOptionValue('<?php echo $option_row; ?>','<?php echo $product_option['type']; ?>');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                      <select id="option-values<?php echo $option_row; ?>" style="display: none;">
                        <?php if (isset($option_values[$product_option['option_id']])) { ?>
                        <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                        <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                        <?php } ?>
                        <?php } ?>
                      </select>
                      <?php } ?>
                    </div>
                    <?php $option_row++; ?>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-recurring">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_recurring; ?></td>
                      <td class="text-left"><?php echo $entry_customer_group; ?></td>
                      <td class="text-left"></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $recurring_row = 0; ?>
                    <?php foreach ($product_recurrings as $product_recurring) { ?>

                    <tr id="recurring-row<?php echo $recurring_row; ?>">
                      <td class="text-left"><select name="product_recurring[<?php echo $recurring_row; ?>][recurring_id]" class="form-control">
                          <?php foreach ($recurrings as $recurring) { ?>
                          <?php if ($recurring['recurring_id'] == $product_recurring['recurring_id']) { ?>
                          <option value="<?php echo $recurring['recurring_id']; ?>" selected="selected"><?php echo $recurring['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                      <td class="text-left"><select name="product_recurring[<?php echo $recurring_row; ?>][customer_group_id]" class="form-control">
                          <?php foreach ($customer_groups as $customer_group) { ?>
                          <?php if ($customer_group['customer_group_id'] == $product_recurring['customer_group_id']) { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                      <td class="text-left"><button type="button" onclick="$('#recurring-row<?php echo $recurring_row; ?>').remove()" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $recurring_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="addRecurring()" data-toggle="tooltip" title="<?php echo $button_recurring_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>

        <!-- Related Options PRO / Связанные опции PRO << -->
				<?php if ($ro_installed) { ?>
					<div class="tab-pane" id="tab-related_options">
						
						
						<ul class="nav nav-tabs" id="ro_nav_tabs">
							<li>
								<button type="button" id='ro_add_tab_button' onclick="ro_add_tab();" data-toggle="tooltip" class="btn"><i class="fa fa-plus-circle"></i></button>
							</li>
						</ul>
			
						<div class="tab-content" id="ro_content">
						
							<input type="hidden" name="ro_data_included" value="1">
							
						</div>
						
						<span class="help-block" style="margin-top: 30px;">
							<?php echo $entry_ro_version.": ".$ro_version; ?> | <?php echo $text_ro_support; ?> | <?php echo $text_extension_page; ?>
						</span>
				
					</div>
			
				<?php } ?>

        <!-- >> Related Options PRO / Связанные опции PRO -->
      
      
            <div class="tab-pane" id="tab-discount">
              <div class="table-responsive">
                <table id="discount" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_customer_group; ?></td>
                      <td class="text-right"><?php echo $entry_quantity; ?></td>
                      <td class="text-right"><?php echo $entry_priority; ?></td>
                      <td class="text-right"><?php echo $entry_price; ?></td>
                      <td class="text-left"><?php echo $entry_date_start; ?></td>
                      <td class="text-left"><?php echo $entry_date_end; ?></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $discount_row = 0; ?>
                    <?php foreach ($product_discounts as $product_discount) { ?>
                    <tr id="discount-row<?php echo $discount_row; ?>">
                      <td class="text-left"><select name="product_discount[<?php echo $discount_row; ?>][customer_group_id]" class="form-control">
                          <?php foreach ($customer_groups as $customer_group) { ?>
                          <?php if ($customer_group['customer_group_id'] == $product_discount['customer_group_id']) { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                      <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][quantity]" value="<?php echo $product_discount['quantity']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>
                      <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][priority]" value="<?php echo $product_discount['priority']; ?>" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>
                      <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][price]" value="<?php echo $product_discount['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                      <td class="text-left" style="width: 20%;"><div class="input-group date">
                          <input type="text" name="product_discount[<?php echo $discount_row; ?>][date_start]" value="<?php echo $product_discount['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                      <td class="text-left" style="width: 20%;"><div class="input-group date">
                          <input type="text" name="product_discount[<?php echo $discount_row; ?>][date_end]" value="<?php echo $product_discount['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                      <td class="text-left"><button type="button" onclick="$('#discount-row<?php echo $discount_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $discount_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="6"></td>
                      <td class="text-left"><button type="button" onclick="addDiscount();" data-toggle="tooltip" title="<?php echo $button_discount_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-special">
              <div class="table-responsive">
                <table id="special" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_customer_group; ?></td>
                      <td class="text-right"><?php echo $entry_priority; ?></td>
                      <td class="text-right"><?php echo $entry_price; ?></td>
                      <td class="text-left"><?php echo $entry_date_start; ?></td>
                      <td class="text-left"><?php echo $entry_date_end; ?></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $special_row = 0; ?>
                    <?php foreach ($product_specials as $product_special) { ?>
                    <tr id="special-row<?php echo $special_row; ?>">
                      <td class="text-left"><select name="product_special[<?php echo $special_row; ?>][customer_group_id]" class="form-control">
                          <?php foreach ($customer_groups as $customer_group) { ?>
                          <?php if ($customer_group['customer_group_id'] == $product_special['customer_group_id']) { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                      <td class="text-right"><input type="text" name="product_special[<?php echo $special_row; ?>][priority]" value="<?php echo $product_special['priority']; ?>" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>
                      <td class="text-right"><input type="text" name="product_special[<?php echo $special_row; ?>][price]" value="<?php echo $product_special['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                      <td class="text-left" style="width: 20%;"><div class="input-group date">
                          <input type="text" name="product_special[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                      <td class="text-left" style="width: 20%;"><div class="input-group date">
                          <input type="text" name="product_special[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                      <td class="text-left"><button type="button" onclick="$('#special-row<?php echo $special_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $special_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="5"></td>
                      <td class="text-left"><button type="button" onclick="addSpecial();" data-toggle="tooltip" title="<?php echo $button_special_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-image">
              <div class="table-responsive">
                <table id="images" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_image; ?></td>
                      <td class="text-right"><?php echo $entry_sort_order; ?></td>
                      <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $image_row = 0; ?>
                    <?php foreach ($product_images as $product_image) { ?>
                    <tr id="image-row<?php echo $image_row; ?>">
                      <td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $product_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="input-image<?php echo $image_row; ?>" /></td>
                      <td class="text-right"><input type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                      <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $image_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="2"></td>
                      <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-reward">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-points"><span data-toggle="tooltip" title="<?php echo $help_points; ?>"><?php echo $entry_points; ?></span></label>
                <div class="col-sm-10">
                  <input type="text" name="points" value="<?php echo $points; ?>" placeholder="<?php echo $entry_points; ?>" id="input-points" class="form-control" />
                </div>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_customer_group; ?></td>
                      <td class="text-right"><?php echo $entry_reward; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($customer_groups as $customer_group) { ?>
                    <tr>
                      <td class="text-left"><?php echo $customer_group['name']; ?></td>
                      <td class="text-right"><input type="text" name="product_reward[<?php echo $customer_group['customer_group_id']; ?>][points]" value="<?php echo isset($product_reward[$customer_group['customer_group_id']]) ? $product_reward[$customer_group['customer_group_id']]['points'] : ''; ?>" class="form-control" /></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="tab-pane" id="tab-design">
              <div class="table-responsive">
                <table class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left"><?php echo $entry_store; ?></td>
                      <td class="text-left"><?php echo $entry_layout; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-left"><?php echo $text_default; ?></td>
                      <td class="text-left"><select name="product_layout[0]" class="form-control">
                          <option value=""></option>
                          <?php foreach ($layouts as $layout) { ?>
                          <?php if (isset($product_layout[0]) && $product_layout[0] == $layout['layout_id']) { ?>
                          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                    </tr>
                    <?php foreach ($stores as $store) { ?>
                    <tr>
                      <td class="text-left"><?php echo $store['name']; ?></td>
                      <td class="text-left"><select name="product_layout[<?php echo $store['store_id']; ?>]" class="form-control">
                          <option value=""></option>
                          <?php foreach ($layouts as $layout) { ?>
                          <?php if (isset($product_layout[$store['store_id']]) && $product_layout[$store['store_id']] == $layout['layout_id']) { ?>
                          <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                          <?php } else { ?>
                          <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#input-description<?php echo $language['language_id']; ?>').summernote({height: 300});
<?php } ?>
//--></script>
  <script type="text/javascript"><!--
// Manufacturer
$('input[name=\'manufacturer\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				json.unshift({
					manufacturer_id: 0,
					name: '<?php echo $text_none; ?>'
				});

				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['manufacturer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'manufacturer\']').val(item['label']);
		$('input[name=\'manufacturer_id\']').val(item['value']);
	}
});

// Category
$('input[name=\'category\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'category\']').val('');

		$('#product-category' + item['value']).remove();

		$('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_category[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-category').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Filter
$('input[name=\'filter\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['filter_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter\']').val('');

		$('#product-filter' + item['value']).remove();

		$('#product-filter').append('<div id="product-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_filter[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-filter').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Downloads
$('input[name=\'download\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/download/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['download_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'download\']').val('');

		$('#product-download' + item['value']).remove();

		$('#product-download').append('<div id="product-download' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_download[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-download').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

// Related
$('input[name=\'related\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'related\']').val('');

		$('#product-related' + item['value']).remove();

		$('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');
	}
});

$('#product-related').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});
//--></script>
  <script type="text/javascript"><!--
var attribute_row = <?php echo $attribute_row; ?>;

function addAttribute() {
    html  = '<tr id="attribute-row' + attribute_row + '">';
	html += '  <td class="text-left" style="width: 20%;"><input type="text" name="product_attribute[' + attribute_row + '][name]" value="" placeholder="<?php echo $entry_attribute; ?>" class="form-control" /><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value="" /></td>';
	html += '  <td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span><textarea name="product_attribute[' + attribute_row + '][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"></textarea></div>';
    <?php } ?>
	html += '  </td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
    html += '</tr>';

	$('#attribute tbody').append(html);

	attributeautocomplete(attribute_row);

	attribute_row++;
}

function attributeautocomplete(attribute_row) {
	$('input[name=\'product_attribute[' + attribute_row + '][name]\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/attribute/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {
					response($.map(json, function(item) {
						return {
							category: item.attribute_group,
							label: item.name,
							value: item.attribute_id
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'product_attribute[' + attribute_row + '][name]\']').val(item['label']);
			$('input[name=\'product_attribute[' + attribute_row + '][attribute_id]\']').val(item['value']);
		}
	});
}

$('#attribute tbody tr').each(function(index, element) {
	attributeautocomplete(index);
});
//--></script>
  <script type="text/javascript"><!--
var option_row = <?php echo $option_row; ?>;

$('input[name=\'option\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/option/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						category: item['category'],
						label: item['name'],
						value: item['option_id'],
						type: item['type'],
						option_value: item['option_value']
					}
				}));
			}
		});
	},
	'select': function(item) {
		html  = '<div class="tab-pane" id="tab-option' + option_row + '">';
		html += '	<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][name]" value="' + item['label'] + '" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + item['value'] + '" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][type]" value="' + item['type'] + '" />';

		html += '	<div class="form-group">';
		html += '	  <label class="col-sm-2 control-label" for="input-required' + option_row + '"><?php echo $entry_required; ?></label>';
		html += '	  <div class="col-sm-10"><select name="product_option[' + option_row + '][required]" id="input-required' + option_row + '" class="form-control">';
		html += '	      <option value="1"><?php echo $text_yes; ?></option>';
		html += '	      <option value="0"><?php echo $text_no; ?></option>';
		html += '	  </select></div>';
		html += '	</div>';

		if (item['type'] == 'text') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
			html += '	</div>';
		}

		if (item['type'] == 'textarea') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><textarea name="product_option[' + option_row + '][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control"></textarea></div>';
			html += '	</div>';
		}

		if (item['type'] == 'file') {
			html += '	<div class="form-group" style="display: none;">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
			html += '	</div>';
		}

		if (item['type'] == 'date') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-3"><div class="input-group date"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
			html += '	</div>';
		}

		if (item['type'] == 'time') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><div class="input-group time"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
			html += '	</div>';
		}

		if (item['type'] == 'datetime') {
			html += '	<div class="form-group">';
			html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
			html += '	  <div class="col-sm-10"><div class="input-group datetime"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
			html += '	</div>';
		}

		if (item['type'] == 'select' || item['type'] == 'radio' || item['type'] == 'checkbox' || item['type'] == 'image') {
			html += '<div class="table-responsive">';
			html += '  <table id="option-value' + option_row + '" class="table table-striped table-bordered table-hover">';
			html += '  	 <thead>';
			html += '      <tr>';
			html += '        <td class="text-left"><?php echo $entry_option_value; ?></td>';
			html += '        <td class="text-right"><?php echo $entry_quantity; ?></td>';
			html += '        <td class="text-left"><?php echo $entry_subtract; ?></td>';
			html += '        <td class="text-right"><?php echo $entry_price; ?></td>';
			html += '        <td class="text-right"><?php echo $entry_option_points; ?></td>';
			html += '        <td class="text-right"><?php echo $entry_weight; ?></td>';

			if (item['type'] == 'radio') {
			html += '        <td class="text-right"><?php echo $text_cioptioncolor; ?></td>';
			}
			
			html += '        <td></td>';
			html += '      </tr>';
			html += '  	 </thead>';
			html += '  	 <tbody>';
			html += '    </tbody>';
			html += '    <tfoot>';
			html += '      <tr>';
			html += '        <td colspan="6"></td>';
			html += '        <td class="text-left"><button type="button" onclick="addOptionValue(' + option_row + ',\'' + item['type'] + '\');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>';
			html += '      </tr>';
			html += '    </tfoot>';
			html += '  </table>';
			html += '</div>';

            html += '  <select id="option-values' + option_row + '" style="display: none;">';

            for (i = 0; i < item['option_value'].length; i++) {
				html += '  <option value="' + item['option_value'][i]['option_value_id'] + '">' + item['option_value'][i]['name'] + '</option>';
            }

            html += '  </select>';
			html += '</div>';
		}

		$('#tab-option .tab-content').append(html);

		$('#option > li:last-child').before('<li><a href="#tab-option' + option_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#tab-option' + option_row + '\\\']\').parent().remove(); $(\'#tab-option' + option_row + '\').remove(); $(\'#option a:first\').tab(\'show\')"></i> ' + item['label'] + '</li>');

		$('#option a[href=\'#tab-option' + option_row + '\']').tab('show');

$("#tab-google .btn.active").removeClass("btn-primary").addClass("btn-success");
$(document).on("click", function(){
	$("#tab-google .btn").removeClass("btn-success").addClass("btn-primary");
	$("#tab-google .btn.active").removeClass("btn-primary").addClass("btn-success");
});

$('.expiry-date').datetimepicker({pickTime: false});
		$('.date').datetimepicker({
			pickTime: false
		});

		$('.time').datetimepicker({
			pickDate: false
		});

		$('.datetime').datetimepicker({
			pickDate: true,
			pickTime: true
		});

		option_row++;
	}
});
//--></script>
  <script type="text/javascript"><!--
var option_value_row = <?php echo $option_value_row; ?>;

function addOptionValue(option_row,product_option_type) {
	html  = '<tr id="option-value-row' + option_value_row + '">';
	html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]" class="form-control">';
	html += $('#option-values' + option_row).html();
	html += '  </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
	html += '  <td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][subtract]" class="form-control">';
	html += '    <option value="1"><?php echo $text_yes; ?></option>';
	html += '    <option value="0"><?php echo $text_no; ?></option>';
	html += '  </select></td>';
	html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price_prefix]" class="form-control">';
	html += '    <option value="+">+</option>';
	html += '    <option value="-">-</option>';
	html += '  </select>';
	html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
	html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points_prefix]" class="form-control">';
	html += '    <option value="+">+</option>';
	html += '    <option value="-">-</option>';
	html += '  </select>';
	html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points]" value="" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>';
	html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight_prefix]" class="form-control">';
	html += '    <option value="+">+</option>';
	html += '    <option value="-">-</option>';
	html += '  </select>';
	html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight]" value="" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>';

				if(typeof product_option_type != 'undefined' && product_option_type=='radio') {
				html += '<td>';
				html += '<div class="ciopimageclr-container"><a data-placement="bottom" href="" id="thumb-ciopimageclr-' + option_row + '-' + option_value_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][ciopimageclr]" value="" id="input-ciopimageclr-' + option_row + '-' + option_value_row + '" /></div><div class="input-group colorpicker"><input class="form-control" type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][ciopcolor]" value="" id="input-ciopcolor-' + option_row + '-' + option_value_row + '" /><span class="input-group-addon"><i></i></span></div>';
				html += '</td>';
				}
			
	html += '  <td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#option-value' + option_row + ' tbody').append(html);

			// << Live Price
			<?php
				if ( isset($liveprice_installed) && $liveprice_installed ) {
			?>
					var ppm_option = $('#option-value-row'+option_value_row).find('select[name*=price_prefix]').find('option[value="-"]');
					ppm_option.before('<option value="%">%</option>');
					ppm_option.before('<option value="*">*</option>');
					ppm_option.before('<option value="/">/</option>');
					ppm_option.before('<option value="=">=</option>');
			<?php
				}
			?>
			
			// >> Live Price
			

				if(typeof product_option_type != 'undefined' && product_option_type=='radio') {
					$('.colorpicker').colorpicker();
				}
			
        $('[rel=tooltip]').tooltip();

	option_value_row++;
}
//--></script>
  <script type="text/javascript"><!--
var discount_row = <?php echo $discount_row; ?>;

function addDiscount() {
	html  = '<tr id="discount-row' + discount_row + '">';
    html += '  <td class="text-left"><select name="product_discount[' + discount_row + '][customer_group_id]" class="form-control">';
    <?php foreach ($customer_groups as $customer_group) { ?>
    html += '    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo addslashes($customer_group['name']); ?></option>';
    <?php } ?>
    html += '  </select></td>';
    html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][quantity]" value="" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>';
    html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][priority]" value="" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>';
	html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
    html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_discount[' + discount_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
	html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_discount[' + discount_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#discount-row' + discount_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#discount tbody').append(html);

$("#tab-google .btn.active").removeClass("btn-primary").addClass("btn-success");
$(document).on("click", function(){
	$("#tab-google .btn").removeClass("btn-success").addClass("btn-primary");
	$("#tab-google .btn.active").removeClass("btn-primary").addClass("btn-success");
});

$('.expiry-date').datetimepicker({pickTime: false});
	$('.date').datetimepicker({
		pickTime: false
	});

	discount_row++;
}
//--></script>
  <script type="text/javascript"><!--
var special_row = <?php echo $special_row; ?>;

function addSpecial() {
	html  = '<tr id="special-row' + special_row + '">';
    html += '  <td class="text-left"><select name="product_special[' + special_row + '][customer_group_id]" class="form-control">';
    <?php foreach ($customer_groups as $customer_group) { ?>
    html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo addslashes($customer_group['name']); ?></option>';
    <?php } ?>
    html += '  </select></td>';
    html += '  <td class="text-right"><input type="text" name="product_special[' + special_row + '][priority]" value="" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>';
	html += '  <td class="text-right"><input type="text" name="product_special[' + special_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
    html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
	html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#special-row' + special_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#special tbody').append(html);

$("#tab-google .btn.active").removeClass("btn-primary").addClass("btn-success");
$(document).on("click", function(){
	$("#tab-google .btn").removeClass("btn-success").addClass("btn-primary");
	$("#tab-google .btn.active").removeClass("btn-primary").addClass("btn-success");
});

$('.expiry-date').datetimepicker({pickTime: false});
	$('.date').datetimepicker({
		pickTime: false
	});

	special_row++;
}
//--></script>
  <script type="text/javascript"><!--
// UKSB Google Shopping Variants
var variant_row = <?php echo $variant_row; ?>;

function addVariant() {
	html  = '  <tr id="variant-row' + variant_row + '">';
	html += '    <td class="col-sm-3 text-left">';
	html += '    <label class="control-label" for="variant[' + variant_row + '][g_size]"><?php echo $entry_p_size; ?></label><br>';
	html += '    <input type="text" name="variant[' + variant_row + '][g_size]" value="" placeholder="<?php echo $entry_p_size; ?>" class="form-control" /><br><br>';
	html += '    <label class="control-label" for="variant[' + variant_row + '][v_mpn]"><?php echo $entry_v_mpn; ?></label><br>';
	html += '    <input type="text" name="variant[' + variant_row + '][v_mpn]" value="" placeholder="<?php echo $entry_v_mpn; ?>" class="form-control" /><br><br>';
	html += '    <label class="control-label" for="variant[' + variant_row + '][g_material]"><?php echo $entry_p_material; ?></label><br>';
	html += '    <input type="text" name="variant[' + variant_row + '][g_material]" value="" placeholder="<?php echo $entry_p_material; ?>" class="form-control" /></td>';
	html += '    <td class="col-sm-3 text-left">';
	html += '    <label class="control-label" for="variant[' + variant_row + '][g_colour]"><?php echo $entry_p_colour; ?></label><br>';
	html += '    <input type="text" name="variant[' + variant_row + '][g_colour]" value="" placeholder="<?php echo $entry_p_colour; ?>" class="form-control" /><br><br>';
	html += '    <label class="control-label" for="variant[' + variant_row + '][v_gtin]"><?php echo $entry_v_gtin; ?></label><br>';
	html += '    <input type="text" name="variant[' + variant_row + '][v_gtin]" value="" placeholder="<?php echo $entry_v_gtin; ?>" class="form-control" /><br><br>';
	html += '    <label class="control-label" for="variant[' + variant_row + '][g_pattern]"><?php echo $entry_p_pattern; ?></label><br>';
	html += '    <input type="text" name="variant[' + variant_row + '][g_pattern]" value="" placeholder="<?php echo $entry_p_pattern; ?>" class="form-control" /></td>';
	html += '    <td class="col-sm-3 text-left">';
	html += '    <label class="control-label" for="variant[' + variant_row + '][v_prices]"><?php echo $entry_v_prices; ?></label><br><br><?php echo $help_v_prices; ?><br><br>';
	html += '    <input type="text" name="variant[' + variant_row + '][v_prices]" value="" placeholder="<?php echo $entry_v_prices; ?>" class="form-control" /></td>';
	html += '    <td class="col-sm-2 text-left"><a href="" id="thumb-image' + variant_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="variant[' + variant_row + '][v_images]" value="" id="v_images' + variant_row + '" /></td>';
	html += '    <td class="col-sm-1 text-left"><button type="button" onclick="$(\'#variant-row' + variant_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '  </tr>';
	
	$('#variants tbody').append(html);
	
	variant_row++;
}


var image_row = <?php echo $image_row; ?>;

function addImage() {
	html  = '<tr id="image-row' + image_row + '">';
	html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
	html += '  <td class="text-right"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';

	$('#images tbody').append(html);

	image_row++;
}
//--></script>
  <script type="text/javascript"><!--
var recurring_row = <?php echo $recurring_row; ?>;

function addRecurring() {
	recurring_row++;

	html  = '';
	html += '<tr id="recurring-row' + recurring_row + '">';
	html += '  <td class="left">';
	html += '    <select name="product_recurring[' + recurring_row + '][recurring_id]" class="form-control">>';
	<?php foreach ($recurrings as $recurring) { ?>
	html += '      <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>';
	<?php } ?>
	html += '    </select>';
	html += '  </td>';
	html += '  <td class="left">';
	html += '    <select name="product_recurring[' + recurring_row + '][customer_group_id]" class="form-control">>';
	<?php foreach ($customer_groups as $customer_group) { ?>
	html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>';
	<?php } ?>
	html += '    <select>';
	html += '  </td>';
	html += '  <td class="left">';
	html += '    <a onclick="$(\'#recurring-row' + recurring_row + '\').remove()" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
	html += '  </td>';
	html += '</tr>';

	$('#tab-recurring table tbody').append(html);
}
//--></script>
  <script type="text/javascript"><!--
$("#tab-google .btn.active").removeClass("btn-primary").addClass("btn-success");
$(document).on("click", function(){
	$("#tab-google .btn").removeClass("btn-success").addClass("btn-primary");
	$("#tab-google .btn.active").removeClass("btn-primary").addClass("btn-success");
});

$('.expiry-date').datetimepicker({pickTime: false});
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
$('#option a:first').tab('show');
//--></script></div>

				<script type="text/javascript"><!--
				$(function() { $('.colorpicker').colorpicker(); });
				//--></script>
			

      
<!-- Related Options PRO / Связанные опции PRO << -->

<script type="text/javascript"><!--

var ro_counter = 0;
var ro_discount_counter = 0;
var ro_special_counter = 0;
var ro_variants = [];
//var ro_variants_options_order = [];
var ro_all_options = <?php echo json_encode($ro_all_options); ?>;
var ro_settings = <?php echo json_encode($ro_settings); ?>;
var ro_variants = <?php echo json_encode($variants_options); ?>;
var ro_data = <?php echo json_encode($ro_data); ?>;
//ro_variants_options_order[0] = [];



var ro_tabs_cnt = 0;

function ro_tab_name_change(ro_tabs_num) {
	
	if ( $('#ro-use-'+ro_tabs_num+'').is(':checked') ) {
		var new_tab_name = $('#rov-'+ro_tabs_num+' option[value="'+$('#rov-'+ro_tabs_num).val()+'"]').html();
	} else {
		var new_tab_name = '<?php echo addslashes($related_options_title); ?>';
	}
	
	$('#ro_nav_tabs a[data-ro-cnt="'+ro_tabs_num+'"]').html(new_tab_name);
	
}

function ro_add_tab(tab_data_param) {

	var tab_data = tab_data_param ? tab_data_param : false;
	
	html = '<li><a href="#tab-ro-'+ro_tabs_cnt+'" data-toggle="tab" data-ro-cnt="'+ro_tabs_cnt+'">ro '+ro_tabs_cnt+'</a></li>';
	//$('#ro_nav_tabs').append(html);
	$('#ro_add_tab_button').closest('li').before(html);
	
	html = '<div class="tab-pane" id="tab-ro-'+ro_tabs_cnt+'" data-ro-cnt="'+ro_tabs_cnt+'">'+ro_tabs_cnt+'</div>';
	$('#ro_content').append(html);
	
	$('#ro_nav_tabs [data-ro-cnt='+ro_tabs_cnt+']').click();
	
	html = '';
	html+= '<input type="hidden" name="ro_data['+ro_tabs_cnt+'][rovp_id]" value="'+(tab_data['rovp_id'] ? tab_data['rovp_id'] : '0')+'">';
	html+= '<div class="form-group">';
	
	html+= '<label class="col-sm-2 control-label"><?php echo addslashes($entry_ro_use); ?></label>';
	
	html+= '<div class="col-sm-10">';
	html+= '<label class="radio-inline">';
		html+= '<input type="radio" name="ro_data['+ro_tabs_cnt+'][use]" id="ro-use-'+ro_tabs_cnt+'" value="1" '+((tab_data['use'])?('checked'):(''))+' onchange="ro_use_check('+ro_tabs_cnt+')" />';
		html+= ' <?php echo $text_yes; ?>';
	html+= '</label>';
	html+= '<label class="radio-inline">';
		html+= '<input type="radio" name="ro_data['+ro_tabs_cnt+'][use]" value="" '+((tab_data['use'])?(''):('checked'))+' onchange="ro_use_check('+ro_tabs_cnt+')" />';
		html+= ' <?php echo $text_no; ?>';
	html+= '</label>';
	html+= '</div>';
	
	//html+= '<input type="checkbox" id="ro-use-'+ro_tabs_cnt+'" name="ro_data['+ro_tabs_cnt+'][use]" onchange="ro_use_check('+ro_tabs_cnt+')" value="1" '+((tab_data['use'])?('checked'):(''))+'>';
	
	//html+= '<label class="col-sm-2 control-label"><input type="checkbox" id="ro-use-'+ro_tabs_cnt+'" name="ro_data['+ro_tabs_cnt+'][use]" onchange="ro_use_check('+ro_tabs_cnt+')" value="1" '+((tab_data['use'])?('checked'):(''))+'>&nbsp;<?php echo addslashes($entry_ro_use); ?></label><br><br>';
	
	html+= '</div>';
	
	html+= '<div id="ro-use-data-'+ro_tabs_cnt+'">';
	html+= '	<div class="form-group">';
	html+= '		<label class="col-sm-2 control-label" for="rov-'+ro_tabs_cnt+'" ><?php echo $entry_ro_variant; ?></label>';
	html+= '		<div class="col-sm-3" >';
	html+= '			<select name="ro_data['+ro_tabs_cnt+'][rov_id]" id="rov-'+ro_tabs_cnt+'" class="form-control" onChange="ro_tab_name_change('+ro_tabs_cnt+');">';
	
	if (ro_settings['ro_use_variants']) {
		for (var i in ro_variants) {
			if (i == 0) {
				html+= '				<option value="0"><?php echo addslashes($text_ro_all_options); ?></option>';
			} else {
				var ro_variant = ro_variants[i];
				html+= '			<option value="'+ro_variant['rov_id']+'" '+(tab_data['rov_id'] && tab_data['rov_id'] == ro_variant['rov_id'] ? 'selected':'')+' >'+ro_variant['name']+'</option>';
			}
		}	
	} else {
		html+= '				<option value="0"><?php echo addslashes($text_ro_all_options); ?></option>';
	}
	
	html+= '			</select>';
	html+= '		</div>';
	html+= '		<button type="button" onclick="ro_fill_all_combinations('+ro_tabs_cnt+');" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="<?php echo addslashes($entry_add_all_variants); ?>"><?php echo addslashes($entry_add_all_variants); ?></button>';
	html+= '		<button type="button" onclick="ro_fill_all_combinations('+ro_tabs_cnt+',1);" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="<?php echo addslashes($entry_add_product_variants); ?>"><?php echo addslashes($entry_add_product_variants); ?></button>';
	html+= '	</div>';
	
	
	
	html+= '	<div class="table-responsive">';
	html+= '		<table class="table table-striped table-bordered table-hover">';
	html+= '			<thead>';
	html+= '				<tr>';
	html+= '					<td class="text-left"><?php echo addslashes($entry_options_values); ?></td>';
	html+= '					<td class="text-left" width="90"><?php echo addslashes($entry_related_options_quantity); ?>:</td>';
			
	var ro_fields = {spec_model: "<?php echo $entry_model; ?>"
									,spec_sku: "<?php echo $entry_sku; ?>"
									,spec_upc: "<?php echo $entry_upc; ?>"
									,spec_ean: "<?php echo $entry_ean; ?>"
									,spec_location: "<?php echo $entry_location; ?>"
									,spec_ofs: "<?php echo $entry_stock_status; ?>"
									,spec_weight: "<?php echo $entry_weight; ?>"
									};
								
	for (var i in ro_fields) {
		if (ro_settings[i] && ro_settings[i] != 0) {
			html+= '<td class="text-left" width="90">'+ro_fields[i]+'</td>';
		}
	}
			
	if (ro_settings['spec_price'] ) {
		html+= '				<td class="text-left" width="90" ><?php echo addslashes($entry_price); ?></td>';
		if (ro_settings['spec_price_discount'] ) {
			html+= '					<td class="text-left" style="90"><?php echo addslashes($tab_discount); ?>: <font style="font-weight:normal;font-size:80%;">(<?php echo addslashes(str_replace(":","",$entry_customer_group." | ".$entry_quantity." | ".$entry_price)); ?>)</font></td>';
		}
		if (ro_settings['spec_price_special'] ) {
			html+= '					<td class="text-left" style="90"><?php echo addslashes($tab_special); ?>: <font style="font-weight:normal;font-size:80%;">(<?php echo addslashes(str_replace(":","",$entry_customer_group." | ".$entry_price)); ?>)</font></td>';
		}
	}
				
	if (ro_settings['select_first'] && ro_settings['select_first'] == 1 ) {
		html+= '				<td class="text-left" width="90" style="white-space:nowrap"><?php echo addslashes($entry_select_first_short); ?>:</td>';
	}
	
					
	html+= '					<td class="text-left" width="90"></td>';
	
	html+= '				<tr>';			
	html+= '		</thead>';
	html+= '		<tbody id="tbody-ro-'+ro_tabs_cnt+'"></tbody>';
	html+= '	</table>';
	
	html+= '	<div class="col-sm-2" >';
	html+= '		<button type="button" onclick="ro_add_combination('+ro_tabs_cnt+', false);" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="<?php echo addslashes($entry_add_related_options); ?>"><?php echo addslashes($entry_add_related_options); ?></button>';
	html+= '	</div>';
			
	html+= '</div>';
	
	html+= '';
	html+= '';
	html+= '</div>';
	
	$('#tab-ro-'+ro_tabs_cnt+'').html(html);
	$('#ro-use-'+ro_tabs_cnt).prop('checked', true);
	ro_use_check(ro_tabs_cnt);
	
	if (tab_data['ro']) {
		for (var i in tab_data['ro']) {
			ro_add_combination(ro_tabs_cnt, tab_data['ro'][i]);
		}
	}
	
	// выбор добавленной закладки 
	$('#ro_nav_tabs a[data-ro-cnt="'+ro_tabs_cnt+'"]').click();
	
	ro_tabs_cnt++;
	
	return ro_tabs_cnt-1;
	
}

function ro_use_check(ro_tabs_num) {
	
	$('#ro-use-data-'+ro_tabs_num).toggle( $('input[type=radio][name="ro_data['+ro_tabs_num+'][use]"][value="1"]').is(':checked') );
	ro_tab_name_change(ro_tabs_num);
	
}

function ro_add_combination(ro_tabs_num, params) {

	var rov_id = $('#rov-'+ro_tabs_num).val();
	var ro_variant = ro_variants[ rov_id ];

	var entry_add_discount = "<?php echo addslashes($entry_add_discount); ?>";
	var entry_del_discount_title = "<?php echo addslashes($entry_del_discount_title); ?>";
	
	var entry_add_special = "<?php echo addslashes($entry_add_special); ?>";
	var entry_del_special_title = "<?php echo addslashes($entry_del_special_title); ?>";
	
	
	str_add = '';
	str_add += "<tr id=\"related-option"+ro_counter+"\"><td>";
	
	var div_id = "ro_status"+ro_counter;
	str_add +="<div id='"+div_id+"' style='color: red'></div>";
	
	for (var i in ro_variant['options']) {
		
		var ro_option = ro_variant['options'][i];
		var option_id = ro_option['option_id'];
	
		str_add += "<div style='float:left;'><label class='col-sm-1 control-label' for='ro_o_"+ro_counter+"_"+option_id+"'> ";
		str_add += "<nobr>"+ro_option['name']+":</nobr>";
		str_add += "</label>";
		str_add += "<select class='form-control' id='ro_o_"+ro_counter+"_"+option_id+"' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][options]["+option_id+"]' onChange=\"ro_refresh_status("+ro_tabs_num+","+ro_counter+")\">";
		str_add += "<option value=0></option>";
					
			for (var j in ro_all_options[option_id]['values']) {
				if((ro_all_options[option_id]['values'][j] instanceof Function) ) { continue; }
				
				var option_value_id = ro_all_options[option_id]['values'][j]['option_value_id'];
				
				str_add += "<option value='"+option_value_id+"'";
				if (params['options'] && params['options'][option_id] && params['options'][option_id] == option_value_id) str_add +=" selected ";
				str_add += ">"+ro_all_options[option_id]['values'][j]['name']+"</option>";
			}

		str_add += "</select>";
		str_add += "</div>";
	}
	
  
  str_add += "</td>";
  str_add += "<td>&nbsp;";
	str_add += "<input type='text' class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][quantity]' size='2' value='"+(params['quantity']||0)+"'>";
  str_add += "<input type='hidden' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][relatedoptions_id]' value='"+(params['relatedoptions_id']||"")+"'>";
  str_add += "</td>";
	
	str_add += ro_add_text_field(ro_tabs_num, ro_counter, 'spec_model', params, 'model');
	str_add += ro_add_text_field(ro_tabs_num, ro_counter, 'spec_sku', params, 'sku');
	str_add += ro_add_text_field(ro_tabs_num, ro_counter, 'spec_upc', params, 'upc');
	str_add += ro_add_text_field(ro_tabs_num, ro_counter, 'spec_ean', params, 'ean');
	str_add += ro_add_text_field(ro_tabs_num, ro_counter, 'spec_location', params, 'location');
	
	if (ro_settings['spec_ofs']) {
		
		str_add += '<td>';
		str_add += '&nbsp;<select name="ro_data['+ro_tabs_num+'][ro]['+ro_counter+'][stock_status_id]" class="form-control">';
		str_add += '<option value="0">-</option>';
		<?php foreach ($stock_statuses as $stock_status) { ?>
			str_add += '<option value="<?php echo $stock_status['stock_status_id']; ?>"';
			if ("<?php echo $stock_status['stock_status_id'] ?>" == params['stock_status_id']) {
				str_add += ' selected ';
			}
			str_add += '><?php echo addslashes($stock_status['name']); ?></option>';
		<?php } ?>
		str_add += '</select>';
		
		str_add += '</td>';
	
	}
	
	if (ro_settings['spec_weight'])	{
		str_add += "<td>&nbsp;";
		str_add += "<select class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][weight_prefix]'>";
		str_add += "<option value='=' "+( (params['weight_prefix'] && params['weight_prefix']=='=')? ("selected") : (""))+">=</option>";
		str_add += "<option value='+' "+( (params['weight_prefix'] && params['weight_prefix']=='+')? ("selected") : (""))+">+</option>";
		str_add += "<option value='-' "+( (params['weight_prefix'] && params['weight_prefix']=='-')? ("selected") : (""))+">-</option>";
		str_add += "</select>";
		str_add += "<input type='text' class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][weight]' value=\""+(params['weight']||'0.000')+"\" size='5'>";
		str_add += "</td>";
	//} else {
	//	str_add += "<input type='hidden' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][weight_prefix]' value=\""+(params['weight_prefix']||'')+"\">";
	//	str_add += "<input type='hidden' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][weight]' value=\""+(params['weight']||'0.000')+"\">";
	}
	
	if (ro_settings['spec_price'])	{
		str_add += "<td>&nbsp;";
		if (ro_settings['spec_price_prefix']) {
			str_add += "<select name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][price_prefix]' class='form-control'>";
			var price_prefixes = ['=', '+', '-'];
			for (var i in price_prefixes) {
				if((price_prefixes[i] instanceof Function) ) { continue; }
				var price_prefix = price_prefixes[i];
				str_add += "<option value='"+price_prefix+"' "+(price_prefix==params['price_prefix']?"selected":"")+">"+price_prefix+"</option>";
			}
			str_add += "</select>";
		}
		str_add += "<input type='text' class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][price]' value='"+(params['price']||'')+"' size='10'>";
		str_add += "</td>";
	//} else {
	//	str_add += "<input type='hidden' name='relatedoptions["+ro_counter+"][price]' value='"+(params['price']||'')+"'>";
	}
	
	
	if (ro_settings['spec_price'] && ro_settings['spec_price_discount'])	{
		str_add += "<td>";
	//} else {
	//	str_add += "<div style='display:none;'>";
	//}
	
		str_add += "<button type='button' onclick=\"ro_add_discount("+ro_tabs_num+", "+ro_counter+", '');\" data-toggle='tooltip' title='"+entry_add_discount+"' class='btn btn-primary'><i class='fa fa-plus-circle'></i></button>";
		str_add += "<div id='ro_price_discount"+ro_counter+"' >";
		str_add += "</div>";
		//if (ro_settings['spec_price'] && ro_settings['spec_price_discount'])	{
			str_add += "</td>";	
		//} else {
		//	str_add += "</div>";
		//}
	}
	
	if (ro_settings['spec_price'] && ro_settings['spec_price_special'])	{
		str_add += "<td>";
	//} else {
	//	str_add += "<div style='display:none;'>";
	//}
	str_add += "<button type='button' onclick=\"ro_add_special("+ro_tabs_num+", "+ro_counter+", '');\" data-toggle='tooltip' title='"+entry_add_special+"' class='btn btn-primary'><i class='fa fa-plus-circle'></i></button>";
	str_add += "<div id='ro_price_special"+ro_counter+"'>";
	str_add += "</div>";
	//if (ro_settings['spec_price'] && ro_settings['spec_price_special'])	{
		str_add += "</td>";	
	//} else {
	//	str_add += "</div>";
	}
	
	if (ro_settings['select_first'] && ro_settings['select_first']==1) {
		str_add += "<td>&nbsp;";
		
		str_add += "<input id='defaultselect_"+ro_counter+"' type='checkbox' onchange='ro_check_defaultselectpriority("+ro_tabs_num+");' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][defaultselect]' "+((params && params['defaultselect']==1)?("checked"):(""))+" value='1'>";
		str_add += "<input id='defaultselectpriority_"+ro_counter+"' type='text' class='form-control' title='<?php echo $entry_select_first_priority; ?>' name='ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][defaultselectpriority]'  value=\""+((params && params['defaultselectpriority'])?(params['defaultselectpriority']):(""))+"\" >";
		str_add += "</td>";	
	}

	str_add += "<td><br>";
	str_add += "<button type=\"button\" class='btn btn-danger' onclick=\"$('#related-option" + ro_counter + "').remove();ro_refresh_status("+ro_tabs_num+");\" data-toggle=\"tooltip\" title=\"<?php echo $button_remove; ?>\" class='btn btn-primary' data-original-title=\"<?php echo $button_remove; ?>\" ><i class=\"fa fa-minus-circle\"></i></button>";
	
  str_add += "</td></tr>";
  
  $('#tbody-ro-'+ro_tabs_num).append(str_add);
	
	if (ro_settings['spec_price'] && ro_settings['spec_price_discount'])	{
		if (params && params['discounts'] ) {
			for (var i_dscnt in params['discounts']) {
				if ( ! params['discounts'].hasOwnProperty(i_dscnt) ) continue;
				ro_add_discount(ro_tabs_num, ro_counter, params['discounts'][i_dscnt]);
			}
		}
	}
	
	if (ro_settings['spec_price'] && ro_settings['spec_price_special'])	{
		if (params && params['specials'] ) {
			for (var i_dscnt in params['specials']) {
				if ( ! params['specials'].hasOwnProperty(i_dscnt) ) continue;
				ro_add_special(ro_tabs_num, ro_counter, params['specials'][i_dscnt]);
			}
		}
	}
	
	ro_update_combination(ro_tabs_num,ro_counter);
	
	if (params==false) {
		ro_refresh_status(ro_tabs_num);
		ro_check_defaultselectpriority(ro_tabs_num);
	}
	
  ro_counter++;
  
}

function ro_refresh_status(ro_tabs_num, ro_num) {
  
	if (ro_num || ro_num==0) {
		ro_update_combination(ro_tabs_num, ro_num);
	}
	
	var rov_id = $('#rov-'+ro_tabs_num).val();
	var ro_variant = ro_variants[ rov_id ];
	
	$('#tab-ro-'+ro_tabs_num+' div[id^=ro_status]').html('');
	
	var opts_combs = [];
	var checked_opts_combs = [];
	$('#tab-ro-'+ro_tabs_num+' tr[id^=related-option]').each( function () {
		var opts_comb = $(this).attr('ro_opts_comb');
		
		if ( $.inArray(opts_comb, opts_combs) != -1 && $.inArray(opts_comb, checked_opts_combs)==-1 ) {
			$('#tab-ro-'+ro_tabs_num+' tr[ro_opts_comb='+opts_comb+']').each( function () {
				$(this).find('div[id^=ro_status]').html('<?php echo $warning_equal_options; ?>');
			});
			checked_opts_combs.push(opts_comb);
		} else {
			opts_combs.push(opts_comb);
		}
	})
	
	return;
	
}

function ro_update_combination(ro_tabs_num, ro_num) {
	
	var rov_id = $('#rov-'+ro_tabs_num).val();
	var ro_variant = ro_variants[ rov_id ];
	var str_opts = "";
	
	for (var i in ro_variant['options']) {
		
		if((ro_variant['options'][i] instanceof Function) ) { continue; }
		
		var option_id = ro_variant['options'][i]['option_id'];
	
		str_opts += "_о"+option_id;
		str_opts += "_"+$('#ro_o_'+ro_num+'_'+option_id).val();
	}
	$('#related-option'+ro_num).attr('ro_opts_comb', str_opts);
	
}

function ro_add_text_field(ro_tabs_num, ro_num, setting_name, params, field_name) {
	str_add = "";
	if (ro_settings[setting_name] && ro_settings[setting_name]!='0')	{
		str_add += "<td>&nbsp;";
		str_add += "<input type='text' class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_num+"]["+field_name+"]' value=\""+(params[field_name]||'')+"\">";
		str_add += "</td>";
	//} else {
	//	str_add += "<input type='hidden' class='form-control' name='ro_data["+ro_tabs_num+"][ro]["+ro_num+"]["+field_name+"]' value=\""+(params[field_name]||'')+"\">";
	}
	return str_add;
}

function ro_add_discount(ro_tabs_num, ro_counter, discount) {
	
	var first_name = "ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][discounts]["+ro_discount_counter+"]";
	var customer_group_id = (discount=="")?(0):(discount['customer_group_id']);
	
	str_add = "";
	str_add += "<table id='related-option-discount"+ro_discount_counter+"' style='width:242px;'><tr><td>";
	
	str_add += "<select name='"+first_name+"[customer_group_id]' class='form-control' title=\"<?php echo htmlspecialchars($entry_customer_group); ?>\" style='float:left;width:80px;'>";
	<?php foreach ($customer_groups as $customer_group) { ?>
	str_add += "<option value='<?php echo $customer_group['customer_group_id']; ?>' "+((customer_group_id==<?php echo $customer_group['customer_group_id']; ?>)?("selected"):(""))+"><?php echo $customer_group['name']; ?></option>";
	<?php } ?>
	str_add += "</select>";
	
	str_add += "<input type='text' class='form-control' style='float:left;width:42px;' size='2' name='"+first_name+"[quantity]' value='"+((discount=="")?(""):(discount['quantity']))+"' title=\"<?php echo htmlspecialchars($entry_quantity); ?>\">";
	str_add += "";
	
	// hidden
	str_add += "<input type='hidden' name='"+first_name+"[priority]' value='"+((discount=="")?(""):(discount['priority']))+"' title=\"<?php echo htmlspecialchars($entry_priority); ?>\">";
	
	str_add += "<input type='text' class='form-control' style='float:left;width:80px;' size='10' name='"+first_name+"[price]' value='"+((discount=="")?(""):(discount['price']))+"' title=\"<?php echo htmlspecialchars($entry_price); ?>\">";
	
	str_add += "<button type=\"button\" onclick=\"$('#related-option-discount" + ro_discount_counter + "').remove();\" data-toggle=\"tooltip\" title=\"<?php echo $button_remove; ?>\" class=\"btn btn-danger\" style='float:left;' data-original-title=\"\"><i class=\"fa fa-minus-circle\"></i></button>";

	str_add += "</td></tr></table>";
	
	$('#ro_price_discount'+ro_counter).append(str_add);
	
	ro_discount_counter++;
	
}

function ro_add_special(ro_tabs_num, ro_counter, special) {
	
	var first_name = "ro_data["+ro_tabs_num+"][ro]["+ro_counter+"][specials]["+ro_special_counter+"]";
	var customer_group_id = (special=="")?(0):(special['customer_group_id']);
	
	str_add = "";
	str_add += "<table id='related-option-special"+ro_special_counter+"' style='width:200px;'><tr><td>";
	
	str_add += "<select name='"+first_name+"[customer_group_id]' class='form-control' style='float:left;width:80px;' title=\"<?php echo htmlspecialchars($entry_customer_group); ?>\">";
	<?php foreach ($customer_groups as $customer_group) { ?>
	str_add += "<option value='<?php echo $customer_group['customer_group_id']; ?>' "+((customer_group_id==<?php echo $customer_group['customer_group_id']; ?>)?("selected"):(""))+"><?php echo $customer_group['name']; ?></option>";
	<?php } ?>
	str_add += "</select>";
	
	// hidden
	str_add += "<input type='hidden' size='2' name='"+first_name+"[priority]' value='"+((special=="")?(""):(special['priority']))+"' title=\"<?php echo htmlspecialchars($entry_priority); ?>\">";
	str_add += "<input type='text'  class='form-control' style='float:left;width:80px;' size='10' name='"+first_name+"[price]' value='"+((special=="")?(""):(special['price']))+"' title=\"<?php echo htmlspecialchars($entry_price); ?>\">";
	str_add += "<button type=\"button\" onclick=\"$('#related-option-special" + ro_special_counter + "').remove();\" data-toggle=\"tooltip\" title=\"<?php echo $button_remove; ?>\" class=\"btn btn-danger\" style='float:left;' data-original-title=\"<?php echo $button_remove; ?>\"><i class=\"fa fa-minus-circle\"></i></button>";
	str_add += "</td></tr></table>";
	
	$('#ro_price_special'+ro_counter).append(str_add);
	
	ro_special_counter++;
	
}

function ro_fill_all_combinations(ro_tabs_num, product_options_only) {

	var rov_id = $('#rov-'+ro_tabs_num).val();
	var ro_variant = ro_variants[ rov_id ];
	//var vopts = ro_variants[ $('[name=related_options_variant]')[0].value ];
	var all_vars = [];
	
	if (product_options_only) {
		var this_product_options = [];
		$('select[name^=product_option][name*=option_value_id]').each(function() {
			if ( $(this).val() ) {
				this_product_options.push($(this).val());
			}
		});
	}
		
	var reversed_options = [];	
	for (var i in ro_variant['options']) {
		if((ro_variant['options'][i] instanceof Function) ) { continue; }
		reversed_options.unshift(i);
	}
		
	//for (var i in ro_variant['options']) {
	//	if((ro_variant['options'][i] instanceof Function) ) { continue; }
	for (var i_index in reversed_options) {
	
		var i = reversed_options[i_index];
		
		var option_id = ro_variant['options'][i]['option_id'];
		
		var temp_arr = [];
		for (var j in ro_all_options[option_id]['values']) {
			if((ro_all_options[option_id]['values'][j] instanceof Function) ) { continue; }
			
			var option_value_id = ro_all_options[option_id]['values'][j]['option_value_id']
			
			if (product_options_only && $.inArray(option_value_id, this_product_options) == -1 ) { //
				continue;
			}
			if (all_vars.length) {
				for (var k in all_vars) {
					if((all_vars[k] instanceof Function) ) { continue; }
					
					var comb_arr = all_vars[k].slice(0);
					comb_arr[option_id] = option_value_id;
					temp_arr.push( comb_arr );
				}
			} else {
				var comb_arr = [];
				comb_arr[option_id] = option_value_id;
				temp_arr.push(comb_arr);
			}
			
		}
		if (temp_arr && temp_arr.length) {
			all_vars = temp_arr.slice(0);
		}
	}
	
	if (all_vars.length) {
		for (var i in all_vars) {
			if((all_vars[i] instanceof Function) ) { continue; }
			
			rop = {};
			for (var j in all_vars[i]) {
				if((all_vars[i][j] instanceof Function) ) { continue; }
				rop[j] = all_vars[i][j];
			}
			
			ro_add_combination(ro_tabs_num, {options: rop});

		}
		
		ro_use_check(ro_tabs_num);
		ro_refresh_status(ro_tabs_num);
		ro_check_defaultselectpriority(ro_tabs_num);
		
	}
	
}

// check priority fields (is it available or not) for default options combination
function ro_check_defaultselectpriority(ro_tabs_num) {
	
	var dsc = $('#tab-ro-'+ro_tabs_num+' input[type=checkbox][id^=defaultselect_]');
	var dsp;
	for (var i=0;i<dsc.length;i++) {
		dsp = $('#defaultselectpriority_'+dsc[i].id.substr(14));
		if (dsp && dsp.length) {
			if (dsc[i].checked) {
				dsp[0].style.display = '';
				if (isNaN(parseInt(dsp[0].value))) {
					dsp[0].value = 0;
				}
				if (parseInt(dsp[0].value)==0) {
					dsp[0].value = "1";
				}
			} else {
				dsp[0].style.display = 'none';
			}
		}
	}
}


if (ro_data) {
	for (var i in ro_data) {
		var ro_tabs_num = ro_add_tab(ro_data[i]);
		
		ro_use_check(ro_tabs_num);
		ro_refresh_status(ro_tabs_num);
		ro_check_defaultselectpriority(ro_tabs_num);
		
	}
	
	
	
}



//--></script>
<!-- >> Related Options PRO / Связанные опции PRO -->
      
      
      
<?php echo $footer; ?>
