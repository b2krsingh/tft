<?php
class ControllerCheckoutShippingMethod extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

		if (isset($this->session->data['shipping_address'])) {
			// Shipping Methods
			$method_data = array();

			$this->load->model('extension/extension');

			$results = $this->model_extension_extension->getExtensions('shipping');

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('shipping/' . $result['code']);

					$quote = $this->{'model_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

					if ($quote) {

            			/** EET Module */
						if ($this->config->get('ee_tracking_status') && $this->config->get('ee_tracking_checkout_status') && $this->config->get('ee_tracking_advanced_settings') && $this->config->get('ee_tracking_language_id') && isset($this->session->data['language'])) {
							$this->load->model('localisation/language');
							$this->load->model('module/ee_tracking');
							$ee_languages = $this->model_localisation_language->getLanguages();
							$ee_start_code = $this->session->data['language'];
							if (substr(VERSION, 0, 7) > '2.1.0.2') {
								$ee_start_language = $ee_start_code;
								$ee_start_path = $ee_start_code;
								$ee_code_id = 'code';
							} else {
								$language_info = $this->model_module_ee_tracking->getLanguageByCode($ee_start_code);
								$ee_start_language = $language_info['directory'];
								if (isset($language_info['filename'])) {
									$ee_start_path = $language_info['filename'];
								} elseif(file_exists(DIR_LANGUAGE . $language_info['directory'] . '/' . $language_info['directory'] . '.php')) {
									$ee_start_path = $language_info['directory'];
								} else {
									$ee_start_path = 'default';
								}
								$ee_code_id = 'directory';
							}
							foreach ($ee_languages as $ee_item) {
								if ($ee_item['language_id'] == $this->config->get('ee_tracking_language_id') && $ee_start_code != $ee_item['code']) {
									$ee_language = new Language($ee_item[$ee_code_id]);
									$this->registry->set('language', $ee_language);
									$ee_quote = $this->{'model_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);
									foreach ($quote['quote'] as $key => $item2) {
										if (isset($ee_quote['quote'][$key]['title'])) {
											$quote['quote'][$key]['ee_title'] = $ee_quote['quote'][$key]['title'];
										}
									}
									$ee_language2 = new Language($ee_start_language);
									$ee_language2->load($ee_start_path);
									$ee_language2->load('checkout/checkout');
									$this->registry->set('language', $ee_language2);
								}
							}
						}
						/** EET Module */
            
						$method_data[$result['code']] = array(
							'title'      => $quote['title'],
							'quote'      => $quote['quote'],
							'sort_order' => $quote['sort_order'],
							'error'      => $quote['error']
						);
					}
				}
			}

			$sort_order = array();

			foreach ($method_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $method_data);

			$this->session->data['shipping_methods'] = $method_data;
		}

		$data['text_shipping_method'] = $this->language->get('text_shipping_method');
		$data['text_comments'] = $this->language->get('text_comments');
		$data['text_loading'] = $this->language->get('text_loading');

		$data['button_continue'] = $this->language->get('button_continue');

		if (empty($this->session->data['shipping_methods'])) {
			$data['error_warning'] = sprintf($this->language->get('error_no_shipping'), $this->url->link('information/contact'));
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['shipping_methods'])) {
			$data['shipping_methods'] = $this->session->data['shipping_methods'];
		} else {
			$data['shipping_methods'] = array();
		}

		if (isset($this->session->data['shipping_method']['code'])) {
			$data['code'] = $this->session->data['shipping_method']['code'];
		} else {
			$data['code'] = '';
		}


		/** EET Module */
		$data['ee_tracking'] = $this->config->get('ee_tracking_status');
		if ($data['ee_tracking']) {
			$data['ee_checkout'] = $this->config->get('ee_tracking_checkout_status');
			$data['ee_checkout_log'] = $this->config->get('ee_tracking_log') ? $this->config->get('ee_tracking_checkout_log') : false;
			$ee_data = array('step_option' => 'No shipping methods available');
			if (isset($this->session->data['shipping_method']['code'])) {
				$ee_code = $this->session->data['shipping_method']['code'];
			} else {
				$ee_code = '';
			}
			if (isset($this->session->data['shipping_methods'])) {
				foreach ($this->session->data['shipping_methods'] as $key => $shipping_method) {
					if (!$shipping_method['error']) {
						foreach ($shipping_method['quote'] as $quote) {
							if ($quote['code'] == $ee_code || !$ee_code) {
								$ee_code = $quote['code'];
								if (isset($quote['ee_title'])) {
									$ee_data['step_option'] = $quote['ee_title'];
								} else {
									$ee_data['step_option'] = $quote['title'];
								}
							}
						}
					}
				}
			}
			$ee_data['step'] = 4;
			$data['ee_data'] = json_encode($ee_data);
		}
		/** EET Module */
            
		if (isset($this->session->data['comment'])) {
			$data['comment'] = $this->session->data['comment'];
		} else {
			$data['comment'] = '';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/checkout/shipping_method.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/checkout/shipping_method.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/checkout/shipping_method.tpl', $data));
		}
	}

	public function save() {
		$this->load->language('checkout/checkout');

		$json = array();

		// Validate if shipping is required. If not the customer should not have reached this page.
		if (!$this->cart->hasShipping()) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		}

		// Validate if shipping address has been set.
		if (!isset($this->session->data['shipping_address'])) {
			$json['redirect'] = $this->url->link('checkout/checkout', '', 'SSL');
		}

		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$json['redirect'] = $this->url->link('checkout/cart');
		}

		// Validate minimum quantity requirements.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$json['redirect'] = $this->url->link('checkout/cart');

				break;
			}
		}

		if (!isset($this->request->post['shipping_method'])) {
			$json['error']['warning'] = $this->language->get('error_shipping');
		} else {
			$shipping = explode('.', $this->request->post['shipping_method']);

			if (!isset($shipping[0]) || !isset($shipping[1]) || !isset($this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]])) {
				$json['error']['warning'] = $this->language->get('error_shipping');
			}
		}

		if (!$json) {
			$this->session->data['shipping_method'] = $this->session->data['shipping_methods'][$shipping[0]]['quote'][$shipping[1]];

			$this->session->data['comment'] = strip_tags($this->request->post['comment']);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}