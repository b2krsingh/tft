<?php
class ControllerCommonFooter extends Controller {

                
                    // capture abandoned cart and customer info from store front
                    // and save into the database
                    protected function CaptureAbandonedCarts()
                    {
                        $aci_config = $this->config->get('aci');
                        $ip = (!empty($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : 'IP Hidden';
                        if ($this->customer->isLogged())
                        {
                            $cart_id = $this->customer->getEmail();
                            $email = $this->customer->getEmail();
                            $registered = 1;
                            $session_id = (!empty($this->session->data['aci_id'])) ? $this->session->data['aci_id'] : session_id();
                        }
                        else
                        {
                            if($aci_config['general']['allow_guest'] == 1)
                            {
                                // Check for autosubscribe plugin
                                if(isset($_COOKIE["Velocity_autosubscribe"]))
                                {
                                    $email = $_COOKIE["Velocity_autosubscribe"];
                                    $cart_id  = $_COOKIE["Velocity_autosubscribe"];
                                }else{
                                    $email = "";
                                    $cart_id  = (!empty($this->session->data['aci_id'])) ? $this->session->data['aci_id'] : session_id();
                                }
                                $registered = 0;
                                $session_id = (!empty($this->session->data['aci_id'])) ? $this->session->data['aci_id'] : session_id();
                            }
                        }
                        if(!empty($cart_id))
                        {
		            		$cartTotalAmount = $this->cart->getTotal();
                            $restoreCart = $this->db->query("SELECT * FROM `" . DB_PREFIX . "abandonedcartincentive` WHERE `restore_id` = '$cart_id' AND `isOrderCompleted` = '0'");
                            $cart        = $this->cart->getProducts();
                            $customer    = $this->customer->getId();
                            $store_id    = (int) $this->config->get('config_store_id');
                            $customer    = (!empty($customer)) ? $customer : '';
                            $cart        = (!empty($cart)) ? $cart : '';
                            $cartTotal   = (!empty($cartTotalAmount)) ? $this->cart->getTotal() : '';

                            $lastpage = "$_SERVER[REQUEST_URI]";

                            if (!empty($customer))
                            {
                                $customer = array(
                                    'id'        => $this->customer->getId(),
                                    'email'     => $email,
                                    'telephone' => $this->customer->getTelephone(),
                                    'firstname' => $this->customer->getFirstName(),
                                    'lastname'  => $this->customer->getLastName(),
                                    'language'  => $this->session->data['language']
                                );
                            }else
							{
								$customer = array(
                                    'id'        => '',
                                    'email'     => $email,
                                    'telephone' => '',
                                    'firstname' => '',
                                    'lastname'  => '',
                                    'language'  => $this->session->data['language']
                                );
							}
                            if (empty($restoreCart->row))
                            {
                                if (!empty($cart))
                                {
                                    $cart     = serialize($cart);
                                    $customer = (!empty($customer)) ? serialize($customer) : '';
                                    $this->db->query("INSERT INTO `" . DB_PREFIX . "abandonedcartincentive` SET `cart`='" . $this->db->escape($cart) . "', `customer_info`='" . $this->db->escape($customer) . "', `order_id` = '0', `ip`='$ip', `date_created`='".date('Y-m-d H:i:s')."', `date_modified`='".date('Y-m-d H:i:s')."', `restore_id`='" . $cart_id . "', `isRegistered`='" . $registered . "', `store_id`='" . $store_id . "', `session_id`='" . $session_id . "', `cartTotal` ='". $cartTotal ."'");
                                    $this->session->data['aci_id'] = $cart_id;
                                }
                            }
                            else
                            {
                                
                                if (!empty($cart))
                                {
                                    $cart = serialize($cart);
                                    $this->db->query("UPDATE `" . DB_PREFIX . "abandonedcartincentive` SET `cart` = '" . $this->db->escape($cart) . "' , `date_modified`='".date('Y-m-d H:i:s')."', `cartTotal` ='". $cartTotal ."' WHERE `restore_id`='$cart_id' AND `isOrderCompleted` = '0'");
                                }
                                if (!empty($customer))
                                {
                                    $customer = serialize($customer);
                                    $this->db->query("UPDATE `" . DB_PREFIX . "abandonedcartincentive` SET `customer_info` = '" . $this->db->escape($customer) . "', `date_modified`='".date('Y-m-d H:i:s')."', `cartTotal` ='". $cartTotal ."' WHERE `restore_id`='$cart_id' AND `isOrderCompleted` = '0'");
                                }
                            }
                        }
                    }
                
            
	public function index() {
		$data['ee_js_position'] = $this->config->get('ee_tracking_js_position');
		$data['ee_js_version'] = $this->config->get('ee_tracking_js_version');

                
                    // Start capturing abandoned carts 
                    $aci_config = $this->config->get('aci');
		    		if(!empty($aci_config)) {
				        if ($aci_config['general']['enable']=='1')
				        {
				            $this->CaptureAbandonedCarts();
				        }
                    }
                
            
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');
		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));
		
		$data['footertop'] = $this->load->controller('common/footertop');
		$data['footerbottom'] = $this->load->controller('common/footerbottom');
		$data['footerleft'] = $this->load->controller('common/footerleft');
		$data['footercenter'] = $this->load->controller('common/footercenter');
		

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

				$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

          $id = $this->config->get('config_store_id');
          $this->load->model('setting/setting');

          //IS ENABLED??
          if($this->config->get('google_remarketing_status_'.$id))
          {
            //DYNAMIC TYPE 
            if($this->config->get('google_remarketing_type_'.$id) == 0)
            {
              $this->load->model('module/google_remarketing');
              $_SESSION['google_remarketing_code'] = $this->model_module_google_remarketing->getDynamicRemarketingCode();
            }
            else //STANDARD TYPE
            {
              $_SESSION['google_remarketing_code'] = html_entity_decode($this->config->get('google_remarketing_code_'.$id));
            }
          }     
        
		
		$data['footertop'] = $this->load->controller('common/footertop');
		$data['footerbottom'] = $this->load->controller('common/footerbottom');
		$data['footerleft'] = $this->load->controller('common/footerleft');
		$data['footercenter'] = $this->load->controller('common/footercenter');



		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}