<footer> <?php echo $footertop; ?>
  <div id="footer" class="container">
    <div class="row"> <?php echo $footerleft; ?>
      <?php /*?><?php if ($informations) { ?>       <div class="col-sm-3 column">         <h5><?php echo $text_information; ?></h5>         <ul class="list-unstyled">           <?php foreach ($informations as $information) { ?>           <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>           <?php } ?>         </ul>       </div>       <?php } ?>       <div class="col-sm-3 column">         <h5><?php echo $text_service; ?></h5>         <ul class="list-unstyled">                      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>         </ul>       </div><?php */?>
      <!--<?php echo $footercenter; ?>       <div class="col-sm-3 column right">         <h5><?php echo $text_extra; ?></h5>         <ul class="list-unstyled">           <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>           <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>           <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>           <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li> 		  <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>         </ul>       </div>-->
      <div class="col-sm-3 column right">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>" title="My Account"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $contact; ?>" title="Contact Us"><?php echo $text_contact; ?></a></li>
          <li><a href="https://thefurniturepeople.com.au/about_us" title="About Us">About Us</a></li>
<li><a href="https://thefurniturepeople.com.au/faq" title="Opening Hours">Opening Hours</a></li>
<li><a href="https://thefurniturepeople.com.au/faq" title="Delivery Info">Delivery Info</a></li>
        </ul>
      </div>
    </div>
    <?php echo $footerbottom; ?>
    <p><?php echo $powered; ?></p>
  </div>
</footer>
<?php if (isset($ee_js_position) && $ee_js_position == 2) { ?>
<script src="catalog/view/javascript/ee_tracking.min.js?eetv=2.2.1<?php echo isset($ee_js_version) ? '.' . $ee_js_version : ''; ?>" type="text/javascript"></script>
<?php } ?>
<!-- OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation. Please donate via PayPal to donate@opencart.com //--> <!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk --> 


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MC6CNW7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


 <!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2gTTz1CHLRTolR5gTJ6pcuiKc3gMi25F";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!-- {literal} -->
<script>
function myFunction() {
    var x = document.getElementById('myDIV');
    if (x.style.display === 'none') {
        x.style.display = 'block';
        btnshow.style.display = 'none';
    } else {
        x.style.display = 'none';
		  btnshow.style.display = 'block';
    }
}
</script>
<!--End of Zopim Live Chat Script-->


<script type="text/javascript">
  window._mfq = window._mfq || [];
  (function() {
    var mf = document.createElement("script");
    mf.type = "text/javascript"; mf.async = true;
    mf.src = "//cdn.mouseflow.com/projects/f7a0e0da-c38f-48d0-b823-77965a3f2228.js";
    document.getElementsByTagName("head")[0].appendChild(mf);
  })();
</script>

<?php if (isset($ee_js_position) && $ee_js_position == 3) { ?>
<script src="catalog/view/javascript/ee_tracking.min.js?eetv=2.2.1<?php echo isset($ee_js_version) ? '.' . $ee_js_version : ''; ?>" type="text/javascript"></script>
<?php } ?>

          <?php if (isset($_SESSION['google_remarketing_code']))
          {
            echo $_SESSION['google_remarketing_code']; unset($_SESSION['google_remarketing_code']);
          } ?>
        
</body></html>